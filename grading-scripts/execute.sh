#!/bin/bash


get_info () {
    COMMIT=$1
    while test ${#AUTHOR_FOLDER} = 0
    do
        TUT_NUMBER=$(git show --format=format:"" --dirstat $COMMIT | rev | cut -d" " -f1 | rev | cut -d/ -f1 | cut -d$'\n' -f1)
        AUTHOR_FOLDER=$(git show --format=format:"" --dirstat $COMMIT | cut -d/ -f2)
        PARENT_COMMIT=$(git show --format=format:%p -s $COMMIT)
        get_info $PARENT_COMMIT
    done
}

get_info $BITBUCKET_COMMIT

printf "$TUT_NUMBER|$AUTHOR_FOLDER" > info

printf '%s\n' "$TUT_NUMBER"
printf '\n%s\n' "Pipeline is searching your folder in,"
printf '%s/%s\n' "$TUT_NUMBER" "$AUTHOR_FOLDER"


if [ -f ${TUT_NUMBER}/${AUTHOR_FOLDER}/run.sh ]
then
    echo running your shell script at ${TUT_NUMBER}/${AUTHOR_FOLDER} && sh ${TUT_NUMBER}/${AUTHOR_FOLDER}/run.sh
else
    echo no shell script found,
    for f in $(find ${TUT_NUMBER}/${AUTHOR_FOLDER} -name '*.py')
    do 
        echo running $f && python $f
    done
fi
