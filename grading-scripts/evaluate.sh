#!/bin/bash


get_info () {
    COMMIT=$1
    while test ${#AUTHOR_FOLDER} = 0
    do
        TUT_NUMBER=$(git show --format=format:"" --dirstat $COMMIT | cut -d" " -f3 | cut -d/ -f1)
        AUTHOR_FOLDER=$(git show --format=format:"" --dirstat $COMMIT | cut -d/ -f2)
        PARENT_COMMIT=$(git show --format=format:%p -s $COMMIT)
        get_info $PARENT_COMMIT
    done
}

get_info $BITBUCKET_COMMIT


printf '\n%s\n' "Looking for json in: ./${TUT_NUMBER}/${AUTHOR_FOLDER}"
JSONPATH=$(find . -name '*.json' -wholename "./${TUT_NUMBER}/${AUTHOR_FOLDER}/**")

printf '\n%s\n' "found json at $JSONPATH"
python grading-scripts/partial_checker.py $TUT_NUMBER $JSONPATH