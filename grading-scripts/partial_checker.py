import json
import sys
          

def flatten(json_file):
    out = {}
    def make_flat(x, name=''):
        if type(x) is dict:
            for a in x:
                make_flat(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                make_flat(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x
    make_flat(json_file)
    return out


#load student answer json#
def load_student_answers():
    with open(sys.argv[2]) as json_file:     
        student_answers = json.load(json_file)
        student_answers = flatten(student_answers)
    return student_answers


#load true answer json#
def load_true_answers():
    num_questions = 0
    with open('{}/data/sample-solutions.json'.format(sys.argv[1])) as json_file:           
        answers = json.load(json_file)
        answers = flatten(answers)
    for v in answers.values():
        if v is not None:
            num_questions += 1
    return answers, num_questions       


def compare(answers, solutions):
    epsilon = 0.1
    def iscorrect(answer, solution, epsilon):
        if solution*(1-epsilon) < answer < solution*(1+epsilon):
            return True
        else:
            return False
    score = 0
    for k, v in solutions.items():
        print('   ', k, ': ', answers[k])
        if v is None:
            print('      not checked')
        elif iscorrect(answers[k], v, epsilon):
            print('      correct!')
            score += 1
        else:
            print('      incorrect -', 'true answer =', v)
    print('')
    return score        


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(sys.argv)
        exit('usage: partial-checker.py <tutnumber e.g. "tut1"> <solutionsfile>')
    true_answers, num_questions = load_true_answers()
    student_answers = load_student_answers()
    score = compare(student_answers, true_answers)
    print('{}/{} sample solutions correct!'.format(score, num_questions))