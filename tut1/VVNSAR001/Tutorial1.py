import numpy as np
import time as t
import matplotlib.pyplot as plt
import json as js
#-------------------------------------
#Initialising the vectors and matrices  
arr = [5,10,20,100,1000]
print(len(arr))
solutions = {}
vvValues = []
vmvValues = []

for p in range (len(arr)):
    n = arr[p]
    print("-----------------------------------------------------")
    print("Dimension = ",n)
    print("-----------------------------------------------------")
    arr1 = np.zeros(n) #creates empty array

    for j in range (n): #populates vector
        arr1[j] = j+1
    
        
    arr1t = np.transpose(arr1) #creates tranpose of vector

    a = []
    for x in range(n): #populating a matrix
        row = []
        for y in range(n):
            m = 37.1*(x+1) + 91.7*(y+1)**2
            row.append(m)
        a.append(row)
   
    mf =np.mod(a,20) - 10 #creating the required matrix 
#-------------------------------------  
    print("Operations without using the numpy module")
    print("-----------------------------------------------------")
#-------------------------------------
#Dot Product of two vectors
    vTime = np.zeros(len(arr))
    startTime = t.time() 
    dotProd = 0 
    for i in range (n): #calculating dot product
        dotProd += arr1[i]*arr1t[i]
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    vTime[p] = endTime
    vvValues.append(dotProd)
    print("Dot Product of two vectors",dotProd)
#    print("Time taken was ",endTime," seconds")
    print("-----------------------------------------------------")
#-------------------------------------
#Product of Matrix with Vector
    mvTime = np.zeros(len(arr))
    startTime = t.time() 
    mv = np.zeros(n)
    for i in range(n):
        for j in range (n):
            mv[i] +=  mf[i][j]*arr1[i]
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    mvTime[p] = endTime 
#    print("Time taken for mv was ",endTime," seconds")
    print("-----------------------------------------------------")
#-------------------------------------
#Product of matrix with matrix
    mmTime = np.zeros(len(arr))
    startTime = t.time() 
    mm = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range (n):
                mm[i][j]=mf[i][k]*mf[k][j]
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    mmTime[p] = endTime 
#    print("Time taken for mm was ",endTime," seconds")
    print("-----------------------------------------------------")
#-------------------------------------
#Product of Matrix and two vectors
    vmvTime = np.zeros(len(arr))
    startTime = t.time() 
    mvm = 0
    mvTemp = np.zeros(n)
    for i in range(n):
        for j in range (n):
            mvTemp[i] +=  mf[i][j]*arr1[j]

    for i in range(n):
        mvm += arr1t[i]*mvTemp[i]
    vmvValues.append(mvm)
    endT = t.time() - startTime    
    endTime = "{0:.20f}".format(endT)
    vmvTime[p] = endTime
    print("Product of Matrix and Two Vectors",mvm)
#    print("Time taken was ",endTime," seconds")
    print("-----------------------------------------------------")
#-------------------------------------
    print("Operations using the numpy module")
    print("-----------------------------------------------------")
#-------------------------------------
#Product of two vectors
    vvTimeNum = np.zeros(len(arr))
    startTime = t.time()
    vvNum = np.dot(arr1,arr1t)
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    vvTimeNum[p] = endTime
    print("Product of Two vectors using Numpy",vvNum)
#    print("Time taken was ",endTime," seconds")
    print("-----------------------------------------------------")

#-------------------------------------
#Product of Matrix and vector
    mvTimeNum = np.zeros(len(arr))
    startTime = t.time()
    mvNum = np.matmul(arr1,mf)
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    mvTimeNum[p] = endTime
#    print("Time taken for mv was ",endTime," seconds")
    print("-----------------------------------------------------")

#-------------------------------------
#Product of Two Matrices
    mmTimeNum = np.zeros(len(arr))
    startTime = t.time()
    mmNum = np.multiply(mf,mf)
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    mmTimeNum[p] = endTime
#    print("Time taken for mm was ",endTime," seconds")
    print("-----------------------------------------------------")
#-------------------------------------
#Product of Matrix and two vectors
    vmvTimeNum = np.zeros(len(arr))
    startTime = t.time()
    vmvNum = np.matmul(arr1t,np.matmul(mf,arr1))
    endT = t.time() - startTime
    endTime = "{0:.20f}".format(endT)
    vmvTimeNum[p] = endTime
    print("Product of Matrix and Two vectors using Numpy",vmvNum)
#    print("Time taken was ",endTime," seconds")
    print("-----------------------------------------------------")
#--------------------
#misc - JSON + FLOPS 
jsonVV = {} 
jsonVMV = {}

for i in range(len(arr)):
    jsonVV.update({str(arr[i]): vvValues[i]})
    jsonVMV.update({str(arr[i]): vmvValues[i]})

solutions.update({"xtx": jsonVV})
solutions.update({"xtMx": jsonVMV})

with open ('vvnsar001_tut1.json','w') as f :
    jsonwrite = js.dumps(solutions)
    f.write(jsonwrite)
    f.close()

flpxtx = np.zeros(len(arr))
flpmx = np.zeros(len(arr))
flpmm = np.zeros(len(arr))
flpxmx = np.zeros(len(arr))

#for i in range(len(arr)):
 #   flpxtx = (2*arr[i] - 1) / vTime[i]
  #  flpmx = ((2*arr[i]**2) - arr[i]) / mvTime[i]
   # flpmm = ((2*arr[i]**2) - 1) / mmTime[i]
   # flpxmx = (2*arr[i]**2) / vmvTime[i]


#print(flpxtx," ",flpmx," ",flpmm," ",flpxmx)
#-------------------------------------
####PLOT######
print("Plots of durations for each operation")
#Vector and Vector 
plt.scatter(arr,vTime)
plt.scatter(arr,vvTimeNum)
plt.plot(arr,vTime,label="Own Implementation")
plt.plot(arr,vvTimeNum, label="Numpy Package")
plt.legend(loc="upper left")
plt.title("Plot of vector dot product durations")
plt.ylabel("Time Taken (seconds)")
plt.xlabel("Matrix/Vector dimensions")
plt.show()
print("-----------------------------------------------------")

#Matrix and Vector 
plt.scatter(arr,mvTime)
plt.scatter(arr,mvTimeNum)
plt.plot(arr,mvTime,label="Own Implementation")
plt.plot(arr,mvTimeNum, label="Numpy Package")
plt.legend(loc="upper left")
plt.title("Plot of multiplication of matrix with vector durations")
plt.ylabel("Time Taken (seconds)")
plt.xlabel("Matrix/Vector dimensions")
plt.show()
print("-----------------------------------------------------")

#Matrix and Matrix
plt.scatter(arr,mmTime)
plt.scatter(arr,mmTimeNum)
plt.plot(arr,mmTime,label="Own Implementation")
plt.plot(arr,mmTimeNum, label="Numpy Package")
plt.legend(loc="upper left")
plt.title("  Plot of multiplication of matrix with matrix durations")
plt.ylabel("Time Taken (seconds)")
plt.xlabel("Matrix/Vector dimensions")
plt.show()
print("-----------------------------------------------------")

#Matrix and Vector Scalar Product
plt.scatter(arr,vmvTime)
plt.scatter(arr,vmvTimeNum)
plt.plot(arr,vmvTime,label="Own Implementation")
plt.plot(arr,vmvTimeNum, label="Numpy Package")
plt.legend(loc="upper left")
plt.title("Plot of scalar product of matrix and two vectors durations")
plt.ylabel("Time Taken (seconds)")
plt.xlabel("Matrix/Vector dimensions")
plt.show()
#-------------------------------------


