#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 17:22:46 2020

@author: ntshembho
"""

#import packages

import numpy as np
import json 
import time 
import matplotlib.pyplot as plt


' ' ' Setting up functions ' ' '

def Mnormal(x):
    M = [[(((float(37.1)*i+float(91.7)*(j**2))%20)-10) for j in range(1,x+1)] for i in range(1,x+1)]    
    return M
#    M = [[0 for z in range(len(x))] for y in range(len(x))]
#    for i in range(len(x)):
#        for j in range(len(x)):
#            M[i][j]= ((37.1*(i+1)+91.7*(j+1)**2) %20.0 -10.0)
#    return M
#        print('M', M[i][j]*M[i][j])
#        print("|"+"M"+"|"+str(i+1)+"|" + str(j+1)+"|" + '= '+ str(M[i][j]), end =' ')
#    print('\n') 
  
def Mnumpy(x):
    M = np.array([[0.00 for z in range(len(x))] for y in range(len(x))])
    for i in range(len(x)):
        for j in range(len(x)):
            M[i][j]= ((37.1*(i+1)+91.7*(j+1)**2) %20.0 -10.0)
    return M

def xvecT(x):
    xvec = []
    for i in range(x):
        xvec.append(i+1)
    return xvec
    
    
' ' ' Calculations ' ' '     


#MM matrix multiplication
def MatrixMulti(M,N):
    Matrix = [[0 for z in range(len(M))] for y in range(len(M))]
    for i in range(len(M)):
        for j in range(len(M)):
            for k in range(len(M)):
                Matrix[i][j] += M[i][k]*N[k][j]
    return Matrix

#Matrix multiplication Numpy
    
def NumpMatrixMulti(M,N): 
    return np.dot(M,N)    

#Vecotr multiplication
def vectMulti(x,y):
#    x = []
#    y = []
    xy = 0
    for i in range(len(x)):
        xy += x[i]*y[i]
    return xy

# matrix vector multiplication
def MatrixVector(M,x):
    Mx = [0]*len(x)
    for i in range(len(x)):
        for j in range(len(x)):
            Mx[i] += M[i][j]*x[j]
    return Mx

#matrix vector multiplication numpy
def MatrixVectorNump(M,x):
    return np.dot(M,x)
 
# vector matrix vector multiplication
def vMv(M,x,y):
    vMv = vectMulti(y, MatrixVector(M,x))
    return vMv

# vector matrix vector multiplication numpy
def vMvNump(M,x,y):
#    vMv = np.array(len(x))
    vMvnump = np.dot(y, np.dot(M,x))
    return vMvnump
    
' ' ' Implimentations 1:  ' ' '
# Calculating xtx and xtMx for n dimmensions

n = [5, 10, 20, 100, 1000]
labelxtx = ['xtx: 5','xtx: 10','xtx: 20','xtx: 100','xtx: 1000']
labelxtMx = ['xtMx: 5','xtMx: 10','xtMx: 20','xtMx: 100','xtMx: 1000']
#k = eval(input("Enter a nummber from 0 to 4 ([5, 10, 20, 100, 1000]):"))
k=0
XTarray = []
Xarray = []
Marray = []

while (k<5):
    M = [[0 for z in range(n[k])] for y in range(n[k])]
    for i in range(n[k]):
        for j in range(n[k]):
            M[i][j]= ((37.1*(i+1)+91.7*(j+1)**2) %20.0 -10.0)
    

    XT = []

    for i in range(n[k]):
        XT.append(i+1)
  
    XT = np.array(XT)  
    X = XT.reshape(-1,1)
    
    Xarray.append(X)
    XTarray.append(XT)
    Marray.append(M)
    
    xTx = vectMulti(XT,X)
    xtMx = vMv(Mnormal(n[k]),X,XT)

    k+=1
#


' ' ' Implementations 2: Execusion Times ' ' '

#defining functions to implement the multiplications defined earlier and calculate the run time, 
#Lastly creating a json file to store the data

def doStuff(function,firstX,secondX, filename):
    
    t = []
    for i in range (5):
        start_time = time.time()
        p = function(firstX[i],secondX[i])
        t.append((time.time() - start_time))
    #print("--- %s seconds ---" % (time.time() - start_time))
    with open(filename, 'w') as f:
        json.dump(t, f)
 
def doStuff2(function,firstX,secondX, thirdX, filename):
    
    t = []
    for i in range (5):
        start_time = time.time()
        p = function(firstX[i],secondX[i], thirdX[i])
        t.append((time.time() - start_time))
    #print("--- %s seconds ---" % (time.time() - start_time))
    with open(filename, 'w') as f:
        json.dump(t, f)  

' ' ' No Library ' ' ' 


#doStuff(vectMulti,XTarray,Xarray,"time xtx") done
#
#doStuff(MatrixVector,Marray,Xarray, "time Mx") done

#doStuff(MatrixMulti, Marray, Marray, "time MM") done 

#doStuff2(vMv, Marray, Xarray, XTarray, "time xtMt") done
        
        
' ' '  Using Library (Numpy) ' ' ' 

#doStuff(NumpMatrixMulti, np.array(XTarray), np.array(Xarray), "time xtx nump") done
        
#doStuff(MatrixVectorNump, np.array(Marray), np.array(Xarray), "time Mx nump") done

#doStuff(NumpMatrixMulti, np.array(Marray), np.array(Marray), "time MM nump") done
        
#doStuff2(vMv, np.array(Marray), np.array(Xarray), np.array(XTarray), "time xtMx nump") done 

' ' ' Plotting ' ' ' 

files = ['./tut1/MTSNTS010/tut1/times.txt','./tut1/MTSNTS010/tut1/time xtx.txt','./tut1/MTSNTS010/tut1/time Mx.txt', './tut1/MTSNTS010/tut1/time MM.txt', './tut1/MTSNTS010/tut1/time xtMt.txt', './tut1/MTSNTS010/tut1/time xtx nump.txt','./tut1/MTSNTS010/tut1/time Mx nump.txt', './tut1/MTSNTS010/tut1/time MM nump.txt', './tut1/MTSNTS010/tut1/time xtMx nump.txt']

r = 0
times = [[0.000 for j in range(5)] for t in range(9)]

while (r<9):
    f = open(files[r])
    for line in f:
        line = line.strip("[")
        line = line.strip("]")
        line = line.strip()
        if line != "":
            columns = line.split(", ")
            times[r] = columns
            
#    times = np.array(times)
#    print('times[r]', times[r])
#    plt.plot(np.array(times[0]), times[r], 'ro')
#    plt.show()  
    r +=1

#    times = times.reshape(-1,1)
#    print('times',times)
print('times', times[0])
  
#xtx
plt.plot(times[0], times[1], 'ro', label = ' xtx no library')
#plt.plot(times[0], times[5], 'bo', label = 'xtx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('xtx')
plt.show()

plt.plot(times[0], times[5], 'bo', label = 'xtx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('xtx nump')
plt.show()


#
##Mx
plt.plot(times[0], times[2], 'ro', label = ' Mx no library')
#plt.plot(times[0], times[6], 'bo', label = 'Mx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('Mx')
plt.show()

plt.plot(times[0], times[6], 'bo', label = 'Mx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('Mx nump')
plt.show()
#
##xtMx
plt.plot(times[0], times[3], 'ro', label = ' xtMx no library')
#plt.plot(times[0], times[7], 'bo', label = 'xtMx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('xtMx')
plt.show()
plt.plot(times[0], times[7], 'bo', label = 'xtMx Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('xtMx nump')
plt.show()
#
##MM
plt.plot(times[0], times[4], 'ro', label = ' MM no library')
#plt.plot(times[0], times[8], 'bo', label = 'MM Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('MM')
plt.show()

plt.plot(times[0], times[8], 'bo', label = 'MM Numpy')
plt.xlabel('Dimmensions')
plt.ylabel('Time [s]')
plt.legend()
plt.savefig('MM nump')
plt.show()





