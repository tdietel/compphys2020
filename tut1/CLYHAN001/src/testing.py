# Contains test methods to produce execution time data for each operation 

import numpy as np
import MatrixOps
import time
from matplotlib import pyplot as plt
import plotting

def testDot(nData):
	numTests = 5
	loopTimes = np.zeros(len(nData))
	npTimes = np.zeros(len(nData))

	for i in range(numTests):
		testLoopTimes = []
		testnpTimes = []

		for N in nData:
			M = MatrixOps.createMatrix(N)
			x = MatrixOps.createVec(N)

			# time own implementation
			t0 = time.perf_counter()
			MatrixOps.dot(x,x)
			testLoopTimes.append(time.perf_counter()-t0)

			# time numpy implementation
			t0 = time.perf_counter()
			np.dot(x,x)
			testnpTimes.append(time.perf_counter()-t0)


		testLoopTimes = np.asarray(testLoopTimes)
		testnpTimes = np.asarray(testnpTimes)

		loopTimes += testLoopTimes
		npTimes += testnpTimes

	loopTimes = loopTimes/float(numTests)
	npTimes = npTimes/float(numTests)

	plotting.plotDotExecutionTime(nData, np.array(loopTimes), np.array(npTimes))

def testMx(nData):
	numTests = 5
	loopTimes = np.zeros(len(nData))
	npTimes = np.zeros(len(nData))

	for i in range(numTests):
		testLoopTimes = []
		testnpTimes = []

		for N in nData:
			M = MatrixOps.createMatrix(N)
			x = MatrixOps.createVec(N)

			# time own implementation
			t0 = time.perf_counter()
			MatrixOps.Mx(M,x)
			testLoopTimes.append(time.perf_counter()-t0)

			# time numpy implementation
			t0 = time.perf_counter()
			np.matmul(M,x)
			testnpTimes.append(time.perf_counter()-t0)


		testLoopTimes = np.asarray(testLoopTimes)
		testnpTimes = np.asarray(testnpTimes)

		loopTimes += testLoopTimes
		npTimes += testnpTimes

	loopTimes = loopTimes/float(numTests)
	npTimes = npTimes/float(numTests)

	plotting.plotMxExecutionTime(nData, np.array(loopTimes), np.array(npTimes))

def testxMx(nData):
	numTests = 5
	loopTimes = np.zeros(len(nData))
	npTimes = np.zeros(len(nData))

	for i in range(numTests):
		testLoopTimes = []
		testnpTimes = []

		for N in nData:
			M = MatrixOps.createMatrix(N)
			x = MatrixOps.createVec(N)

			# time own implementation
			t0 = time.perf_counter()
			MatrixOps.xMx(M,x)
			testLoopTimes.append(time.perf_counter()-t0)

			# time numpy implementation
			t0 = time.perf_counter()
			np.dot(x, np.matmul(M,x))
			testnpTimes.append(time.perf_counter()-t0)


		testLoopTimes = np.asarray(testLoopTimes)
		testnpTimes = np.asarray(testnpTimes)

		loopTimes += testLoopTimes
		npTimes += testnpTimes

	loopTimes = loopTimes/float(numTests)
	npTimes = npTimes/float(numTests)

	plotting.plotxMxExecutionTime(nData, np.array(loopTimes), np.array(npTimes))

def testMM(nData):
	numTests = 1
	loopTimes = np.zeros(len(nData))
	npTimes = np.zeros(len(nData))

	for i in range(numTests):
		testLoopTimes = []
		testnpTimes = []

		for N in nData:
			M = MatrixOps.createMatrix(N)
			x = MatrixOps.createVec(N)

			# time own implementation
			t0 = time.perf_counter()
			MatrixOps.MM(M)
			testLoopTimes.append(time.perf_counter()-t0)

			# time numpy implementation
			t0 = time.perf_counter()
			np.matmul(M,M)
			testnpTimes.append(time.perf_counter()-t0)


		testLoopTimes = np.asarray(testLoopTimes)
		testnpTimes = np.asarray(testnpTimes)

		loopTimes += testLoopTimes
		npTimes += testnpTimes

	loopTimes = loopTimes/float(numTests)
	npTimes = npTimes/float(numTests)

	plotting.plotMMExecutionTime(nData, np.array(loopTimes), np.array(npTimes))

