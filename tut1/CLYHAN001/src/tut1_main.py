# Main code: produces required solutions.json file and plots for execution times for each operation

import numpy as np
import MatrixOps
import time
from matplotlib import pyplot as plt
import testing

# Computes and prints results for xtx and xtMx (for N=5,10,20,100,1000) to solutions.json file
# Needs to be changed to produce .json file properly using json package
def produceResults(nData):
	xxResults = []
	xMxResults = []

	for N in nData:
		M = MatrixOps.createMatrix(N)
		x = MatrixOps.createVec(N)

		xxResults.append(MatrixOps.dot(x,x))
		xMxResults.append(MatrixOps.xMx(M,x))

	f = open('solutions.json', 'w+')
	f.write("{\"xtx\":{")
	for i in range(len(nData)-1):
		f.write('"' + str(nData[i]) + '":' + str(xxResults[i]) +', ')
	f.write('"' + str(nData[-1]) + '":' + str(xxResults[-1]) +'},\n')

	f.write("\"xtMx\":{")
	for i in range(len(nData)-1):
		f.write('"' + str(nData[i]) + '":' + str(int(round(xMxResults[i]))) +', ')
	f.write('"' + str(nData[-1]) + '":' + str(int(round(xMxResults[-1]))) +'}}')

	f.close()

# Computes execution times for all matrix operations implemented
def mainRunTests():
	nData = [[5,10,20,100,1000],[5,10,20,50,100,200,300]]
	testing.testDot(nData[0])
	testing.testMx(nData[0])
	testing.testxMx(nData[0])
	testing.testMM(nData[1])

if __name__ == '__main__':
	produceResults(np.array([5,10,20,100,1000])) 				# Produce solutions.json file
	mainRunTests()												# Produces plots of execution time
