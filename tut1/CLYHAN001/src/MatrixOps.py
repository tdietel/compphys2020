# Contains methods for each of the required matrix operations

import numpy as np

# Create specified matrix
def createMatrix(N):
	matrix = []
	for i in range(1, N+1):
		row = np.zeros(N)
		for j in range(1, N+1):
			row[j-1] = ((37.1*i + 91.7*j*j) % 20) - 10.0
		matrix.append(row)
	matrix = np.asarray(matrix)

	return matrix

# Create specified vector
def createVec(N):
	vec = []
	for i in range(N):
		vec.append(i+1)
	vec = np.asarray(vec)

	return vec

# Dot product of two vectors
def dot(a,b):
	if len(a)!=len(b): 
		print("Error: vectors have different dimensions")
		return
	else:
		N = len(a)
		result = 0
		for i in range(N):
			result += a[i]*b[i]

		return result

# Multiplication of a matrix with a vector Mx
def Mx(M, x):
	N = len(M)
	result = np.zeros(N)

	for row in range(N):
		result[row] = dot(M[row], x)

	return result

# Multiplication of two vectors and a matrix xMx
def xMx(M, x):
	result1 = Mx(M, x)
	result2 = dot(x, result1)

	return result2

# Multiplication of two matrices MM
def MM(M):
	N = len(M)
	result = []

	for i in range(N):
		rowResult = []
		for j in range(N):
			elResult = 0
			for k in range(N):
				elResult += M[i][k]*M[k][j]
			rowResult.append(elResult)
		result.append(rowResult)

	return result
