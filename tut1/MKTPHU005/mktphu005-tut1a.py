#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 18:47:01 2020

@author: Phuti MOkoti, mktphu005
"""

import numpy as np
from matplotlib import pyplot as plt
import time
import csv
import json
#import tables
from tabulate import tabulate


'''
section 1: define the matrix operations
n1 a grid of matrxi sizes 
Operations1 are my implementations execution times
Operations2 is numpy execution time
Flops estimates FLOP per second per matrix size for each operation
Compare12 takes a ratio of numpy/my time for each n
'''

n1 = np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')


D1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
FlopsD1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
D2 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
CompareD12 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')

Mx1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
FlopsMx1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
Mx2 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
CompareMx12= np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')

xMx1 = np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
FlopsxMx1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
xMx2 = np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
ComparexMx12 = np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')

MM1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
FlopsMM1 = np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
MM2 =np.array([5, 10, 20,50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')
CompareMM12 = np.array([5, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000], dtype='f')

''' 
section 2: for each n, we compute each of the operations and store execution times, Flops and Compare 
Execution time is measured by time taken to print output of each matrix operation for each n
'''
a = 0
for n in n1:
    print("n=", n)
    k = int(n)
    s = (k,k)
    x = np.arange( 1, k+1 , dtype=int)
    y = x
    M = np.ones(s)

    #Defining Matrix M[i][j]
    for i in range(len(x)): 
        for j in range(len(x)):
                M[i][j] = ((37.1*(i+1) + 91.7*(j+1)**2)%20.0 -10.0)

    #Defining D = xTx
    #D1 is my dot product implementaion
    def Dot(x,y,k):
        xDoty = 0
        for i in range(len(x)):
            xDoty += x[i]*y[i]
        return xDoty
    
    #time elapsed for D1
    startD1 = time.time()
    print("xDotx1 =" ,Dot(x,y,k))
    endD1 = time.time()
    
    #Numpy dot product time
    startD2 = time.time()
    print("xTx =" ,x.dot(x))
    endD2 = time.time()
    

   #Defining M = Mx
    def Mx( M, x,k):
        Mx = np.zeros(k)
        for i in range(len(x)):
            Mx[i-1] += Dot(M[i-1],x,k)
        return(Mx)
        
    
    startMx1 = time.time()
    print("Mx1  =" , Mx(M, x, k))
    endMx1 = time.time()

    
    startMx2 = time.time()
    print("Mx2  =" ,M.dot(x))
    endMx2 = time.time()
    

    #Defining xMx
    def xMx(x, M, y, k):
        return(Dot(y,Mx(M, x, k),k))
    
    startxMx1 = time.time()
    print("xMx1  =" , Dot(x,Mx(M, x, k),k))
    endxMx1 = time.time()

 
    startxMx2 = time.time()
    print("xMx2  =" ,x.dot((M.dot(x))))
    endxMx2 = time.time()
    


    
    #Defining MM
    def MM(M, L, k):
        s = (k,k)
        MM = np.zeros(s)
        #iterate through rows of X
        for i in range(len(M)):
            #iterate through columns of Y
            for j in range(len(M[0])):
                #iterate through rows of Y
                for k in range(len(M)):
                    MM[i][j] += M[i][k] * L[k][j]
        return(MM)

    startMM1 = time.time()
    print("MM1 =" ,MM(M, M, k))
    endMM1 = time.time()

    startMM2 = time.time() 
    print("MM2 =" ,M.dot(M))
    endMM2 = time.time()


    D1[a] = endD1 - startD1
    D2[a] = endD2 - startD2
    CompareD12[a] = D2[a]/D1[a]
    FlopsD1[a] = D1[a]/k**2

    Mx1[a] = endMx1 - startMx1
    Mx2[a] = endMx2 - startMx2
    CompareMx12[a] = Mx2[a]/Mx1[a]
    FlopsMx1[a] = MM1[a]/k**2
    
    xMx1[a] = endxMx1 - startxMx1
    xMx2[a] = endxMx2 - startxMx2
    ComparexMx12[a] = xMx2[a]/xMx1[a]
    FlopsxMx1[a] = MM1[a]/k**2

    MM1[a] = endMM1 - startMM1
    MM2[a] = endMM2 - startMM2
    CompareMM12[a] = MM2[a]/MM1[a]
    FlopsMM1[a] = MM1[a]/k**2
    a += 1



#calculating the values of xTx and xTMx for n = 5, 10, 20, 100, 1000
n3 = np.array([5, 10, 20, 100, 1000]) 
xTx1 = np.array([5, 10, 20, 100, 1000])
xTMx1 = np.array([5, 10, 20, 100, 1000])

b = 0
for l in n3:
    k = int(l)
    s = (k,k)
    x = np.arange( 1, k+1 , dtype=int)
    y = x
    M = np.ones(s)

    #Defining Matrix M[i][j]
    for i in range(len(x)): 
        for j in range(len(x)):
                M[i][j] = ((37.1*(i+1) + 91.7*(j+1)**2)%20.0 -10.0)
                
    xTx1[b]= Dot(x,y,k)
    xTMx1[b]= Dot(x,Mx(M, x, k),k)
    b += 1
 
#tabulating the values of xTx and xTMx for n = 5, 10, 20, 100, 1000
headers=['n', 'xtx', 'xtMx']
xTx1 =   xTx1.tolist()
xTMx1 = xTMx1.tolist()
n3 = n3.tolist()
table1 = zip(n3,xTx1, xTMx1)
file = open("mktphu005Tut1-DotProductValuesTable.txt", "w") 
file.write(tabulate(table1, headers=headers)) 
file.close()


#write to .json file
data = {}
data['xtx'] = dict(zip(n3, xTx1))
data['xtMx'] = dict(zip(n3, xTMx1))


with open('mktphu005-tut1-solutions.json', 'w') as f:
    json.dump(data, f)

'''
section 3: the plots
'''

#plot of all execution times for each operations for both implementations
plt.figure(1)
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
fig.suptitle(' execution time as a function of n, up to execution times of a few seconds, for each  of the given matrix operations')
ax1.plot(n1, D1, 'o',label='my xTx Time')
ax1.plot(n1, D2, 'o', label='numpy xTx Time')
ax1.legend( ['my xTx Time', 'numpy xTx Time'], loc='best', frameon=False)
ax2.plot(n1, Mx1, 'o', n1, Mx2, 'o')
ax2.legend(['my Mx1 Time', 'numpy Mx1 Time'], loc='best', frameon=False)
ax3.plot(n1, xMx1,'o', n1, xMx2,'o')
ax3.legend(['my xMx Time', 'numpy xMx Time'], loc='best', frameon=False)
ax4.plot(n1, MM1, 'o',n1, MM2,'o')
ax4.legend(['my MM Time', 'numpy MM Time'], loc='best', frameon=False)


for ax in fig.get_axes():
    ax.label_outer()
#plt.legend(['my implementation time', 'numpy application time'], loc='upper right')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

# plotting xtx exectution times
plt.figure(2)
plt.plot(n1, D1, 'o', label='my xTx Time')
plt.plot(n1, D2,'o', label='numpy xTx Time')
plt.legend()
plt.title('execution time as a function of n, for dot product of two vectors: xTx')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

# plotting Mx exectution times
plt.figure(3)
plt.plot(n1, Mx1, 'o', label='my Mx Time')
plt.plot(n1, Mx2, 'o', label='numpy Mx Time')
plt.legend()
plt.title('execution time as a function of n, for  multiplication of a matrix with a vector: Mx')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

# plotting xMx exectution times
plt.figure(4)
plt.plot(n1, xMx1,'o', label='my xMx Time')
plt.plot(n1, xMx2,'o',label='numpy xMx Time')
plt.legend()
plt.title('execution time as a function of n, for  multiplication of two vectors and a matrix: xTMx ')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

# plotting MM exectution times
plt.figure(5)
plt.plot(n1, MM1,'o', label='my MM Time')
plt.plot(n1, MM2,'o', label='numpy MM Time')
plt.legend()
plt.title('execution time as a function of n, for multiplication of two matrices: MM')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

# plotting  ratios of execution times for each n 
plt.figure(6)
plt.plot(n1, CompareD12 ,  '--o', label='xTx times')
plt.plot(n1, CompareMx12 , '--o', label=' Mx Times')
plt.plot(n1, ComparexMx12 , '--o', label='xMx Times')
plt.plot(n1, CompareMM12 , '--o', label='MM times')
plt.legend()
plt.title(' Comparing the execution times of my implementation with numpy as a function of n')
plt.xlabel("size of matrix (n)")
plt.ylabel(" ratio of numpy time/ my implementation time  (s/s)")
plt.show()

# plotting numpy time vs my execution time log scale 
plt.figure(7)
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
fig.suptitle('  Comparing numpy execution time with log scale of my implemenation execution time for each n')
ax1.plot(D2, np.log(D1) , '--o' , label='xTx')
ax1.legend( ['xTx'], loc='best', frameon=False)
ax2.plot(Mx2, np.log(Mx1)  , '--o', label=' Mx ')
ax2.legend(['Mx1'], loc='best', frameon=False)
ax3.plot(xMx2, np.log(xMx1) , '--o', label='xMx')
ax3.legend(['xMx ' ], loc='best', frameon=False)
ax4.plot(MM2, np.log(MM1) , '--o', label='MM')
ax4.legend(['MM' ], loc='best', frameon=False)

for ax in fig.get_axes():
    ax.label_outer()
#plt.legend(['my implementation time', 'numpy application time'], loc='upper right')
plt.xlabel("numpy time (s)")
plt.ylabel("log of my implementation time (s)")
plt.show()


#plots of estimated FLOPS for ecah operation oper n
plt.figure(8)
plt.plot(n1, FlopsD1 , '--o' , label='xTx')
plt.plot(n1, FlopsMx1 , '--o', label=' Mx ')
plt.plot(n1, FlopsxMx1 , '--o', label='xMx')
plt.plot(n1, FlopsMM1 , '--o', label='MM')
plt.legend()
plt.title('  estimating the number of floating point operations per second (FLOPS) of my system')
plt.xlabel("size of matrix (n)")
plt.ylabel(" loating point operations per second (number/s)")
plt.show()

# subplots of estimated flops per n for each operation
plt.figure(9)
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
fig.suptitle('  estimated flops per n for each operation ')
ax1.plot(n1, FlopsD1 , '--o' , label='xTx')
ax1.legend( ['my xTx FLOPS'], loc='best', frameon=False)
ax2.plot(n1, FlopsMx1 , '--o', label=' Mx ')
ax2.legend(['my Mx1 Time FLOPS'], loc='best', frameon=False)
ax3.plot(n1, FlopsxMx1 , '--o', label='xMx')
ax3.legend(['my xMx FLOPS ' ], loc='best', frameon=False)
ax4.plot(n1, FlopsMM1 , '--o', label='MM')
ax4.legend(['my MM FLOPS' ], loc='best', frameon=False)

for ax in fig.get_axes():
    ax.label_outer()
#plt.legend(['my implementation time', 'numpy application time'], loc='upper right')
plt.xlabel("size of matrix (n)")
plt.ylabel("Time per operation (s)")
plt.show()

