import MatrixOperations as MO

# this file calculates and outputs into json the values of xTx and xTMx

def run():
    n = [5,10,20,100,1000]
    
    results = {}
    results["xtx"] = {}
    results["xtMx"] = {}
    
    for dim in n:
        x = MO.setupx(dim)
        M = MO.setupM(dim)
        xtx = MO.vectorvector(x,x)  #calculates xTx
        results["xtx"][str(dim)] = round(xtx,3)  #rounding to 4 decimal places here to slightly account for floating point precision issues
        xtMx = MO.vectormatrixvector(x,M,x) #calculates xTMx
        results["xtMx"][str(dim)] = round(xtMx,3)
        
    MO.write_json(results)  #writes the results to json file as required
