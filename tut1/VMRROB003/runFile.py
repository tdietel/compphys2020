import Part1
import Part2
import PlotFigures


# this file is used to ensure the ordering of the various other parts of the tutorial submission
# i.e. Part 1 before Part 2 (pipeline does NOT run in alphabetical order in my experience)
if __name__ == "__main__":
    Part1.run()
    Part2.run()
    PlotFigures.run()