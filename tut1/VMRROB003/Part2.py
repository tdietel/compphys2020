import MatrixOperations as MO
import numpy as np

# this file deals with the timing of the various operations

def run():
    results = {}
    noNumpy(results)  # calculates timings for python implementation
    numpy(results) # calculates timings for numpy implementation
    MO.write_json(results, "timings.txt")
  

def createDictEntry(results, uRange, times, key1, key2):  # creates an entry in to the dictionary with given keys for n and time arrays
    for i in range(len(uRange)):
        results[key1][key2][uRange[i]] = times[i]

#####################################################   NO NUMPY
def MM(): #times MM calculation with python implementation
    MMTime = []
    MMRange = range(20,220,20)  # selects dimension range to test over
    for n in MMRange:
        M = MO.setupM(n)
        MMTime.append(MO.timed_func(MO.matrixmatrix, [M,M])[1])  # times the function and saves timing
    return MMRange,MMTime
  

def xx(): #times xTx calculation with python implementation
    xtxRange = range(500000,10000000,500000)  
    xtxTime = []
    for n in xtxRange:
        x = MO.setupx(n)
        xtxTime.append(MO.timed_func(MO.vectorvector, [x,x])[1])
    return xtxRange, xtxTime

def xM(): #times Mx and xTMx calculation with python implementation
    xMrange = range(250,2500,200)
    MxTime = []
    xtMxTime = []
    for n in xMrange:
        x = MO.setupx(n)
        M = MO.setupM(n)
        MxTime.append(MO.timed_func(MO.matrixvector, [M,x])[1])
        xtMxTime.append(MO.timed_func(MO.vectormatrixvector, [x,M,x])[1])
    return xMrange, MxTime, xtMxTime



def noNumpy(results):
    
    #gets all timing values
    mmrange, mmtime = MM()
    xtxRange, xtxTime= xx()
    xMrange, MxTime, xtMxTime=xM()
    
    key1 = 'noNumpy'
    
    #saves all to dictionary
    results[key1] = {}
    results[key1]['xtx'] = {}
    results[key1]['Mx'] = {}
    results[key1]['xtMx'] = {}
    results[key1]['MM'] = {}
    
    createDictEntry(results, xtxRange, xtxTime, key1, 'xtx')
    createDictEntry(results, xMrange, MxTime, key1, 'Mx')
    createDictEntry(results, xMrange, xtMxTime, key1, 'xtMx')
    createDictEntry(results, mmrange, mmtime, key1, 'MM')
    
 

##############################################   NUMPY
    
#all methods work same as above, but now with Numpy methods to perform the matrix operations
    
def MMNumpy():
    MMTime = []
    MMRange = range(100,3000,300)
    for n in MMRange:
        M = MO.setupMNumpy(n)
        MMTime.append(MO.timed_func(np.dot, [M,M])[1])
    return MMRange,MMTime
  

def xxNumpy():
    xtxRange = range(1000000,100000000,5000000)
    xtxTime = []
    for n in xtxRange:
        x = MO.setupxNumpy(n)
        xtxTime.append(MO.timed_func(np.dot, [x,x])[1])
    return xtxRange, xtxTime

def xMNumpy():
    xMrange = range(1000,8000,500)
    MxTime = []
    xtMxTime = []
    for n in xMrange:
        x = MO.setupxNumpy(n)
        M = MO.setupMNumpy(n)
        MxTime.append(MO.timed_func(np.dot, [M,x])[1])
        xtMxTime.append(MO.timed_func(MO.vectormatrixvectornumpy, [x,M,x])[1])
    return xMrange, MxTime, xtMxTime    
    
def numpy(results):
    mmrange, mmtime = MMNumpy()
    xtxRange, xtxTime= xxNumpy()
    xMrange, MxTime, xtMxTime=xMNumpy()
    
    key1 = 'Numpy'
    
    results[key1] = {}
    results[key1]['xtx'] = {}
    results[key1]['Mx'] = {}
    results[key1]['xtMx'] = {}
    results[key1]['MM'] = {}
    
    createDictEntry(results, xtxRange, xtxTime, key1, 'xtx')
    createDictEntry(results, xMrange, MxTime, key1, 'Mx')
    createDictEntry(results, xMrange, xtMxTime, key1, 'xtMx')
    createDictEntry(results, mmrange, mmtime, key1, 'MM')
    
##########################

