#PLLVIN013 CP TUT2
#importing the good stuff
import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import json

#To print the complex matrix
def complex_matrix(i):
    result = ""
    if abs(i)>1e-5:
        return "    "
    elif abs(np.imag(i))<1e-5:
        return"%4.1f" %np.real(i)
    elif abs(np.real(i))<1e-5:
        return "%4.1fj" %np.imag(i)
    


length = 5
cells = 500
Line_length = 5  
Cells = 500  
divisions = cells/length  
R = (30e-6 + 0j)/divisions  
L = (270e-9 + 0j)/divisions  
C = (110e-12 + 0j)/divisions
G = (1e109 + 0j)/divisions
R_int = 50 + 0j  
R_l = (100 + 0j)  
R_s = (0 + 0j)  
R_o = (100e18 + 0j)  

U_O = 100 + 0j  
w = 2*np.pi*(100e6 + 0j)  
R_characteristic = 49.5


#Defining the matrix
def matrix(R ,L, C, G, R_load, w, n):
    R_int = 50 #Ohms
    if w != 0 or R != 0:
        z_s = 1./(R+1j*w*L)
    else:
        z_s = 1e100
    if G != 0:
        z_p = 1./G+1j*w*C
    else:
        z_p = 1j*w*C
    
    M = np.zeros((2*n+1, 2*n+1), dtype = complex)
    for i in range (1,n):
        M[2*i-1][2*i-2:2*i+1] = [z_s, -z_p, -z_s]
        M[2*i][2*i-1:2*i+2] = [-1, 1, 1]
    M[0][0:2] = [-1, -1]
    M[1][0:3] = [1./(R_int + +1j*w*L), -z_p, -z_s]
    M[2*n-1][-3:] = [z_s, -z_p, -R_load]
    M[2*n][-2:] = [-1, 1]
    return M



vector = np.zeros(2*cells+1, dtype=complex)
vector[0] = U_O
v_l = np.linalg.solve(matrix(R, L,  C, G,R_l, w, cells ), vector)
v_s = np.linalg.solve(matrix(R, L,  C, G,R_s, w, cells ), vector) 
v_o = np.linalg.solve(matrix(R, L,  C, G,R_o, w, cells), vector) 

v_l = v_l[1::2]
v_s = v_s[1::2]
v_o = v_o[1::2]





plt.figure()
plt.plot([n for n in range(cells)], np.absolute(v_l),color="navy",  label="Termination Resistor")
plt.plot([n for n in range(cells)], np.absolute(v_s),color="orange", label="Short Circuit")
plt.plot([n for n in range(cells)], np.absolute(v_o),color="darkred",  label="Open Circuit")
plt.xlabel("Cable Length(cm)")
plt.legend()
plt.ylabel("Voltages(V)")
plt.show()

# Fourier Transforming of signal
N = 100  
T = 160e-9  
t = np.linspace(0.0, T, N) 
deltat = t[1] - t[0]  
t_0 =  20e-9
tau = 5e-9  # decay constant
signal = U_O*np.exp(-(t - t_0)/tau)*np.heaviside(t - t_0, np.real(U_O))
fourier_amplitudes = np.fft.fft(signal)
frequencies = np.fft.fftfreq(len(t), deltat)  # frequencies for fourier transform
plt.figure()
plt.plot(t*1e9, signal, label=" Exponential Decay Signal")
plt.title("Exponential decay signal  as a function of time")
plt.xlabel("time(ns)")
plt.ylabel('Voltage')
plt.legend()
plt.show()
plt.figure()
plt.plot(frequencies, fourier_amplitudes, color='darksalmon', label=" Discrete Fourier Transform of Signal")
plt.title("Fourier transform of the detected signal")
plt.xlabel("Frequency (Hz)")
plt.legend()
plt.show()


volts = []
for k, f in enumerate(frequencies):
	v = np.linalg.solve(matrix(R, L,  C, G, R_characteristic, 2*np.pi*f, cells), vector)
	v = v[0:-2:2]
	v_multiplied = v[-1]*fourier_amplitudes[k]
	volts.append(v_multiplied)

end_response = np.fft.ifft(volts)

# plotting response :)
plt.figure()
plt.plot(t*1e9, np.absolute(end_response), color="teal", label="Response Delay")
plt.xlabel("time(ns)")
plt.ylabel("Voltage(V)")
plt.title("Response to an right-sided exponential decay signal at end of a transmission line")
plt.legend()
plt.show()


lag = 27e-9
speed_signal = length/lag

dictionary = {}
a = {}
open_circuit = {}
shorted_circuit = {}
loaded_circuit = {}
open_circuit.update({"middle": np.absolute(v_o[250])})
open_circuit.update({"end": np.absolute(v_o[-1])})
shorted_circuit.update({"middle": np.absolute(v_s[250])})
shorted_circuit.update({"end": np.absolute(v_s[-1])})
loaded_circuit.update({"middle": np.absolute(v_l[250])})
loaded_circuit.update({"end": np.absolute(v_l[-1])})
a.update({"open": open_circuit})
a.update({"short": shorted_circuit})
a.update({"100ohm": loaded_circuit})
dictionary.update({"a": a})
dictionary.update({"c": R_characteristic})
dictionary.update({"f": speed_signal})

with open('tut2/PLLVIN013/answers.json', 'w') as f:
    json.dump(dictionary, f)
