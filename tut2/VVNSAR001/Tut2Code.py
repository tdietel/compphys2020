import numpy as np 
import matplotlib.pyplot as plt
import json

##Initial Values of transmission line
nCells = 500
length = 5
cpl = nCells / length
c = (110*10**(-12)+0j)/cpl #Capacitance
L = (270*10**(-9)+0j)/cpl #Inductance
R = (30*10**(-6)+0j)/cpl #resistance
G = (1*10**9+0j)/cpl #loss ohmic

Ri = 50 + 0j#initial resistance
iV = 10000 + 0j #input voltage
w= 2*np.pi*100e6 + 0j #omega


#Reactances
xC = -1j/(w*c)  # for capacitor
xL = (w*L)*1j  # for inductor

#Impedances
zGC = 1/(1/xC + 1/G)  # impedance of resistor G and Capacitor C (parallel addition)
zLR = xL + R  # impedance of inductor L and resistor R  (series addition)

V = 1500

Rt = 100#termination
Rs = 0 #short
Ro = 10**23 #open

Etype = ["OC","SC","TC"] #end type of circuit
currents = []
circCurrents = []
circVoltages = []
circHorisontalCurrents = []
circNodes = []
#when we consider different components we know that current gets divided so...
Gratio = G/(G+xC)
Cratio = xC/(G+xC)

#Generating the matrix:
for circuit in Etype:
    m = []  
    x = np.zeros(nCells, dtype=complex)  
    x[0] = iV  
    for i in range(nCells): 
        if i == 0:  #initial cell
            Mi = np.zeros(nCells, dtype=complex)  
            curr1 = Ri + zLR + zGC 
            curr2 = - zGC
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        elif i == nCells - 1:  #checking cases for last cell
            Mi = np.zeros(nCells, dtype=complex)
            if circuit == "TC":  
                curr1 = Rt + zGC
                curr2 = -zGC
                Mi[i] = curr1
                Mi[i - 1] = curr2
            elif circuit == "SC":  
                curr1 = Rs + zGC
                curr2 = -zGC
                Mi[i] = curr1
                Mi[i - 1] = curr2
            elif circuit == "OC":  
                curr1 = Ro + zGC
                curr2 = -zGC
                Mi[i] = curr1
                Mi[i - 1] = curr2
        else:  #rest of the cells
            Mi = np.zeros(nCells, dtype=complex)
            curr1 = zLR + 2*zGC
            curr2 = -zGC
            Mi[i - 1] = curr2  
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        m.append(Mi)
    currents = np.linalg.solve(m, x) #solving for the currents using matrix stuff
    #next we want to consider the vertical currents which we can find using I0 = I1 + I2
    FinalCurrents = []
    for n in range(nCells):
        if n != (nCells-1): #all cells but final cell
            Vcurr = currents[n] - currents[n+1] #from current law of kirchoff
            currG = Vcurr * Gratio
            currC = Vcurr * Cratio
            FinalCurrents.append(currents[n])
            FinalCurrents.append(currG)
            FinalCurrents.append(currC)
        else: 
            FinalCurrents.append(currents[n])
    
    nodeVoltages = []
    FinalVoltages = [iV]
    
    for m in range(nCells): #finalizing voltages
        if m==0: #first cell
            voltage = iV *(Ri+zLR) #decreasing the voltage based on the components in first cell
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        elif m==nCells-1:  #last cell
            pass 
        else:
            voltage = nodeVoltages[m-1] - currents[m]*(Ri+zLR)
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        
    circHorisontalCurrents.append(currents)
    circCurrents.append(FinalCurrents)
    circNodes.append(nodeVoltages)
    circVoltages.append(FinalVoltages)

midVolt = []
endVolt = [] #calcualting the middle voltages by just taking the half of the nCells to get the 1/2th cell
for i in range(len(circVoltages)):
    middle = circVoltages[i][nCells//2]
    end = circVoltages[i][nCells - 1]
    midVolt.append(middle)
    endVolt.append(end)

#Termination resistor value
theoryy = np.sqrt(zLR/((1/G)+(1j*w*c)))
theoryZ = np.round(np.abs(theoryy),2)

print("Theoretical Termination Resistor Value=", theoryZ) #theoretical value
#calculating it using our model
resistantCircVoltages = []
famplitude = [] 
rRange = np.arange(45,55,0.5) #the resistance range to check over - based on our theoretical value
for l in range (len(rRange)):
    m = []  
    x = np.zeros(nCells, dtype=complex)  
    x[0] = iV  
    for i in range(nCells): 
        if i == 0:  #initial cell
            Mi = np.zeros(nCells, dtype=complex)  
            curr1 = Ri + zLR + zGC 
            curr2 = - zGC
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        elif i == nCells - 1:  #checking cases for last cell
            Mi = np.zeros(nCells, dtype=complex)
            curr1 = rRange[l] + zGC
            curr2 = -zGC
            Mi[i] = curr1
            Mi[i - 1] = curr2    
        else:  #rest of the cells
            Mi = np.zeros(nCells, dtype=complex)
            curr1 = zLR + 2*zGC
            curr2 = -zGC
            Mi[i - 1] = curr2  
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        m.append(Mi)
    currents = np.linalg.solve(m, x) #solving for the currents using matrix stuff
    #next we want to consider the vertical currents which we can find using I0 = I1 + I2
    nodeVoltages = []
    FinalVoltages = [iV]
    
    for m in range(nCells): #finalizing voltages
        if m==0: #first cell
            voltage = iV *(Ri+zLR) #decreasing the voltage based on the components in first cell
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        elif m==nCells-1:  #last cell
            pass 
        else:
            voltage = nodeVoltages[m-1] - currents[m]*(Ri+zLR)
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        
    resistantCircVoltages.append(FinalVoltages)
    ampli = np.max(resistantCircVoltages) - np.min(resistantCircVoltages)
    famplitude.append(ampli)
zValue = []
for k in range(len(famplitude)):
    if famplitude[k]==np.min(famplitude):
        zValue.append(rRange[k])



#plotting voltages for the wire
plt.figure()
plt.title("Graph of voltages for the transmission line against the length")
plt.plot([n for n in range(nCells)], np.absolute(circVoltages[0]), label="Open Circuit")
plt.plot([n for n in range(nCells)], np.absolute(circVoltages[1]), label="Short Circuit")
plt.plot([n for n in range(nCells)], np.absolute(circVoltages[2]), label="Terminated Circuit")
plt.xlabel("length (cm)")
plt.ylabel("Voltage(V)")
plt.legend()
plt.show()

#plotting Currents for the wire 
plt.figure()
plt.title("Graph of currents for a transmission line against the length")
plt.plot([n for n in range(nCells)], np.absolute(circHorisontalCurrents[0]), label="Open Circuit")
plt.plot([n for n in range(nCells)], np.absolute(circHorisontalCurrents[1]), label="Short Circuit")
plt.plot([n for n in range(nCells)], np.absolute(circHorisontalCurrents[2]), label="Terminated Circuit")
plt.xlabel("Length (cm)")
plt.ylabel("Current(Amperes)")
plt.legend()
plt.show()



#fourier
tau = 5*10**-9
N = 500  
T = 100e-9  
t = np.linspace(0, T, N)  
t0 = 50e-9  
fts = t[1] - t[0] 
Potential = iV*np.exp(-(t - t0)/tau)*np.heaviside(t - t0, np.real(iV))
  
plt.figure()
plt.plot(t, Potential) 
plt.title("Plot of exponential function ")
plt.xlabel("Time(ns)")
plt.ylabel('Potential Difference (Voltage) ')
plt.legend()
plt.show()
#fourier transform figure
fts = t[1] - t[0] 
fourier = np.fft.fft(Potential)
f = np.fft.fftfreq(len(t), fts)

plt.figure()
plt.plot(f, fourier) 
plt.xlabel("Frequency (Hz)")
plt.ylabel('Potential Difference (Voltage) ')
plt.title("Plot of fourier transformed function")
plt.show() 

#for e and f we again need to model the  circuit 
expResp = np.zeros(len(f),dtype=complex)
for l,x in enumerate(f):
    #we need to define new constants for the fourier case
    wF = 2*np.pi*x+0j
    zLRf = R + 1j*wF*L
    zGCf = 1/((1/G) + 1/1j*wF*c)
        
    m = []  
    x = np.zeros(nCells, dtype=complex)  
    x[0] = iV  
    for i in range(nCells): 
        if i == 0:  #initial cell
            Mi = np.zeros(nCells, dtype=complex)  
            curr1 = Ri + zLRf + zGCf 
            curr2 = - zGCf
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        elif i == nCells - 1:  #checking cases for last cell
            Mi = np.zeros(nCells, dtype=complex)
            curr1 = theoryZ + zGCf
            curr2 = -zGCf
            Mi[i] = curr1
            Mi[i - 1] = curr2    
        else:  #rest of the cells
            Mi = np.zeros(nCells, dtype=complex)
            curr1 = zLRf + 2*zGCf
            curr2 = -zGCf
            Mi[i - 1] = curr2  
            Mi[i] = curr1  
            Mi[i + 1] = curr2  
        m.append(Mi)
    currents = np.linalg.solve(m, x) #solving for the currents using matrix stuff
    #next we want to consider the vertical currents which we can find using I0 = I1 + I2
    nodeVoltages = []
    FinalVoltages = [iV]
   
    for m in range(nCells): #finalizing voltages
        if m==0: #first cell
            voltage = iV *(Ri+zLR) #decreasing the voltage based on the components in first cell
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        elif m==nCells-1:  #last cell
            pass 
        else:
            voltage = nodeVoltages[m-1] - currents[m]*(Ri+zLR)
            nodeVoltages.append(voltage)
            FinalVoltages.append(voltage)
        
    expo = FinalVoltages[499]*fourier[i]
    expResp[l] = expo       #checking last cell
invFourier = np.fft.ifft(expResp)
#plotting response from fourier trasnform
plt.figure()
plt.plot(t,invFourier)
plt.xlabel("time (seconds") 
plt.ylabel("potential difference (voltage)") 
plt.title("Inverse Fourier transformed exponential decay from the right.") 
plt.show()
speed = 0.12
print()
answers = {}
a = {}
oc = {}
sc = {}
o_100 = {}
oc.update({"middle": np.absolute(midVolt[2])})
oc.update({"end": np.absolute(endVolt[2])})
sc.update({"middle": np.absolute(midVolt[1])})
sc.update({"end": np.absolute(endVolt[1])})
o_100.update({"middle": np.absolute(midVolt[0])})
o_100.update({"end": np.absolute(endVolt[0])})
a.update({"open": oc})
a.update({"short": sc})
a.update({"100ohm": o_100})
answers.update({"a": a})
answers.update({"c": theoryZ})
answers.update({"f": speed})

print("mid oc",midVolt[2]," mid sc ",midVolt[1]," midtc ",midVolt[0]," end oc ",endVolt[2]," end sc ",endVolt[1]," end tc", endVolt[0])
# section 11: writing dictionary to a .json file
with open('tut2/VVNSAR001/answers.json', 'w') as f:
    json.dump(answers, f)















   
