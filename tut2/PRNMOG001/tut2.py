#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 12:29:39 2020

@author: rayhaan
"""
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fft, fftfreq, ifft
import json

#defined function to write to json file to path specified
def WJSON(path, fileName, data):
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)

#Defining Constants
Ri = 50 + 0j #Internal Resistiance
f = 100e6 #Frequency of input voltage
Rl = 100 ##Load Resistor Resistor
seg = 500 #Number of segments
Lw = 5 #Length of wire
cm = seg/Lw #Number of cells per metre
C = 110e-12/cm #pFm^-1
L = 270e-9/cm #nHm^-1
R = 30e-6/cm #muOhmm^-1
G = 1e12/cm #GOhmm^-1
U0 = 100 #Input voltage of sinusoidal input
w = 2*np.pi*f #angular frequency omega
R_short = 0
R_open = 100**50
tau = 5e-9 #Decay time
Z0 = np.sqrt((1j*w*L + R)/(1j*w*C + G))

##Setting up matrix equation, always clockwise KVL
#Reac
LR = 1j*w*L #reactance of inductor
CR = -1j/(w*C) #reactance of capcitor

#Imped
TLR = R + LR##Impedances just add in series
SGC = 1/((1/G) + 1/(CR))## take inverse of inverse addition of impedances in parallel

##Circuits
circuit = ['100ohm','short','open']# so i dont have to use multiple python scripts
##storing stuff
HC = []
CC = []
CNV = []
CV = []

##Ax = b
for c in circuit:
    b = np.zeros(seg, dtype = complex) # the b in our equation Ax = b
    b[0] = U0
    A = []
    for i in range(seg): ##creating an array with the currents(unknown) we looking at in the range, from 0 to 499
        if i == 0: ##First cell working with current I_0
            row = np.zeros(seg, dtype = complex)
            wcur = Ri + TLR + SGC
            acur = -SGC
            row[i] = wcur #frist entry going around loop in first cell with currents I_0
            row[i + 1] = acur #Next with currents I_1
        elif i == seg - 1:
            row = np.zeros(seg, dtype = complex)
            if c == '100ohm': #End of circuit with terminal resistor
                wcur = SGC + Rl; #with loop
                acur = -SGC #against loop
                row[i] = wcur 
                row[i-1] = acur
            elif c == 'short': #End of circuit with short circuit imples Rl = 0
                wcur = SGC + R_short
                acur = -SGC
                row[i] = wcur
                row[i-1] = acur
            elif c == 'open': #End f circuit with open circuit imples Rl = really big
                wcur = SGC + R_open
                acur = -SGC 
                row[i] = wcur
                row[i-1] = acur
        else:
            row = np.zeros(seg, dtype = complex) #middle cells
            wcur = TLR + 2*SGC
            acur = -SGC
            row[i] = wcur
            row[i-1] = acur
            row[i+1] = acur
        A.append(row)
    I = np.linalg.solve(A,b) #Solving for currents, solving for x in Ax = b
    ##Calculating currents through G resisitor and C capacitor
    Ia = []
    i = 0
    #ratio C and G capacitor and resistor
    Gr = G/(G + CR)
    Cr = C/(G + CR)
    for i in range(seg):
        if i != (seg -1):
            Iv = I[i] - I[i+1]
            IG = Iv*Gr
            IC = Iv*Cr
            Ia.append(I[i])
            Ia.append(IG)
            Ia.append(IC)
        else:
            Ia.append(I[i])
    Nv = []
    Av = [U0]
    i = 0
    for i in range(seg):
            if i == 0:
                V = U0 - I[i]*(Ri + TLR)
                Nv.append(V)
                Av.append(V)
            elif i == (seg-1):
                pass
            else:
                V = Nv[i-1] - I[i]*TLR
                Nv.append(V)
                Av.append(V)
    
    HC.append(I) #
    CC.append(Ia) #
    CNV.append(Nv)##Storing data
    CV.append(Av)#
    
##Plot boiz
a = np.linspace(0,5,500)
#
plt.figure(1)
plt.title("Voltage vs Length along cable")
plt.plot(a, np.absolute(CV[0]), label="Terminal Load",color = 'b')
plt.plot(a, np.absolute(CV[1]), label="Short Circuit",color = 'r')
plt.plot(a, np.absolute(CV[2]), label="Open Circuit",color = 'g')
plt.xlabel("m")
plt.ylabel("Voltage(V)")
plt.legend(loc = 'best')
plt.savefig('Vvslength.png')
##
plt.figure(2)
plt.title("Currents vs Length along cable")
plt.plot(a, np.absolute(HC[0]), label="Terminal Load",color = 'b')
plt.plot(a, np.absolute(HC[1]), label="Short Circuit",color = 'r')
plt.plot(a, np.absolute(HC[2]), label="Open Circuit",color = 'g')
plt.xlabel('m')
plt.ylabel("Current(A)")
plt.legend(loc = 'best')
plt.savefig('Ivslength.png')
            
#Voltage at end and middle in wire for different cases               
i=0
Vmid = []
Vend = []
for i in range(len(CV)): ##Creating small lists with middle and end voltage values
    Vm = CV[i][250]
    Ve = CV[i][499]
    Vmid.append(np.abs(Vm))
    Vend.append(np.abs(Ve))
    
c = 0
i = 0
dic = {'a':{'100ohm':{'middle':Vmid[0],'end':Vend[0]},'short':{'middle':Vmid[1],'end':Vend[1]},'open':{'middle':Vmid[2],'end':Vend[2]}},'c':'null','f':'null'} #creating the dictionary
    
dic1 = {'c':50} #characteristic impedance of transmission line
dic.update(dic1) #updating dictionry with c's answer

V = 0
##Fourier Transform of input signal

t = np.linspace(0,40e-9,1000) #defining time for the input signal
V = U0*np.exp(-t/tau) #Dthe expression for the voltage as a fucntion of time

ffts = fft(V) #taking the fourier transform of the input voltage

T = t[1] - t[0]  # sampling interval 
N = t.size #size of N

# 1/T = frequency
f = np.linspace(0, 1 / T, N) #the frequency samples

#PLotting input voltage 
plt.figure(4)
plt.plot(t,V,color = 'r', label = 'Input voltage')
plt.title('Input signal voltage vs time ')
plt.xlabel('Time(s)')
plt.ylabel('Votage(V)')
plt.legend(loc = 'best')
plt.savefig('InputSig.png')


#PLotting fourier transform
plt.figure(3)
plt.ylabel("Amplitude")
plt.xlabel("Frequency [Hz]")
plt.title('Fourier transform of exponential input voltage')
plt.plot(f[:N // 2], np.abs(ffts)[:N // 2] * 1 / N,color = 'b',label = 'Fourier Transform')  # 1 / N is a normalization factor
plt.legend(loc='best')
plt.show()
plt.savefig('Fourierexp.png')

V0 = 1/(np.sqrt(L*C*cm**2)) #Signal speed in a transmission line
dic2 = {'f':V0} 
dic.update(dic2)

Z0 = np.sqrt(L/C)
dic3 = {'c':Z0}
dic.update(dic3)
WJSON('/tut2/PRNMOG001/','asnwers',dic)
#Solving again for voltages using exponential input signal 
#no idea
                
                
                
                
                
                
                
                
                
                
                
                
        
        