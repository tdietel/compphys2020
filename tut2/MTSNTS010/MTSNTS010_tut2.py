#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 16:24:32 2020

@author: ntshembho
"""

' ' ' CP TUT 2 ' ' ' 

import numpy as np
import json
import matplotlib.pyplot as plt


' ' ' Values ' ' '

#Ri = 50 #ohms
#C = 110*10e-14 #pFm-1
#L = 270e-11 #nHm-1
#R = 20e-8 #ohms
#G = 10e7 #ohms-1 (10e7)
#f = 100e6 #Hz (frequency)
#w = 2*np.pi*f #Hz
U0 = 100
#j=1j

C= 110*10**(-14)  #Capacitance
L=270*10**(-11)  #inductance
R= 20*10**(-8)    #Risitance
G= 10**7          #Ohmic Losses
Ri=50 
           
f=100*10**6       #fequency of sinusoidal input voltage
w=2*np.pi*f
j=1j

' ' ' Impedence Calculations ' ' '
#j = 1
Zc = 1/(j*w*C)
Zl = j*w*L
Zlr = Zl + R
Zg = G
Zgc = 1/(j*w*C + 1/G)



' ' ' Defining Matrix ' ' ' 

n = 500 # number of Kirchof loops needed 

Rl = 100 # ohms  terminator resistor 
Rs = 0 #ohms, short 
Ro = 10e20 #float('inf')

R_a=[Rl,Rs,Ro]

labels = ['terminator resistor', 'short circuit','open circuit']

V_mid = []
V_end = []
max_min = []

def Matrix(Ra):
    M = np.zeros((2*n+1, 2*n+1), dtype=complex)
    # first entries
#    j=1j
    M[0][0] = Ri+R+j*w*L
    M[0][1] = Zgc
    M[1][0]=1
    M[1][1]=-1
    M[1][2]=-1
    #Last entry 
    M[2*n][2*n] = Ra
    M[2*n][2*n-1] = -Zgc
    #enter rest of the loops
    for i in range(2,2*n):
        if i%2==0:
        #current loops
            M[i][i] = 1
            M[i][i+1] = -1
            M[i][i+2] = -1
            #voltage loops 
            M[i+1][i-1] = -Zgc
            M[i+1][i] = R+j*w*L
            M[i+1][i+1] = Zgc
        
    return M
        
#Define vector 
U = np.zeros((2*n+1), dtype = complex)
U[0] = U0

x = np.linspace(0,5,501)
#
for i in range(len(R_a)):
    I = np.linalg.solve(Matrix(R_a[i]),U)
    Imag = np.absolute(I)
    Ic = [] #complex current
    Im = [] #magnitude of current 
    for k in range(len(I)):
        if k%2==0:
            Ic.append(I[k])
            Im.append(Imag[k])

    
    voltage = [] #across LR sections
    for k in range(len(Ic)):
        voltage.append(Ic[k]*(R+j*w*L))
    
    V_mid.append(voltage[251]-voltage[250])
    V_end.append(voltage[-1]-voltage[0])
    
    volt_abs = np.absolute(voltage) 
    plt.plot(x,volt_abs, label = labels[i]) #plotting amplitudes for a)
    plt.xlabel('length [m]')
    plt.ylabel('Voltage [V]')
    plt.legend()
    plt.title('The voltage along the wire')
    plt.savefig('voltages')
    max_min.append(max(volt_abs) - min(volt_abs))
plt.show()
Open={"middle":abs(V_mid[2]), "end": abs(V_end[2])}
short={"middle":abs(V_mid[1]), "end":abs(V_end[1])}
term ={"middle":abs(V_mid[0]), "end":abs(V_end[0])}
answers={"a":{"open":Open, "short":short, "100ohm":term}, "b":50}

with open('tut2/MTSNTS010/solutions.json','w') as f:
    json.dump(answers,f)   
 
' ' ' Fourier Transform ' ' ' 
# new words
#constants

#tau = 5e-9 #decay rate 
t0=50
tf=200 #ns
t=np.linspace(0, tf,200) #nanosec
t_step=t[1]-0

V0=100
tau=5 #nano sec

Vt=V0*np.exp(-(t-t0)/tau)*np.heaviside(t-t0, V0)  #voltage function

ft=np.fft.fft(Vt)
freq=np.fft.fftfreq(len(Vt), t_step)


plt.plot(t,Vt, '-r')
plt.xlabel('time [ns]')
plt.ylabel("Voltage [V]")
plt.title("Exponential volatge signal (time domain")
plt.savefig('time')
plt.figure()
plt.plot(freq,ft, '-r')
plt.xlabel('frequency [Hz]')
plt.ylabel('Voltage [V]')
plt.title("Exponential volatge signal (frequency domain)")
plt.savefig('freq')
plt.show()

