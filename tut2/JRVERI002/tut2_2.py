# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 14:33:12 2020

@author: erinj
"""

###CP tut 2

import numpy as np
import json
import matplotlib.pyplot as plt

####################################################################
# Transmission Line  With sinusoidal voltage#
###################################################################
#will solve a matrix equation of the form MI=A

##Values %100
C= 110*10**(-14)  #Capacitance
L=270*10**(-11)  #inductance
R= 20*10**(-8)    #Risitance
G= 10**7          #Ohmic Losses
Ri=50 
           
f=100*10**6       #fequency of sinusoidal input voltage
w=2*np.pi*f
j=1j

Z=1/(1/G+j*w*C)  #impedence of capacitor+resistor

#choose voltage
U0=150000

n=500 #number of cells


#matrix (M) for the coefficients of the current vector I
M=np.zeros((2*n+1,2*n+1), dtype=complex)

#manually enter matrix elements for first cell
M[0][0]=Ri+R+j*w*L
M[0][1]=Z
M[1][0]=1
M[1][1]=-1
M[1][2]=-1


Rl=100      #for termination resisitor
Rs=0        #for short
Ro=10**(20)  #for open
Ropt_search=np.linspace(48,51, 40)     #optimum resistance for termination resistor 

R_list=[Rl,Rs,Ro]
for i in range(len(Ropt_search)):  #create list of resistace values to loop over
    R_list.append(Ropt_search[i])

end=["100ohm", "Short", "Open"]
s_end=[]
s_mid=[]

max_min=[]

for l in range(len(R_list)):
    #manually enter last cell
    M[2*n][2*n]=R_list[l]
    M[2*n][2*n-1]=-Z

    for i in range(2,2*n):
        if i%2==0:
            #current node
            M[i][i]=1
            M[i][i+1]=-1
            M[i][i+2]=-1
            #voltage loop
            M[i+1][i-1]=-Z
            M[i+1][i]=R+j*w*L
            M[i+1][i+1]=Z
                  
    
    #create vector A
    V=np.zeros(2*n+1, dtype=complex)
    V[0]=U0
    
    #solve MI=V using numpy module
    I=np.linalg.solve(M,V)   #complex currents
    
    Iabs=np.absolute(I)   #magnitude of the complex currents
    
    
    print
    If=[]    #mag of current through RL wires
    Ic=[]    #complex current throug RL wires
    for i in range(len(I)):
        if i%2==0:
            If.append(Iabs[i])
            Ic.append(I[i])
    
    
           
    volts=[] #Voltage across RL sections
    for i in range(len(If)):
        volts.append(Ic[i]*(R+j*w*L))
           
    
    
    volts_diff=[] #voltage difference between segments

    for i in range(len(volts)-1):
        volts_diff.append(volts[i+1]-volts[i])
        
    ab=np.absolute(volts_diff)
    
    
    s_end.append(volts[-1]-volts[0]) 
    s_mid.append(volts[250]-volts[251])
    
    ##plots
    
    if l<=2:
        x=np.linspace(0,5,len(volts))
        
        plt.plot(If, label = end[l]) 
        plt.legend()
        plt.xlabel("x (cm)")
        plt.ylabel("Voltage")
        plt.title("Magnitude of the voltage along the wire")
    
            
    max_min.append(max(np.absolute(volts))-min(np.absolute(volts)))
    
for m in range(len(max_min)):
    if max_min[m]==min(max_min):
        Ropt=R_list[m]
        print("Min Volatge Diff", min(max_min))
        print("Resistance", R_list[m])

plt.figure()
plt.plot(R_list[3:],max_min[3:])
plt.xlabel('Resitance (Ohms)')
plt.ylabel('Voltage difference (V)')
plt.title('Difference between the maximum and minimum voltage vs termination resistance')


#################################################################
# Fourier Transform #
#################################################################

t0=50
tf=200 #ns
t=np.linspace(0, tf,200) #nanosec
t_step=t[1]-0

V0=100
tau=5 #nano sec

Vt=V0*np.exp(-(t-t0)/tau)*np.heaviside(t-t0, V0)  #voltage function

ft=np.fft.fft(Vt)
freq=np.fft.fftfreq(len(Vt), t_step)

plt.figure()
plt.plot(t,Vt)
plt.xlabel('time(ns)')
plt.ylabel("Voltage")
plt.title("Exponential volatge signal in time domain")

plt.figure()
plt.plot(freq,ft)
plt.xlabel('frequency')
plt.ylabel('Voltage')
plt.title("Exponential volatge in frequency domain")





n=500 #number of cells

#matrix (M) for the coefficients of the current vector I
M=np.zeros((2*n+1,2*n+1), dtype=complex)
 
volt_drop=[]    #array for the voltage drop over the last resistor  
posfreq=[]        
for m in range(len(freq)):
    
    posfreq.append(freq[m])
    w=2*np.pi*freq[m]
    Z=1/(1/G+j*w*C)
    
    #manually enter last cell
    M[2*n][2*n]=Ropt
    M[2*n][2*n-1]=-Z
    
    for i in range(2*n):
        if i%2==0:
            #current node
            M[i][i]=1
            M[i][i+1]=-1
            M[i][i+2]=-1
            #voltage loop
            M[i+1][i-1]=-Z
            M[i+1][i]=R+j*w*L
            M[i+1][i+1]=Z
    
    #create vector A
    V=np.zeros(2*n+1, dtype=complex)
    V[0]=ft[m]
    
    
    #solve MI=V using numpy module
    I=np.linalg.solve(M,V)   #complex currents
    
    Iabs=np.absolute(I)   #magnitude of the complex currents
    
    If=[]    #mag of current through RL wires
    Ic=[]    #complex current throug RL wires
    for i in range(len(I)):
        if i%2==0:
            If.append(Iabs[i])
            Ic.append(I[i])
    
           
    volt_drop.append(Ic[-1]*Ropt)
        


plt.figure()
plt.plot(posfreq,volt_drop)
plt.xlabel('Frequency')
plt.ylabel('Voltage')
plt.title('Plot of the output voltage in frequency domain')

plt.figure()
ifft=np.fft.ifft(ft*volt_drop)
plt.plot(t,ifft)
plt.xlabel("Time (ns)")
plt.ylabel("Voltage")
plt.title("Plot of the output voltage in time domain")


Open={"middle":abs(s_mid[2]), "end": abs(s_end[2])}
short={"middle":abs(s_mid[1]), "end":abs(s_end[1])}
term={"middle":abs(s_mid[0]), "end":abs(s_end[0])}
submit={"a":{"open":Open, "short":short, "100ohm":term}, "c":Ropt, "f":10**8}

with open('tut2/JRVERI002/answers.json','w') as f:
    json.dump(submit,f)

