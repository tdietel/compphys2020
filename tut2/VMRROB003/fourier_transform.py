import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack

# function defining detector pulse
def function(t, V0, t0, tau):
    if t >= t0: # heaviside condition
        return V0 * np.exp(-(t-t0)/tau)
    else :
        return 0


def transform(V0, tau, t0):
    tmax = 40e-9
    step = tmax/200
    tdata = np.arange(0,tmax,step)
    
    ydata = []
    for i in tdata:
        ydata.append(function(i, V0, t0, tau))
    
    #setup time domain figure
    plt.figure()
    plt.subplot(211)
    plt.plot(tdata, ydata)
    plt.xlabel("t (s)")
    plt.ylabel("Voltage (V)")
    
    #performs the Fourier transform on the pulse data
    yf=scipy.fftpack.fft(ydata)
    #calculates the associated omega values for each fft return
    freq = np.fft.fftfreq(len(ydata), tdata[1] - tdata[0])* 2* np.pi
    
    # setup frequency domain subplot
    ax = plt.subplot(212)
    ax.set_xlim([0, 9e9])
    
    #plots the transformed data
    markerline, stemline, baseline, = ax.stem(freq,np.abs(yf),linefmt='k-',markerfmt='bo',basefmt='k.')
    
    
    plt.setp(stemline, linewidth = 1.25)
    plt.setp(markerline, markersize = 3)
    plt.title("Detector signal and Fourier transform")
    plt.xlabel("ω (1/s)")
    plt.ylabel("Voltage (V)")
    plt.tight_layout()
    plt.savefig("tut2/VMRROB003/fourier_transform_d.png")
    
    #returns list of data of the form (frequency, amplitude)
    return tdata, freq, yf
    
#transform(1, 5e-9, 5e-9)