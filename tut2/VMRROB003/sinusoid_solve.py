import json
import numpy as np
from matrix_equation import setupSystem
import matplotlib.pyplot as plt
import sys
from fourier_transform import transform
import scipy.fftpack

def load_constants(file_loc):
    # loads all constants from the json input file
    with open(file_loc, 'r') as read_file:
        inputdata = json.load(read_file)
    l = inputdata['l']
    n = inputdata['n']
    C = inputdata['C']
    R = inputdata['R']
    G = inputdata['G']
    L = inputdata['L'] 
    
    return l,n,C,L,R,G
    
def find_currents(n,l,R,L,C,G,omega, Ui, Ri):
    # sets up the equation system and then solves it for given parameters
    matrix, vector = setupSystem(n,l, R,L,C,G,omega, Ui, Ri)   
    I = np.linalg.solve(matrix, vector)
    return I, np.abs(I), np.angle(I, deg = True)
    
def find_voltages(n, R,L,omega, currents, Ri, load):
    Vdata = np.zeros(n, dtype = np.complex_) # sets up V(x)
    Vdata[0] = 1 - R * currents[0] - omega * L * currents[0] * 1j - Ri *currents[0]  # calculates the initial voltage
    for i in range(1,n): #determines the voltage drop over the rest of the n loops
        Vdata[i] = Vdata[i-1] - R*currents[3*i] - omega * L * currents[3*i] * 1j
    Vterm = Vdata[i-1] - load*currents[-1]
    return np.abs(Vdata), Vterm # returns |V(x)|

def plot_voltage(length, n, Vdata, ax, legend_string):
    #plots the voltage data over the length of the cable
    xrange = np.linspace(0, length, n)
    ax.plot(xrange, Vdata, label = legend_string)
    
    
    
if __name__ == "__main__":
    #setting up json file
    json_results = {"a" : None, "c": None, "f": None}
    json_results["a"] = {"open": {"middle": None, "end": None}, "short": {"middle": None, "end": None},"100ohm": {"middle": None, "end": None}}
    
    length = 5
    l,n,C,L,R,G = load_constants("tut2/data/input.json")  # loading constants from json input

    correct_factor = length/n # separating the impedances per length into individual elements
    C *= correct_factor
    R *= correct_factor
    G *= correct_factor
    L *= correct_factor
    
    # Internal resistor
    Ri = 50
    # Angular Frequency of input
    omega = 2*np.pi*1e8
    Ui = 1  # Using simple input voltage amplitude of 1V
    
    ###########################################################
    
    # Part a, b
    ax = plt.figure().gca()
    loads = [l,sys.maxsize, 0]  # each of the required voltages
    load_string = ["Load " + str(l) + " Ω", "Open Circuit", "Short Circuit"] #labels used for legend
    for i in range(len(loads)): 
        I, mags, phases = find_currents(n,loads[i],R,L,C,G,omega, Ui, Ri)  # calculates currents for each load
        Vdata, vterm = find_voltages(n,R,L,omega, I, Ri, loads[i]) # calculates the voltage |V(x)|
        plot_voltage(length, n, Vdata, ax, load_string[i]) # plots |V(x)|
        
        # saving results to json file
        if i == 0:
            json_results["a"]["100ohm"]["middle"] = mags[3*250+1]*G
            json_results["a"]["100ohm"]["end"] = mags[-1]*loads[i]
        elif i == 1:
            json_results["a"]["open"]["middle"] = mags[3*250+1]*G
            json_results["a"]["open"]["end"] = mags[-1]*loads[i]
        else:
            json_results["a"]["short"]["middle"] = mags[3*250+1]*G
            json_results["a"]["short"]["end"] = mags[-1]*loads[i]
       
        
    ##############################################################
    
    #Part b continued
    #setup rest of figure for b
    plt.title("Voltage as a function of length along the cable")
    plt.xlabel("Length (m)")
    plt.xlim([0,5])
    plt.ylabel("Voltage (V)")
    plt.legend()
    plt.savefig("tut2/VMRROB003/voltage_plot_b.png")
    
    ##############################################################
#    # Part c
    diff = [] # difference between max and min amplitude
    loads = np.linspace(20,80,200)  # know analytic value is ~50, looks for numerical result in this range
    for i in range(len(loads)):
        I,mags, phases = find_currents(n,loads[i],R,L,C,G,omega, Ui, Ri)
        Vdata, vterm = find_voltages(n,R,L,omega, I, Ri, loads[i])
        diff.append(np.max(Vdata)-np.min(Vdata))
    result = np.where(diff == np.amin(diff))  # finds all min values of difference
    opt_resist = loads[result[0][0]]
#    opt_resist = 49.54773869346734
    print('Resistance :', opt_resist)
    json_results['c'] = opt_resist
#        
    ############################################################
    
    
    # Part d
    
    tau = 5e-9 #given
    t0 = 5e-9 #using as default value
    
    tdata, omegas, amps = transform(Ui, tau, t0) # calculates Fourier transform of detector signal
    
    v_over_resistor = [0.9]  # DC input - assume little loss of signal since R << 1
    for i in range(1,len(omegas)):
        I, mags, phases = find_currents(n,opt_resist,R,L,C,G,omegas[i], Ui, Ri)
        v_over_resistor.append(I[-1]*opt_resist)

    plt.figure()
    plt.title("Voltage over termination resistor multiplied by frequency amplitude")
    plt.xlabel("ω (1/s)")
    plt.ylabel("Voltage (V)")
    plt.xlim([0,1.5e10])
    plt.plot(omegas[1:], v_over_resistor[1:]*amps[1:], 'ko', markersize = 3)
    plt.savefig("tut2/VMRROB003/v_of_omega_d.png")
    
    #do inverse transform of V(omega) to get V(t)
    modv = v_over_resistor*amps
    yt = scipy.fftpack.ifft(modv)
    
    plt.figure()
    plt.title("Inverse transform of voltage over termination resistor")
    plt.xlabel("t (s)")
    plt.ylabel("Voltage (V)")
    plt.plot(tdata, np.real(yt),'ko', markersize = 3)
    plt.savefig("tut2/VMRROB003/v_of_t_f.png")
    
    
    delay = tdata[np.argmax(np.real(yt))] - t0  # delay calculated by finding max point of wave in V(t) and subtracting from t0
    print(delay)
    json_results["f"] = delay
    
    #save json results
    with open("tut2/VMRROB003/output.json", 'w+') as dump_file:
        json.dump(json_results, dump_file)
        
        
        
        
        
        
        
        
        
        
        
        
        