import numpy as np

def setupSystem(n,l,R,L,C,G,omega, Ui, Ri):
    outputvector = np.zeros(n*3+1)  # setup output vector with -input voltage
    outputvector[0] = -Ui
    systemMatrix = np.zeros((3*n+1, 3*n+1), dtype = np.complex_)
    for i in range(0,n):  # setup matrix per loop
        #loop of resistor, inductor, G resistor 
        if i ==0: #first loop with input voltage
            systemMatrix[0][0] = -R - omega * L * 1j - Ri
            systemMatrix[0][1] = -G
        else: #other loops
            systemMatrix[3*i][3*i] = -R - omega * L * 1j
            systemMatrix[3*i][3*i-1] = 1/(omega*C*1j)
            systemMatrix[3*i][3*i+1] = -G
        
        #loop of G resistor and capacitor
        systemMatrix[3*i+1][3*i+1] = G
        systemMatrix[3*i+1][3*i+2] = -1/(omega*C*1j)
        #current law
        systemMatrix[3*i+2][3*i] = 1
        systemMatrix[3*i+2][3*i+1] = -1
        systemMatrix[3*i+2][3*i+2] = -1
        systemMatrix[3*i+2][3*i+3] = -1
    #final loop with termination resistor
    systemMatrix[3*n][3*n] = -l
    systemMatrix[3*n][3*n-1] = 1/(omega*C*1j)
    return systemMatrix, outputvector
    