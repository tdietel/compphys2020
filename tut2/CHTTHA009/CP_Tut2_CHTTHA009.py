# thavish chetty - chttha009 - computational physics tutorial 2
# section 1: importing modules
import numpy as np
import matplotlib.pyplot as plt
import json


# section 2: initializing variables
Line_length = 5  # length of transmission line in metres
Cells = 500  # number of cells in transmission line
cpm = Cells/Line_length  # number of cells per metre
R = (30e-6 + 0j)/cpm  # resistance per cell
L = (270e-9 + 0j)/cpm  # inductance per cell
C = (110e-12 + 0j)/cpm
G = (1e109 + 0j)/cpm
R_int = 50 + 0j  # internal resistor
R_load = 100 + 0j  # terminal load
R_zero = (0 + 0j)  # tiny resistance for short circuit
R_inf = (100e24 + 0j)  # massive resistance for open circuit

U_O = 10 + 0j  # voltage source of 110Kv
w = 2*np.pi*100e6 + 0j  # angular frequency w

# Reactances
X_C = -1j/(w*C)  # for capacitor
X_L = (w*L)*1j  # for inductor

# Impedances
Z_GC = 1/(1/X_C + 1/G)  # impedance of resistor G and Capacitor C (parallel addition)
Z_LR = X_L + R  # impedance of inductor L and resistor R  (series addition)

# Current-Divider Ratio
Ratio_Resistor = G/(G + X_C)  # ratio of vertical downward current going through resistor G
Ratio_Capacitor = X_C/(G + X_C)  # ratio vertical downward current going through capacitor C

# Characteristic Impedance and Resistance
Z_charac = np.sqrt((R + w*L*1j)/((1/G) + C*w*1j))
R_charac = np.real(Z_charac)  # ANSWER TO QUESTION c

# Fourier Transform:
N = 1000  # number of points
T = 200e-9  # final time point
t = np.linspace(0.0, T, N)  # array of data points
time_step = t[1] - t[0]  # difference in time between each points
t_o = 50e-9  # starting time
tau = 5e-9  # decay constant
V_t = U_O*np.exp(-(t - t_o)/tau)*np.heaviside(t - t_o, np.real(U_O))
fourier = np.fft.fft(V_t)
f = np.fft.fftfreq(len(t), time_step)  # frequencies for fourier transform
plt.figure()
plt.plot(t*1e9, V_t, label="Signal")
plt.title("Exponential decay signal in time domain")
plt.xlabel("time(ns)")
plt.ylabel('Voltage(V)')
plt.legend()
plt.show()
plt.figure()
plt.plot(f, fourier, color='green', label="Fourier Transform")
plt.xlabel("Frequency (Hz)")
plt.legend()
plt.show()

# Section 3: setting up system of linear equations and solving
circuit_type = ["Terminal Load", "Short Circuit", "Open Circuit"]  # empty array for all three circuit types
circuit_currents = []  # object containing all currents for every circuit type
circuit_node_voltages = []  # object containing all voltages at the nodes for every circuit type
circuit_voltages = []  # object including node voltages plus source voltage
circuit_horizontal_currents = []
for circuit in circuit_type:
    equations = []  # empty array for equations
    answers = np.zeros(Cells, dtype=complex)  # vector of zeroes for answers to equations
    answers[0] = U_O  # initialising first answer with the voltage source
    for mesh in range(Cells):  # dividing each cell as a mesh with clockwise oriented currents
        if mesh == 0:  # the special case of mesh number one with the source voltage
            equation = np.zeros(Cells, dtype=complex)  # initialising equation with zeroes
            pro_current = R_int + Z_LR + Z_GC  # coefficient for the current traversing the mesh
            opposing_current = - Z_GC  # coefficient for the current opposing the mesh
            equation[mesh] = pro_current  # appending coefficient to equation
            equation[mesh + 1] = opposing_current  # appending coefficient to equation
        elif mesh == Cells - 1:  # the special case of the last mesh with terminal load
            equation = np.zeros(Cells, dtype=complex)
            if circuit == "Terminal Load":  # last cell has terminal load resistor
                pro_current = R_load + Z_GC
                opposing_current = -Z_GC
                equation[mesh] = pro_current
                equation[mesh - 1] = opposing_current
            elif circuit == "Open Circuit":  # last cell has near infinite resistance
                pro_current = R_inf + Z_GC
                opposing_current = -Z_GC
                equation[mesh] = pro_current
                equation[mesh - 1] = opposing_current
            elif circuit == "Short Circuit":  # last cell has negligible resistance
                pro_current = R_zero + Z_GC
                opposing_current = -Z_GC
                equation[mesh] = pro_current
                equation[mesh - 1] = opposing_current
        else:  # for all the intermediate cells
            equation = np.zeros(Cells, dtype=complex)
            pro_current = Z_LR + 2*Z_GC
            opposing_current = -Z_GC
            equation[mesh - 1] = opposing_current  # coefficient of current coming from left mesh
            equation[mesh] = pro_current  # coefficient of current coming from centre mesh
            equation[mesh + 1] = opposing_current  # coefficient of current coming from right mesh
        equations.append(equation)
    currents = np.linalg.solve(equations, answers)  # solving the system of equations (Ax = b)
    # adding in the junction law to solve for the vertical currents
    all_currents = []  # array for all currents horizontal and vertical streams
    for n in range(Cells):
        if n != (Cells - 1):  # for all cells except final cell with load resistor
            vertical_current = currents[n] - currents[n + 1]  # current from this mesh
            # downward minus opposing current upward
            current_G = vertical_current * Ratio_Resistor  # calculating current through resistor G
            current_C = vertical_current * Ratio_Capacitor  # calculating current through capacitor C
            all_currents.append(currents[n])  # first appending horizontal current
            all_currents.append(current_G)  # second current for resistor
            all_currents.append(current_C)  # third current for capacitor
        else:  # for final mesh with terminating resistor, open circuit or short
            all_currents.append(currents[n])
    node_voltages = []  # creating array for all node voltages (499 nodes)
    all_voltages = [U_O]  # creating array for all node voltages plus source voltage (500 voltages)
    for n in range(Cells):
        if n == 0:  # for first mesh
            voltage = U_O - currents[n]*(R_int + Z_LR)  # potential difference between source and first node
            node_voltages.append(voltage)
            all_voltages.append(voltage)
        elif n == (Cells - 1):  # last mesh
            pass
        else:
            voltage = node_voltages[n - 1] - currents[n]*Z_LR  # potential difference between nodes
            node_voltages.append(voltage)
            all_voltages.append(voltage)
    circuit_horizontal_currents.append(currents)
    circuit_currents.append(all_currents)
    circuit_node_voltages.append(node_voltages)
    circuit_voltages.append(all_voltages)


# section 5: plotting time :)
plt.figure(1)
plt.title("Plotting voltages for a transmission line over the length of the cable")
plt.plot([n for n in range(Cells)], np.absolute(circuit_voltages[0]),color="limegreen", label="Terminal Load")
plt.plot([n for n in range(Cells)], np.absolute(circuit_voltages[1]), color="violet", label="Short Circuit")
plt.plot([n for n in range(Cells)], np.absolute(circuit_voltages[2]), label="Open Circuit")
plt.xlabel("cm")
plt.ylabel("Voltage(V)")
plt.legend()
plt.show()

plt.figure(2)
plt.title("Plotting currents for a transmission line over the length of the cable")
plt.plot([n for n in range(Cells)], np.absolute(circuit_horizontal_currents[0]), color="limegreen", label="Terminal Load")
plt.plot([n for n in range(Cells)], np.absolute(circuit_horizontal_currents[1]), color="violet", label="Short Circuit")
plt.plot([n for n in range(Cells)], np.absolute(circuit_horizontal_currents[2]), label="Open Circuit")
plt.xlabel("cm")
plt.ylabel("Current(A)")
plt.legend()
plt.show()


# section 6:  voltage differences
middle_voltages = []
end_voltages = []
for k in range(len(circuit_voltages)):
    middle = circuit_voltages[k][Cells//2]
    end = circuit_voltages[k][Cells - 1]
    middle_voltages.append(middle)
    end_voltages.append(end)

# section 7: voltage response and mini sinusoidal oscillations (refer to fourier transform section earlier)
# setting up system of linear equations and solving for our fourier response (to lazy to readjust my code i just copied
# and pasted)
voltage_response = np.zeros(len(f), dtype=complex)  # empty array
end_voltages = []
for i, freq in enumerate(f):  # looping over all frequencies of the exponential decay
    w_fourier = 2*np.pi*freq + 0j
    Z_LR = R + 1j*w_fourier*L
    Z_GC = 1/(1/G + 1j*w_fourier*C)
    equations = []  # empty array for equations
    answers = np.zeros(Cells, dtype=complex)  # vector of zeroes for answers to equations
    answers[0] = U_O  # initialising first answer with the voltage source
    for mesh in range(Cells):  # dividing each cell as a mesh with clockwise oriented currents
        if mesh == 0:  # the special case of mesh number one with the source voltage
            equation = np.zeros(Cells, dtype=complex)  # initialising equation with zeroes
            pro_current = R_int + Z_LR + Z_GC  # coefficient for the current traversing the mesh
            opposing_current = - Z_GC  # coefficient for the current opposing the mesh
            equation[mesh] = pro_current  # appending coefficient to equation
            equation[mesh + 1] = opposing_current  # appending coefficient to equation
        elif mesh == Cells - 1:  # the special case of the last mesh with terminal load
            equation = np.zeros(Cells, dtype=complex)
            pro_current = (Z_charac - np.imag(Z_charac)) + Z_GC
            opposing_current = -Z_GC
            equation[mesh] = pro_current
            equation[mesh - 1] = opposing_current
        else:  # for all the intermediate cells
            equation = np.zeros(Cells, dtype=complex)
            pro_current = Z_LR + 2*Z_GC
            opposing_current = -Z_GC
            equation[mesh - 1] = opposing_current  # coefficient of current coming from left mesh
            equation[mesh] = pro_current  # coefficient of current coming from centre mesh
            equation[mesh + 1] = opposing_current  # coefficient of current coming from right mesh
        equations.append(equation)
    currents = np.linalg.solve(equations, answers)  # solving the system of equations (Ax = b)
    node_voltages = []  # creating array for all node voltages (499 nodes)
    all_voltages = [U_O]  # creating array for all node voltages plus source voltage (500 voltages)
    for n in range(Cells):
        if n == 0:  # for first mesh
            voltage = U_O - currents[n]*(R_int + Z_LR)  # potential difference between source and first node
            node_voltages.append(voltage)
            all_voltages.append(voltage)
        elif n == (Cells - 1):  # last mesh
            pass
        else:
            voltage = node_voltages[n - 1] - currents[n]*Z_LR  # potential difference between nodes
            node_voltages.append(voltage)
            all_voltages.append(voltage)

    end_voltages.append(all_voltages[499])
    response = all_voltages[499]*fourier[i]  # fourier amplitudes multiplied by voltage at end of circuit
    voltage_response[i] = response

# section 8: inverse fourier transforming our response at end of line back into time domain
end_response = np.fft.ifft(voltage_response)

# plotting response :)
plt.figure()
#plt.plot(t*1e9, np.absolute(end_voltages), label='Voltage')
plt.plot(t*1e9, end_response, color="chocolate", label="Signal Response")
plt.xlabel("time(ns)")
plt.ylabel("Voltage(V)")
plt.title("Response to an right-sided exponential decay signal at end of a transmission line")
plt.legend()
plt.show()

# section 9: speed calculation:
response_delay = 27e-9  # I witnessed a 27 nanosecond delay in my response at the end of the cable
speed = Line_length/response_delay  # speed along cable


# section 10 : updating dictionary for question (a)
answers = {}
a = {}
oc = {}
sc = {}
o_100 = {}
oc.update({"middle": np.absolute(middle_voltages[2])})
oc.update({"end": np.absolute(end_voltages[2])})
sc.update({"middle": np.absolute(middle_voltages[1])})
sc.update({"end": np.absolute(end_voltages[1])})
o_100.update({"middle": np.absolute(middle_voltages[0])})
o_100.update({"end": np.absolute(end_voltages[0])})
a.update({"open": oc})
a.update({"short": sc})
a.update({"100ohm": o_100})
answers.update({"a": a})
answers.update({"c": round(R_charac, 2)})
answers.update({"f": speed})

# section 11: writing dictionary to a json file
with open('tut2/CHTTHA009/answers.json', 'w') as f:
    json.dump(answers, f)











