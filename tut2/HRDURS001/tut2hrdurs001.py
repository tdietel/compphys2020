#HRDURS001
#URSULA HARDIE
#TUT 2 CP PHY4000W
#05/03/2020

'''
READ ME:

1.The sympy matrix was used as a sanity check to see if the values were in the correct entries. 
2.The print(M_sym)   command should be active if you want to see the matrix
3.An example of the symbolic matrix has been included in the PDF write up. 
'''


#  #	#	#	#	#	#	#

#	DEFINITIONS AND ARRAYS	#	

#	#	#	#	#	#	#	#	




#IMPORT PACKAGES
import sys
import cmath 
import json
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np 
import scipy as sp
import sympy as sym     #For visualization of the matrix
import numpy.linalg     #np.linalg solves guassian reduction problems 


n=500 #the number of loops of our circuit

np.set_printoptions(threshold=20, edgeitems=10) #print output is longer for sanity check



#DEFINITIONS


#Writing of a Json file
def write_json(my_dic):
  f=open("tut2/HRDURS001/HRDURS001_tut2.json","w") #name of file
  new_json=json.dumps(my_dic)
  f.write(new_json)     #rewritten after each build
  f.close()


#Array creation for Transmission equations
def CIRCUIT(R_I,Zs,Zp,R_L,M, V0, V_line):
    M[0][0]=Zs+R_I
    M[0][1]=Zp
    M[1][0]=1
    M[1][1]=-1
    M[1][2]=-1
    M[2*n][2*n-1]=-Zp
    M[2*n][2*n]=R_L


    for i in range(2,2*n):
           if i%2==0:
               #voltage loop 1
               M[i][i]=1
               M[i][i+1]=-1
               M[i][i+2]=-1
               M[i+1][i-1]=-Zp
               M[i+1][i]=Zs
               M[i+1][i+1]=Zp
    
    V=np.zeros(2*n+1, dtype=complex)
    V[0]=V0
    I=np.linalg.solve(M,V)
    I_abs=np.absolute(I)
    #print(I_abs)

    I_line=[]
    I_other=[]
    for i in range(len(I)):
        if i%2==0:
            I_line.append(I_abs[i])
        else:
            I_other.append(I_abs[i])

    voltage=[]

    for i in range(len(I_line)):
        voltage.append(I_line[i]*(zs))
    V_line.append(np.absolute(voltage))





'''

#SYMBOLIC MATRIX 
M_sym=np.zeros((2*n+1,2*n+1), dtype=object) #Our symbol matrix

Zs=sym.Symbol("Zs") #Series impedence of inductor+resistor
Zp=sym.Symbol("Zp") #combined paralel impedence of capacitor and the shunt conductance (G)
R_I=sym.Symbol("R_I") #Internal Resistance
R_L=sym.Symbol("R_L") #Load Resistance


M_sym[0][0]=Zs+R_I
M_sym[0][1]=Zp
M_sym[1][0]=1
M_sym[1][1]=-1
M_sym[1][2]=-1
M_sym[2*n][2*n-1]=-Zp
M_sym[2*n][2*n]=R_L


for i in range(1,2*n+1):
       if i%2==0 and i<2*n:
           #voltage loop 1
           M_sym[i][i]=Zs
           M_sym[i][i+1]=Zp
           M_sym[i][i-1]=-Zp
           M_sym[i+1][i]=1
           M_sym[i+1][i+1]=-1
           M_sym[i+1][i+2]=-1

#print(M_sym)        
'''


   
#NUMERICALLY EVALUATING CIRCUIT
l=5
cpm=l/n
w=2*np.pi*100e6
V0=100e6
C=110e-14
L=270e-11
Ri= 50
R=20e-8
Rl=100
G=1e7
j=1j

#small indices are for non symbolic values
zs=R+(j*w*L)
zp=1/(1/G+j*w*C)

M=np.zeros((2*n+1,2*n+1), dtype=complex)

volt_term=[]
volt_short=[]
volt_open=[]


#The 3 cases of our circuit

M_term=CIRCUIT(Ri, zs, zp, 100,M,V0, volt_term)
vt=np.reshape(volt_term, (501,))

M_short=CIRCUIT(Ri, zs, zp, 0.0001,M,V0,volt_short)  #Voltage close to 0
vs=np.reshape(volt_short, (501,))

M_open=CIRCUIT(Ri,zs,zp,10**(13),M,V0,volt_open)
vo=np.reshape(volt_open, (501,))


#Writing Json file
answers={}
answers["a"]={}
answers["c"]=49.77000000000243
answers["f"]=0

answers["a"]["open"]={}
answers["a"]["short"]={}
answers["a"]["100ohm"]={}

answers["a"]["open"]["middle"]=vo[251]
answers["a"]["open"]["end"]=vo[500]
answers["a"]["short"]["middle"]=vs[251]
answers["a"]["short"]["end"]=vs[500]
answers["a"]["100ohm"]["middle"]=vt[251]
answers["a"]["100ohm"]["end"]=vt[500]



cells=np.linspace(0,5,n+1)

#Plotting the 3 voltages

plt.plot(cells,np.reshape(volt_short, (501,)), label='Shorted Cable')
plt.plot(cells,np.reshape(volt_open, (501,)), label='Open Cable')
plt.plot(cells,np.reshape(volt_term, (501,)), label='Terminal Resistor')
plt.xlabel('Cable length (m)')
plt.ylabel('Voltage (V)')
plt.legend()

plt.show()



#Finding 'optimal' termination resistance 
volt_find=[]
amp_a=[]
run=np.arange(45,55,0.005)
#print(run)
for i in range(len(run)):
    find_term=CIRCUIT(Ri, zs, zp, run[i],M,V0, volt_find)
    amp=np.max(volt_find)-np.min(volt_find)
    amp_a.append(amp)
z_0=[]
for j in range(len(amp_a)):
	if amp_a[j]==np.min(amp_a):
		z_0.append(run[j])
    


print("Optimal Termination Resistor Value=", np.mean(z_0)) #Our 
print("Theoretical Termination Resistor Value=", np.sqrt(zs/((1/G)+(j*w*C))))

Z_opt=np.mean(z_0)

#Caracteristic impedence (From transmission line theory)

Z0_theory=np.sqrt(zs/((1/G)+(j*w*C)))

Z_opt=49.77000000000243




#Fourier transform



#Final and initial time of signal (in nano seconds)
t_0=50
t_f=200

#Time step for voltage function
time=np.linspace(0,t_f,t_f)
time_step=time[1]

U0=100 #choose initial voltage ofsignal

T=5 #Decay

#The structure of the signal
V=U0*(np.e**(-(time-t_0)/T))*np.heaviside(time-t_0, U0)


#Implementing fast fourier transform
ftrans=np.fft.fft(V)
f=np.fft.fftfreq(len(V),time_step)

plt.plot(time,V)
plt.xlabel('time(ns)')
plt.ylabel("Voltage")
plt.title("time domain")
plt.show()
plt.plot(f,ftrans)
plt.xlabel('frequency')
plt.ylabel('Voltage')
plt.title("frequency domain")

plt.show()







U=ftrans



MF=np.zeros((2*n+1, 2*n+1), dtype=complex)

MF[2*n][2*n-1]=-zp
MF[2*n][2*n]=Z_opt

volt_drop=[]              
for m in range(len(f)):
    wfft=2*np.pi*f[m]
    for i in range(2*n):
            if i%2==0:
                   #voltage loop 1
                MF[i][i]=1
                MF[i][i+1]=-1
                MF[i][i+2]=-1
                MF[i+1][i-1]=-zp
                MF[i+1][i]=R+j*wfft*L
                MF[i+1][i+1]=zp
    #Array being implimented
    Vfft=np.zeros(2*n+1, dtype=complex)
    Vfft[0]=ftrans[m]
    
    
    #solve MI=V using numpy module
    I=np.linalg.solve(MF,Vfft)   #complex currents
    
    Iabs=np.absolute(I)   #magnitude of the complex currents
    
    
    
    Imag=[]    #ab val of current 
    Icom=[]    #complex current 
    for i in range(len(I)):
        if i%2==0:
            Imag.append(Iabs[i])
            Icom.append(I[i])
    
    
           
    volt_drop.append(Icom[-1]*Z_opt)


plt.plot(f,volt_drop)
plt.xlabel('Frequency')
plt.ylabel('Voltage')
plt.show()




write_json(answers)
