# Main run file

import numpy as np
import cmath
from matplotlib import pyplot as plt
import matrix
import json
import partA
import partB

# writes solutions to solutions.json file
def makeJson():
	Dict = {}

	# Results for a
	aResults = partA.getMiddleEndVoltages()
	aDict = {}
	aDict["open"] = {"middle": aResults[0], "end": aResults[1]}
	aDict["short"] = {"middle": aResults[2], "end": aResults[3]}
	aDict["100ohm"] = {"middle": aResults[4], "end": aResults[5]}

	Dict["a"] = aDict
	Dict["c"] = partA.terminationResistor()

	Dict["f"] = partB.producePartBResults()

	filename = "tut2/CLYHAN001/solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

def main():
	# produces output solutions.json file, and produces plots for parts d and e (Fourier transform of signal and measured voltage across the termination resistor at the end of the transmission line)
	makeJson()
	# Produces and saves other relevant plots (parts a-c)
	partA.getVerticalVoltages()
	partA.plotVoltageAmplitudes()

if __name__ == '__main__':
	main()

