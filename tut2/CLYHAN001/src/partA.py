# CP tut 2
# When run, produces required solutions.json file and other relevant figures for report

import numpy as np
import cmath
from matplotlib import pyplot as plt
import matrix
import json

# transmission line parameters
L = 5									# length (m)
cells = 500
conversion = L/cells
C = 110 * ((10)**(-12)) * conversion	# capacitance
L = 270 * ((10)**(-9)) * conversion		# inductance
R = 30 * ((10)**(-6)) * conversion		# resistance
G = 1 * ((10)**(9)) * conversion		# ohmic losses
Ri = 50									# internal resistance
w = 2*np.pi*100*(10**6)					# frequency of input voltage, 100MHz
RL = 100								# termination resister
U0 = 1

# Solves system of equations for given RL
def solveCurrents(RL,w=w):
	coeffMatrix = matrix.populateMatrix(cells, C, L, R, G, Ri, w, RL)
	vec = matrix.populateVector(cells, U0)
	solution = np.linalg.solve(np.asarray(coeffMatrix),vec)

	return solution


# Converts currents solution to voltages
def convertToVoltages(currents):
	#extract relevant currents
	topCurrents = []
	for i in range(cells+1):
		topCurrents.append(currents[3*i])

	voltages = []

	# middle nodes
	for i in range(0,cells):
		voltage_i = (complex(0,1)*w*L+R)*topCurrents[i] #- voltages[i-1]
		voltages.append(voltage_i)

	# take modulus to get real voltages
	for el in range(len(voltages)):
		voltages[el] = abs(voltages[el])

	return voltages


# Plots voltage drop over capacitors along cable
def getVerticalVoltages():
	RL = 100							# termination resistor
	RL_open = 10**12				# open cable
	RL_short = 0						# shorted cable
	RL = [RL, RL_open, RL_short]
	results = []

	for rl in RL:
		coeffMatrix = matrix.populateMatrix(cells, C, L, R, G, Ri, w, rl)
		vec = matrix.populateVector(cells, U0)
		currents = np.linalg.solve(np.asarray(coeffMatrix),vec)

		# extract relevant currents
		capCurrents = []
		for i in range(cells):
			capCurrents.append(currents[3*i+2])

		capVoltages = []

		for current in range(cells):
			capVoltages.append(np.absolute(capCurrents[current]*(1/(complex(0,1)*w*C))))

		results.append(capVoltages)

	plt.plot(results[0], label=r'Termination resistor $R_L = 100 \Omega$')
	plt.plot(results[1], label=r'Open cable')
	plt.plot(results[2], label=r'Shorted cable')
	plt.xlabel('Length (cm)')
	plt.ylabel('Voltage (V)')
	plt.title('Voltage drop across the capacitor branches along the cable for \n termination resistor, open-ended and shorted cables')
	plt.legend()
	plt.savefig('tut2/CLYHAN001/capVoltages')
	plt.clf()
	

### Part (a)
# Gets middle (over capacitor) and end (over RL) vertical voltages
def getMiddleEndVoltages():
	RL = 100							# termination resistor
	RL_open = 10**12					# open cable
	RL_short = 0						# shorted cable
	RL = [RL_open, RL_short, RL]
	results = []

	for rl in RL:
		coeffMatrix = matrix.populateMatrix(cells, C, L, R, G, Ri, w, rl)
		vec = matrix.populateVector(cells, U0)
		currents = np.linalg.solve(np.asarray(coeffMatrix),vec)

		# compute end voltage and middle voltage over capacitor and G resistor branches - same result for both
		middleVoltageG = currents[int(3*cells/2 + 1)]*G
		middleVoltageC = currents[int(3*cells/2 + 2)]*(1/(complex(0,1)*w*C))
		endVoltage = currents[-1]*rl

		results.append(np.absolute(middleVoltageC))
		results.append(np.absolute(endVoltage))

	return results

### Part (b)
# Plots the amplitude of the voltage along the cable in the three cases #
def plotVoltageAmplitudes():
	RL = 100							# termination resistor
	RL_open = 10**12				# open cable
	RL_short = 0						# shorted cable
	RL = [RL, RL_open, RL_short]
	voltages = []

	for i in range(len(RL)):
		coeffMatrix = matrix.populateMatrix(cells, C, L, R, G, Ri, w, RL[i])
		vec = matrix.populateVector(cells, U0)
		solution = np.linalg.solve(np.asarray(coeffMatrix),vec)

		voltages.append(convertToVoltages(solution))

	plt.plot(voltages[0], label=r'Termination resistor $R_L = 100 \Omega$')
	plt.plot(voltages[1], label=r'Open cable')
	plt.plot(voltages[2], label=r'Shorted cable')
	plt.xlabel('Length (cm)')
	plt.ylabel('Voltage (V)')
	plt.title('Amplitude of voltage along the cable for termination resistor, \n open-ended and shorted cables')
	plt.legend()
	plt.savefig('tut2/CLYHAN001/VoltageAmplitudes')
	plt.clf()

### Part (c) 
# Finds value of RL for which the difference between the minimum and maximum amplitude of the voltage along the transmission line is smallest
def terminationResistor():
	RL = np.linspace(30,70,50)
	amplitudes = []

	for i in range(len(RL)):
		coeffMatrix = matrix.populateMatrix(cells, C, L, R, G, Ri, w, RL[i])
		vec = matrix.populateVector(cells, U0)
		solution = np.linalg.solve(np.asarray(coeffMatrix),vec)
		voltages = convertToVoltages(solution)

		amplitudes.append(max(voltages)-min(voltages))
	
	# RL for which difference is minimum
	minRL = RL[np.where(amplitudes == np.amin(amplitudes))[0][0]]
	print(minRL)

	plt.plot(RL, amplitudes, 'bx')
	plt.title("Difference between the minimum and maximum amplitude of the \n voltage along the transmission line")
	plt.xlabel(r'$R_L$ ($\Omega$)')
	plt.ylabel("Amplitude difference (V)")
	plt.savefig("tut2/CLYHAN001/optimalRL")
	plt.clf()

	return minRL


