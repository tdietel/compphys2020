# Exponential decay input voltage signal - parts d-f
import numpy as np
from matplotlib import pyplot as plt
from scipy import fftpack
import partA

def signal(t,t0=5*(10**(-9))):
	tau = 5*(10**(-9))
	V0 = 1
	return V0*np.exp(-(t-t0)/tau)*np.heaviside(t-t0,1)

# Plots exponential signal
def plotSignal(t):
	tau = 5*(10**(-9))
	plt.plot(t,signal(t, tau))
	plt.xlabel("Time (s)")
	plt.ylabel("Voltage (V)")
	plt.title("Exponential decay signal plotted in time domain")
	plt.savefig("tut2/CLYHAN001/signal")
	plt.clf()
	# plt.show()

# Performs Fourier transform of exponential decay signal
def fastFourier(t):
	tau = 5*(10**(-9))
	N = 200							# sample points
	dt = tau/20.0					# sample spacing
	t = np.linspace(0,N*dt,N)
	sig = signal(t, tau)

	sigFourier = np.fft.fft(sig)
	tFourier = np.fft.fftfreq(len(t),dt)

	# print(abs(sigFourier))
	# print(tFourier)

	return[sigFourier, tFourier]
	

# Plots fourier transform of signal 
def plotFourier(t):
	# Perform Fourier transform
	fourierResults = fastFourier(t)
	sigFourier = fourierResults[0]
	freqFourier = fourierResults[1]

	# Plot transform over frequency domain
	fig, ax = plt.subplots()
	ax.stem(freqFourier*2*np.pi, np.abs(sigFourier), basefmt=' ', use_line_collection=True)
	plt.xlim(0,max(freqFourier)*2*np.pi/4)
	plt.xlabel(r'Angular frequency $\omega$ (rad/s)')
	plt.ylabel("Fourier amplitude (V)")
	plt.title("Fourier transform of exponential decay signal")
	plt.savefig("tut2/CLYHAN001/fourierTransform")
	plt.clf()
	# plt.show()

# Solves for currents for exponential decay signal
def solveExponentialSignal(t):
	fourierResults = fastFourier(t)
	N = 200														# number of sample points for fft

	# Restrict number of Fourier frequencies considered
	fourierAmplitudes = fourierResults[0][1:]					# weights from Fourier transform
	freqFourier = fourierResults[1][1:]							# frequencies from Fourier transform

	RLoptimal = 49.59183673469388								# optimal value for RL found in part (c)

	weightedVoltages = np.zeros(len(freqFourier), dtype=np.complex_)

	for freq in range(len(freqFourier)):
		w = freqFourier[freq]*2*np.pi 							# convert to angular frequency
		currents = partA.solveCurrents(RLoptimal, w)			# solve for currents in circuit

		# get voltage drop over RL for this frequency
		endVoltage = currents[-1]*RLoptimal 				
		weightedVoltage = endVoltage*fourierAmplitudes[freq]	# weight result by fourier amplitude
		weightedVoltages[freq] = weightedVoltage

	# Perform inverse fourier transform to reconstruct voltage signal response in time domain
	signalResponse = np.fft.ifft(weightedVoltages)

	plt.plot(t[1:], np.absolute(signalResponse),'bx')
	plt.xlabel("Time (s)")
	plt.ylabel("Voltage across termination resistor (V)")
	plt.title("Voltage measured across termination resistor for exponential decay signal")
	plt.savefig("tut2/CLYHAN001/terminationResistorVoltage_parte")
	plt.clf()
	# plt.show()

	return [t[1:], np.absolute(signalResponse)]

# part f
def getSignalSpeed(signalResponseData):
	tau = 5*(10**(-9))
	times = signalResponseData[0]
	voltages = signalResponseData[1]

	timeDifference = times[np.where(voltages == np.amin(voltages))[0][0]]-tau
	print(timeDifference)
	return timeDifference


def producePartBResults():
	tau = 5*(10**(-9))
	N = 200							# sample points
	dt = tau/20.0					# sample spacing
	t = np.linspace(0,N*dt,N)

	plotSignal(t)
	plotFourier(t)
	signalResponseResults = solveExponentialSignal(t)
	timeDifference = getSignalSpeed(signalResponseResults)
	return timeDifference
