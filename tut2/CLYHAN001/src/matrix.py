# Code to populate coefficient matrix A and vector b for equation Ax=b, where x is the vector of currents

import numpy as np

def newRow(numCols):
	matrixRow = []
	for col in range(numCols):
		matrixRow.append(0)

	return matrixRow

def populateMatrix(cells, C, L, R, G, Ri, w, RL):
	N = cells
	numCols = 3*N + 1
	numRows = 3*N + 1

	matrix = []
	for i in range(numRows):
		matrix.append([])

	# matrix entries
	a = Ri + complex(0,1)*w*L + R
	b = G
	c = 1/(complex(0,1)*w*C)
	d = -complex(0,1)*w*L - R


	# row 0 (leftmost loop)
	matrixRow = newRow(numCols)
	matrixRow[0] = a
	matrixRow[1] = b

	matrix[0] = matrixRow


	# row 1 to 2N-1 (middle loops)
	# odd rows
	row = 1
	col = 1
	while row <= 2*N-1:
		matrixRow = newRow(numCols)
		matrixRow[col] = b
		matrixRow[col+1] = -c

		matrix[row] = matrixRow
		row += 2
		col += 3

	# even rows
	row = 2
	col = 2
	while row <= 2*N-1:
		matrixRow = newRow(numCols)
		matrixRow[col] = c
		matrixRow[col+1] = d
		matrixRow[col+2] = -b

		matrix[row] = matrixRow
		row += 2
		col += 3


	# row 2N (rightmost loop)
	row = 2*N
	col = 3*N - 1
	matrixRow = newRow(numCols)
	matrixRow[col] = c
	matrixRow[col+1] = -RL

	matrix[row] = matrixRow


	# row 2N+1 to 3N (current node equations)
	row = 2*N + 1
	col = 0
	while row <= 3*N:
		matrixRow = newRow(numCols)
		matrixRow[col] = 1
		matrixRow[col+1] = -1
		matrixRow[col+2] = -1
		matrixRow[col+3] = -1

		matrix[row] = matrixRow
		row += 1
		col += 3


	## matrix is complete ##
	return matrix

def populateVector(cells, U0):
	N = cells
	numRows = 3*N + 1

	vec = np.zeros(numRows)

	# for sinusoidal input voltage - vector is just populated with voltage amplitude... is that correct?
	vec[0] = -U0

	return vec
