#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 29 23:29:26 2020

@author: rayo
"""

import numpy as np
import matplotlib.pyplot as plt
import timeit
from scipy import fftpack
from scipy.signal import find_peaks
import json
#######################
#Tutorial 5
#######################
#Question1
#######################
#1a)
#######################
r = 1 #radius of sphere

Vsphere = (4/3)*np.pi*r**3 #volume of box
Vbox = (2*r)**3 #volume of box

Phit = Vsphere/Vbox #probability of a hit

runs = range(1,300+1) #Number of points #should be 300
reps = 3 #Number of repitions

Area = [[] for _ in range(reps)]

for k in range(reps):
    Area[k]
    for j in range(len(runs)):
        
        Nhits = 0 #Number of points inside sphere
        for i in range(runs[j]):
            
            x = np.random.uniform(low =-1, high = 1) #
            y = np.random.uniform(low =-1, high = 1) ## Randomly draw from uniform dist frm [-1,1]
            z = np.random.uniform(low =-1, high = 1) #
            
            d = np.sqrt(x**2 + y**2 + z**2) #calculate distance from centre of sphere
            
            if d <= r: #check if the point lies in sphere
                Nhits += 1 # if so add 1 to the Nhits
        Vsphere = (Nhits/runs[j])*Vbox
        Area[k].append(Vsphere)



colors = ['r','g','grey']
y = [Vsphere for i in range(len(runs))] #just a horizontal line of Area of Sphere

plt.figure(0)
for i in range(reps):
    plt.plot(runs,Area[i],label='Estimated volume of sphere',alpha = 0.7,  color = colors[i])
plt.plot(runs,y,label='Volume',color = 'black')
plt.legend()
plt.xlabel('Points Used(N)')
plt.ylabel('Volume(units^3)')
plt.savefig('tut5/PRNMOG001/VolumeConvergence.png',dpi = 300)


#########################
#1b)
#########################
samples = 10000 #samples ##################leave at 10000
Nhits = 0 #number of points inside sphere

for i in range(samples):

            
            x = np.random.uniform(low =-1, high = 1) #
            y = np.random.uniform(low =-1, high = 1) ## Randomly draw from uniform dist frm [-1,1]
            z = np.random.uniform(low =-1, high = 1) #
            
            d = np.sqrt(x**2 + y**2 + z**2) #calculate distance from centre of sphere
            
            if d <= r: #check if the point lies in sphere
                Nhits += 1 # if so add 1 to the Nhits
                

Vol = (Nhits/samples)*Vbox #volume of sphere using monte carlo
Unc = Vol/np.sqrt(Nhits) #the uncertainty of the value caluclated equal to standard deviation
print(Vol) 
print(Unc)
#The value calcuated agrees with the literature value of 4.188790205.  

########################
#1c) Hypersphere stuff *sigh*
########################
def column_sums(square):
    return [sum(i) for i in zip(*square)]

def nsphere(d,r):
    #d = 9 #dimensions of sphere
    N = 10000 #Number of points used, #############leave at 10000
    #r = 1 #just for specifying 
    Nhits = 0 #Number of points inside sphere 
    x_2 = np.square(np.random.uniform(low = -1, high = 1, size = (d,N))) #coordinate entries
    R = column_sums(x_2) #just sums coordinates^2 entries



    for i in range(N):
        if R[i] < r: #just checks if R lies within the sphere
            Nhits += 1 

    V2D = ((2**d)*Nhits)/N #calculates volume of hypersphere
    #print(Nhits)
    unc = V2D/np.sqrt(Nhits) #uncertainty
    #print('The volume of a',d,'dim sphere is ',V2D)
    #print('The uncertainty is',unc)
    val = [V2D,unc] #for output
    return val

d = range(3,11) #dimensions of hypersphere
r = 1 #radius of hypersphere
number = 20 #number of times timeit will check calculation speed
time = [] #just storing the times
uncertainties = [] #storing the uncertainties
Volumes = [] #storing the volumes

for i in d: #loop through d
    val = nsphere(i,r) #calculate the volume and uncertainty
    Volumes.append(val[0]) #store the volume
    uncertainties.append(val[1]) #store uncertainty
    time.append(timeit.timeit(globals = globals(), stmt = 'nsphere(i,r)',number = number)/number) #check and store calculation time

plt.figure(2)
plt.scatter(d,time,label = 'Execution Time')
plt.ylabel('Time(s)')
plt.xlabel('Dimensions(n)')    
plt.savefig('tut5/PRNMOG001/DvT.png',dpi=300)

plt.figure(1)
plt.scatter(d,Volumes,label = 'Volumes')
plt.xlabel('Dimensions(n)')
plt.ylabel('Volume(units^3)')
plt.savefig('tut5/PRNMOG001/DvV.png',dpi = 300)

plt.figure(9)
plt.scatter(d,uncertainties,label = 'Uncertainties')
plt.xlabel('Dimensions(n)')
plt.ylabel('Uncertainty')
plt.savefig('tut5/PRNMOG001/DvU.png',dpi = 300)
    
#############################
#Question 2
#############################
#2a well the motions thing
#############################
def alpha1(eta1,eta2,w1,w2,m1,m2,L1,L2,g):
    cos12 = np.cos(eta1 - eta2)
    sin12 = np.sin(eta1 - eta2)
    sin1 = np.sin(eta1)
    sin2 = np.sin(eta2)
    xi = cos12**2*m2 - m1 - m2
    w1dot = ( L1*m2*cos12*sin12*w1**2 + L2*m2*sin12*w2**2- m2*g*cos12*sin2      + (m1 + m2)*g*sin1)/(L1*xi)
    return w1dot


def alpha2(eta1,eta2,w1,w2,m1,m2,L1,L2,g):
    cos12 = np.cos(eta1 - eta2)
    sin12 = np.sin(eta1 - eta2)
    sin1 = np.sin(eta1)
    sin2 = np.sin(eta2)
    xi = cos12**2*m2 - m1 - m2
    w2dot = -( L2*m2*cos12*sin12*w2**2 + L1*(m1 + m2)*sin12*w1**2+ (m1 + m2)*g*sin1*cos12  - (m1 + m2)*g*sin2 )/(L2*xi)
    return w2dot

def F11(w1):
    return w1

def F21(w2):
    return w2

L1 = 1 #2m length wire
L2 = 2 #1m length wire
g = 9.81 # acc in m/s^2
#eta1 = 0 # about 10 degrees 
#eta2 = 0.5 # about 6 degrees
m1 = 3 #masses 2 kg
m2 = 1 #masses 1 kg
#w1 = 0 #initial angular veolcity
#w2 = 0 #initial angular velocity
ti = 0 #initial time 
tf = 100 #final time 
h = 0.01 #time step
tlist = np.arange(ti,tf+h,h)
#w1list = []
#w2list = []
#eta1list = []
#eta2list = []
#w1list.append(w1)
#w2list.append(w2)
#eta1list.append(eta1)
#eta2list.append(eta2)

def double_pendulum(eta1,eta2,w1,w2):
    w1list = []
    w2list = []
    eta1list = []
    eta2list = []
    w1list.append(w1)
    w2list.append(w2)
    eta1list.append(eta1)
    eta2list.append(eta2)
    for i in range(len(tlist)-1):
        ##intials, will be 4, basically euler 
        k1e1 = F11(w1)
        k1o1 = alpha1(eta1,eta2,w1,w2,m1,m2,L1,L2,g)
        k1e2 = F21(w2)
        k1o2 = alpha2(eta1,eta2,w1,w2,m1,m2,L1,L2,g)
        #second step
        k2e1 = F11(w1+h*k1o1/2)
        k2o1 = alpha1(eta1+h*k1e1/2,eta2 + h*k1e2/2,w1+h*k1o1/2,w2 + h*k1o2/2,m1,m2,L1,L2,g)
        k2e2 = F21(w2+h*k1o2/2)
        k2o2 = alpha2(eta1+h*k1e1/2,eta2 + h*k1e2/2,w1+h*k1o1/2,w2 + h*k1o2/2,m1,m2,L1,L2,g)
        # third step
        k3e1 = F11(w1+h*k2o1/2)
        k3o1 = alpha1(eta1+h*k2e1/2,eta2 + h*k2e2/2,w1+h*k2o1/2,w2 + h*k2o2/2,m1,m2,L1,L2,g)
        k3e2 = F21(w2+h*k2o2/2)
        k3o2 = alpha2(eta1+h*k2e1/2,eta2 + h*k2e2/2,w1+h*k2o1/2,w2 + h*k2o2/2,m1,m2,L1,L2,g)
        # final step
        k4e1 = F11(w1+h*k3o1)
        k4o1 = alpha1(eta1+h*k3e1,eta2 + h*k3e2,w1+h*k3o1,w2 + h*k3o2,m1,m2,L1,L2,g)
        k4e2 = F21(w2+h*k3o2)
        k4o2 = alpha2(eta1+h*k3e1,eta2 + h*k3e2,w1+h*k3o1,w2 + h*k3o2,m1,m2,L1,L2,g)
        #caluclating updates
        eta1 += (h/6)*(k1e1 + 2*k2e1 + 2*k3e1 + k4e1)
        w1 += (h/6)*(k1o1 + 2*k2o1 + 2*k3o1 + k4o1)
        eta2 += (h/6)*(k1e2 + 2*k2e2 + 2*k3e2 + k4e2)
        w2 += (h/6)*(k1o2 + 2*k2o2 + 2*k3o2 + k4o2)
        #save updates
        w1list.append(w1)
        w2list.append(w2)
        eta1list.append(eta1)
        eta2list.append(eta2)
    return [eta1list,eta2list,w1list,w2list]
    
NC = double_pendulum(0,0.5,0,0)

plt.figure(3)
plt.plot(tlist,NC[1],label = r'$\theta$ vs t',color = 'red')
plt.xlabel('t(s)')
plt.ylabel(r'$\theta$')    
plt.legend()
plt.savefig('tut5/PRNMOG001/NChaoticEta.png',dpi = 300)

plt.figure(4)
plt.plot(tlist,NC[3],label = r'$\omega$ vs t',color = 'blue')
plt.ylabel(r'$\omega$')
plt.xlabel('t(s)')
plt.legend()
plt.savefig('tut5/PRNMOG001/NChaoticW.png',dpi = 300)

C = double_pendulum(np.pi/2,np.pi/2,0,0)

plt.figure(5)
plt.plot(tlist,C[1],label = r'$\theta$ vs t',color = 'orange')
plt.xlabel('t(s)')
plt.ylabel(r'$\theta$')    
plt.legend()
plt.savefig('tut5/PRNMOG001/ChaoticEta.png',dpi = 300)

plt.figure(6)
plt.plot(tlist,C[3],label = r'$\omega$ vs t',color = 'purple')
plt.ylabel(r'$\omega$')
plt.xlabel('t(s)')
plt.legend()
plt.savefig('tut5/PRNMOG001/ChaoticW.png',dpi = 300)

#############################
#2b fourier transform of x horizontal motion #more sighs 
#############################
#we want the horizontal component, so if we look a the diagram, taking the sine of
#theta2 will give us just the horizontal motion
#def Fourier(A)

def Fourier(A,h,height,d): #matrix of angles, time step, dsitance between peaks(index), r plot out untill
    NCeta = np.sin(A[1]) #sin of mass 2 angles
    
    
    sig_fft = fftpack.fft(NCeta) #raw fft
    fft_theo = 2.0*np.abs(sig_fft/(len(A[1]))) #theoretical fft(crrect scale)
    
    sample_freq = fftpack.fftfreq(len(A[1]),h) #frequencies
    mask = sample_freq > 0 #ignore mirror about y axis
    
    sample_freq = sample_freq[mask] #
    fft_theo = fft_theo[mask]       # applying mask
    
    indices,_ = find_peaks(fft_theo,distance = d,height = height)
    #plt.plot(sample_freq,fft_theo,label = 'FFT')
    #plt.scatter(sample_freq[indices],fft_theo[indices],color = 'r',label = 'Dominant Frequencies')
    #plt.yscale('log')
    #plt.legend()
    #plt.xlim([0.0,rr])
    #plt.xlabel(r'$\omega(s^{-1})$')
    #plt.ylabel(r'$\theta$')
    df = sample_freq[indices]
    return [df,indices,sample_freq,fft_theo]

NCf =  Fourier(NC,h,0.003,1) #returns non chaotic frequencies
        
plt.figure(7)
plt.plot(NCf[2],NCf[3],label = 'FFT')
plt.scatter(NCf[2][NCf[1]],NCf[3][NCf[1]],color = 'r',label = 'Dominant Frequencies')
plt.legend()
plt.xlim([0.0,1])
plt.xlabel(r'$f\,(s^{-1})$')
plt.ylabel(r'$\theta$')
plt.savefig('tut5/PRNMOG001/NCfourier.png',dpi = 300)

Cf = Fourier(C,h,0.039,1) #chaotic frequencies

plt.figure(8)
plt.plot(Cf[2],Cf[3],label = 'FFT')
plt.scatter(Cf[2][Cf[1]],Cf[3][Cf[1]],color = 'r',label = 'Dominant Frequencies')
plt.legend()
plt.xlim([0.0,3])
plt.xlabel(r'$f\,(s^{-1})$')
plt.ylabel(r'$\theta$')
plt.savefig('tut5/PRNMOG001/Cfourier.png',dpi = 300)

###################################
#Dictionary time
###################################
volumes = {'3d':Volumes[0],'4d':Volumes[1],'5d':Volumes[2],'6d':Volumes[3],'7d':Volumes[4],'8d':Volumes[5],'9d':Volumes[6],'10d':Volumes[7]}
uncert = {'3d':uncertainties[0],'4d':uncertainties[1],'5d':uncertainties[2],'6d':uncertainties[3],'7d':uncertainties[4],'8d':uncertainties[5],'9d':uncertainties[6],'10d':uncertainties[7]}
Dic = {'1':{'volume':volumes,'uncertainty':uncert}}

def WJSON(path, fileName, data):
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)

WJSON('/tut5/PRNMOG001/','results',Dic)
    
