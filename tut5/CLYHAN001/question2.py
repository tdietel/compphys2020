import numpy as np
from matplotlib import pyplot as plt
from scipy import fftpack

savepath = 'tut5/CLYHAN001/'

# Fourth order Runge Kutta 
def rk4(func,t,y0):
    y = np.zeros((len(t),len(y0)))
    y[0] = y0

    for i in range(len(t)-1):
    
        dt = t[i+1] - t[i]
        
        k1 = dt * func( t[i]          , y[i]          )
        k2 = dt * func( t[i] + 0.5*dt , y[i] + 0.5*k1 )
        k3 = dt * func( t[i] + 0.5*dt , y[i] + 0.5*k2 )
        k4 = dt * func( t[i] + 1.0*dt , y[i] + 1.0*k3 )

        y[i+1] = y[i] + 1./6. * ( k1 + 2*k2 + 2*k3 + k4)

    return y

# generalized velocity class - followed format of DrivenOscillator notebook
# has the form [omega1, omega2, omega1dash, omega2dash]
# as defined on the tutorial sheet, omega is theta dash (the time derivative of theta)
class genvelocity:
    def __init__(self,m1,m2,L1,L2):
        self.m1 = m1
        self.m2 = m2
        self.L1 = L1
        self.L2 = L2

    # y is the dynamical variable vector and has the form [theta1, theta2, omega1, omega2]
    def __call__(self,t,y):			# returns the generalized velocity vector 
        omega1 = y[2]
        omega2 = y[3]

        # specified in tutorial sheet
        g = 9.81
        omega1dash = (-g*(2*self.m1+self.m2)*np.sin(y[0])-self.m2*g*np.sin(y[0]-2*y[1])-2*np.sin(y[0]-y[1])*self.m2*(self.L2*omega2**2 + self.L1*(omega1**2)*np.cos(y[0]-y[1])))/(self.L1*(2*self.m1 + self.m2 - self.m2*np.cos(2*y[0]-2*y[1])))
        omega2dash = (2*np.sin(y[0]-y[1])*(self.L1*(self.m1+self.m2)*omega1**2+g*(self.m1+self.m2)*np.cos(y[0])+self.L2*self.m2*(omega2**2)*np.cos(y[0]-y[1])))/(self.L2*(2*self.m1 + self.m2 - self.m2*np.cos(2*y[0]-2*y[1])))

        return np.array([omega1, omega2, omega1dash, omega2dash])

# find trajectory of masses given intial conditions y0
def get_path(y0, m1, m2, L1, L2):
	total_time = 20
	dt = 1e-3

	g = genvelocity(m1, m2, L1, L2)
	t = np.arange(0,total_time,dt)
	y = rk4(g, t, y0)

	return [t, y]

# Find and plot Fourier transform of the horizontal coordinate x2 of the second mass m2
def plot_fourier_transform(t, x2, filename):
	dt = t[1]-t[0]

	sigFourier = np.fft.fft(x2)
	tFourier = np.fft.fftfreq(len(t),dt)			

	# Plot transform over frequency domain
	fig, ax = plt.subplots()
	ax.stem(tFourier*2*np.pi, np.abs(sigFourier), basefmt=' ', use_line_collection=True)
	plt.xlim(0,20)
	plt.xlabel(r'Angular frequency $\omega$ (rad/s)')
	plt.ylabel("Fourier amplitude")
	plt.title(r'Fourier transform of horizontal coordinate of mass $m_2$')
	plt.savefig(savepath+'fourierTransform_'+filename)
	# plt.show()
	plt.clf()


def plot_paths(y0, m1, m2, L1, L2, filename):
	path = get_path(y0, m1, m2, L1, L2)
	y = path[1]
	t = path[0]

	# cartesian coordinates of mass m1
	x1 = L1*np.sin(y[:,0])
	y1 = -L1*np.cos(y[:,0])

	# cartesian coordinates of mass m2
	x2 = L2*np.sin(y[:,1]) + x1
	y2 = -L2*np.cos(y[:,1]) + y1

	# plot position of mass m2 - y2 vs x2
	plt.plot(x2, y2)
	plt.xlabel("x (m)")
	plt.ylabel("y (m)")
	plt.title(r'Trajectory for mass $m_2$')

	# plt.show()
	plt.savefig(savepath+'m2Position_'+filename)
	plt.clf()

	# plot phase space of mass m2 - omega2 versus theta2
	plt.plot(y[:,1],y[:,3])
	plt.xlabel(r'$\theta_2$')
	plt.ylabel(r'$\omega_2$')
	plt.title(r'Phase space for mass $m_2$')

	# plt.show()
	plt.savefig(savepath+'phaseSpace_'+filename)
	plt.clf()

	# fourier transform plot
	plot_fourier_transform(t, x2, filename)

# Called by runFile
def make_plots():
	plot_paths([1,1,0,0],1,1,1,1, filename='stable')
	plot_paths([4,-2,-3.5,2],1,1,1,1, filename='chaotic')
	plot_paths([1,1,0,0],1,1,0.25,0.25, filename='shorterL')


