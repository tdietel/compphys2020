import numpy as np 
from matplotlib import pyplot as plt
import json
import question1
import question2

# writes solutions to solutions.json file
def makeJson():
	Dict = {}

	# Results for Q1
	dict1 = {}
	# volume estimates
	volResults = question1.hypersphere_results()
	volDict = dict(zip(volResults[0], volResults[1]))
	# uncertainty estimates
	uncertaintyDict = dict(zip(volResults[0], volResults[2]))

	dict1["volume"] = volDict
	dict1["uncertainty"] = uncertaintyDict

	Dict["1"] = dict1

	filename = "tut5/CLYHAN001/solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

def main():
	makeJson()
	question1.make_plots()
	question2.make_plots()
	question1.hypersphere_results(100000)

if __name__=='__main__':
	main()
