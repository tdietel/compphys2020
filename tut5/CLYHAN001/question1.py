# Estimates the volume of a d-dimensional sphere using Monte Carlo integration methods

import numpy as np
from matplotlib import pyplot as plt
import random
from math import gamma
import time

savepath = 'tut5/CLYHAN001/'

# Estimate volume of hypersphere in d dimensions using hit-and-miss monte carlo integration
def find_hypersphere_vol(d, N, r=1):
	random.seed(1)
	known_hypersphere_vol =	(np.pi**(d/2))*(r**d)/gamma(1 + d/2)		# literature value for volume in d dimensions				
	hypercube_vol = (2*r)**d 											# volume of d-dimensional hypercube

	count = 0								# number of points inside sphere

	# loop over sample points
	for i in range(N):
		position = 0
		for j in range(d):
			position += (random.uniform(-r,r))**2
		position = np.sqrt(position)

		if position<r:				# check whether generated point lies inside sphere or not
			count += 1

	fraction_of_points = count/N
	hypersphere_vol_estimate = fraction_of_points * hypercube_vol
	uncertainty = np.sqrt(hypercube_vol*hypersphere_vol_estimate/N)

	return [hypersphere_vol_estimate, uncertainty]

# Plot of estimated value of the integral in d dimensions as a function of samples drawn
def convergence_plot(d, r=1):
	known_sphere_vol = (np.pi**(d/2))*(r**d)/gamma(1 + d/2)
	Nvals = np.arange(20, 5000, 20)
	estimates = []

	for N in Nvals:
		estimates.append(find_hypersphere_vol(d, N)[0])

	plt.plot(Nvals, estimates, label="Estimate")
	plt.hlines(known_sphere_vol, min(Nvals), max(Nvals), label="Theoretical value")
	plt.hlines(0.95*known_sphere_vol, min(Nvals), max(Nvals), linestyle='--', label="5%")
	plt.hlines(1.05*known_sphere_vol, min(Nvals), max(Nvals), linestyle='--')
	plt.xlabel("Number of samples N")
	plt.ylabel("Volume of sphere")
	plt.title("Volume estimates of a "+str(d)+"-dimensional sphere with radius r = 1 using \n Monte-Carlo integration")
	plt.legend()
	# plt.show()
	plt.savefig(savepath+'convergence_'+str(d)+'d')
	plt.clf()

# Plot computation time for integration as a function of hypersphere dimension
def computation_time_plot(dim, time):
	plt.plot(dim, time)
	plt.xlabel('Dimension d')
	plt.ylabel('Time (s)')
	plt.title(r'Computation time for $N=10^5$ samples')
	# plt.show()
	plt.savefig(savepath+'time_dim')
	plt.clf()

# Plot estimated volumes of hypersphere for 3<=d<=10, with uncertainties and literature values for comparison
def volume_plot(d, volume, uncertainty, expected):
	plt.errorbar(d, volume, uncertainty, label='Estimate', capsize=3)
	plt.scatter(d, expected, label='Literature value', color='r')
	plt.legend()
	plt.xlabel('Dimension d')
	plt.ylabel(r'Volume estimate ($m^3$)')
	plt.title(r'Volume estimate for $N=10^5$ samples')
	# plt.show()
	plt.savefig(savepath+'vol_dim')
	plt.clf()

# Compute estimates for hypersphere volumes, print out table and produce relevant plots
def hypersphere_results(N=10000):
	print("N="+str(N))
	results = []
	uncertainties = []
	dimensions = []
	computation_time = []
	lit_val = []

	r=1
	for d in range(3,11,1):
		t0 = time.perf_counter()
		estimates = find_hypersphere_vol(d, N)
		estimate = estimates[0]
		uncertainty = estimates[1]
		time_taken = time.perf_counter()-t0
		actual = (np.pi**(d/2))*(r**d)/gamma(1 + d/2)
		difference = abs(actual - estimate)

		# prints out table for report
		print(d, ' & ', round(estimate,3), ' $\pm$ ', round(uncertainty,3), ' & ', round(actual,6), ' & ', round(difference,4), ' & ', round(time_taken,4), ' \\\\')
		print('\\hline')

		dimensions.append(str(d)+"d")
		results.append(estimate)
		uncertainties.append(uncertainty)
		computation_time.append(time_taken)
		lit_val.append(actual)


	### Some relevant plots ###
	computation_time_plot(np.arange(3,11,1), computation_time)
	volume_plot(np.arange(3,11,1), results, uncertainties, lit_val)

	return [dimensions, results, uncertainties]

# Called by runFile
def make_plots():
	# plots of integration convergence
	convergence_plot(3)
	convergence_plot(5)
	convergence_plot(10)

