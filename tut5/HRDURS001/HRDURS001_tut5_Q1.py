from __future__ import division
import json
import numpy as np
import matplotlib.pyplot as plt
import math
import random
from scipy.special import gamma 
from scipy.optimize import curve_fit
import timeit
from timeit import default_timer as timer

font = {'family': 'serif',
		'color':  'navy',
		'weight': 'normal',
		'size': 16,
		}


#DEFINITIONS
def V_ncube(n, r): #formula for cube in n-dimentions
	return (2*r)**n

def V_nsphere(n, r): #formula for sphere in n-dimentions
	return ((np.pi**(np.true_divide(n,2))/(gamma(1+np.true_divide(n,2))))*r**n)

def point_matrix(r, n, num_points): #generates n-dim array of random points between -r and r
	return np.random.uniform(-r,r,(num_points, n))

def normal(x,mu,sigma,a):  #gaussian fit
	return a*(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-0.5*((x-mu)/sigma)**2)

#initial parameters
r=1  #radius of sphere
num_points=10000
area=0  #This is the inital value of points in the sphere. The  code adds +1 as the generation progresses

###################
# For user input  #
###################
n=3 #this is the dimention of the system 



#######################
#### QUESTION A #######
#######################
#Finding the convergence for 3dim sphere

A=[]
#sample_number=np.full(99, 1000)
sample_number=np.arange(10, 1000, 10)

for j in range(len(sample_number)): #this loop runs over differet sample sizes 
	area_1=0
	for i in range(sample_number[j]): #this loop rejects / accepts points (hit/miss)
		b=point_matrix(r,n,sample_number[j])[i]**2
		a=np.sqrt(sum(b))
		if a <= 1:
			area_1+=1
	A.append(V_ncube(n, r)*np.true_divide(area_1, sample_number[j]))    
	

actualvol=np.full(len(sample_number),V_nsphere(n, r))
plt.scatter(sample_number, A, label="Estimated Volume through Montecarlo method")
plt.xlim(10, 10000)
plt.plot(sample_number, actualvol, label="Volume of 3D sphere")
plt.xlabel("Volume of 3D Sphere", fontdict=font)
plt.legend()
plt.ylabel("Number of random points", fontdict=font)
plt.show()


#Literature values of volumes 
volumes=[4.1887 ,4.9348,5.2637,5.1677,4.7248 ,4.0587 ,3.2985, 2.5502 ]
dimentions=[3,4,5,6,7,8,9,10]
cubes=[8, 16, 32, 64, 128, 256, 512, 1024]

###################
###QUESTION C #####
###################
#Finding undertainty of samples drawn


time=[] #aray for computation time 
VS=[]
US=[]
for n in range(3,11):
	area=0
	start=timer()
	for i in range(num_points): #this rejects points with mag>1
		b=point_matrix(r,n,10000)[i]**2
		a=np.sqrt(sum(b))
		if a <= 1:
			area+=1
	end=timer()	
	VS.append(V_ncube(n, r)*area/ 10000) #prints 
	US.append((V_ncube(n,r)*(V_ncube(n, r)*area/ 10000)/10000)**(1/2))
	time.append(end-start)

plt.plot(time, VS , marker='o')
plt.xlabel("computation time (s)", fontdict=font)
plt.ylabel("Estimated sphere volume", fontdict=font)
plt.errorbar(time, VS, yerr=US, xerr=None)
plt.show()







#Plots area of n-cube and sphere as a function of dimention 

plt.plot(dimentions, volumes, marker='o', label="n-ball volume" )
#plt.plot(dimentions, cubes, marker='o', label='n-cube volume')
plt.plot(dimentions, VS, marker='o', label='estimated volume')
plt.errorbar(dimentions, VS, yerr=US, xerr=None)
plt.legend()
plt.xlim(2,11)
plt.xlabel("n", fontdict=font)
plt.ylabel("Volume", fontdict=font)
plt.show()



#Writing json file 
data={}
data["1"]={}
data["1"]["volume"]={}
data["1"]["volume"]["3d"]=VS[0]
data["1"]["volume"]["4d"]=VS[1]
data["1"]["volume"]["5d"]=VS[2]
data["1"]["volume"]["6d"]=VS[3]
data["1"]["volume"]["7d"]=VS[4]
data["1"]["volume"]["8d"]=VS[5]
data["1"]["volume"]["9d"]=VS[6]
data["1"]["volume"]["10d"]=VS[7]

data["1"]["uncertainty"]={}
data["1"]["uncertainty"]["3d"]=US[0]
data["1"]["uncertainty"]["4d"]=US[1]
data["1"]["uncertainty"]["5d"]=US[2]
data["1"]["uncertainty"]["6d"]=US[3]
data["1"]["uncertainty"]["7d"]=US[4]
data["1"]["uncertainty"]["8d"]=US[5]
data["1"]["uncertainty"]["9d"]=US[6]
data["1"]["uncertainty"]["10d"]=US[7]


with open('tut5/HRDURS001/HRDURS001.json', 'w') as outfile:
	json.dump(data, outfile)


