#URSULA HARDIE tut5 q2 (CP PHY4000W)

from __future__ import division #because python is integer inclined
import numpy as np 
import matplotlib 
matplotlib.use('TKAgg') #for animation
import matplotlib.pyplot as plt 
#plt.rcParams['animation.ffmpeg_path'] = '/Users/owner/opt/anaconda3/bin/ffmpeg' #so the code knows where ffmpeg is 
import scipy as sp 
import matplotlib.animation as animation  #for animation
#from matplotlib.animation import FFMpegWriter

#defining animation writer
#Writer = animation.writers['ffmpeg']
#writer = Writer(fps=60, metadata=dict(artist='Me'), bitrate=3000)

#font customizing for plots
font = {'family': 'serif',
        'color':  'navy',
        'weight': 'normal',
        'size': 11,
        }

#definition for Runge-kutta method
def RK4(diffeq, y0, t, h):
    
    k1 = h*diffeq(y0, t)                    
    k2 = h*diffeq(y0+0.5*k1, t + h/2.)      
    k3 = h*diffeq(y0+0.5*k2, t + h/2.)      
    k4 = h*diffeq(y0+k3, t + h)             
    return y0 + (k1+k4)/6.0 + (k2+k3)/3.0   

#Initial values
g=9.8 #graviational acceleration
m1=24 #mass of upper bob (kg)
m2=16 #mass of lower bob (kg)
L1=1 #length of 1st pendulum
L2=1 #length of 2nd 
rev=360  #360 degrees in a a revoltion

#constants to make writing things more simply
a=2*m1+m2 
b=m1+m2
a=2*m1+m2
b=m1+m2
dt=0.01 #time increment


def f(derivs, t): # f is our differential equation "matrix'"
    matrix=np.zeros((2,2))  #each entry is a different equation
    matrix[0,0]=derivs[0,1]
    matrix[1,0]=derivs[1,1]
    t1=derivs[0,0]
    t2=derivs[1,0]
    w2=derivs[1,1]
    w1=derivs[0,1]
    matrix[0,1]=(-g*a*np.sin(t1)-m2*g*np.sin(t1-2*t2)-2*np.sin(t1-t2)*m2*((w2**2)*L2+(w1**2)*L1*np.cos(t1-t2)))/(L1*(a-m2*np.cos(2*t1-2*t2)))
    matrix[1,1]=(2*np.sin(t1-t2)*((w1**2)*L1*b+g*b*np.cos(t1)+(w2**2)*L2*m2*np.cos(t1-t2)))/(L2*(a-m2*np.cos(2*t1-2*t2)))

    return matrix

    

#Starts pendulum simulation

def start():
    t1=np.pi-0.1 #angle 1
    t2=np.pi-0.1 #angle 2
    init_cond=np.array([[t1,0.0], [t2,1]]) #array of initial conditions
    t, h, tf=0.0,0.01,50.0 #initial time, time increment, final time
    time=[]
    ang1=[]
    ang2=[]
    X1 =[]
    Y1 =[]
    X2 =[]
    Y2=[]
    while(t<tf):  #We run over the rime perid 
        init_cond=RK4(f, init_cond, t, h)
        time.append(t)
        ang1.append(np.rad2deg(init_cond[0,0])%rev)
        ang2.append(np.rad2deg(init_cond[1,0])%rev)
        #Convert to cartesian
        x1, y1 = L1*np.sin(init_cond[0,0]), -L1*np.cos(init_cond[0,0])
        x2, y2 = x1 + L2*np.sin(init_cond[1,0]), y1 - L2*np.cos(init_cond[1,0])
        X1.append(x1)
        Y1.append(y1)
        X2.append(x2)
        Y2.append(y2)
        t = t + h

     #plotting the animation
    fig = plt.figure()
    ax = fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
    ax.set_aspect('equal')
    ax.set_title("theta1=pi-0.1, theta2=pi-0.1 L1=1m, L2=1m, m1=8kg, m2=8kg, w2=6, w2=6", fontdict=font)
    ax.set_xlabel("x", fontdict=font)
    ax.set_ylabel("y", fontdict=font)
    

    line, = ax.plot([], [], 'o-', lw=2)
    tail1, = ax.plot([],[],'m')
    
    time_template = 'time = %.1fs'
   
    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes, fontdict=font)
  


    def init():
        line.set_data([], [])
        tail1.set_data([], [])
        
        time_text.set_text('')
        

        return line, time_text


    def animate(i):
        thisx = [0, X1[i], X2[i]]
        thisy = [0, Y1[i], Y2[i]]
        tail1.set_data(X2[:i],Y2[:i])
        
        line.set_data(thisx, thisy)
        time_text.set_text(time_template % (i*dt))
        
        return line, time_text, tail1


    ani = animation.FuncAnimation(fig, animate, range(1, len(Y1)),
                              interval=dt*1000, blit=True, init_func=init)


    
    #ani.save("animation.mov", writer=writer)
    plt.show()
    #Fourier transform
    #plt.title("theta1=pi-0.1, theta2=pi-0.1 L1=1m, L2=1m, m1=8kg, m2=8kg, w2=6, w2=6", fontdict=font)
    '''
    F=np.fft.fft(X2)
    W=np.fft.fftfreq(len(X2))
    plt.xlabel('Frequency')
    plt.ylabel('x(f)')
    print(len(F),len(W))
    #print(len(ttt))
    plt.plot(W, F)
    plt.show()
    '''

start()





