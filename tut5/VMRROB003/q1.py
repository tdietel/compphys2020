import matplotlib.pyplot as plt
import numpy as np
from scipy.special import gamma
import time
import json

#figure_folder = "./plots/"
#json_folder = "./"
figure_folder = "tut5/VMRROB003/"
json_folder = "tut5/VMRROB003/"


def find_volume(dim, trials=10000, radius=1):
    coords = np.random.uniform(-radius, radius, (trials,dim))  # creates vectors of random positions for the number of trials
    radii = np.sqrt(np.einsum('ij, ij->i', coords, coords)) # finds the vector of radial values - dot product of list of vectors, 1 dot product for each element
    Nacc = np.count_nonzero(radii<radius) # finds number of values in sphere
    approx_vol = (Nacc/trials)*n_cube_volume(dim,radius)
    
    #using Vsphere=Nacc/N *V_cube, we get uncertainty V_cube/N * sqrt(Nacc) - derivation in report
    std_dev = (n_cube_volume(dim, radius)/trials) * np.sqrt(Nacc)
    
    return approx_vol, std_dev

def n_sphere_vol(dim, radius=1): # formula for expected volume of sphere in n dimensions
    return (radius**dim) *(np.pi**(dim/2))/gamma(dim/2+1)

def n_cube_volume(dim,radius=1): #formula for expected volume of cube in n dimensions
    return (2*radius)**dim
 
    
def get_estimate_for_samples(dim, N): #given a sample range, find the volumes and uncertainties as a function of number of samples
    vols = np.zeros(N.shape)
    uncertainties = np.zeros(N.shape)
    for i in range(len(N)):
        approx_vol,std_dev = find_volume(dim, N[i])
        vols[i]=approx_vol
        uncertainties[i]= std_dev
        
    return vols, uncertainties

def q1a(): #visualising convergence of integration
    dim=3
    N = np.linspace(100,100000,100, dtype=int) # the range of number of samples we want to plot
    N_runs = 5
    theoretical_vol = np.ones(N.shape)*n_sphere_vol(dim) #expected value of the volume
    vol_results = []
    for i in range(N_runs):
        vols, uncertainties = get_estimate_for_samples(dim,N)
        vol_results.append(vols)
        
    #plots the best estimate, without uncertainty, for 5 runs - to visualise overall convergence    
    plt.figure() 
    plt.title("Estimated value of a 3D sphere volume for 5 runs")
    plt.xlabel("Samples drawn")
    plt.ylabel("Volume of sphere")
    plt.plot(N, theoretical_vol, 'k', label = "Theoretical volume")
    for i in range(N_runs):
        plt.plot(N, vol_results[i], 'o', markersize = 2)
    plt.legend()
    plt.savefig(figure_folder + "convergence_5runs.png")
    
    #plots 1 run with uncertainty to show decreasing uncertainty with number of samples
    plt.figure()
    plt.title("Estimated value of a 3D sphere volume for 1 run with uncertainty")
    plt.xlabel("Samples drawn")
    plt.ylabel("Volume of sphere")
    plt.errorbar(N, vols, uncertainties,fmt = 'k_', label = "Estimated volume", capsize = 2.5, elinewidth = 0.5)
    plt.plot(N, theoretical_vol, label = "Theoretical volume")
    plt.legend()
    plt.savefig(figure_folder + "convergence_uncertainty.png")
        
def q1b():
    dim=3 
    trials = 10000
    approx_vol,std_dev = find_volume(dim,trials) #finds the volume and uncertainty
    expected_vol = n_sphere_vol(dim)
    print("Q1B")
    print("__________________")
    print("Expected volume:", expected_vol)
    print("Calculated volume:", approx_vol, "+-", std_dev)
    print("Absolute error:", np.abs(approx_vol-expected_vol))
    print("Relative error:", np.abs(approx_vol-expected_vol)/expected_vol)
    
def plot_higher_convergence():#checking convergence of higher dimensions - only going to use 6/9 for comparisons
    N = np.linspace(100,100000,100, dtype=int) # the range of number of samples we want to plot
    vols6, uncertainties6 = get_estimate_for_samples(6,N) 
    theoretical_vol6 = np.ones(N.shape)*n_sphere_vol(6) #expected value of the volume
    vols9, uncertainties9 = get_estimate_for_samples(9,N) 
    theoretical_vol9 = np.ones(N.shape)*n_sphere_vol(9)
    
    plt.figure()
    plt.title("Estimated volume of a 6D hypersphere for 1 run with uncertainty")
    plt.xlabel("Samples drawn")
    plt.ylabel("Volume of sphere")
    plt.errorbar(N, vols6, uncertainties6,fmt = 'k_', label = "Estimated volume", capsize = 2.5, elinewidth = 0.5)
    plt.plot(N, theoretical_vol6, label="Theoretical volume")
    plt.legend()
    plt.savefig(figure_folder + "convergence_6D.png")
    
    plt.figure()
    plt.title("Estimated volume of a 9D hypersphere for 1 run with uncertainty")
    plt.xlabel("Samples drawn")
    plt.ylabel("Volume of sphere")
    plt.errorbar(N, vols9, uncertainties9,fmt = 'k_', label = "Estimated volume", capsize = 2.5, elinewidth = 0.5)
    plt.plot(N, theoretical_vol9, label="Theoretical volume")
    plt.legend()
    plt.savefig(figure_folder + "convergence_9D.png")    
    
def q1c(): 
    dims = np.arange(3,11) #dimension 3 to 10
    trials = 10000
    results = np.zeros((len(dims),3)) #want 2d array of (volume, uncertainty,comp time) for each dim
    
    for i in range(len(dims)): #for each dimension
        start_time = time.time() #start timer
        approx_vol , std_dev = find_volume(dims[i],trials) #finds volume and uncertainty for dimension
        time_elapsed = time.time() - start_time #stop timer
        results[i] = [approx_vol, std_dev, time_elapsed] #record full result
    
    volumes, std_devs, times = results.T
    save_to_json(volumes, std_devs) #writes results to json file
    
    #plot volume of sphere as function of dimension
    smoothdims = np.arange(3,10,0.01)
    plt.figure()
    plt.title("Estimated volume of a hypersphere for 1 run with uncertainty")
    plt.xlabel("Dimension of sphere")
    plt.ylabel("Volume of sphere")
    plt.errorbar(dims, volumes, std_devs,fmt = 'k_', label = "Estimated volume", capsize = 2.5, elinewidth = 0.5)
    plt.plot(smoothdims, n_sphere_vol(smoothdims), label="Theoretical volume")
    plt.legend()
    plt.savefig(figure_folder + "volumeofdim.png")
    
    plt.figure() #plot uncertainty as function of dimension
    plt.plot(dims, std_devs, 'ko', label="Uncertainty")
    plt.title("Uncertainty on estimated volume of hypersphere for 1 run")
    plt.xlabel("Dimension of sphere")
    plt.ylabel("Uncertainty")
    plt.savefig(figure_folder + "uncertaintyofdim.png")
    
    #plot computation time for calculating volume as function of dimension
    #on my machine at least this is only meaningful for 10x the number of samples
    plt.figure()
    plt.title("Computation time for calculating volume of hypersphere")
    plt.plot(dims, times,'ko', label = "Computation time")
    plt.xlabel("Dimensions of sphere")
    plt.ylabel("Computation time (s)")
    plt.savefig(figure_folder + "timeofdim.png")
    
    plot_higher_convergence()
    
        
def save_to_json(volumes, std_devs): #saves results to json file
    labels = ["3d","4d","5d","6d","7d","8d","9d","10d"]
    results_dic = {}
    results_dic['1']={}
    results_dic['1']['volume'] = {}
    results_dic['1']['uncertainty'] = {}
    
    for i in range(len(labels)):
        results_dic['1']['volume'][labels[i]]=volumes[i]
        results_dic['1']['uncertainty'][labels[i]]=std_devs[i]
    
    with open(json_folder+"solutions.json", 'w') as outfile:
        json.dump(results_dic, outfile)   
       
def main():
    q1a()
    q1b()
    q1c()



if __name__ == "__main__":
    main()