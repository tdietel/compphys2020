import numpy as np
from numpy import sin,cos
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.fftpack

g=9.81 #constant gravity
#figure_folder = "./plots/"
#animations_folder = "./animations/"

figure_folder = "tut5/VMRROB003/"
animations_folder = "tut5/VMRROB003/animations/"

def rk4(func,t,y0): #fourth order Runge-Kutta as from examples - t unnecessary but retaining it to stay general for this method
    y = np.zeros((len(t),len(y0))) #the dynamic variable vector for all times - to be updated by method
    y[0] = y0 #initial conditions

    for i in range(len(t)-1):
    
        dt = t[i+1] - t[i]
        
        k1 = dt * func( t[i]          , y[i]          )
        k2 = dt * func( t[i] + 0.5*dt , y[i] + 0.5*k1 )
        k3 = dt * func( t[i] + 0.5*dt , y[i] + 0.5*k2 )
        k4 = dt * func( t[i] + 1.0*dt , y[i] + 1.0*k3 )

        y[i+1] = y[i] + 1./6. * ( k1 + 2*k2 + 2*k3 + k4) # computes the approximation to the next value

    return y

class genvelocity: #generalized velocity vector callable class
    def __init__(self,m1=1,m2=1,L1=1,L2=1): #can initialise different lengths/masses
        self.m1 = m1
        self.m2 = m2
        self.L1 = L1
        self.L2 = L2

    def __call__(self,t,y): #returns [omega1, omega2, omega1', omega2']
        return_array = np.zeros(4)
        return_array[0] = y[2] #omega1
        return_array[1] = y[3] #omega2
        denominator = 2*self.m1+self.m2-self.m2*cos(2*y[0]-2*y[1])
        omega1primenum = -1*g*(2*self.m1+self.m2)*sin(y[0])-self.m2*g*sin(y[0]-2*y[1]) - 2*sin(y[0]-y[1])*self.m2*(y[3]**2 * self.L2 + y[2]**2 *self.L1 * cos(y[0]-y[1]))
        omega2primenum = 2*sin(y[0]-y[1])*(y[2]**2 *self.L1*(self.m1+self.m2)+g*(self.m1+self.m2)*cos(y[0])+y[3]**2*self.L2*self.m2*cos(y[0]-y[1]))
        
        return_array[2] = omega1primenum/(self.L1*denominator) #omega1' as given in problem
        return_array[3] = omega2primenum/(self.L2*denominator) #omega2' as given in problem
        return np.asarray(return_array)    
    
def find_trajectory(y0=np.asarray([1,1,0,0]),L1=1,L2=1,m1=1,m2=1): #reasonable defaults
    genv = genvelocity(m1,m2,L1,L2) #initialises the generalized velocity class with the length and mass parameters
    t = np.arange(0, 20, 1e-2) # sets up the time range and step - 20 seconds with 0.01s timesteps
    y = rk4(genv,t,y0) #finds the trajectory y of both masses 
    return t,y

def plot_fourier(tdata,ydata, savename): #plots the relevant Fourier transforms
    plt.figure()
    plt.subplot(211)
    plt.plot(tdata, ydata)
    plt.title("x coordinate of second mass")
    plt.xlabel("t (s)")
    plt.ylabel("x2 (m)")
    #plotting Fourier transform
    
    yf=scipy.fftpack.fft(x2)
    #calculates the associated omega values for each fft return
    freq = np.fft.fftfreq(len(x2), t[1] - t[0])* 2* np.pi
    
    # setup frequency domain plot
    ax = plt.subplot(212)
    
    #plots the transformed data
    markerline, stemline, baseline, = ax.stem(freq,np.abs(yf),linefmt='k-',markerfmt='bo',basefmt='k.')
    
    ax.set_xlim([0, 20])
    plt.setp(stemline, linewidth = 1.25)
    plt.setp(markerline, markersize = 3)
    plt.title("Fourier transform of x coordinate of second mass")
    plt.xlabel("ω (1/s)")
    plt.ylabel("Amplitude")
    plt.tight_layout()
    plt.savefig(figure_folder+savename+"fourier.png")


#initialise parameters
L1= 1
L2 =1
m1 = 1
m2 = 1

y01 = np.asarray([1,1,0,0]) #non-chaotic - normal pendulum almost
y02 = np.asarray([2,-2,5,-1]) #lots of energy - leads easier to chaotic motion
y03 = np.asarray([2,-2.2,5,-1]) #slight change in initial angle for comparison
y04 = np.asarray([1,1,10,10]) # spinning in circles - another non-chaotic, maybe interesting

#use to generate all plots of all initial conditions
y0s = [y01,y02,y03,y04]
savenames = ["init1_", "init2_", "init3_","init4_"]

#uncomment these two lines to select 1 initial condition, then animation will work if wanted
#y0s = [y04] 
#savenames = ["init4_"]

# for each initial condition
for i,y0 in enumerate(y0s):
    
    t,y = find_trajectory(y0,L1,L2,m1,m2) #finds the trajectory of the masses for given initial conditions
    
    #Calculates cartesian positions of each mass from the angles
    x1 = L1*sin(y[:, 0])
    y1 = -L1*cos(y[:, 0])
    
    x2 = L2*sin(y[:, 1]) + x1
    y2 = -L2*cos(y[:, 1]) + y1
    
    #Plots for report
    #Trajectory
    
    plt.figure()
    plt.title("Trajectory of second mass")
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")
    plt.plot(x2,y2)
    plt.savefig(figure_folder+savenames[i]+"traj.png")
    
    #Fourier transform of horizontal coord - includes x2(t)
    plot_fourier(t,x2, savenames[i])
    
    #phase space of theta2/omega2
    plt.figure()
    plt.title("Phase space of second mass")
    plt.xlabel("Theta 2")
    plt.ylabel("Omega 2")
    plt.plot(y[:,1], y[:,3])
    plt.savefig(figure_folder+savenames[i]+"phase.png")
    
    
#Animation - doesn't work in a loop and is extremely slow to save - uncomment and choose 1 initial condition to use
#############################################################################
#    Writer = animation.writers['ffmpeg']
#    writer = Writer(fps=60, metadata=dict(artist='Me'), bitrate=500)
#    
#    fig = plt.figure()
#    ax = fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
#    ax.grid()
#    
#    line, = ax.plot([], [], 'o-', lw=2)
#    time_template = 'frame = %.1fs'
#    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
#    
#    
#    def init():
#        line.set_data([], [])
#        time_text.set_text('')
#        return line, time_text
#    
#    
#    def animate(i):
#        thisx = [0, x1[i], x2[i]]
#        thisy = [0, y1[i], y2[i]]
#    
#        line.set_data(thisx, thisy)
#        time_text.set_text(time_template % (i))
#        return line, time_text
#    
#    ani = animation.FuncAnimation(fig, animate, np.arange(1, len(y)),
#                                  interval=10, blit=True, init_func=init)
#    
#    ani.save(animations_folder + savenames[i] + "animation_new.mp4", writer=writer)
#    plt.show()
 
    

    
