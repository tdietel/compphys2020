# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 10:06:36 2020

@author: erinj
"""

import numpy as np
import matplotlib.pyplot as plt
import time
import json

##Monte Carlo volume of a sphere



np.random.seed(2)
#volume of a sphere radius r
r=1
vol_s=(1/2)*(np.pi**2)*r**4
print('Sphere volume=',vol_s)

def volume_s(R,dim):
    #c values from wikipedia
    c=[2,1,4/3,1/2,8/15,1/6,16/105,1/24,32/945,1/120]
    power=int(dim/2)
    return c[dim-1]*np.pi**power*R**dim

print(volume_s(r,4))
#volume of a cube with sides l=2r
def volume_c(R,dim):
    l=2*R
    return(l**dim)

def calc(r,dims,n):
    volumes=[]
    times=[]
    uncertain=[]
    for i in range(len(dims)):
        prob=[]
        vol=[]
        t=[]
        error=[]
        for s in range(len(n)):
            t1=time.perf_counter()
            coords=[]
            counter=0
            for k in range(dims[i]):
                coords.append(np.random.uniform(-r,r,n[s]))
            for j in range(n[s]):
                rad=0
                for h in range(len(coords)):
                    rad+=coords[h][j]**2
                if np.sqrt(rad)<r:
                    counter+=1
            error.append(1/np.sqrt(counter))
            probability=counter/n[s]
            prob.append(probability)
            vol.append(probability*volume_c(r,dims[i]))
            t2=time.perf_counter()
            t.append(t2-t1)
        volumes.append(vol)
        times.append(t)
        uncertain.append(error)
    return [volumes ,times, uncertain]
##Returns [[[volmes for each N]* dimensions],[[time for each N]*dim]]
# if a=calc(r,dims,N)
# a[0] accesses volumes a[1] accesses times
# a[0][dims][N]         a[1][dims][N]


############################################


##part a 
plt.figure()
dim=[3]
N=[10,50,100,200,400,800,1000,2000, 3000,5000,8000, 10000]  #sample size

b=calc(r,dim,N)
a_uncertain=[]
v=[]
t=[]
for i in range(5):
    a=calc(r,dim,N)
    plt.plot(N,a[0][0])  #plotting convergence of est. volume for increasing N
    plt.xlabel('N')      
    plt.ylabel('volume')
    plt.title('Dimensions='+str(3))
    v.append(a[0][0][-1])  #calculating av volume and time taken for N=10000
    t.append(a[1][0][-1])  
    a_uncertain.append(a[2][0][-1])


vav3=np.mean(v) 
uav3=np.mean(a_uncertain)  
tav3=np.mean(t) 
print('b)')
print('Average est=', np.mean(v))
print('Average unc=',uav3)
print('Av execution time=', np.mean(t))


line=[]
for i in range(len(N)):
    line.append(volume_s(r,3))
plt.plot(N,line,'--',label='Literature volume')
plt.legend()





#part c
dims=[4,5,6,7,8,9,10]
N=[10,50,100,200,400,800,1000,2000, 3000,5000,8000, 10000]  #sample size
Nb=[10000]

#Plot convergence for each dimension

av=[vav3]
at=[tav3]
ae=[uav3]
tt=[]


for i in range(len(dims)):
    v=[]
    e=[]
    t=[]
    for j in range(5):
        c1=calc(r,dims,Nb)
        e.append(c1[2][i][-1]) #uncertainty of each est for N=10000
        v.append(c1[0][i][-1])  #volume for N=10000
        t.append(c1[1][i][-1])  #time taken for N=10000
    av.append(np.mean(v))  #find averages
    at.append(np.mean(t))
    ae.append(np.mean(e))
    print(dims[i])
    print('Av vol for 10000 =', np.mean(v))
    print('av uncertainty=', np.mean(e))
    print('Av execution time=', np.mean(t))
    
d=[3,4,5,6,7,8,9,10]    
vol={}
unc={}
for i in range(len(av)):
    vol[str(d[i])+'d']=av[i]
    unc[str(d[i])+'d']=ae[i]

sol={"1":{"volume":vol,"uncertainty":unc}}

with open('tut5/JRVERI002/answers.json','w') as f:
    json.dump(sol,f)


dimplot=[4,10]
for i in range(len(dimplot)):
    plt.figure()
    line=[]
    for k in range(len(N)):
        line.append(volume_s(r,dimplot[i]))
    for j in range(5):
        c1=calc(r,dimplot,N)
        plt.plot(N,c1[0][i]) #plots the convergence of est volume for various N
        plt.title('dimensions ='+ str(dimplot[i]))
    plt.plot(N,line, '--', label='Literature volume')
    plt.legend()


#plot volume and time averages
plt.figure()
plt.plot(d, av)
plt.xlabel('Dimensions')
plt.ylabel('Average Volume')
plt.figure()
plt.plot(d, at)
plt.xlabel('Dimensions')
plt.ylabel('Average execution time')



##########################
#Question 2
#########################

##Solving the double pendulum with RK4

##y=[theta1,theta2,w1,w2]
##g=[w1,w2,dw1,dw2]

L1=6
L2=5

def g(y):  #takes input of the form y=[theta1,theta2,w1,w2]
    L1=6
    L2=5
    m1=0.5
    m2=0.5
    g=9.8
    theta1=y[0]
    theta2=y[1]
    w1=y[2]
    w2=y[3]
    numerator1=-g*(2*m1+m2)*np.sin(theta1)-m2*g*np.sin(theta1-2*theta2)-2*np.sin(theta1-theta2)*m2*((w2**2)*L2+(w1**2)*L1*np.cos(theta1-theta2))
    denominator1=L1*(2*m1+m2-m2*np.cos(2*theta1-2*theta2))
    dw1=numerator1/denominator1
    
    numerator2= 2*np.sin(theta1-theta2)*((w1**2)*L1*(m1+m2)+g*(m1+m2)*np.cos(theta1)+(w2**2)*L2*m2*np.cos(theta1-theta2))
    denominator2= L2*(2*m1+m2-m2*np.cos(2*theta1-2*theta2))
    dw2= numerator2/denominator2
    
    
    return np.array([w1,w2,dw1,dw2]) #returns g=[w1,w2,dw1,dw2]


# 4th order Runge Kutta

def RK4(f,y0,dt, N):  #f is the function, y0 are our IC's, t is the time interval t=[start,stop], N=number of time steps
    y=np.zeros((N,len(y0)))
    for i in range(len(y0)):
        y[0][i]=y0[i]
    tp=np.zeros(N)
    for i in range(len(y)-1):
        tp[i+1]=tp[i]+dt
        k1=dt*f(y[i])
        k2=dt*f(y[i]+0.5*k1)
        k3=dt*f(y[i]+0.5*k2)
        k4=dt*f(y[i]+k3)
        y[i+1]=y[i]+(1/6)*(k1+2*k2+2*k3+k4)
    return y,tp,dt        #returns [y],[t],dt #y=theta1,theta2,w1,w2

#Initial conditons
theta10=np.pi/4
theta20=np.pi/4
w10=1.2
w20=1.82
y0=[theta10,theta20,w10,w20]

N=500



a=RK4(g,y0,0.5,N)

theta2=[]
omega2=[]

theta1=[]
omega1=[]

for i in range(len(a[0])):
    theta2.append(a[0][i][1])
    omega2.append(a[0][i][3])
    theta1.append(a[0][i][0])

plt.figure()
plt.plot(theta2,omega2)
plt.title('Phase portrait')
plt.xlabel('theta')
plt.ylabel('omega')

plt.figure()
plt.plot(a[1], theta2)
plt.title('Position-time plot')
plt.xlabel('time (s)')
plt.ylabel('theta')

x2=[]
y2=[]
for i in range(len(theta1)):
    x2.append(L1*np.sin(theta1[i])+L2*np.sin(theta2[i]))
    y2.append(-1*(L1*np.cos(theta1[i])+L2*np.cos(theta2[i])))
    
plt.figure()
plt.plot(x2,y2)
plt.xlabel('x')
plt.ylabel('y')
   
ftx=np.fft.fft(x2)
freq=np.fft.fftfreq(len(x2))

plt.figure()
plt.plot(freq,ftx) 
plt.xlabel('Frequency')
plt.ylabel('X(f)')
    

    
