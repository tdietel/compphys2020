# monte carlo integration and ordinary differential equations - thavish chetty
# section 1: importing necessary functions
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy.special import gamma
import timeit
import json


# section 2: defining functions for monte-carlo
def volume_sphere(n, r):
    v = (np.pi**(n/2))/(gamma(n/2 + 1))*(r**n)
    return v


def volume_cube(n, r):
    v = (2*r)**n
    return v


def monte_carlos():
    print("Monte Carlo goes here")
    r = 1
    m = 1000
    M = 10000
    runs = 5
    trials = np.linspace(1, m, m - 1)
    dimensions = [3, 4, 5, 6, 7, 8, 9, 10]
    global names
    names = ["3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d"]
    for dim in dimensions:
        p_approximations = []
        v_approximations = []
        p_exacts = []
        v_exacts = []
        for i in range(runs):
            p_a = []
            v_a = []
            p_e = []
            v_e = []
            for j in range(1, m, 1):
                parameters = []
                for k in range(dim):
                    xk = np.random.uniform(-1, 1, j)
                    parameters.append(xk)
                parameters = np.reshape(parameters, (j, dim))
                distances = []
                for coordinates in parameters:
                    coordinates = np.square(coordinates)
                    distance = np.sqrt(np.sum(coordinates))
                    distances.append(distance)

                m_acc = 0
                for sample_point in distances:
                    if sample_point < r:
                        m_acc = m_acc + 1
                    else:
                        pass

                v_cube = volume_cube(dim, r)
                p_approx = m_acc/j
                v_approx = v_cube*p_approx
                v_exact = volume_sphere(dim, r)
                p_exact = v_exact/v_cube

                p_a.append(p_approx)
                p_e.append(p_exact)
                v_a.append(v_approx)
                v_e.append(v_exact)

            p_approximations.append(p_a)
            p_exacts.append(p_e)
            v_approximations.append(v_a)
            v_exacts.append(v_e)


        #plt.figure()

        #plt.ylabel("Volume")
        #plt.xlabel("Trials")
        #for values in v_approximations:
         #   plt.plot(trials, values, "o", markersize=2)
        #plt.show()

    p_approximation = []
    global v_approximation
    global uncertainty
    v_approximation = []
    uncertainty = []
    times = []
    for dim in dimensions:
        start_time = timeit.default_timer()
        parameters = []
        for k in range(dim):
            xk = np.random.uniform(-1, 1, M)
            parameters.append(xk)
        parameters = np.reshape(parameters, (M, dim))
        distances = []
        for coordinates in parameters:
            coordinates = np.square(coordinates)
            distance = np.sqrt(np.sum(coordinates))
            distances.append(distance)

        m_acc = 0
        for sample_point in distances:
            if sample_point < r:
                m_acc = m_acc + 1
            else:
                pass

        v_cube = volume_cube(dim, r)
        p_approx = m_acc / M
        v_approx = v_cube * p_approx
        unc = v_approx/np.sqrt(m_acc)
        v_approximation.append(v_approx)
        p_approximation.append(p_approx)
        uncertainty.append(unc)
        elapsed = timeit.default_timer() - start_time
        elapsed_ms = elapsed*10e3
        times.append(elapsed_ms)

    print("Volumes")
    print(v_approximation)

    print("Uncertainties")
    print(uncertainty)

    print("Times")
    print(times)

    plt.figure()
    plt.xlabel("Dimensions (N)")
    plt.ylabel("Volume (Vn)")
    plt.plot(dimensions, v_approximation, marker='o', color="midnightblue", markersize=2)
    plt.errorbar(dimensions, v_approximation, ls='none', color='darkslategrey',  yerr=uncertainty)
    plt.show()

    plt.figure()
    plt.xlabel("Dimensions (N)")
    plt.ylabel("Uncertainty")
    plt.plot(dimensions, uncertainty, marker='o', color="orange", markersize=2)
    plt.show()

    plt.figure()
    plt.xlabel("Dimensions (N)")
    plt.ylabel("Elapsed Time(ns)")
    plt.plot(dimensions, times, marker='o', markersize="2", color="crimson")
    plt.show()




def aa_1(o1, o2, w1, w2, l1, l2, m1, m2, g):
    top = -g*(2*m1 + m2)*np.sin(o1) - m2*g*np.sin(o1 + 2*o2) - 2*np.sin(o1 - o2)*m2*((w2**2)*l2 + (w1**2)*l1*np.cos(o1 - o2))
    bottom = l1*(2*m1 + m2 - m2*np.cos(2*o1 - 2*o2))
    alpha = top/bottom
    return alpha


def aa_2(o1, o2, w1, w2, l1, l2, m1, m2, g):
    top = 2*np.sin(o1 - o2)*((w1**2)*l1*(m1 + m2) + g*(m1 + m2)*np.cos(o1) + (w2**2)*l2*m2*np.cos(o1 - o2))
    bottom = l2*(2*m1 + m2 - m2*np.cos(2*o1 - 2*o2))
    alpha = top/bottom
    return alpha


def x_2(o1, o2, l1, l2):
    x = l1*np.sin(o1) + l2*np.sin(o2)
    return x



def double_pendulum():
    g = 9.8
    m1 = 10
    m2 = 12
    l1 = 5
    l2 = 7
    t1 = 10
    t2 = 15
    h = 0.01  #step for 4th-order runge-kutte
    N = 10000

    # initial conditions:
    o1 = [np.pi/2, np.pi/2]
    o2 = [np.pi/2, 3*np.pi/2]
    w1 = [0.00, 0.00]
    w2 = [0.00, 0.00]
    x2 = [x_2(o1[0], o2[0], l1, l2), x_2(o1[1], o2[1], l1, l2)]

    for k in range(len(o1)):
        angles_1 = []
        angles_2 = []
        freq_1 = []
        freq_2 = []
        xpositions_2 = []
        angles_1.append(o1[k])
        angles_2.append(o2[k])
        freq_1.append(w1[k])
        freq_2.append(w2[k])
        xpositions_2.append(x2[k])

        for i in range(N - 1):

            # calculating 4-th-order runge-kutte terms
            do1a = h*w1[k]
            do2a = h*w2[k]

            dw1a = h*aa_1(o1[k], o2[k], w1[k], w2[k], l1, l2, m1, m2, g)
            dw2a = h*aa_2(o1[k], o2[k], w1[k], w2[k], l1, l2, m1, m2, g)

            do1b = h*(w1[k] + dw1a/2)
            do2b = h*(w2[k] + dw2a/2)

            dw1b = h*aa_1(o1[k] + do1a/2, o2[k], w1[k] + dw1a/2, w2[k], l1, l2, m1, m2, g)
            dw2b = h*aa_2(o1[k], o2[k] + do2a/2, w1[k], w2[k] + dw2a/2, l1, l2, m1, m2, g)

            do1c = h*(w1[k] + dw1b/2)
            do2c = h*(w2[k] + dw2b/2)

            dw1c = h*aa_1(o1[k] + do1b/2, o2[k], w1[k] + dw1a/2, w2[k], l1, l2, m1, m2, g)
            dw2c = h*aa_2(o1[k], o2[k] + do2b/2, w1[k], w2[k] + dw2a/2, l1, l2, m1, m2, g)

            do1d = h*(w1[k] + dw1c)
            do2d = h*(w2[k] + dw2c)

            dw1d = h*aa_1(o1[k] + do1c, o2[k], w1[k] + dw1a, w2[k], l1, l2, m1, m2, g)
            dw2d = h*aa_2(o1[k], o2[k] + do2c, w1[k], w2[k] + dw2a, l1, l2, m1, m2, g)

            do1 = (do1a + 2*do1b + 2*do1c + do1d)/6
            do2 = (do2a + 2*do2b + 2*do2c + do2d)/6

            dw1 = (dw1a + 2*dw1b + 2*dw1c + dw1d)/6
            dw2 = (dw2a + 2*dw2b + 2*dw2c + dw2d)/6

            # update equations
            o1[k] = o1[k] + do1
            o2[k] = o2[k] + do2
            w1[k] = w1[k] + dw1
            w2[k] = w2[k] + dw2
            x2[k] = x_2(o1[k], o2[k], l1, l2)

            angles_1.append(o1[k])
            angles_2.append(o2[k])
            freq_1.append(w1[k])
            freq_2.append(w2[k])
            xpositions_2.append(x2[k])

        steps = np.linspace(0, N, N)
        time = steps*h


        plt.figure()
        plt.plot(time, xpositions_2)
        plt.xlabel("Time(s)")
        plt.ylabel("Horizontal position of Mass 2 (m)")
        #plt.plot(steps, angles_1)
        #plt.plot(steps, angles_2)
        plt.show()

        xfrequencies = fft(xpositions_2)
        tfrequencies = np.linspace(0.0, 0.5*h, N//2)
        plt.figure()
        plt.title("Fourier Transform of the horizontal coordinate of Mass 2")
        plt.xlabel("Frequencies (Hz)")
        plt.plot(tfrequencies[: N//16]*1000, (2/N)*np.abs(xfrequencies[:N//2])[: N//16])
        plt.show()


answers = {}


def question_1():
    print("Updating q1 answers")
    q1 = {}
    vol = {}
    unc = {}
    for i, label in enumerate(names):
        vol.update({label: v_approximation[i]})
    for j, label in enumerate(names):
        unc.update({label: uncertainty[j]})
    q1.update({"volume": vol})
    q1.update({"uncertainty": unc})
    answers.update({"1": q1})


def write_file():
    with open('tut5/CHTTHA009/answers.json', 'w') as f:
        json.dump(answers, f)


if __name__ == '__main__':
    #monte_carlos()
    double_pendulum()
    question_1()
    write_file()


