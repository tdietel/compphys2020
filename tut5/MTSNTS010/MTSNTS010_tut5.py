#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: ntshembho
"""

# imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
import json
import timeit

' ' ' Question 1 ' ' ' 
#
# variales
r = 1

## functions

def sphere3D(N):
#    np.random.seed(0)
    x1 = np.random.uniform(low=-1,high=1,size=N)
    x2 = np.random.uniform(low=-1,high=1,size=N)
    x3 = np.random.uniform(low=-1,high=1,size=N)
#    print(x1)
    Nacc = 0
    for i in range(0,N):
        if np.sqrt(x1[i]**2+x2[i]**2+x3[i]**2)<=1:
            Nacc+=1
            
    P = Nacc/N
    d = 3
    vol_est = P*2**d  
#    u = vol_est/np.sqrt(Nacc)

    return vol_est


def plotSphere3D():
    N = []
    for i in range(100,10000):
        if i%100==0:
            N.append(i)
    vol_est = []    
#    uncert = []
    for i in N:  
        vol_est.append(sphere3D(i))


    print('uncert 3d', sphere3D(10000))    
    plt.plot(N,vol_est,'o')
    plt.xlabel('N')
    plt.ylabel('Volume of 3DSphere')
    plt.savefig('3d convergence',dpi=300)
    plt.show()

    
plotSphere3D()

def hypersphere(d):
#    start = timer()
    N = 10000 #basic n used 
    
#    np.random.seed(0)
    x = np.square(np.random.uniform(low=-1,high=1,size=(d,N)))
    R = sum(x)
    Nacc = 0
    for i in range(0,N):
        
        if np.sqrt(R[i])<=1:
            Nacc+=1
    P = Nacc/N
    vol_est = P*2**d  
    u = vol_est/np.sqrt(Nacc)
#    time = timeit.timeit()
#    stop = timer()
#    time = stop-start
    return vol_est,u 

Names = ['3d','4d','5d','6d','7d','8d','9d','10d']
d = [3,4,5,6,7,8,9,10]
volumes = []
uncert = []
times = []
number = 20
for i in range(len(d)):
    v,u = hypersphere(d[i])
    volumes.append(v)
    uncert.append(u)
#    times.append(t)
    times.append(timeit.timeit(globals = globals(), stmt = 'hypersphere(d[i])',number = number)/number) #check and store calculation time

plt.plot(d,volumes,'ro')
plt.ylabel('Volumes')
plt.xlabel('Dimenssions')
#plt.savefig('vols', dpi=300)
plt.show()

plt.plot(d,uncert,'mo')
plt.ylabel('uncertainty')
plt.xlabel('Dimenssions')
#plt.savefig('uncert', dpi=300)
plt.show()

plt.plot(d,times,'o')
plt.ylabel('computation time [s]')
plt.xlabel('Dimenssions')
#plt.savefig('time', dpi=300)
plt.show()


## creating dictionary 
vols = {'3d':volumes[0], '4d':volumes[1], '5d':volumes[2], '6d':volumes[3], '7d':volumes[4], '8d':volumes[5], '9d':volumes[6],'10d':volumes[7]}
vol = {'volume':vols}
uncerts = {'3d':uncert[0], '4d':uncert[1], '5d':uncert[2], '6d':uncert[3], '7d':uncert[4], '8d':volumes[5], '9d':uncert[6],'10d':uncert[7]}
uncertainty = {'uncertainty':uncerts}

data = {'1': {'volume':vols, 'uncertainty':uncerts}}

#with open('solutions.json','w') as f:
#    json.dump(data,f)
    
with open('tut5/MTSNTS010/solutions.json','w') as f:
    json.dump(data,f)





' ' ' Question 2 ' ' '

def genvel(t1,t2,w1,w2):
    g = -9.81
    L1 = 1
    L2 = 1
    m1 = 1
    m2 = 1
    
    num1 = -g*(2*m1 + m2)*np.sin(t1) - m2*g*np.sin(t1 + 2*t2) - 2*np.sin(t1 - t2)*m2*((w2**2)*L2 + (w1**2)*L1*np.cos(t1 - t2))
    den1 = L1*(2*m1 + m2 - m2*np.cos(2*t1 - 2*t2))
    w1dot = num1/den1

    num2 = 2*np.sin(t1 - t2)*((w1**2)*L1*(m1 + m2) + g*(m1 + m2)*np.cos(t1) + (w2**2)*L2*m2*np.cos(t1 - t2))
    den2 = L2*(2*m1 + m2 - m2*np.cos(2*t1 - 2*t2))
    w2dot = num2/den2
    
    return np.array([w1,w2,w1dot,w2dot]) #returning w1,w2,wdot1,wdot2
 
def rk4(t,theta1,theta2,om1,om2):
    
    t1 = []
    t2 = []
    w1 = []
    w2 = []
    t1.append(theta1)
    t2.append(theta2)
    w1.append(om1)
    w2.append(om2)
    
    for i in range (len(t)-1):   
        
        dt = t[i+1] - t[i]

        k1 = genvel(theta1,theta2,om1,om2)
        k2 = genvel(theta1 + dt*0.5*k1[0], theta2 + dt*0.5*k1[1], om1 + dt*0.5*k1[2], om2 + dt*0.5*k1[3])
        k3 = genvel(theta1 + dt*0.5*k2[0], theta2 + dt*0.5*k2[1], om1 + dt*0.5*k2[2], om2 + dt*0.5*k2[3])
        k4 = genvel(theta1 + dt*1.0*k3[0], theta2 + dt*1.0*k3[1], om1 + dt*1.0*k3[2], om2 + dt*1.0*k3[3])
        
        R = dt*1/6*( k1 + 2*k2 + 2*k3 + k4)
        
        theta1+= R[0]
        theta2+= R[1]
        om1 += R[2]
        om2 += R[3]
        
        t1.append(theta1)
        t2.append(theta2)
        w1.append(om1)
        w2.append(om2)
    

    return np.array([t1,t2,w1,w2])


t = np.arange(0, 100, 1e-1)

NC = rk4(t,np.pi/2,np.pi/2,0,0) ##Non-Chaotic initial conditions 

plt.figure(3)
plt.plot(t,NC[1],label = r'$\theta$2 vs t',color = 'red')
#plt.plot(t,NC[0],label = r'$\theta$1 vs t',color = 'purple')
plt.xlabel('t(s)')
plt.ylabel(r'$\theta$')    
plt.legend()
#plt.savefig('nc theta',dpi=300)

plt.figure(4)
plt.plot(t,NC[3],label = r'$\omega$2 vs t',color = 'blue')
#plt.plot(t,NC[2],label = r'$\omega$1 vs t',color = 'orange')
plt.ylabel(r'$\omega$')
plt.xlabel('t(s)')
#plt.savefig('nc w',dpi=300)
plt.legend()


C = rk4(t,np.pi/2,3*np.pi/2,0,0) ##Chaotic inital conditions 

plt.figure(5)
plt.plot(t,C[1],label = r'$\theta$2 vs t',color = 'orange')
#plt.plot(t,C[0],label = r'$\theta$1 vs t',color = 'purple')
plt.xlabel('t(s)')
plt.ylabel(r'$\theta$')    
plt.legend()
#plt.savefig('c theta',dpi=300)

plt.figure(6)
plt.plot(t,C[3],label = r'$\omega$2 vs t',color = 'purple')
#plt.plot(t,C[2],label = r'$\omega$1 vs t',color = 'orange')
plt.ylabel(r'$\omega$')
plt.xlabel('t(s)')
plt.legend()
#plt.savefig('c w',dpi=300)


#Fourier
def x_2(t1,t2): #getting x component of m2
    x = np.sin(t1) + np.sin(t2)
    return x

def fourierNC(func):
    h=0.01
    N = 1000
    x_pos = x_2(func[0],func[1])
    xfrequencies = fft(x_pos)
    tfrequencies = np.linspace(0.0, 0.5*h, N//2)
    plt.figure()
    plt.plot(tfrequencies, (2/N)*np.abs(xfrequencies[:N//2]))
    plt.xlabel('f')
    plt.ylabel(r'$\theta$')
    plt.savefig('fourier NC',dpi=300)
    plt.show()
    
def fourierC(func):
    h=0.01
    N = 1000
    x_pos = x_2(func[0],func[1])
    xfrequencies = fft(x_pos)
    tfrequencies = np.linspace(0.0, 0.5*h, N//2)
    plt.figure()
    plt.plot(tfrequencies, (2/N)*np.abs(xfrequencies[:N//2]))
    plt.xlabel('f')
    plt.ylabel(r'$\theta$')
    plt.savefig('fourier C', dpi=300)
    plt.show()


NCf = fourierNC(NC)

Cf = fourierC(C)

