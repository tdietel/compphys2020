# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 18:06:09 2020

@author: hp
"""
import numpy as np
import matplotlib.pyplot as plt 
from scipy.special import gamma
import time
#############
#Q2) 
#Start off by defining the equations of motion that we're given
g = 9.8

def eom1(m1,m2,t1,t2,o1,o2,L1,L2,g):
    nom = -g*(2*m1+m2)*np.sin(t1)-m2*g*np.sin(t1-2*t2)-2*np.sin(t1-t2)*m2*((o2**2)*L2 + (o1**2)*L1*np.cos(t1-t2))
    denom = L1*(2*m1+m2-m2*np.cos(2*t1-2*t2))
    return nom/denom

def eom2(m1,m2,t1,t2,o1,o2,L1,L2,g):
    nom = 2*np.sin(t1-t2)*((o1**2)*L1*(m1+m2)+g*(m1+m2)*np.cos(t1)+(o2**2)*L2*m2*np.cos(t1-t2))
    denom = L2*(2*m1+m2-m2*np.cos(2*t1-2*t2))
    return nom/denom
##using the eoms to setup our system
y0 = [np.pi/4,np.pi/4,1.2,1.82] #array of initial angle conditions t1,t2,o1,o2
def doublePendulum(y): #y is our initial conditions
    m1=6
    m2=2
    t1=y[0]
    t2=y[1]
    o1=y[2]
    o2=y[3]
    L1=1
    L2=2
    g=9.8
    om1dot=eom1(m1,m2,t1,t2,o1,o2,L1,L2,g)
    om2dot=eom2(m1,m2,t1,t2,o1,o2,L1,L2,g)
    return np.array([o1,o2,om1dot,om2dot])
print("TestOutput  ",doublePendulum(y0))
##now applying this array that we obtain from our defined system to RK4
n=100 #timesteps    
def rk4(func,dt,y0,n): 
    tStep = np.zeros(n)
    y=np.zeros((n,len(y0)))
    for i in range(len(y0)):
        y[0][i]=y0[i]
    for i in range(len(y)-1):
        tStep[i+1]= tStep[i]+dt
            
        k1 = dt*func(y[i])
        k2 = dt*func(y[i] + 0.5*k1)
        k3 = dt*func(y[i] + 0.5*k2)
        k4 = dt*func(y[i] + 1*k3)
        y[i+1] = y[i] + (1/6)*(k1 + 2*k2 + 2*k3 + k4)
    return y,tStep
    
#t = np.arange(0,100,1e-1)
y = rk4(doublePendulum,0.2,y0,n)

print(y)

t1a=[]
t2a=[]
o1a=[]
o2a=[]
#we're going to mainly be looking at m2 
for i in range(len(y[0])):
    t2a.append(y[0][i][1])
    o2a.append(y[0][i][3])
    t1a.append(y[0][i][0])
  
#phase diagram
plt.figure()
plt.plot(t2a,o2a)
plt.xlabel("Theta Angles")
plt.ylabel("Omega Angles")
plt.show()

x=[]
y=[]
L1=1
L2=2
for i in range(len(t1a)):
    x.append(L1*np.sin(t1a[i])+L2*np.sin(t2a[i]))
    
ftx=np.fft.fft(x)
freq=np.fft.fftfreq(len(x))

plt.figure()
plt.plot(freq,ftx) 
plt.xlabel('Frequency')
plt.ylabel('Fourier Transformed Horisontal Component')

