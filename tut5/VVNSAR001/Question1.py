# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 14:35:55 2020

@author: hp
"""

import numpy as np
import matplotlib.pyplot as plt 
from scipy.special import gamma
import time
################################################################################
##Question 1 a) 
#We first want to setup our geometry
################################################################################
def volSphere(r,dim):
    return ((np.pi**(np.true_divide(dim,2))/(gamma(1+np.true_divide(dim,2))))*r**dim) #sphere volume
    
def volBox(r,dim):
    return (2*r)**dim #box volume

#Next we want to visualize the convergence of integration for the 3d case 
n = 3 #dimension of the system
nPoints = 1000 #number of points 
r = 1 #radius   
indPoint = range(1,nPoints+1)  
runs = 5 #repeated runs to test different random values
integrations = [[] for _ in range(runs)]

##Applying the typical 2-dimensional montecarlo circle to 3d 

def randDistCalculation(dim,r):
     #generating random points for each coordinate
     sumRand = 0
     randSquared = np.square(np.random.uniform(-r,r,size=dim))
     for i in range(dim):
         sumRand += randSquared[i]
     dist = np.sqrt(sumRand) #distance from the centre of sphere 
     return dist

for b in range(runs):
   integrations[b]
   for i in range(len(indPoint)):
        numHits = 0
        for j in range(indPoint[i]):
            distance = randDistCalculation(3,r)
            #we then want to check if the point lies in the sphere 
            if distance <= r:
                numHits += 1 #then one of the hits was inside the sphere
            
        vSphere = (numHits/indPoint[i])*volBox(3,r)
        integrations[b].append(vSphere)

plt.figure(0)
for i in range(runs):
    plt.plot(indPoint,integrations[i],color='red')
plt.xlabel("Number of Used Points")
plt.ylabel("Volume of Sphere (Cubed units)")
plt.legend()
plt.show()
################################################################################
##
#1b)
##
r = 1
nSamples = 10000
numHits = 0
for j in range(nSamples):
            distance = randDistCalculation(3,r)
            #we then want to check if the point lies in the sphere 
            if distance <= r:
                numHits += 1 #then one of the hits was inside the sphere
vSphere = (numHits/nSamples)*volBox(3,r) 
mu = vSphere/np.sqrt(numHits) #because we know it follows poisson so the uncertainty of nhits is just the sqrt
print(mu)
################################################################################
#1c) 
######
r = 1
uncertainties = []
volumes = []
times = []
nSamples = 10000
numHits = 0
dim = [3,4,5,6,7,8,9,10]
print("Dimension    Volume    Uncertainty    Time Elapsed")
for i in range(len(dim)):
    numHits=0
    start = time.time()
    for j in range(nSamples):
        distance = randDistCalculation(dim[i],r)
        #we then want to check if the point lies in the sphere 
        if distance <= r:
            numHits += 1 #then one of the hits was inside the sphere
    vSphere = (numHits/nSamples)*volBox(dim[i],r)
    mu = vSphere/np.sqrt(numHits)
    end = time.time()
    elTime = end-start
    print(dim[i],"      ",vSphere,"     ",mu,"     ",elTime)
    volumes.append(vSphere)
    uncertainties.append(mu)
    times.append(elTime)

plt.figure(1)
plt.plot(dim,volumes)
plt.xlabel("Dimensions")
plt.ylabel("Volume of Sphere (cubic units)")
plt.figure(2)
plt.plot(dim,uncertainties)
plt.xlabel("Dimensions")
plt.ylabel("Uncertainties of volume of sphere (cubic units)")
plt.figure(3)  
plt.plot(dim,times)  
plt.xlabel("Dimensions")    
plt.ylabel("Elapsed Time (s)")
plt.show()

    
    