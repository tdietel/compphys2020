{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.animation\n",
    "import numpy as np\n",
    "import scipy.sparse as sp\n",
    "from IPython.display import HTML"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Gaussian Wave Packet\n",
    "\n",
    "This function generates a localized wave packet with given position, width or position uncertainty, and wave number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def wavepkt(x,pos,width,k):\n",
    "\n",
    "    psi = np.sqrt(np.exp( -  ( (x-pos) / width )**2 )) * np.exp(1j*k*x)\n",
    "\n",
    "    # normalize the wave function\n",
    "    psi2 = psi*psi.conjugate()\n",
    "    psi /= np.sqrt(np.sum(psi2) * (x[1]-x[0]))\n",
    "    \n",
    "    return psi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Here we display the wave packet in the space and wave number domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "#hideme"
    ]
   },
   "outputs": [],
   "source": [
    "#hideme\n",
    "\n",
    "x = np.linspace(-10., 10., 1200)\n",
    "psi = wavepkt(x,pos=-3,width=0.1, k=-50)\n",
    "\n",
    "fig_wavepkt,ax_wavepkt = plt.subplots(1,2)\n",
    "\n",
    "#plt.figure(\"Space domain\")\n",
    "ax_wavepkt[0].plot(x,psi.real)\n",
    "ax_wavepkt[0].plot(x,psi.imag)\n",
    "ax_wavepkt[0].plot(x,(psi*psi.conjugate()).real)\n",
    "\n",
    "ax_wavepkt[0].set_xlabel(\"Position x\")\n",
    "ax_wavepkt[0].set_ylabel(\"Wave function psi\")\n",
    "\n",
    "# plt.plot(x,np.exp(-3 * (x+3)**2))\n",
    "\n",
    "w = np.fft.fftfreq(len(x),x[1]-x[0])\n",
    "f = np.fft.fft(psi)\n",
    "\n",
    "ax_wavepkt[1].plot(w,f.real)\n",
    "ax_wavepkt[1].plot(w,f.imag)\n",
    "\n",
    "ax_wavepkt[1].set_xlabel(\"Wave number k\")\n",
    "ax_wavepkt[1].set_ylabel(\"Wave function psi\")\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Solve the Schrödinger Equation\n",
    "\n",
    "$$\n",
    "  - \\underbrace{\\frac{\\hbar}{i}\\frac{\\partial \\psi(\\vec{x},t)}{\\partial t}}\n",
    "  _{\\text{time dependence}}\n",
    "  = - \\underbrace{\\frac{\\hbar^2}{2m}\\nabla^2\\psi(\\vec{x},t)}_\\text{kinetic energy}\n",
    "  +\n",
    "  \\underbrace{V(\\vec{x},t)\\,\\psi(\\vec{x},t)}_\\text{potential energy}\n",
    "$$\n",
    "\n",
    "## Discretisation\n",
    "\n",
    "\\begin{align*}\n",
    "  \\tfrac{\\partial\\psi}{\\partial t}\n",
    "  = \\frac{\\psi(x,t+\\Delta t) - \\psi(x,t)}{\\Delta t}\n",
    "  \\qquad\n",
    "  \\tfrac{\\partial^2 \\psi}{\\partial x^2}\n",
    "  = \\frac{\\psi(x-\\Delta x,t) - 2\\psi(x,t) + \\psi(x+\\Delta x,t)}{\\Delta x^2}\n",
    "\\end{align*}\n",
    "\n",
    "We rewrite using $\\psi_k(t) = \\psi(x_k,t)$, and $D = \\tfrac{\\hbar \\Delta t}{2m\\Delta x^2}$:\n",
    "\n",
    "\\begin{equation}\n",
    "  \\psi_k(t+\\Delta t)\n",
    "  = \\psi_k(t)\n",
    "  + i D \\Big( \\psi_{k-1}(\\tau) - 2\\psi_k(\\tau) + \\psi_{k+1}(\\tau) \\Big)\n",
    "  - i\\frac{\\Delta t}{\\hbar}V_k \\psi_k(\\tau)\n",
    "\\end{equation}\n",
    "\n",
    "We still have a choice where we want to evaluate the first derivative, i.e. how we chose $\\tau$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Euler Method\n",
    "\n",
    "The Euler method is the simplest choice of $\\tau$, at the last point where we calculated the wave function: $\\tau=4$. \n",
    "\n",
    "We also introduce $\\psi^{*}_k=\\psi_k(t+\\Delta t)$.\n",
    "\n",
    "$$\n",
    "  \\psi^{*}_k  =\n",
    "  \\psi_k\n",
    "  + i D \\Big( \\psi_{k-1} - 2\\psi_k + \\psi_{k+1} \\Big)\n",
    "  - \\tfrac{i\\Delta t}{\\hbar}V_k \\psi_k\n",
    "$$\n",
    "\n",
    "This could be written as a matrix equation, but it is faster to implement using numpy slices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class euler:\n",
    "    \n",
    "    def __init__(self,x,V,dt):\n",
    "        self.dx = x[1]-x[0]\n",
    "        self.D = dt / self.dx**2\n",
    "        self.V = V\n",
    "    \n",
    "    def __call__(self,psi):\n",
    "        psi[1:-1] += 1j * self.D * ( psi[0:-2] - 2*psi[1:-1] + psi[2:] )\n",
    "        # TODO: V not considered\n",
    "        return psi\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def solve(psi, solver, nstep=200, nsubstep=1):\n",
    "    psidata = np.zeros((nstep,len(psi)), dtype=complex)\n",
    "    psidata[0] = psi\n",
    "\n",
    "    for i in range(1,len(psidata)):\n",
    "        for j in range(nsubstep):\n",
    "            psi = solver(psi)\n",
    "        psidata[i] = psi\n",
    "    \n",
    "    return psidata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# %%timeit -n 1 -r 1\n",
    "\n",
    "#    print(psi[1:-1].shape)\n",
    "\n",
    "x = np.linspace(-5., 5., 500)\n",
    "V = np.zeros_like(x)\n",
    "\n",
    "psi = wavepkt(x,pos=0.0,width=0.6,k=30)\n",
    "\n",
    "psidata = solve(psi, euler(x,V, dt=1e-4), nstep=200, nsubstep=1)\n",
    "\n",
    "print(psidata.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "def animate(x,psidata,V=None):\n",
    "\n",
    "    # fig_anim, = plt.figure(\"wavefct\")\n",
    "    fig,ax = plt.subplots() #(\"wavefct\")\n",
    "    pltr, = ax.plot(x,psidata[0].real)\n",
    "    plti, = ax.plot(x,psidata[0].imag)\n",
    "    pltp, = ax.plot(x,(psidata[0]*psidata[0].conjugate()).real)\n",
    "\n",
    "    if V is not None and V.max() != 0:\n",
    "         ax.plot(x,V/V.max())\n",
    "\n",
    "    def update_plot(i):\n",
    "        #     l.set_data(t[:i], x[:i])\n",
    "        pltr.set_ydata(psidata[i].real)\n",
    "        plti.set_ydata(psidata[i].imag)\n",
    "        pltp.set_ydata( (psidata[i]*psidata[i].conjugate()).real ) \n",
    "\n",
    "    ani = matplotlib.animation.FuncAnimation(fig, update_plot, frames=len(psidata), interval=50)\n",
    "\n",
    "    return HTML(ani.to_jshtml())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "animate(x,psidata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The Backward Euler Method\n",
    "\n",
    "Evaluate $\\tfrac{\\partial^2 \\psi}{\\partial x^2}$ at time $\\tau = t+\\Delta t$:\n",
    "\\begin{equation}\n",
    "    \\psi^{*}_k  =\n",
    "    \\psi_k\n",
    "    + iD \\left(\\psi^{*}_{k-1} - 2\\psi^{*}_k + \\psi^{*}_{k+1} \\right)\n",
    "    - \\tfrac{i\\Delta t}{\\hbar}V_k \\psi^{*}_k\n",
    "\\end{equation}\n",
    "\n",
    "We separate $\\psi^{*} = \\psi(x,t+\\Delta t)$ and $\\psi = \\psi(x,t)$:\n",
    "\\begin{equation}\n",
    "  \\psi^{*}_k \\left(1  + i\\,2D + iV_k \\right)\n",
    "  -  iD \\psi^{*}_{k-1}\n",
    "  -  iD \\psi^{*}_{k+1}\n",
    "  =\n",
    "  \\psi_k\n",
    "\\end{equation}\n",
    "\n",
    "We can write this system of linear equations, using $D = \\tfrac{\\hbar \\Delta t}{2m\\Delta x^2}$:\n",
    "\n",
    "\\begin{equation}\n",
    "  \\left(\n",
    "  \\begin{array}{ccccccc}\n",
    "    1+i(2D+V_1) & -iD        & & \\\\\n",
    "    -iD        & 1+i(2D+V_2) & -iD & & \\\\\n",
    "    %0          & -iD        & 1+i(2D+V_k) & -iD & & \\\\\n",
    "    & \\ddots & \\ddots & \\ddots \\\\\n",
    "    && -iD        & 1+i(2D+V_{N-1}) & -iD \\\\\n",
    "    &&& -iD        & 1+i(2D+V_N)  \\\\\n",
    "  \\end{array}\n",
    "  \\right)\n",
    "  \\vec\\psi^{*} = \\vec\\psi\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class bweuler:\n",
    "    \n",
    "    def __init__(self,x,V,dt):\n",
    "    \n",
    "        nx = len(x)\n",
    "        dx = x[1]-x[0]\n",
    "        D = dt / dx**2\n",
    "        \n",
    "        self.A = np.zeros([nx,nx],dtype=complex)\n",
    "\n",
    "        for i in range (nx):\n",
    "            # self.A[i,i] = 2j * D + 1 \n",
    "            self.A[i,i] = 2j * D + 1 + 1j * dt * V[i] # includes potential\n",
    "            self.A[i,(i-1+nx)%nx] = -1j * D\n",
    "            self.A[i,(i+1)%nx] = -1j * D\n",
    "\n",
    "    \n",
    "    def __call__(self,psi):\n",
    "        return np.linalg.solve(self.A,psi)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# %%timeit -n 1 -r 1\n",
    "\n",
    "#    print(psi[1:-1].shape)\n",
    "\n",
    "x = np.linspace(-5., 5., 500)\n",
    "V = np.zeros_like(x)\n",
    "\n",
    "psi = wavepkt(x,pos=0.0,width=0.6,k=30)\n",
    "\n",
    "psidata = solve(psi, bweuler(x,V,dt=1e-4), nstep=200, nsubstep=1)\n",
    "\n",
    "print(psidata.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animate(x,psidata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The Crank-Nicholson Method\n",
    "\n",
    "Use mean of forward and backward Euler methods to evaluate\n",
    "$\\partial^2 \\psi/\\partial x^2$:\n",
    "\n",
    "\\begin{equation}\n",
    "  \\psi^{*}_k = \\psi_k + iD \\tfrac{\n",
    "    \\psi_{k-1} - 2\\psi_k + \\psi_{k+1} \\,+\\,\n",
    "    \\psi^{*}_{k-1} - 2\\psi^{*}_k + \\psi^{*}_{k+1}\n",
    "  }{2}\n",
    "  - \\tfrac{i\\Delta t}{\\hbar}V_k \\tfrac{\\psi_k + \\psi^{*}_k}{2}\n",
    "\\end{equation}\n",
    "\n",
    "We multiply the equation by two and sort terms starred and unstarred\n",
    "terms:\n",
    "\\begin{align*}\n",
    "  2\\psi_k^*\n",
    "  - iD &\\left(\\psi^{*}_{k-1} - 2\\psi^{*}_k + \\psi^{*}_{k+1}\\right)\n",
    "  + \\tfrac{i\\Delta t}{\\hbar}V_k \\psi^{*}_k\n",
    "  \\\\&=\n",
    "  2\\psi_k\n",
    "  + iD \\left(\\psi_{k-1} - 2\\psi_k + \\psi_{k+1}\\right)\n",
    "  - \\tfrac{i\\Delta t}{\\hbar}V_k \\psi_k\n",
    "\\end{align*}\n",
    "\n",
    "\n",
    "This is as an implicit linear equation system that we need to solve:\n",
    "$$\n",
    "\\vec{s} = \\mathbb{A}\\vec\\psi^{*} = \\mathbb{B}\\vec\\psi\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We write the left hand side in matrix form, using $C_k = 2D +\n",
    "\\tfrac{\\Delta t}{\\hbar}V_k$:\n",
    "\\begin{align*}\n",
    "  s_k &=\n",
    "  2\\psi_k^*\n",
    "  - iD \\left(\\psi^{*}_{k-1} - 2\\psi^{*}_k + \\psi^{*}_{k+1}\\right)\n",
    "  + \\tfrac{i\\Delta t}{\\hbar}V_k \\psi^{*}_k\n",
    "  \\\\&=\n",
    "  \\left( 2 + 2iD +\\tfrac{i\\Delta t}{\\hbar}V_k \\right) \\psi_k^*\n",
    "  -iD \\psi_{k-1}^*\n",
    "  -iD \\psi_{k+1}^*\n",
    "  \\\\&=\n",
    "  \\left( 2 + iC_k \\right) \\psi_k^*\n",
    "  -iD \\psi_{k-1}^*\n",
    "  -iD \\psi_{k+1}^*\n",
    "  \\\\&= \\left(\n",
    "  \\begin{array}{ccccccc}\n",
    "    2+iC_1 & -iD        & & \\\\\n",
    "    -iD  & 2+iC_2 & -iD & & \\\\\n",
    "    %0          & -iD        & 1+i(D+V_k) & -iD & & \\\\\n",
    "    & \\ddots & \\ddots & \\ddots \\\\\n",
    "    %&& -iD        & 2+iC_{L-1} & -iD \\\\\n",
    "    && -iD        & 2+iC_L  \\\\\n",
    "  \\end{array}\n",
    "  \\right)\n",
    "  \\vec\\psi^{*}\n",
    "  = \\mathbb{A}\\vec\\psi^{*}\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "And in the same way, we find for the right hand side\n",
    "\\begin{equation}\n",
    "  s_k =\n",
    "  \\left(\n",
    "  \\begin{array}{ccccccc}\n",
    "    2-iC_1 & +iD        & & \\\\\n",
    "    +iD  & 2-iC_2 & +iD & & \\\\\n",
    "    %0          & +iD        & 1+i(D+V_k) & +iD & & \\\\\n",
    "    & \\ddots & \\ddots & \\ddots \\\\\n",
    "    %&& +iD        & 2-iC_{L-1} & +iD \\\\\n",
    "    && +iD        & 2-iC_L  \\\\\n",
    "  \\end{array}\n",
    "  \\right)\n",
    "  \\vec\\psi\n",
    "  = \\mathbb{B}\\vec\\psi\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We have therefore reduced our problem to a matrix equation:\n",
    "\\begin{equation}\n",
    "  \\mathbb{A}\\psi^* = \\mathbb{B}\\psi\n",
    "  \\qquad \\Rightarrow \\qquad\n",
    "  \\psi^* = \\mathbb{A}^{-1}\\mathbb{B}\\psi\n",
    "\\end{equation}\n",
    "\n",
    "Once we have inverted the matrix $\\mathbb{A}$, the problem is\n",
    "reduced to a simple matrix multiplication. We can then calculate the\n",
    "wave function a time $\\Delta t$ in the future $\\psi^* = \\psi(x,t+\\Delta t)$\n",
    "using the current value $\\psi = \\psi(x,t)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class crank_nicholson:\n",
    "    \n",
    "    def __init__(self,x,V,dt):\n",
    "    \n",
    "        nx = len(x)\n",
    "        dx = x[1]-x[0]\n",
    "        D = dt / dx**2\n",
    "        \n",
    "        self.A = np.zeros([nx,nx],dtype=complex)\n",
    "\n",
    "        for i in range (nx):\n",
    "            # self.A[i,i] = 2j * D + 1 \n",
    "            self.A[i,i] = 2j * D + 1 + 1j * dt * V[i] # includes potential\n",
    "            self.A[i,(i-1+nx)%nx] = -1j * D\n",
    "            self.A[i,(i+1)%nx] = -1j * D\n",
    "\n",
    "        # create matrices for Crank-Nicolson method\n",
    "        A = np.zeros([nx,nx],dtype=complex)\n",
    "        B = np.zeros([nx,nx],dtype=complex)\n",
    "\n",
    "        for i in range (nx):\n",
    "            A[i,i]                = 2 + 1j * (2*D + dt*V[i])\n",
    "            if i > 0:    A[i,i-1] = -1j * D\n",
    "            if i < nx-1: A[i,i+1] = -1j * D\n",
    "\n",
    "            B[i,i]                = 2 - 1j * (2*D + dt*V[i])\n",
    "            if i > 0:    B[i,i-1] = +1j * D\n",
    "            if i < nx-1: B[i,i+1] = +1j * D\n",
    "\n",
    "\n",
    "        #print (A[0:4,0:4])\n",
    "\n",
    "    \n",
    "        # print (\"inverting matrix...\",)\n",
    "        self.M = np.dot(np.linalg.inv(A),B)\n",
    "        # print (\" done\")\n",
    "\n",
    "    \n",
    "    def __call__(self,psi):\n",
    "        return self.M.dot(psi)\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# %%timeit -n 1 -r 1\n",
    "\n",
    "#    print(psi[1:-1].shape)\n",
    "\n",
    "x = np.linspace(-5., 5., 500)\n",
    "V = np.zeros_like(x)\n",
    "\n",
    "psi = wavepkt(x,pos=0.0,width=0.6,k=30)\n",
    "\n",
    "psidata = solve(psi, crank_nicholson(x,V,dt=1e-4), nstep=200, nsubstep=1)\n",
    "\n",
    "print(psidata.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animate(x,psidata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# %%timeit -n 1 -r 1\n",
    "\n",
    "#    print(psi[1:-1].shape)\n",
    "\n",
    "x = np.linspace(-5., 5., 500)\n",
    "V = np.zeros_like(x)\n",
    "\n",
    "psi = wavepkt(x,pos=0.0,width=0.6,k=30)\n",
    "\n",
    "psidata = solve(psi, crank_nicholson(x,V,dt=3e-4), nstep=200, nsubstep=10)\n",
    "\n",
    "print(psidata.shape)\n",
    "\n",
    "animate(x,psidata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The Runge Kutta Method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class runge_kutta:\n",
    "    \n",
    "    def __init__(self, x, V, dt=1e-3):\n",
    "        \n",
    "        dd = np.ones(len(x))\n",
    "\n",
    "        LL = sp.spdiags( [ -2.0*dd, dd, dd ],\n",
    "                         [ 0, 1, -1 ],\n",
    "                         len(x), len(x) )\n",
    "\n",
    "        VV = sp.spdiags( [V], [0], len(x), len(x) )\n",
    "\n",
    "        LL /= (x[1]-x[0])**2\n",
    "\n",
    "        #self.x = x\n",
    "        #self.V = V\n",
    "        self.M = 1j* (0.5*LL - VV).tocsr()\n",
    "\n",
    "        self.dM = dt*self.M\n",
    "        \n",
    "    def __call__(self,psi):\n",
    "\n",
    "        k1 = self.dM.dot(psi)\n",
    "        k2 = self.dM.dot(psi + 0.5*k1)\n",
    "        k3 = self.dM.dot(psi + 0.5*k2)\n",
    "        k4 = self.dM.dot(psi + 1.0*k3)\n",
    "        \n",
    "        return psi + 1./6.*(k1 + 2.*k2 + 2.*k3 + k4)\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# %%timeit -n 1 -r 1\n",
    "\n",
    "#    print(psi[1:-1].shape)\n",
    "\n",
    "x = np.linspace(-5., 5., 500)\n",
    "V = np.zeros_like(x)\n",
    "\n",
    "psi = wavepkt(x,pos=0.0,width=0.6,k=30)\n",
    "\n",
    "psidata = solve(psi, runge_kutta(x,V,dt=3e-4), nstep=200, nsubstep=10)\n",
    "\n",
    "print(psidata.shape)\n",
    "\n",
    "animate(x,psidata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the space domain \n",
    "x = np.linspace(-10., 10., 1200)\n",
    "\n",
    "# Set the potential\n",
    "\n",
    "V = np.zeros_like(x)\n",
    "V[int(0.8*len(x)):int(0.82*len(x))] = 500.0\n",
    "#V = 800* np.exp( - ( (x-0.55) / 0.8 )**2 )\n",
    "#V = 0.*1./np.hypot(x,y)\n",
    "\n",
    "# Set the initial wave packet\n",
    "psi = wavepkt(x,pos=-3,width=0.6,k=50)\n",
    "\n",
    "# Run the calculation\n",
    "psidata = solve(psi, runge_kutta(x,V,dt=1e-4), nstep=300, nsubstep=20)\n",
    "\n",
    "# And animate it\n",
    "animate(x, psidata, V)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
