{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example: $\\chi^2$ Fit of a Straight Line\n",
    "==============================\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# the usual setup\n",
    "import numpy as np\n",
    "import scipy.optimize\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generate our own dataset. How this is done is not important - after all we want to extract the information in this cell from our fit. But we can later see that our extracted results are meaningful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#x = np.array( [ 0.21, 0.82, 1.66, 2.20, 2.70, 3.84, 4.32 ] )\n",
    "N = 10   # number of data points\n",
    "M = 0.4  # slope\n",
    "A = 1.1  # axis offset\n",
    "\n",
    "np.random.seed(99)\n",
    "x  = np.random.uniform( 0.0, 5.0, N )\n",
    "ey = np.random.uniform( 0.1, 1.0, N )\n",
    "y  = np.random.normal(A*x + M, ey)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's always a good idea to plot your data before you do anything else. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.errorbar(x,y,ey,0,\"ro\")\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"y\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Straight line fit to the data points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do a straight line fit to the data points, as you have probably done a million times in your lab practicals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x,a,m):\n",
    "    return a + x*m\n",
    "\n",
    "popt,pcov = scipy.optimize.curve_fit( f, x,y, sigma=ey )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fit gives you the optimal set of parameters and a covariance matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Optimal fit parameters:\", popt)\n",
    "print(\"Covariance Matrix:\",pcov)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And it also helps to plot the fit together with the data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.errorbar(x,y,ey,0,\"ro\")\n",
    "plt.plot(x,f(x,*popt))\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"y\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that scipy.optimize does a good job at fitting the straight line to the data points. But many students use this as a black box, and we want to understand better what's going on behind the scences."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The $\\chi^2$ Distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have seen in the lecture that the concept behind these fits is the $\\chi^2$ distribution. As a reminder, it is defined as\n",
    "$$\n",
    "\\sum_{i=1}^n{\\frac{(y_i-f(x_y,\\vec p))^2}{\\sigma_i^2}}\n",
    "$$\n",
    "\n",
    "We can define a function chi2 that does this calculation for us."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We use numpy.vectorize to create a function that can handle the \n",
    "# parameters a,m as numpy arrays. This is necessary when we create\n",
    "# a 2D visualization of chi2 with numpy.meshgrid below.\n",
    "\n",
    "chi2 = np.vectorize(\n",
    "    lambda a,m: np.sum( ( (y-f(x,a,m)) / ey )**2 )\n",
    ")\n",
    "\n",
    "# For scalar arguments, this is equivalent to:\n",
    "def scalar_chi2(a,m):\n",
    "    return np.sum( ( (y-f(x,a,m)) / ey )**2 )\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can try a few different values of a and m to confirm that scipy.optimize actually found the minimum of chi2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(chi2(0.285, 1.105))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(chi2(0.290,1.11))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As for the data points itself, it can be helpful to plot chi2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aa = np.linspace(0.18,0.39,60)\n",
    "mm = np.linspace(1.08,1.14,60)\n",
    "a,m = np.meshgrid(aa,mm)\n",
    "\n",
    "cc = chi2(a,m)\n",
    "\n",
    "plt.contourf(aa,mm,cc);\n",
    "plt.colorbar();\n",
    "plt.xlabel(\"a\");\n",
    "plt.ylabel(\"m\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the minimum of chi2 is where scipy.optimize found it. But we still do not know how scipy.optimize found the minimum, or how it derived the errors (given in the form of the covariance matrix). \n",
    "\n",
    "*Note: the plot can still be improved - it might be better to draw a 3D surface... Play around with different visualisations, and discuss with your classmates if you find something better. You may also create a pull request to include it into the official example.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
