{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from numpy import sin,cos\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.integrate\n",
    "import ipywidgets as widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Schrödinger Equation for a Box Potential\n",
    "\n",
    "We want to solve the Schrödinger equation numerically.\n",
    "\n",
    "$$\n",
    "  \\mathcal{H}\\,|\\psi\\rangle = E\\,|\\psi\\rangle\n",
    "  \\qquad\\text{with }\n",
    "  \\mathcal{H} = - \\frac{\\hbar^2}{2m} \\frac{d^2}{dx^2} + V(x)\n",
    "$$\n",
    "\n",
    "This example considers a box potential:\n",
    "$$\n",
    "V(x) = \n",
    "\\begin{cases}\n",
    "0 \\text{ for } 0<x<1 \\\\\n",
    "\\infty \\text{ otherwise}\n",
    "\\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the wave function vanishes everywhere except in the interval $[0,1]$, where it is described by the differential equation\n",
    "$$\n",
    "  \\mathcal{H}\\,|\\psi\\rangle \n",
    "  % = - \\frac{\\hbar^2}{2m} \\frac{d^2}{dx^2} + V(x)\\,|\\psi\\rangle\n",
    "  = - \\frac{\\hbar^2}{2m} \\frac{d^2}{dx^2}\\,|\\psi\\rangle\n",
    " = E\\,|\\psi\\rangle\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Runge-Kutta Algorithm\n",
    "\n",
    "We can solve this with the tools for first-order initial value problems. In this case, we need to introduce a dynamic variable and a generalized velocity:\n",
    "$$\n",
    "y = \\left(\\begin{array}{x} \\psi \\\\ \\psi' \\end{array}\\right)\n",
    "\\qquad\n",
    "g_E(y)\n",
    "= \\left(\\begin{array}{c} \\psi' \\\\ \\psi'' \\end{array}\\right)\n",
    "= \\left(\\begin{array}{c}\\psi' \\\\ -\\frac{2mE}{\\hbar^2}\\psi\\end{array}\\right)\n",
    "= \\left(\\begin{array}{cc} \n",
    "    0 & 1 \\\\\n",
    "    -\\frac{2mE}{\\hbar^2} & 0\n",
    "\\end{array}\\right)\\, y\n",
    "$$\n",
    "\n",
    "We will use $2mE/\\hbar^2=1$, i.e. we will measure the energy in units of $\\hbar^2/2m$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class genvelocity:\n",
    "    def __init__(self,E):\n",
    "        self.E = E\n",
    "\n",
    "    def __call__(self,t,y):\n",
    "        return np.array([y[1], -self.E*y[0]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this generalized velocity, we can actually run the integration, using e.g. the Runge-Kutta algorithm implemented in `scipy.integrate`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = genvelocity(1)\n",
    "y0 = np.array([0,1])\n",
    "\n",
    "sol = scipy.integrate.solve_ivp(g,[0,1],y0,max_step=0.01)\n",
    "plt.plot(sol.t,sol.y[0]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Runge-Kutta ensures that the Schrödinger equation is satisfied anywhere within the interval $(0,1)$, but the boundary condition $\\psi(0)=\\psi(1)=0$ is not satisfied at the upper edge. To satisfy the boundary condition, we need to adjust the energy $E$ of the particle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def draw_rk(E):\n",
    "    sol = scipy.integrate.solve_ivp(genvelocity(E),[0,1],[0,1],max_step=1e-3)\n",
    "    plt.plot(sol.t,sol.y[0]);\n",
    "        \n",
    "widgets.interact(draw_rk, E=widgets.FloatLogSlider(min=0, max=4, value=5, continuous_update=False));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def search_rk (E1,E2):\n",
    "\n",
    "    d1 = scipy.integrate.solve_ivp(genvelocity(E1),[0,1],[0,1]).y[0,-1]\n",
    "    d2 = scipy.integrate.solve_ivp(genvelocity(E2),[0,1],[0,1]).y[0,-1]\n",
    "        \n",
    "    while abs(d2) > 1e-15:\n",
    "        \n",
    "        # subsequent guesses are done via secant method\n",
    "        E3 = (E1*d2 - E2*d1) / (d2 - d1)\n",
    "        d3 = scipy.integrate.solve_ivp(genvelocity(E3),[0,1],[0,1]).y[0,-1]\n",
    "        \n",
    "        d1=d2\n",
    "        d2=d3\n",
    "        E1=E2\n",
    "        E2=E3\n",
    "        \n",
    "    return E3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E1 = search_rk(1,2)\n",
    "\n",
    "print(f\"Found eigenvalue:      E = {E1}      n = sqrt(E)/pi = {np.sqrt(E1)/np.pi}\")\n",
    "\n",
    "sol = scipy.integrate.solve_ivp(genvelocity(E1),[0,1],[0,1],max_step=0.01)\n",
    "plt.plot(sol.t,sol.y[0]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E1 = search_rk(50,60)\n",
    "\n",
    "print(f\"Found eigenvalue:      E = {E1}      n = sqrt(E)/pi = {np.sqrt(E1)/np.pi}\")\n",
    "\n",
    "sol = scipy.integrate.solve_ivp(genvelocity(E1),[0,1],[0,1],max_step=0.01)\n",
    "plt.plot(sol.t,sol.y[0]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E1 = search_rk(80,90)\n",
    "\n",
    "sol = scipy.integrate.solve_ivp(genvelocity(E1),[0,1],[0,1],max_step=0.01)\n",
    "plt.plot(sol.t,sol.y[0]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Implementation with the Numerov Method\n",
    "\n",
    "The Runge-Kutta method requires that we transscribe the Schrödinger equation as a first order ODE by explicitly introducing the first derivative of the wave function as a component of a dynamical variable. In quantum mechanics, this first derivative $\\psi'$ does not have a physical meaning, and its introduction is quite artificial.\n",
    "\n",
    "Alternatively, we can use the Numerov method, that allows us to integrate the second order ODE directly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def numerov(x,q,s):\n",
    "\n",
    "    u = np.zeros(len(q))\n",
    "    u[0] = 0\n",
    "    u[1] = x[1]-x[0]\n",
    "\n",
    "    for i in range(1,len(u)-1):\n",
    "        g = (x[i]-x[i-1])**2 / 12.\n",
    "        c0 = 1 + g * q[i-1]\n",
    "        c1 = 2 - 10 * g * q[i]\n",
    "        c2 = 1 + g * q[i+1]\n",
    "        d  = g * (s[i+1] + s[i-1] + 10*s[i])\n",
    "        u[i+1] = ( c1*u[i] - c0*u[i-1] + d ) / c2\n",
    "\n",
    "    return u"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can verify that the numerov method yields the same wave functions as the Runge-Kutta method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def draw_num(E):\n",
    "    x = np.linspace(0,1,1000)\n",
    "    q = E*np.ones_like(x)\n",
    "    s = np.zeros_like(x)\n",
    "\n",
    "    psi = scipy.integrate.solve_ivp(genvelocity(E),[0,1],[0,1],max_step=1e-3)\n",
    "    plt.plot(x,psi);\n",
    "        \n",
    "widgets.interact(draw_rk, E=widgets.FloatLogSlider(min=0, max=4, value=5, continuous_update=False));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
