#!/usr/bin/env python3

import numpy as np
import ipywidgets as widgets
import matplotlib.pyplot as plt

class grid:


    def __init__(self, shape, n=1, neighbors=None):

        self.spins = np.zeros([n] + [i for i in shape], dtype=bool)
        self.nnup = np.zeros_like(self.spins,dtype=int)

        if neighbors is None:
            self.neighbor_pattern = grid.calculate_next_neighbors(shape)
        else:
            self.neighbor_pattern = neighbors

        self.randomize(0.5)

    def calculate_next_neighbors(shape):

        nneighbors = 2 * len(shape)

        neighbors = np.zeros((nneighbors,len(shape)+1), dtype=int)

        for dim in range(len(shape)):
            neighbors[2*dim:2*dim+2,dim+1] = (-1,1)

# for i in range(1,len(shape)+1):
#             for j,v in enumerate((-1,1)):
#                 # print(i,j,v)
#                 neighbors[2*i+j,i] = v

#         print(neighbors)
        return neighbors

    def randomize(self,p=0.5):
        self.spins = ( np.random.uniform(0,1,size=self.spins.shape) < p )
        self.build_indices()

    def build_indices(self):

        it = np.nditer(self.nnup, flags=['multi_index'],op_flags=['writeonly'])

        for n in it:
            # retrieve the spins of the neighbors
            spins = [self.spins[n] for n in self.neighbors(it.multi_index) ]

            # for some reason, the 'writeonly' flag does not seem to work,
            # so we fill the array explicitly
            self.nnup[it.multi_index] = np.sum(spins)


    def count_up_down(self):
        nup = np.sum(self.spins, axis=tuple(range(1,len(self.spins.shape))))
        ndown = self.spins[0].size - nup
        return nup,ndown


    def count_aligned_misaligned(self):

        misaligned = np.zeros( (self.spins.shape[0]), dtype=int )

        it = np.nditer(self.spins, flags=['multi_index'])

        for si in it:
            i = it.multi_index

            for j in self.neighbors(i):
                sj = self.spins[j]

                # we use XOR for fast checking of alignment
                misaligned[i[0]] += si^sj

        misaligned = misaligned/2
        aligned = 2*self.spins[0].size - misaligned

        return aligned,misaligned

    def neighbors(self,pos):
        nb = np.mod(self.neighbor_pattern + np.array(pos,dtype=int), self.spins.shape )
        return [tuple(i) for i in nb]


    def run(self,w,nsteps,nflip):

        nupdate = 0

        # We create an array for the random numbers. The rows are the
        # bits to be tested for flipping. The first column is the index
        # of the grid, and the other columns are the indices within the
        # grid.

        # We want to generate nflip numbers per grid, i.e. the number
        # of rows has to be the nflip times the number of grids.
        nrows = nflip * self.spins.shape[0]

        pos = np.zeros( (nrows, len(self.spins.shape)), dtype=int)

        # The first component of each row is the grid. This is not
        # random, and we fill it once
        for i in range(self.spins.shape[0]):
            pos[i*nflip:(i+1)*nflip, 0] = i

        # The random part will be filled in the loop, here we just
        # determine the shape of the random array
        genshape = self.spins.shape[1:]

        for i in range(nsteps):

            # create nflip positions per grid at once
            pos[:,1:] = np.random.randint( genshape, size=[nrows,len(genshape)] )

            # for each grid position, calculate the index in the probability (w) array
            idx = [self.spins[tuple(p)] + 2*self.nnup[tuple(p)] for p in pos]

            # select a random number for each grid position
            x = np.random.uniform(0,1,size=nrows)

#             print(pos)
#             print(idx)
#             print(w[idx]>1)

            sel = [ tuple(p) for p in pos[w[idx] > x] ]

            for p in sel:

                if self.spins[p]:
                    self.spins[p] = False
                    for n in self.neighbors(p):
                        self.nnup[n] -= 1
                else:
                    self.spins[p] = True
                    for n in self.neighbors(p):
                        self.nnup[n] += 1

                #                 print()
                nupdate += 1
#         print(nupdate)



class model:


    def __init__(self,shape=(4,4),ngrids=1,J=1,muB=0.0,T=0.1):

        self.muB = muB
        self.T = T
        self.J = J

        self.grid = grid(shape, n=ngrids)



    def calculate_flip_probabilities(self):

        maxneighbors = len(self.grid.neighbor_pattern)

        w = np.zeros( (2 * (maxneighbors+1)) )

        for spin in [True,False]:
            for nnup in range(maxneighbors+1):
                idx = 2*nnup + spin
                w[idx] = flip_probability(spin,nnup, self.J,self.muB, self.T)

        return w

    def energy(self):

        # for the interaction energy, we need to know how many neighbors are
        # (mis)aligned

        aligned,misaligned = self.grid.count_aligned_misaligned()

        # to calculate the energy in the external B field, we only need
        # numbers of up,down spins
        nup,ndown = self.grid.count_up_down()

        return -self.J*(aligned-misaligned) - self.muB*(nup-ndown)

    def magnetisation(self):
        nup,ndown = self.grid.count_up_down()
        return (nup-ndown) / self.grid.spins[0].size

    def run(self,nsteps,nflip):

        w = self.calculate_flip_probabilities()

#         print(w)

        self.grid.run(w, nsteps, nflip)

    def ramp_T(self, T, nsamples=20, nstep=500, nflip=300,
               verbose=False, plot=True):

        ngrid = self.grid.spins.shape[0]
        TT = np.linspace(self.T,T,nsamples)
        MM = np.zeros((ngrid,len(TT)))
        EE = np.zeros((ngrid,len(TT)))

        for i,t in enumerate(TT):

            if verbose:
                print(f"T = {t:6.3f}", end="  ")

            self.T = t
            self.run(1000,200)

            MM[:,i] = self.magnetisation()
            EE[:,i] = self.energy()

            if verbose:
                print(f"<M> = {np.average(MM[:,i]):6.3f}", end="  ")
                print(f"<E> = {np.average(EE[:,i]):.1f}")


        if plot:
            plot_M_E(TT,MM,EE,"temperature $T$ ($k_B J$)")
        return TT,MM,EE

def delta_energy(spin, nnup, J, muB):
    # spin flips up -> down
    # (all signs inverted for spin down -> up)

    # spin: original spin at position
    # nnnup: number of next neighbor spins in up

    # energy from -J*s[i]*s[j]
    # old state:   - J * nnup + J * nndown
    # new state:   + J * nnup - J * nndown
    # difference:  2J * nnup - 2J * nndown = 2J(2nnup-4) = 4J(nnup-2)

    # energy from -mu*B*s[i]
    # old state:  -mu*B
    # new state:  +mu*B
    # difference: +2*mu*B

    if spin:
        return +( J * (4*(nnup-2)) + 2*muB )
    else:
        return -( J * (4*(nnup-2)) + 2*muB )

def flip_probability(spin, nnup, J, muB, T):

    denergy = delta_energy(spin, nnup, J, muB)
    w = np.exp( - denergy / T)

    return w


def animate_sequence(framedata):

    def show_frame(i):
        return plt.imshow(framedata[i], vmin=0,vmax=1)

    print(len(framedata))

    play = widgets.Play( value=0, min=0, max=len(framedata)-1, step=1,
    interval=500,  continuous_update=False,
                         description="Press play", disabled=False )

    slider = widgets.IntSlider(value=0, min=0, max=len(framedata)-1, step=1, continuous_update=False)

    widgets.jslink((play, 'value'), (slider, 'value'))
    # display(widgets.HBox([play, slider]))

    display(play)
    # display(slider)

    return widgets.interact(show_frame, i=slider,  continuous_update=False)

def plot_M_E(x,MM,EE,xlabel=""):
    fig, (axM,axE) = plt.subplots(1,2,figsize=(12, 5))

    for M in MM: axM.plot(x,M)
    for E in EE: axE.plot(x,E)

    if xlabel is not None:
        axM.set_xlabel(xlabel)
        axE.set_xlabel(xlabel)

    axM.set_ylabel("magnetization $M$ ")
    axE.set_ylabel("energy $E$ ($J$)");
