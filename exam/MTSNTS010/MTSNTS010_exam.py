#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: ntshembho
"""

import numpy as np
import matplotlib.pyplot as plt
import json

' ' ' Part 1 ' ' '
# Values used



def Q1():
    
    #defining constants
    K = 223 #K, T/m
    mp = 1.6727e-27 #kg, mass of proton
    q = 1.602e-19 #C,charge of proton
    c = 299792458 #m/s
    E = 7e12 #eV
    L = 3.1 #m, length of vessel

    
    #velocities/momentum
    pz = E*q/c
    x0 = 0.001 #m
    y0 = 0 #m
    z0 = 0 #m
    vx0 = 0
    vy0 = 0
    vz0 = np.sqrt((pz/mp)**2/(1 + (pz/mp)**2/c**2))
    r0 = [x0,y0,z0,vx0,vy0,vz0]

    #time
    tf = L/vz0
    h = 1000
    t = np.arange(0,tf,tf/h)

    #solving ODE 
    def f(r0):
#        y = [x,y,z,vx,vy,vz]
        x = r0[0]
        y = r0[1]
        z = r0[2]
        vx = r0[3] 
        vy = r0[4] 
        vz = r0[5]
        gamma = np.sqrt(1-(vx**2+vy**2+vz**2)/c**2)
        a = q*K*gamma*vz/mp
        ax = -a*x
        ay = a*y
        az = 0*z
        
        dxdt = vx
        dydt = vy
        dzdt = vz
        dvxdt = ax
        dvydt = ay
        dvzdt = az
        
        return np.array([dxdt,dydt,dzdt, dvxdt,dvydt,dvzdt])
    
    #using RK4 method for solving ODE
    def rk4(t,r0):
        x = []
        y = []
        z = []
        vx = []
        vy = []
        vz = []
        x.append(r0[0])
        y.append(r0[1])
        z.append(r0[2])
        vx.append(r0[3])
        vy.append(r0[4])
        vz.append(r0[5])       
        
        for i in range (len(t)-1):   
        
            dt = t[i+1] - t[i]
    
            k1 = f(r0)
            k2 = f(r0 + dt*0.5*k1)
            k3 = f(r0 + dt*0.5*k2)
            k4 = f(r0 + dt*1.0*k3) 
            
            R = dt*1/6*( k1 + 2*k2 + 2*k3 + k4)
            
            r0[0]+= R[0]
            r0[1]+= R[1]
            r0[2]+= R[2]
            r0[3]+= R[3]
            r0[4]+= R[4]
            r0[5]+= R[5]
            
            x.append(r0[0])
            y.append(r0[1])
            z.append(r0[2])
            vx.append(r0[3])
            vy.append(r0[4])           
            vz.append(r0[5]) 
            
        return np.array([x,y,z,vx,vy,vz])
    
    x,y,z,vx,vy,vz =rk4(t,r0)


    #extracting final values
    xfinal = x[-1]
    yfinal = y[-1]
    zfinal = z[-1]

    
    def v_to_pGev(v):
        gamma = 1/np.sqrt(1-(vx[-1]**2+vy[-1]**2+vz[-1]**2)/c**2)
        p = gamma*mp*v*(c/q)*10e-9
        return p
    
    pxfinal = v_to_pGev(vx[-1])
    pyfinal = v_to_pGev(vy[-1])
    pzfinal = v_to_pGev(vz[-1])
    
    # plotting trajectories
    def deltax():
        xrange = np.linspace(0.0002,0.01,5)
        zrange = np.linspace(0,3.1,1001)

        x = []
        y = []
        for i in xrange:
            x1, y1, z, vx, vy, vz = rk4(t,[i,0.0,0.0,0.0,0.0,vz0])
            x.append(x1)
            y.append(y1)

        fig,axs = plt.subplots(2,2)
        axs[0,0].plot(zrange*1e3,x[4]*1e3)
        axs[0,1].plot(zrange*1e3,x[3]*1e3)
        axs[1,0].plot(zrange*1e3,x[2]*1e3)
        axs[1,1].plot(zrange*1e3,x[0]*1e3)
        plt.xlabel('z [mm]')
        plt.ylabel('x [mm]')
        plt.savefig('xtrag',dpi=300)
        plt.show()
   
    def deltay():
        yrange = np.linspace(0.0002,0.01,5)
        zrange = np.linspace(0,3.1,1001)
        x = []
        y = []
        for i in yrange:
            x1, y1, z, vx, vy, vz = rk4(t,[0.0,i,0.0,0.0,0.0,vz0])
            x.append(x1)
            y.append(y1)
            
        fig,axs = plt.subplots(2,2)
        axs[0,0].plot(zrange*1e3,y[4]*1e3)
        axs[0,1].plot(zrange*1e3,y[3]*1e3)
        axs[1,0].plot(zrange*1e3,y[2]*1e3)
        axs[1,1].plot(zrange*1e3,y[0]*1e3)
        plt.xlabel('z [mm]')
        plt.ylabel('y [mm]')
#        plt.savefig('ytrag',dpi=300)
        plt.show()
        
    def deltaAngles():

        vxrange = np.linspace(-100,100,5) #np.cos(theta)
        vyrange = np.linspace(-100,100,5)#np.sin(theta)
        zrange = np.linspace(0,3,5)
        x = []
        y = []
        z = []
        for i in vxrange:
            for j in vyrange:
                for k in zrange:
                    x1, y1, z1, vx, vy, vz = rk4(t,[0.0,0.0,k,i,j,vz0])
                    x.append(x1)
                    y.append(y1)
                    z.append(z1)
            

        plt.plot(z[4]*1e3,y[4]*1e3)
        plt.plot(z[3]*1e3,y[3]*1e3)
        plt.plot(z[2]*1e3,y[2]*1e3)
        plt.plot(z[1]*1e3,y[1]*1e3)
        plt.xlabel('z [mm]')
        plt.ylabel('y [mm]')
#        plt.savefig('ytrag',dpi=300)
        plt.show()

        plt.plot(z[4]*1e3,x[4]*1e3)
        plt.plot(z[3]*1e3,x[3]*1e3)
        plt.plot(z[2]*1e3,x[2]*1e3)
        plt.plot(z[1]*1e3,x[1]*1e3)
        plt.xlabel('z [mm]')
        plt.ylabel('x [mm]')
#        plt.savefig('xtrag',dpi=300)
        plt.show()
        

    trig1 = deltax()
    trig2 = deltay()
    trig3 = deltaAngles()   
    
    data = [xfinal,yfinal,zfinal,pxfinal,pyfinal,pzfinal]
    
    return data
    

#trig = Q1()    
    
    
def Q2():
    
   #defining constants
   N = 48 #cables
   B = 1 ##guess at number of bundles
   L = 3.1 #m
   I = 11870 #Amps/cable
   ri = 0.056 #m, inner apatrue
   ro = 0.118 #m, ourter apature
#   A = np.pi*ro**2-np.pi*ri**2
   area = np.pi/3 #angle taken up by each bundle
   mu0 = 4*np.pi*1e-7
   d = 5

    # Geometry using cylindrical coordinates
   def radii(angle):
        p = np.linspace(angle-np.pi/6,angle+np.pi/6,d)
        x = []
        y = []
        r = np.linspace(ri,ro,d)
        for i in range(len(r)):
            for j in range(len(p)):
                x.append(r[i]*np.cos(p[j]))
                y.append(r[i]*np.sin(p[j]))
            
        return np.array([x,y])
    
   #creating arrays for the different magnetic segments
   s1 = radii(2*np.pi*0)
   s1x = s1[0].flatten()
   s1y = s1[1].flatten()

   s2 = radii(np.pi/2)
   s2x = s2[0].flatten()
   s2y = s2[1].flatten()
   
   s3 = radii(np.pi)
   s3x = s3[0].flatten()
   s3y = s3[1].flatten()
   
   s4 = radii(3/2*np.pi)
   s4x = s4[0].flatten()
   s4y = s4[1].flatten()

#   print("s4",s4)
   xrange = np.linspace(-0.2,0.2,d**2)
   yrange = np.linspace(-0.2,0.2,d**2) 
   
   #rest of coordinates
   def coord():
       xrange = np.linspace(-0.2,0.2,d**2)
       yrange = np.linspace(-0.2,0.2,d**2)
       r = np.linspace(0,ro,d)
       x = []
       y = []
       for i in range(len(xrange)):
           for j in range(len(yrange)):
               for k in range(len(r)):
                   if np.sqrt(xrange[i]**2+yrange[j]**2)<r[k]:
                       x.append(xrange[i])
                       y.append(yrange[j])
    
       return np.array([x,y])
   
   xlist,ylist = coord()

   #plotting configuration
   
   plt.plot(s1[0],s1[1],'.')
   plt.plot(s2[0],s2[1],'.')
   plt.plot(s3[0],s3[1],'.')
   plt.plot(s4[0],s4[1],'.') 
#   plt.plot(XY[0],XY[1])
   plt.xlabel('x [m]')
   plt.ylabel('y [m]')
   plt.savefig('set up',dpi=300)
   plt.show()

   #using Biot-Savat

   def Bpos(x,y,sx,sy): #sx, sy points on the magnets

       Bmag = (mu0/2*np.pi)*(I/(np.sqrt((sx-x)**2+(sy-y)**2))) 
       Bx = Bmag*(-np.sin(np.arctan2(sy - y, sx - x)))
       By = Bmag*np.cos(np.arctan2(sy - y, sx - x))

       return np.array([Bx,By])
   
   def Bneg(x,y,sx,sy):
       Bmag = (mu0/2*np.pi)*(-I/(np.sqrt((sx-x)**2+(sy-y)**2))) 
       Bx = Bmag*(-np.sin(np.arctan2(sy - y, sx - x)))
       By = Bmag*np.cos(np.arctan2(sy - y, sx - x))

       return np.array([Bx,By])    
       
   def Btot(x,y): #adding the different magnet components
       return np.array([np.sum(Bpos(x,y,s2x,s2y)[0]+Bpos(x,y,s4x,s4y)[0] + Bneg(x,y,s1x,s1y)[0] + Bneg(x,y,s3x,s3y)[0]),np.sum(Bpos(x,y,s2x,s2y)[1]+Bpos(x,y,s4x,s4y)[1] + Bneg(x,y,s1x,s1y)[1] + Bneg(x,y,s3x,s3y)[1])])
   
   
    #solving the magnetic field for the whole area
   def solution():
        Bx=np.zeros((len(xrange),len(xrange)))
        By=np.zeros([len(xrange),len(xrange)])
        
        for i in range(len(xrange)):
            for j in range(len(xrange)):
                Bx[i][j]=Btot(xrange[i],yrange[j])[0]
                By[i][j]=Btot(xrange[i],yrange[j])[1]
            
        return Bx,By
    
   Bx,By = solution()
   
   #plotting the magnetic field
   def magplot():
       plt.streamplot(xrange,yrange,Bx,By,color = 'k')
       plt.ylabel('y [m]')
       plt.xlabel('x [m]')
       plt.savefig('magfield',dpi=300)
       plt.show()
   b = magplot()
   
   #looking at the ideal field
   def idealfield(x,y):
       K=223
       Bx = -x*K
       By = y*K
       return np.array([Bx,By])
   
   def ideal_plot():
       Bxnew = np.zeros([len(xrange),len(xrange)])
       Bynew = np.zeros([len(xrange),len(xrange)])
       
       for i in range(len(xrange)):
           for j in range(len(yrange)):
               Bxnew[i][j] = idealfield(xrange[i],yrange[j])[0]
               Bynew[i][j] = idealfield(xrange[i],yrange[j])[1]
               
       plt.streamplot(xrange,yrange,Bxnew,Bynew,color = 'm')
       plt.ylabel('y [m]')
       plt.xlabel('x [m]')
       plt.savefig('ideal magfield',dpi=300)
       plt.legend()
       plt.show()
 

#   c = ideal_plot()
   return Btot(0.001,0.0)
       
#Q = Q2()

##Dictionary 
a = Q1()
xandp = {'x':a[0],'y':a[1],'z':a[2],'px':a[3],'py':a[4],'pz':a[5]}

e = Q2()
B = {'Bx':e[0], 'By':e[1], 'Bz':0}

one = {'b':xandp,'e':B}
data = {'1': one}

#with open('solutions.json','w') as f:
#    json.dump(data,f)

with open('exam/MTSNTS010/solutions.json','w') as f:
    json.dump(data,f)
