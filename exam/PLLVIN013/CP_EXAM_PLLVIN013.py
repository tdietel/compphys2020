import numpy as np
import matplotlib.pyplot as plt
import json
import itertools
from scipy.integrate import solve_ivp
import collections
from mpl_toolkits.mplot3d import Axes3D

#Units in SI - kg, m, m/s, eV, C etc. - need to convert
mass_proton = 1.67262192369e-27 #kg
length = 3.10 #m
charge = 1.602176634e-19 #C
c = 299792458.0 #m/s
p_beam = (7.00e12*(charge)) #beam in J 
field_grad = 223. #Tm^(-1)
momemtum_z = (p_beam/c) #units - j/m^(-2)

def quadrupole():
    def rela_momentum_speed(m, p):
        v  = np.sqrt(((p/m)**2)/(1 + ((p/m)**2)/c**2))
        return v
    
    def lorentz(vx, vy, vz):
        gamma = 1/(np.sqrt(1 - (vx**2 + vy**2 + vz**2)/c**2))
        return gamma
    
    #we have some initial velocity z - relativistic - in terms of momentum
    vx_i,vy_i,vz_i=0,0,rela_momentum_speed(mass_proton,momemtum_z)#driven by some momentum

    #initial position
    x_i,y_i,z_i = 0.001,0.,0.
    gamma_i = lorentz(x_i, y_i, z_i)
    x_range = np.arange(0.0, 0.1 + 0.001, 0.001)
    y_range = np.arange(0.0, 0.1 + 0.001, 0.001)
    z_range = np.arange(0.0, 0.1 + 0.001, 0.001)
    
    #defining some time interval
    Time_elap_i = length/vz_i
    n_steps = 10**3
    interval = Time_elap_i/n_steps
    print('Elapsed time', Time_elap_i)
    
    def force_due_to_quad(t,r):
        x,y,z,vx,vy,vz = r[0],r[1],r[2],r[3],r[4],r[5]
        gamma = 1/(lorentz(vx,vy,vz))
        k_grad = (charge*p_beam*vz_i*gamma)/(mass_proton)
        dydt = vy
        dxdt = vx
        dvxdt = -(k_grad)*x
        dvydt = (k_grad)*y
        dzdt = vz_i
        dvzdt = 0.0
        return np.array([dxdt, dydt, dzdt, dvxdt, dvydt, dvzdt])
    
    t_range = np.arange(0.0, Time_elap_i + interval, interval)
    solution = solve_ivp(force_due_to_quad, (0.0, Time_elap_i), [x_i, y_i, z_i, vx_i, vy_i, vz_i], t_eval=t_range, method="RK45")
    x, y, z, vx, vy, vz = solution.y
    
    #Defining the gamma and momentum again for final x and p (1b)
    gamma_final = lorentz(vx[n_steps], vy[n_steps], vz[n_steps])
    new_gamma = (1/gamma_final)*mass_proton
    
    #for 1b) - solutions for x and p
    final_v = [vx[n_steps], vy[n_steps], vz[n_steps]]
    global final_x
    global final_p
    final_x = [x[n_steps], y[n_steps], z[n_steps]]
    final_p = (new_gamma*vx[n_steps])*6.242e9, (new_gamma*vy[n_steps])*6.242e9, (new_gamma*vz[n_steps])*6.242e9
    initial_p = (gamma_i*vx[n_steps]), (gamma_i*vy[n_steps]), (gamma_i*vz[n_steps])
    
    #print('Final velocity',final_v)
    
    #For question c plots
    c_i = plt.figure()
    for i in x_range:
        solution = solve_ivp(force_due_to_quad, (0.0, Time_elap_i), [i, 0.0, z_i, vx_i, vy_i, vz_i], t_eval = t_range, method="RK45")
        x, y, z, vx, vy, vz = solution.y
        ax = c_i.gca(projection='3d')
        ax.plot(x,y, t_range)
        plt.xlabel('x(m)')
        plt.ylabel('y(m)')
        ax.set_zlabel('interval(s)')
        #plt.savefig('xy.png', dpi = 100)
    plt.show()
    
    c_ii = plt.figure()
    for j in y_range:
        solution = solve_ivp(force_due_to_quad, (0.0, Time_elap_i), [0.0, j, z_i, vx_i, vy_i, vz_i], t_eval = t_range, method="RK45")
        x, y, z, vx, vy, vz = solution.y
        ax = c_ii.gca(projection='3d')
        ax.plot(t_range, x,y)
        plt.xlabel('interval(s)')
        plt.ylabel('x(m)')
        ax.set_zlabel('y(m)')
        #plt.savefig('tx.png', dpi = 100)
    plt.show()
    
    c_iii = plt.figure()
    for k in z_range:
        solution = solve_ivp(force_due_to_quad, (0.0, Time_elap_i), [0.0, 0.0, k, -0.5, 0.5, vz_i], t_eval = t_range, method="RK45")
        x, y, z, vx, vy, vz = solution.y
        ax = c_iii.gca(projection='3d')
        ax.plot(x,y,z, color = 'k')
        plt.xlabel('x(m)')
        plt.ylabel('y(m)')
        ax.set_zlabel('z(m)')
        #plt.savefig('xyz.png', dpi = 100)
    plt.show()
    
    plt.figure()
    plt.plot(x, z, color = 'k')
    plt.xlabel('x(m)')
    plt.ylabel('z(m)')
    plt.show()
quadrupole() 


plt.show()
#final position and momentum for questions 1b
print('Final position = ',final_x)
print('Final momentum = ', final_p)

#Second part of the question - incomplete
length = 3.10 #m
current = 11870. #A
negative_current = -1*(current)
bundle = 48 
n = 20

def actual_quad():
    a_position = np.linspace(-0.2,0.2,n)
    b_position = np.linspace(-0.2,0.2,n)
    a,b = np.meshgrid(a_position,b_position)
    
    mu_0 = 1.25663706212e-6
    def biot_savart_law(x,y,a,b): 
        mag_B = (mu_0)/((2*np.pi)*(current/(np.sqrt((a-x)**2 + (b-y)**2))))
        b_y = mag_B*(np.cos(np.arctan2(b-y, a-x)))
        b_x = mag_B*(-np.sin(np.arctan2(b-y, a-x)))
        return mag_B,b_x, b_y    
    global mag_x_y_coordinate
    mag_x_y_coordinate = biot_savart_law(0,0,0.001,0) #for a single beam - without geometry
actual_quad()   
print("Magnetic field mag, x and y for single wire ",mag_x_y_coordinate)
    
def wire_bundle(r, n):
    wire = []
    for r, n in zip(r, n):
        t = np.linspace(np.pi/2 - np.pi/6, np.pi/2 + np.pi/6, n)
        x = r * np.cos(t)
        y = r * np.sin(t)
        wire.append(np.c_[x, y])
    return wire

width = 5.335
radius = np.linspace(0.056,0.118,width)
n = [9+i for i in range(len(radius))]
wire = wire_bundle(radius, n)

#changing up the variables
def values_xyz(a):
    x1,y1 = [],[]
    for i in range(len(wire)):
        x2,y2 = zip(*wire[:][i])
        x1.append(x2)
        y1.append(y2)

    x3,y3 = [],[]
    for i in range(len(wire)): #extraction of coordinates of wires
        x4,y4 = np.array(x1[i]), np.array(y1[i])
        x3.append(x4) #update 
        y3.append(y4)

    x3,y3 = np.array(x3),np.array(y3)
    return [x3,y3]
x3,y3 = values_xyz(wire)

#in order to extract our values as 1d list
def single_array(x):
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in single_array(i)]
    else:
        return [x]
#the final values to plot
x6,y6 = single_array(x3),single_array(y3)

#rotating each segment s.t we make up our quadrupole - about the origin
def simple_rotation(x,y,radians):
    start = []
    end = []
    ox, oy = origin
    for i in range(len(x)):
        qx = ox + np.cos(radians)*x[i] + np.sin(radians)*y[i]
        qy = oy - np.sin(radians)*x[i] + np.cos(radians)*y[i]
        start.append(qx)
        end.append(qy)
    return [start, end]
origin = (0,0)

#rotating each segment - plotting as 1 quadrupole
x1,y1 = simple_rotation(x6,y6,0)
x2,y2 = simple_rotation(x6,y6,np.pi/2)
x3,y3 = simple_rotation(x6,y6,np.pi)
x4,y4 = simple_rotation(x6,y6,3*np.pi/2)

fig, ax = plt.subplots()

#grouping the negative and positive quad sects
x_negative,y_negative = np.append(x1,x3),np.append(y1,y3)
x_positive,y_positive = np.append(x2,x4),np.append(y2,y4)


plt.scatter(x_negative,y_negative, label = 'Positive current')
plt.scatter(x_positive,y_positive, label = 'Negative current')
ax.set_aspect('equal')

mag_x = np.append(x_negative,x_positive)
mag_y = np.append(y_negative,y_positive)

x = np.linspace(-0.120,0.120,20)
y = np.linspace(-0.120,0.120,20)
z = 0

x,y,z = np.meshgrid(x,y,z)

def magnetic_field(x,y):
    current = 11870.  #Amps in the wire
    mu_0 = 1.25663706212e-6
    global mag_x
    global mag_y
    x1,y1 = mag_x,mag_y
    bx1 = []
    by1 = []
    bz1 = []
    for i in range(len(mag_x)): #Magnetic constant   
        if i < 109:
            mag = (mu_0/(2*np.pi))*(current/np.sqrt((x-x1[i])**2+(y-y1[i])**2)) #Magnitude of the vector B
            by = mag * (np.cos(np.arctan2(y-y1[i],x-x1[i])))            #By
            bx = mag * (-np.sin(np.arctan2(y-y1[i],x-x1[i])))           #Bx
            bz = z*0
            #bx1 += bx
            bx1.append(bx)
            by1.append(by)
            bz1.append(bz)       
        elif i >= 109:
            mag = (mu_0/(2*np.pi))*(-current/np.sqrt((x-x1[i])**2+(y-y1[i])**2)) #Magnitude of the vector B
            by = mag * (np.cos(np.arctan2(y-y1[i],x-x1[i])))            #By
            bx = mag * (-np.sin(np.arctan2(y-y1[i],x-x1[i])))           #Bx
            bz = z*0
            #bx1 += bx
            bx1.append(bx)
            by1.append(by)
            bz1.append(bz)#Bz 
    bx2 = 0
    by2 = 0
    for i in range(len(bx1)):
        #    print(bx[i])
        bx2 += bx1[i]
        by2 += by1[i]
    bx2 = np.array(bx2)
    by2 = np.array(by2)
    bx3 = single_array(bx2)
    by3 =single_array(by2)
    return bx3,by3,bz1

bx,by,bz = magnetic_field(x,y)

plt.figure()
plt.quiver(x,y,bx,by, color = 'k', label = 'Magnetic field')
plt.scatter(x_negative,y_negative, color = 'gold', label = 'Positive current')
plt.scatter(x_positive,y_positive, color = 'slateblue', label = 'Negative current')
plt.xlabel('x(m)')
plt.ylabel('y(m)')
plt.legend(loc=1)


plt.show()


solutions = {}

def question_1():
    print("Question 1")
    q1 = {}
    q1b = {}
    q1e = {}
    q1b.update({"x" : float(final_x[0])})
    q1b.update({"y" : float(final_x[1])})
    q1b.update({"z" : float(final_x[2])})
    q1b.update({"px" : float(final_p[0])})
    q1b.update({"py" : float(final_p[1])})
    q1b.update({"pz" : float(final_p[2])})
    q1e.update({"Bx" : float(np.sum(bx))})
    q1e.update({"By" : float(np.sum(by))})
    q1e.update({"Bz" : float(np.sum(bz))})
    q1.update({"b": q1b})
    q1.update({"e" : q1e})
    solutions.update({"1" : q1})

def write_file():
    with open('exam/PLLVIN013/solutions.json', 'w') as f:
        json.dump(solutions,f)

question_1()
write_file()