# Part 1 of exam: trajectories of proton through quadrupole field
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

savepath = 'exam/CLYHAN001/'
# savepath = ''

# problem setup and constants (taken from wikipedia)
E0 = 7e12   # proton beam energy [eV]
K = 223     # field gradient [T/m]
c = 3e8             # speed of light [m/s]
m0 = 938.272088e6   # rest mass of proton [eV/c2]
m_proton = 1.672621923e-27    # mass of proton [kg]
p_conversion = 5.344286e-28     # conversion factor between eV/c and kg.m/s 
e = 1.602176634e-19     # proton charge [C]
maxZ = 1.55   #[m]
pipe_radius = 28e-3     # radius of beampipe
dt = 1e-12

# gamma factor from proton energy
def gamma(E):
    return E/m0

# momentum in eV/c to velocity in m/s
def momentum_to_velocity(E):
    gam = gamma(E)
    p = E*p_conversion      # convert momentum in eV/c to kg.m/s
    v = p/(m_proton*gam)

    return v

# convert velocity in m/s to momentum in GeV/c
def velocity_to_momentum(v):
    # gam = gamma_v(v)
    gam = gamma(E0)
    p = gam*m_proton*v
    p = p/p_conversion
    p = p*1e-9
    return p

# Fourth order Runge Kutta 
def rk4(func,dt,y0):
    y = [y0]
    t = 0

    # break when particle leaves magnetic field (at end of beampipe at z=1.55m, or side of beampipe (r>28mm))
    while y[-1][2]<maxZ and t/dt<1e6 and (np.sqrt(y[-1][0]**2 + y[-1][1]**2)<pipe_radius):
        k1 = dt * func( t          , y[-1]          )
        k2 = dt * func( t + 0.5*dt , y[-1] + 0.5*k1 )
        k3 = dt * func( t + 0.5*dt , y[-1] + 0.5*k2 )
        k4 = dt * func( t + 1.0*dt , y[-1] + 1.0*k3 )

        y.append(y[-1] + 1./6. * ( k1 + 2*k2 + 2*k3 + k4))
        t += dt

    return y

# generalized velocity class
class genvelocity:
    def __init__(self):
        self.m = m_proton
        self.q = e

    # y is the dynamical variable vector and has the form [x,y,z,vx,vy,vz]
    def __call__(self,t,y):			# returns the generalized velocity vector 
        vx = y[3]
        vy = y[4]
        vz = y[5]

        prefactor = K*self.q/(self.m)
        vxdot = prefactor * (-vz*y[0])
        vydot = prefactor * (vz*y[1])
        vzdot = prefactor * (vx*y[0]-vy*y[1])

        return np.array([vx, vy, vz, vxdot, vydot, vzdot])

# find trajectory of particle given intial conditions y0
def get_path(y0):
    total_time = 20

    g = genvelocity()
    y = rk4(g, dt, y0)

    return y

# produces 3D plot of particle trajectory
def visualize_path(path, title, savename):
    N = len(path)   
    x = np.zeros(N)
    y = np.zeros(N)
    z = np.zeros(N)
    t = np.zeros(N)

    for i in range(N):
        x[i] = path[i][0]
        y[i] = path[i][1]
        z[i] = path[i][2]
        t[i] = i*dt

    fig = plt.figure(figsize=(10,7))
    ax = fig.add_subplot(111, projection='3d')
    im = ax.scatter(x,y,z, c=t)
    fig.colorbar(im, shrink=0.7, pad=0.05, label='Time elapsed (s)')
    plt.title("Trajectory of proton through quadrupole field: initial position "+title)
    ax.set_xlabel('x (m)',labelpad=10)
    ax.set_ylabel('y (m)',labelpad=10)
    ax.set_zlabel('z (m)',labelpad=10)
    plt.savefig(savepath+savename)
    plt.clf()

# produces 2D plot of multiple trajectories
def visualize_multiple_paths(paths, labels, index1, index2, xlabel, ylabel, title,savename):
    j = 0
    norm = plt.Normalize(0, len(paths[0])*dt)
    fig = plt.figure(figsize=(9,6.5))

    for path in paths:
        N = len(path)   
        x = np.zeros(N)
        y = np.zeros(N)
        t = np.zeros(N)

        for i in range(N):
            x[i] = path[i][index1]
            y[i] = path[i][index2]
            t[i] = i*dt

        plt.scatter(x,y,s=0.5,c=t,norm=norm)
        j += 1
    
    plt.title("Trajectories of proton through quadrupole field:"+title)
    plt.colorbar(label='Time elapsed (s)')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(savepath+savename)
    plt.clf()

# label trajectories, without time colorbar
def visualize_multiple_paths_labels(paths, labels, index1, index2, xlabel, ylabel, title, savename):
    j = 0
    fig = plt.figure(figsize=(9,6.5))

    for path in paths:
        N = len(path)   
        x = np.zeros(N)
        y = np.zeros(N)

        for i in range(N):
            x[i] = path[i][index1]
            y[i] = path[i][index2]

        plt.plot(x,y,label=labels[j])
        j += 1
    
    plt.title("Trajectories of proton through quadrupole field:"+title)
    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(savepath+savename)
    plt.clf()

# called by runFile for solutions.json output
def answer_question():
    y0 = np.array([1e-3,0,-1.55,0,0,momentum_to_velocity(E0)])
    path = get_path(y0)     # get trajectory from initial condition
    final_state = path[-1]  # position and velocity when proton leaves the magnetic field at z=1.55m
    pos = np.array([final_state[0],final_state[1],final_state[2]])
    v = np.array([final_state[3],final_state[4],final_state[5]])   # final velocity
    p = velocity_to_momentum(v)

    return [pos[0],pos[1],pos[2],p[0],p[1],p[2]]

def indiv_plot(y0, label, savename):
    path = get_path(y0)
    visualize_path(path, label, savename)

    return path

# function to make all plots for report - called by runFile.py
def make_plots():
    # protons move parallel to the nominal beam, entering field at x positions between 1e-3 and 1e-2m, y=0
    y_y0 = [np.array([1e-3,0,-1.55,0,0,momentum_to_velocity(E0)]), np.array([2.5e-3,0,-1.55,0,0,momentum_to_velocity(E0)]), np.array([5e-3,0,-1.55,0,0,momentum_to_velocity(E0)]), np.array([7.5e-3,0,-1.55,0,0,momentum_to_velocity(E0)]), np.array([1e-2,0,-1.55,0,0,momentum_to_velocity(E0)])]
    ypaths = []
    yplot_labels = ["x=1mm, y=0, \n initial momentum in +z direction", "x=2.5mm, y=0, \n initial momentum in +z direction", "x=5mm, y=0, \n initial momentum in +z direction", "x=7.5mm, y=0, \n initial momentum in +z direction", "x=1cm, y=0, \n initial momentum in +z direction"]
    yplot_savenames = ["x_1mm", "x_2-5mm", "x_5mm", "x_7-5mm", "x_1cm"]
    ylabels = [r'$x_0$=1mm', r'$x_0$=2.5mm', r'$x_0$=5mm', r'$x_0$=7.5mm', r'$x_0$=1cm']

    for i in range(len(y_y0)):
        ypaths.append(indiv_plot(y_y0[i], yplot_labels[i],yplot_savenames[i]))

    visualize_multiple_paths(ypaths, ylabels, 0, 2, 'x (m)', 'z (m)', "\n protons move parallel to the nominal beam at x positions up to \n 1 cm and y = 0", "xplots_combined1")
    visualize_multiple_paths_labels(ypaths, ylabels, 0, 2, 'x (m)', 'z (m)', "\n protons move parallel to the nominal beam at x positions up to \n 1 cm and y = 0", "xplots_combined2")


    # protons move parallel to the nominal beam, entering field at y positions between 1e-3 and 1e-2m, x=0
    y_y0 = [np.array([0,1e-3,-1.55,0,0,momentum_to_velocity(E0)]), np.array([0,2.5e-3,-1.55,0,0,momentum_to_velocity(E0)]), np.array([0,5e-3,-1.55,0,0,momentum_to_velocity(E0)]), np.array([0,7.5e-3,-1.55,0,0,momentum_to_velocity(E0)]), np.array([0,1e-2,-1.55,0,0,momentum_to_velocity(E0)])]
    ypaths = []
    yplot_labels = ["x=0, y=1mm, \n initial momentum in +z direction", "x=0, y=2.5mm, \n initial momentum in +z direction", "x=0, y=5mm, \n initial momentum in +z direction", "x=0, y=7.5mm, \n initial momentum in +z direction", "x=0, y=1cm, \n initial momentum in +z direction"]
    yplot_savenames = ["y_1mm", "y_2-5mm", "y_5mm", "y_7-5mm", "y_1cm"]
    ylabels = [r'$y_0$=1mm', r'$y_0$=2.5mm', r'$y_0$=5mm', r'$y_0$=7.5mm', r'$y_0$=1cm']

    for i in range(len(y_y0)):
        ypaths.append(indiv_plot(y_y0[i], yplot_labels[i], yplot_savenames[i]))

    visualize_multiple_paths(ypaths, ylabels, 1, 2, 'y (m)', 'z (m)', "\n protons move parallel to the nominal beam at y positions up to \n 1 cm and x = 0", "yplots_combined1")
    visualize_multiple_paths_labels(ypaths, ylabels, 1, 2, 'y (m)', 'z (m)', "\n protons move parallel to the nominal beam at y positions up to \n 1 cm and x = 0", "yplots_combined2")

    # weird trajectories
    path = get_path(np.array([1e-3,1e-3,-1.55, 0, momentum_to_velocity(E0)*np.sin(np.radians(0.01)), momentum_to_velocity(E0)*np.cos(np.radians(0.01))]))
    visualize_path(path, 'x=y=1mm', 'weird1_trajectory')

    path = get_path(np.array([0,1e-3,-1.55, momentum_to_velocity(E0)*np.sin(np.radians(0.01)), 0, momentum_to_velocity(E0)*np.cos(np.radians(0.01))]))
    visualize_path(path, 'x=0, y=1mm', 'weird2_trajectory')



