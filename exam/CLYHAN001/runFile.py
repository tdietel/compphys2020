import question1
import question2
import json
import numpy as np

savepath = 'exam/CLYHAN001/'
# savepath = ''

# writes solutions to solutions.json file
def makeJson():
	Dict = {}

	# Results for Q1
	dict1 = {}
	# part b result (final state of proton as it leaves the magnetic field)
	finalState = question1.answer_question()
	bDict = dict(zip(["x", "y", "z", "px", "py", "pz"], finalState))
	# part e result (magnetic field at (1mm,0,0))
	B_field = question2.answer_question()
	eDict = dict(zip(["Bx", "By", "Bz"], B_field.tolist()))

	dict1["b"] = bDict
	dict1["e"] = eDict

	Dict["1"] = dict1

	filename = savepath + "solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

def main():
	makeJson()
	question1.make_plots()
	question2.make_plots()

if __name__=='__main__':
	main()
