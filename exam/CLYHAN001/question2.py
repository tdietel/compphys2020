# Part 2 of exam (e-g): Use Biot Savart to compute magnetic field in beampipe
import numpy as np
from matplotlib import pyplot as plt

savepath = 'exam/CLYHAN001/'
# savepath = ''

# constants and data
K = 223
mu0 = np.pi*4e-7 	# vaccuum permeability
R_inner = 28e-3		# inner radius of magnet [m]
R_outer = 59e-3	# outer radius of magnet [m]
I_wire =  11870		# current per cable [A]
num_cables = 48		# cables per bundle
z_len = 3.10		# length of magnet (in z direction) [m]
z_min = -1.55
z_max = 1.55
bundle_angles = [[-30,30],[60,120],[150,210],[240,300]]		# angles covered by 4 bundles


# computes the current density in a bundle (current/cross sectional area)
def current_per_area():
	A = np.pi*(R_outer**2 - R_inner**2)*60/360
	I_total = I_wire*num_cables
	return I_total/A

# computes magnetic field at reference point r (vector) due to one bundle
def biot_savart(r_vec,bundle_label,dtheta,dr,dz,I_direc):
	
	B = np.zeros(3)		# magnetic field at reference point r

	I_density = current_per_area()

	z_vals = np.arange(z_min,z_max,dz)
	theta_vals = np.arange(bundle_angles[bundle_label][0],bundle_angles[bundle_label][1],dtheta)
	r_vals = np.arange(R_inner,R_outer,dr)

	for z in z_vals:
		for r in r_vals:
			for theta in theta_vals:
				# current for segment
				dA = np.pi*((r+dr)**2 - r**2)*dtheta/360
				I = dA*I_density

				# position of current segment
				l_vec = np.array([r*np.cos(np.radians(theta)),r*np.sin(np.radians(theta)),z])

				# contribution to magnetic field
				rdash = r_vec - l_vec
				dB = (mu0/(4*np.pi)) * (I*dz*np.cross(I_direc,rdash))/(np.linalg.norm(rdash)**3)

				# add contribution to total field at reference point
				B += dB


	return B

# compute the magnetic field at a given reference point
def compute_field(ref_point, dr, dz, dtheta):
	# units vectors for current directions
	zplus = np.array([0,0,1])
	zminus = np.array([0,0,-1])
	# contribution from each bundle
	B1 = biot_savart(ref_point,0,dtheta,dr,dz,zminus)
	B2 = biot_savart(ref_point,1,dtheta,dr,dz,zplus)
	B3 = biot_savart(ref_point,2,dtheta,dr,dz,zminus)
	B4 = biot_savart(ref_point,3,dtheta,dr,dz,zplus)
	B = B1+B2+B3+B4

	return B

# produces field maps for the ideal field 
def make_ideal_field_map(dx, dy):
	x = np.arange(-19e-3, 19e-3, dx)
	y = np.arange(-19e-3, 19e-3, dy)

	X, Y = np.meshgrid(x,y)

	Bx = np.zeros(X.shape)
	By = np.zeros(X.shape)

	for i in range(len(x)):
		for j in range(len(y)):
			Bx[i,j] = K*y[j]
			By[i,j] = K*x[i]

	fig, ax = plt.subplots()
	ax.streamplot(x, y, By, Bx)
	plt.xlabel('x (m)')
	plt.ylabel('y (m)')
	plt.title("Map of ideal magnetic field inside beam pipe at z=0")
	plt.savefig(savepath+'idealmagField_streamplot')
	plt.clf()

	fig, ax = plt.subplots()
	ax.quiver(X, Y, Bx, By, scale=10)
	plt.xlabel('x (m)')
	plt.ylabel('y (m)')
	plt.title("Map of ideal magnetic field inside beam pipe at z=0")
	plt.savefig(savepath+'idealmagField_quiver')
	plt.clf()

# produces field maps at z=0 for the actual magnetic field
def make_field_map(dx,dy):
	dtheta = 4
	dr = 1e-3
	dz = 2e-1
	x = np.arange(-19e-3, 19e-3, dx)
	y = np.arange(-19e-3, 19e-3, dy)

	X, Y = np.meshgrid(x,y)

	Bx = np.zeros(X.shape)
	By = np.zeros(X.shape)

	for i in range(len(x)):
		for j in range(len(y)):
			ref_point = np.array([x[i],y[j],0])
			B = compute_field(ref_point, dr, dz, dtheta)
			Bx[i,j] = B[0]
			By[i,j] = B[1]

	fig, ax = plt.subplots()
	ax.streamplot(x, y, By, Bx)
	plt.xlabel('x (m)')
	plt.ylabel('y (m)')
	plt.title("Map of actual magnetic field inside beam pipe at z=0")
	plt.savefig(savepath+'magField_streamplot')
	plt.clf()

	fig, ax = plt.subplots()
	ax.quiver(X, Y, Bx, By, scale=10)
	plt.xlabel('x (m)')
	plt.ylabel('y (m)')
	plt.title("Map of actual magnetic field inside beam pipe at z=0")
	plt.savefig(savepath+'magField_quiver')
	plt.clf()

# table for report: comparison between ideal and actual magnetic fields
def make_comparison_table():
	ref_points = [np.array([1e-3,0,0]),np.array([-1e-3,0,0]),np.array([1e-2,0,0]),np.array([2e-2,0,0]),np.array([0,1e-3,0]),np.array([0,-1e-3,0]),np.array([0,1e-2,0]),np.array([0,2e-2,0]),np.array([1e-3,1e-2,0]),np.array([-1e-2,2e-2,0])]	
	actual = []
	ideal = []
	points = ['(1e-3,0,0)', '(-1e-3,0,0)', '(1e-2,0,0)', '(2e-2,0,0)', '(0,1e-3,0)', '(0,-1e-3,0)', '(0,1e-2,0)', '(0,2e-2,0)', '(1e-3,1e-2,0)', '(-1e-2,2e-2,0)']

	dtheta = 4
	dr = 1e-3
	dz = 2e-1
	
	for ref_point in ref_points:
		actual.append(compute_field(ref_point, dr, dz, dtheta))
		ideal.append(K*np.array([ref_point[1],ref_point[0],0]))

	for i in range(len(points)):
		print(points[i]+ ' & ' + str(ideal[i][0]) + ' & ' + str(round(actual[i][0],6)) + ' & ' + str(ideal[i][1]) + ' & ' + str(round(actual[i][1],6)) + ' & ' + str(ideal[i][2]) + ' & ' + str(actual[i][2]) + '\\\\')
		print('\\hline')
		

# called by runFile for solutions.json output
def answer_question():
	B_field = compute_field(ref_point=np.array([1e-3,0,0]), dr=1e-3, dz=2e-1, dtheta=4)

	return B_field

def make_plots():
	make_field_map(8e-3,8e-3)
	make_ideal_field_map(8e-3,8e-3)
