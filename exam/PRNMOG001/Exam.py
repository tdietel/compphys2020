#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 01:22:12 2020

@author: rayo
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from mpl_toolkits.mplot3d import axes3d
import timeit
import json
plt.close('all')

#1b)#####################################
#globalled constants
c = 299792458.0 #m/s
q = 1.602176634e-19 #C
s = 10000 #number of steps

def quad(x1,vx1,y1,vy1,z1,vz1): #function to calculate motion of proton through quadrupole field
    #Constants
    mp = 1.67262192369e-27 #kg mass of proton
    q = 1.602176634e-19 #C charge of proton
    ti = 0.0 #s starting time
    c = 299792458.0 #m/s speed of light
    pz = (7.0e12*q/c) # initial momentum
    zf = 3.10 #m final position
    
    
    #Initial conditions
    vz = np.sqrt(((pz/mp)**2)/(1+((pz/mp)/c)**2)) #converting between relativsitic momentum to realtivsitic velocity
    print('The initial velocity is =',vz,'m/s')
    tf =  zf/vz #final time
    print('The final time is = ',tf,'s')
    global s
    h = tf/s #time steps
    t_vals = np.arange(ti,tf+h,h) #time points at which solve_ivp will soluve the ODE's
    
    def F(t,r):
        q = 1.602176634e-19 #C charge on proton
        K = 223.0 #Tm^-1
        mp = 1.67262192369e-27 #kg mass of proton
        c = 299792458.0 #m/s speed of light
        pz = (7.0e12*q/c) # momentum in SI units
        vz = np.sqrt(((pz/mp)**2)/(1+((pz/mp)/c)**2)) #converting between relativsitic momentum to realtivsitic velocity
        x = r[0]
        vx = r[1]
        y = r[2]
        vy = r[3]

        gamma = np.sqrt(1 - (vx**2 + vy**2 +vz**2)/(c**2)) #1/lorentz factor
        
        k_2 = (q*K*vz*gamma)/(mp) #constant defined in equations of motion
        
        dydt = vy            #
        dxdt = vx            #
        dvxdt = -(k_2)*x     ## g vector
        dvydt = (k_2)*y      ##
        dzdt = vz            #
        dvzdt = 0.0          #

        return np.array([dxdt,dvxdt,dydt,dvydt,dzdt,dvzdt])
    
    y0 = [x1,vx1,y1,vy1,0,vz] #initial conditions
    
    sol = solve_ivp(F,(ti,tf+h),y0,t_eval = t_vals,method = 'RK45') #solving using RK4 method
    
    x,vx,y,vy,z,vz = sol.y #extracting solutions
    
    
    
    return [x,vx,y,vy,z,vz]


y1 = quad(0.001,0,0,0,0,0) #inputting initial conditions
#for the final velcoity and momentum we just pull out the last element of the arrays
x = y1[0]
vx = y1[1]
y = y1[2]
vy = y1[3]
z = y1[4]
vz = y1[5]
n = s-1

vx0 = vx[n]
vy0 = vy[n]
vz0 = vz[n]

def vtm(v): #converts SI velcity to natural units momentum 
    global vx0
    global vy0
    global vz0
    global c
    global q
    m = 1.67262192369e-27 #mass of proton
    gamma = 1/np.sqrt(1 - (vx**2+vy**2+vz**2)/(c**2)) # lorentz factor
    p = (gamma*m*v)*(c/q)*10**-9 #momentum in GeV/c
    return p

px = vtm(vx[n])[n]
py = vtm(vy[n])[n]
pz = vtm(vz[n])[n]
x10 = x[n]
y10 = y[n]
z10 = z[n]

ba = {'x':float(x10),'y':float(y10),'z':float(z10),'px':float(px),'py':float(py),'pz':float(pz)}


#1c)#######################################

###########################################
x1 = []
y1 = []
z1 = []
oof = np.linspace(0.002,0.01,20)
ys = []


##################2nd part of exam
    
def circle_points(r, n):
    circles = []
    for r, n in zip(r, n):
        i = np.pi/2 - np.pi/6
        f = np.pi/2+ np.pi/6
        t = np.linspace(i,f, n)
        x = r * np.cos(t)
        y = r * np.sin(t)
        circles.append(np.c_[x, y])
    return circles

s = 10
r = np.linspace(0.056,0.118,s)
n = [9+i for i in range(len(r))]
#n = [6 for i in range(len(r))]

circles = circle_points(r, n)

print('\n')
print('The number of wires used per bundle = ',len(circles[0])*s)
print('\n')

a = []
b = []
x = []
y = []
for i in range(s): #extraction of coordinates of wires
    a1,b1 = zip(*circles[:][i])
    a.append(a1)
    b.append(b1)
#circles
for i in range(s): #extraction of coordinates of wires
    x1 = list(a[i])
    y1 = list(b[i])
    x.append(x1)
    y.append(y1)
    
  
def flatten(l): return flatten(l[0]) + (flatten(l[1:]) if len(l) > 1 else []) if type(l) is list else [l] #flattening function

x0 = flatten(x)
y0 = flatten(y)
x00, indices = np.unique(x0, axis = 0, return_index = True)
y00 = []
for i in range(len(indices)):
   y00.append(y0[indices[i]])
    
def rotate(x,y, radians): #rotationg function due to symmetry
    x1 = []
    y1 = []
    for i in range(len(x)):
        xx = x[i] * np.cos(radians) + y[i] * np.sin(radians)
        yy = -x[i] * np.sin(radians) + y[i] * np.cos(radians)
        x1.append(xx)
        y1.append(yy)
    return [x1, y1]

x01, y01 = rotate(x00,y00,np.pi/2)
x02, y02 = rotate(x01,y01,np.pi/2)
x03, y03 = rotate(x02,y02,np.pi/2)


x1 = np.append(x00,x02)   #
y1 = np.append(y00,y02)   ##Grouping positive and negative bundles
x2 = np.append(x01,x03)   ##
y2 = np.append(y01,y03)   #
# plt.figure(1)
# plt.scatter(x00,y00, label = 'y+')
# plt.scatter(x01,y01,label = 'x+')
# plt.scatter(x02,y02,label = 'y-')
# plt.scatter(x03,y03,label = 'x-')
# #plt.legend()
# plt.xlabel(r'x(m)')
# plt.ylabel(r'y(m)')
# plt.axes().set_aspect('equal')
# plt.savefig('rotated.png', dpi = 100)
# #plt.savefig('nodup.png', dpi = 100)
# #y0 = np.unique(y0, axis = 0)


#####################################################################################



x = np.linspace(-0.120,0.120,25)  #
y = np.linspace(-0.120,0.120,25)  # grid 
z = 0#np.linspace(0,3.1,10)

x,y,z = np.meshgrid(x,y,z)

def B(x,y):
    I = 12230 # current in wire
    J =  -I
    global x1
    global y1
    global x2
    global y2
    bx2 = []
    by2 = []
    bx3 = []
    by3 = []
    mu = 1.26 * 10**(-6)                           
    for i in range(len(x1)):
        
        mag1 = (mu/(2*np.pi))*(I/np.sqrt((x - x1[i])**2 + (y - y1[i])**2)) 
        
        by = mag1 * (np.cos(np.arctan2(y - y1[i],x - x1[i])))             
        bx = mag1 * (-np.sin(np.arctan2(y - y1[i],x - x1[i])))            

        bz = z*0                                                          
        bx2.append(bx)
        by2.append(by)
        
    for i in range(len(x2)):
        mag2 = (mu/(2*np.pi))*(J/np.sqrt((x - x2[i])**2 + (y - y2[i])**2)) 
        
        by0 = mag2 * (np.cos(np.arctan2(y - y2[i],x - x2[i])))             
        bx0 = mag2 * (-np.sin(np.arctan2(y - y2[i],x - x2[i])))            
        
        #bz = z*0                                                          
        bx3.append(bx0)
        by3.append(by0)

    for i in range(len(x1)):
        bx += bx2[:][i] 
        by += by2[:][i] 
    for i in range(len(x2)):
        bx += bx3[:][i] 
        by += by3[:][i] 
    return [bx,by,bz]



def C(x,y):
    I = 12230 #Amps, in the wire
    J =  -I
    global x1
    global y1
    global x2
    global y2
    bx2 = []
    by2 = []
    bx3 = []
    by3 = []
    mu = 1.26 * 10**(-6)                           
    for i in range(len(x1)):
        
        mag1 = (mu/(2*np.pi))*(I/np.sqrt((x - x1[i])**2 + (y - y1[i])**2)) #part of biot savart that controls magnitude
        
        by = mag1 * (np.cos(np.arctan2(y - y1[i],x - x1[i])))             # magnetic field in y
        bx = mag1 * (-np.sin(np.arctan2(y - y1[i],x - x1[i])))            #magnetic field in z

        bz = z*0                                                            #bz = 0 since curent in z
        bx2.append(bx)
        by2.append(by)
        
    for i in range(len(x2)):
        mag2 = (mu/(2*np.pi))*(J/np.sqrt((x - x2[i])**2 + (y - y2[i])**2))  #same same as aboe
        
        by0 = mag2 * (np.cos(np.arctan2(y - y2[i],x - x2[i])))             #same as above
        bx0 = mag2 * (-np.sin(np.arctan2(y - y2[i],x - x2[i])))            #same as above
        
        #bz = z*0                                                          
        bx3.append(bx0)
        by3.append(by0)

    for i in range(len(x1)):
        bx += bx2[:][i]   #summing vector fields from positive z direction currents
        by += by2[:][i] 
    for i in range(len(x2)):
        bx += bx3[:][i]  #summing vector field from negative z direction currents
        by += by3[:][i] 
    return [bx,by,bz]

B = B(x,y)
bx,by,bz = B                             
C = C([0.001],[0]) #to determine B field 1mm in x direction
cx,cy,cz = C


x0 = bx.flatten() # flattening
y0 = by.flatten() #

r = np.power(np.add(np.power(x0,2), np.power(y0,2)),0.5) #for normalisaion, didn't bother using 


e = {'Bx':float(cx[0]),'By':float(cy[0]),'Bz':float(cz[0][0][0])}
dic = {'1':{'b':ba,'e':e}} #forming dictionary 


plt.figure(10)
plt.quiver(x,y,x0,y0,color = 'k',label = 'B field')  #quiver for vector field plot

v = timeit.timeit(globals = globals(), stmt = 'plt.quiver(x,y,x0,y0)',number = 10)
ii = v/10

print('\n')
print('The time for the quiver plot is = ',ii,'s')
print('\n')


#plt.quiver(x,y,mx,my,color = 'k',label = 'B field')
plt.scatter(x1,y1,color = 'r',label = '+z current')  # placing wires for visualisation purposes
plt.scatter(x2,y2, color = 'b', label = '-z current')#
plt.legend(loc = 2)
plt.xlabel(r'$x(m)$')
plt.ylabel(r'$y(m)$')
plt.rcParams["figure.facecolor"] = "w"
plt.savefig('exam/PRNMOG001/Bfield.png',dpi = 100)

print('\n')
print('The value of Bx =',0,'T using vector given')
print('The value of By =',0.223,'T using vector given')

    
def plots(): #long function for plots of trajectories 
    #Constants
    mp = 1.67262192369e-27 #kg
    #mp = 938.27234 #Mev/c^2
    #K = 233.0 #Tm^-1
    #pzn = 7.0e12 #MeV
    q = 1.602176634e-19 #C
    #q = 0.30286#8.5424546e-2 
    #tf = 1.0 #s
    ti = 0.0 #s
    #h =  1e-12#time step
    c = 299792458.0 #m/s
    pz = (7.0e12*q/c) #
    zf = 3.10 #m
    print(pz)
   
    #Initial conditions
    vz00 = np.sqrt(((pz/mp)**2)/(1+((pz/mp)/c)**2)) #converting between relativsitic momentum to realtivsitic velocity
    print('The initial velocity is =',vz00,'m/s')
    tf1 =  zf/vz00
    print('The final time is = ',tf1,'s')
    steps = 10000
    deltat1 = tf1/steps
    
    t_vals1 = np.arange(ti,tf1,deltat1)
    
    def G(t,r):
        q = 1.602176634e-19 #C charge on proton
        K = 223.0 #Tm^-1
        mp = 1.67262192369e-27 #kg mass of proton
        c = 299792458.0 #m/s speed of light
        pz = (7.0e12*q/c) # momentum in SI units
        vz0 = np.sqrt(((pz/mp)**2)/(1+((pz/mp)/c)**2)) #converting between relativsitic momentum to realtivsitic velocity
        x = r[0]
        vx = r[1]
        y = r[2]
        vy = r[3]

        gamma = np.sqrt(1 - (vx**2 + vy**2 +vz0**2)/(c**2)) #1/lorentz factor
        
        k_2 = (q*K*vz0*gamma)/(mp) #constant defined in equations of motion
        
        dydt = vy            #
        dxdt = vx            #
        dvxdt = -(k_2)*x     ## g vector
        dvydt = (k_2)*y      ##
        dzdt = vz0           #
        dvzdt = 0.0          #

        return np.array([dxdt,dvxdt,dydt,dvydt,dzdt,dvzdt])
    
    def H(t,r):
        q = 1.602176634e-9 #C charge on proton
        K = 223.0 #Tm^-1
        mp = 1.67262192369e-17 #kg mass of proton
        c = 299792458.0 #m/s speed of light
        pz = (7.0e9*q/c) # momentum in SI units
        vz0 = np.sqrt(((pz/mp)**2)/(1+((pz/mp)/c)**2)) #converting between relativsitic momentum to realtivsitic velocity
        x = r[0]
        vx = r[1]
        y = r[2]
        vy = r[3]

        gamma = np.sqrt(1 - (vx**2 + vy**2 +vz0**2)/(c**2)) #1/lorentz factor
        
        k_2 = (q*K*vz0*gamma)/(mp) #constant defined in equations of motion
        
        dydt = vy            #
        dxdt = vx            #
        dvxdt = -(k_2)*x     ## g vector
        dvydt = (k_2)*y      ##
        dzdt = vz0           #
        dvzdt = 0.0          #

        return np.array([dxdt,dvxdt,dydt,dvydt,dzdt,dvzdt])
    
    oof1 = np.linspace(0.002,0.01,25)
    
    x11 = []
    y11 = []
    z11 = []
    #oof = np.linspace(0.002,0.01,20)
    ys1 = []
    
    
    x12 = []
    y12 = []
    z12 = []
    ys2 = []
    
    ys3 = []
    x210 = []
    y210 = []
    z210 = []
    
    ys4 = []
    x211 = []
    y211 = []
    z211 = []
    
    phi = np.linspace(0,np.pi,100) #finite angle of approach into quadrupole from 0 to 180 degrees
    oofx = [200*np.cos(i) for i in phi] #creating x points of velocity
    oofy = [200*np.sin(i) for i in phi] #creating y points of velocity 
    
    ys5 = []
    x310 = []
    y310 = []
    z310 = []
    
    ys6 = []
    x410 = []
    y410 = []
    z410 = []
    
    
    for i in range(len(oof1)):
        y0 = [oof1[i],0,0,0,00,vz00]
        ys1.append(y0)
        sol = solve_ivp(G,(ti,tf1),ys1[i],t_eval = t_vals1)
        x2,vx2,y2,vy2,z2,vz2 = sol.y
        x11.append(x2)
        y11.append(y2)
        z11.append(z2)
        
    fig = plt.figure(11)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oof1)):
        #plt.plot(z11[i],x11[i])
        ax.plot(x11[i], y11[i],z11[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    #ax.legend()
    ax.xaxis.labelpad = 15
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/G1.png',dpi = 100)
    
    for i in range(len(oof1)):
        y0 = [oof1[i],0,0,0,0,vz00]
        ys2.append(y0)
        sol = solve_ivp(H,(ti,tf1),ys2[i],t_eval = t_vals1)
        x1,vx1,y1,vy1,z1,vz1 = sol.y
        x12.append(x1)
        y12.append(y1)
        z12.append(z1)
        
    fig = plt.figure(12)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oof)):
        #plt.plot(z12[i],x12[i])
        ax.plot(x12[i], y12[i],z12[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    #ax.legend()
    ax.xaxis.labelpad = 20
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/H1.png',dpi = 100)
    
    ########################################
    for i in range(len(oof1)):
        y0 = [0,0,oof1[i],0.0,0.0,vz00]
        ys3.append(y0)
        sol = solve_ivp(G,(ti,tf1),ys3[i],t_eval = t_vals1)
        x3,vx,y3,vy,z3,vz = sol.y
        x210.append(x3)
        y210.append(y3)
        z210.append(z3)
        
    fig = plt.figure(13)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oof1)):
        #plt.plot(z11[i],x11[i])
        ax.plot(x210[i], y210[i],z210[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    ax.legend()
    ax.xaxis.labelpad = 15
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/G2.png',dpi = 100)
    
    for i in range(len(oof1)):
        y0 = [0,0,oof1[i],0.0,0.0,vz00]
        ys4.append(y0)
        sol = solve_ivp(H,(ti,tf1),ys3[i],t_eval = t_vals1)
        x4,vx,y4,vy,z4,vz = sol.y
        x211.append(x4)
        y211.append(y4)
        z211.append(z4)
        
    fig = plt.figure(14)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oof1)):
        #plt.plot(z11[i],x11[i])
        ax.plot(x211[i], y211[i],z211[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    ax.legend()
    ax.xaxis.labelpad = 15
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/H2.png',dpi = 100)
    
    ##########################################
    
    for i in range(len(oofx)):
        y0 = [0,oofx[i],0,oofy[i],0.0,vz00]
        ys5.append(y0)
        sol = solve_ivp(G,(ti,tf1),ys5[i],t_eval = t_vals1) #RK45 to loop over to find solution at multiple initial conditions
        x5,vx,y5,vy,z5,vz = sol.y
        x310.append(x5)
        y310.append(y5)
        z310.append(z5)
        
    fig = plt.figure(15)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oofx)):
        #plt.plot(z11[i],x11[i])
        ax.plot(x310[i], y310[i],z310[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    ax.legend()
    ax.xaxis.labelpad = 20
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/G3.png',dpi = 100)
    
    for i in range(len(oofx)):
        y0 = [0,oofx[i],0,oofy[i],0.0,vz00]
        ys6.append(y0)
        sol = solve_ivp(H,(ti,tf1),ys6[i],t_eval = t_vals1)
        x6,vx,y6,vy,z6,vz = sol.y
        x410.append(x6)
        y410.append(y6)
        z410.append(z6)
        
    fig = plt.figure(16)
    fig.tight_layout()
    ax = fig.gca(projection='3d')
    for i in range(len(oofx)):
        #plt.plot(z11[i],x11[i])
        ax.plot(x410[i], y410[i],z410[i],color = 'k')
    ax.set_xlabel(r'$x(m)$')
    ax.set_ylabel(r'$y(m)$')
    ax.set_zlabel(r'$z(m)$')
    ax.legend()
    ax.xaxis.labelpad = 15
    ax.tick_params(axis='x', labelrotation=45)
    plt.savefig('exam/PRNMOG001/H3.png',dpi = 100)
    
    
    


plots()

def WJSON(path, fileName, data): #just writing json file
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)
        
WJSON('exam/PRNMOG001/','results',dic)
