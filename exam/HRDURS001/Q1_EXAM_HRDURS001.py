from __future__ import division 
import numpy as np
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d.axes3d import Axes3D
import scipy as sp
import json

font = {'family': 'serif',
        'color':  'navy',
        'weight': 'normal',
        'size': 14,
        }

#PROTON TRAJECTORIES------------------


#INITIAL VALUES 

Li=0.0  #initial length 
Lf=3.101  #final length (meters)
dl=0.001 # meters (1mm) increment
k=223.00 #tesla per meter (and this is the feild gradient)
#we will take increments in 1mm at a time
gamma=(7*10**6)/983            #relativistic gamma and beta
beta=np.sqrt(1-(1/(gamma**2)))


length=np.arange(Li,Lf, dl)    #length of the magnet

COORD=np.zeros((5,1))

def transfermatrix(M,w,L,b0,gamma0):  #matrix which contains the eoms from hamiltonian formulism
	M[0][0]=np.cos(w*L)
	M[1][0]=-w*np.sin(w*L)
	M[0][1]=np.sin(w*L)/(w)
	M[1][1]=np.cos(w*L)
	M[2][2]=np.cosh(w*L)
	M[2][3]=np.sinh(w*L)/w
	M[3][2]=w*np.sinh(w*L)
	M[3][3]=np.cosh(w*L)
	M[4][4]=1


	return M


M=np.zeros((5,5))

#X deviations up to 1cm-----------------------
deviations=[0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01 ]
#print(deviations)
for j in range(len(deviations)):
	x = [deviations[j]]
	px = [0.0]
	y = [0.00]
	py = [0.0]
	z = [0.0]

	for i in range(len(length)):
		X=np.array([x[i], px[i],y[i], py[i], z[i] ])
		QUAD_M= transfermatrix(M, np.sqrt(k), 0.001, beta, gamma)
		test=np.dot(QUAD_M, X)
		x.append(test[0])
		px.append(test[1])
		y.append(test[2])
		py.append(test[3])
		z.append(length[i])


	plt.plot(z,x, label=deviations[j])



plt.xlabel('z (meters)', fontdict=font)
plt.ylabel('x (meters)', fontdict=font)
plt.legend()
plt.show()



#y deiviations --------------------------------------------------------
M=np.zeros((5,5))

deviations=[0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01 ]
#print(deviations)
for j in range(len(deviations)):
	x = [0.0]
	px = [0.0]
	y = [deviations[j]]
	py = [0.0]
	z = [0.0]

	for i in range(len(length)):

		X=np.array([x[i], px[i],y[i], py[i], z[i]])

		QUAD_M= transfermatrix(M, np.sqrt(k), 0.001, beta, gamma)
		test=np.dot(QUAD_M, X)
		x.append(test[0])
		px.append(test[1])
		y.append(test[2])
		py.append(test[3])
		z.append(length[i])


	plt.plot(z,y, label=deviations[j])

plt.xlabel('z (meters)', fontdict=font)
plt.ylabel('y (meters)', fontdict=font)
plt.legend()
plt.yscale('log')
plt.show()





# entering at an angle. At first lets try a theta=20 and phi=5----------------------------------------------
ptot=7*10**6
M=np.zeros((5,5))
theta=0.00002*(np.pi/180)
phi=5*(np.pi/180)

x = [0.0]
px = [ptot*np.sin(theta)*np.cos(phi)]
y = [0.0]
py = [ptot*np.sin(theta)*np.sin(phi)]
z = [0.0]


for i in range(len(length)):
	X=np.array([x[i], px[i],y[i], py[i], z[i]])

	QUAD_M= transfermatrix(M, np.sqrt(k), 0.001, beta, gamma)
	test=np.dot(QUAD_M, X)
	x.append(test[0])
	px.append(test[1])
	y.append(test[2])
	py.append(test[3])
	z.append(length[i])



fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(z, x, y)
ax.set_xlabel('z')
ax.set_ylabel('x')
ax.set_zlabel('y')

plt.show()




# entering at an angle. At first lets try a theta=0.0001 and phi=30
ptot=7*10**6
M=np.zeros((5,5))
theta=10**(-6)*(np.pi/180)
phi=30*(np.pi/180)

x = [0.0]
px = [ptot*np.sin(theta)*np.cos(phi)]
y = [0.0]
py = [ptot*np.sin(theta)*np.sin(phi)]
z = [0.0]


for i in range(len(length)):
	X=np.array([x[i], px[i],y[i], py[i], z[i]])

	QUAD_M= transfermatrix(M, np.sqrt(k), 0.001, beta, gamma)
	test=np.dot(QUAD_M, X)
	x.append(test[0])
	px.append(test[1])
	y.append(test[2])
	py.append(test[3])
	z.append(length[i])



fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(z, x, y)
ax.set_xlabel('z')
ax.set_ylabel('x')
ax.set_zlabel('y')
ax.set_zscale('log')
ax.set_xlim(0,3.1)
#ax.set_ylim(-1,1)
ax.set_zlim(-30,10000)
ax.set_title('Theta=10e-6, phi=181')

plt.show()


M=np.zeros((5,5))

#X deviations up to 1cm-----------------------
deviations=[0.001]
#print(deviations)
for j in range(len(deviations)):
	x = [deviations[j]]
	px = [0.0]
	y = [0.00]
	py = [0.0]
	z = [0.0]

	for i in range(len(length)):
		X=np.array([x[i], px[i],y[i], py[i], z[i] ])
		QUAD_M= transfermatrix(M, np.sqrt(k), 0.001, beta, gamma)
		test=np.dot(QUAD_M, X)
		x.append(test[0])
		px.append(test[1])
		y.append(test[2])
		py.append(test[3])
		z.append(length[i])


	plt.plot(z,x, label=deviations[j])




a_file=open("exam/HRDURS001/data.json", "r")
#a_file=open("data.json", "r")
json_object=json.load(a_file)
a_file.close()


json_object['1']['b']['x']=x[-1]
json_object['1']['b']['y']=y[-1]
json_object['1']['b']['z']=z[-1]
json_object['1']['b']['px']=px[-1]/(1000)
json_object['1']['b']['py']=py[-1]/(1000)
json_object['1']['b']['pz']=np.sqrt(ptot**2-((px[-1])**2+py[-1]**2))/(1000)

a_file=open("exam/HRDURS001/data.json", "w")
#a_file=open("data.json", "w")
json.dump(json_object, a_file, indent=4)
a_file.close()
