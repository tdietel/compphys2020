import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Circle, Wedge, Polygon
from matplotlib.collections import PatchCollection
import numpy as np
import json
import time


r2 = 0.059
r1 = 0.028
L = 3.10  # meter
cable_no = 48  #number of cables per sector
current = 11870 #Amperes per cable

#method to find the H field through using the wires located in the dofferent poles. We have a certain ammount of wires per pole and it is not 48, so the current is divided as if there are 48 wires.
def H_field(x,y, wires1, wires2, wires3, wires4):
    bx=0
    by=0
    bz=0
    for i in range(len(wires1)):
        j=current*cable_no/(len(wires1))
        mag = (1/(2*np.pi))*(j/np.sqrt((x-wires1[i][0])**2+(y-wires1[i][1])**2))    #Magnitude of the vector B
        by += mag * (np.cos(np.arctan2(y-wires1[i][1],x-wires1[i][0])))              #By
        bx += mag * (-np.sin(np.arctan2(y-wires1[i][1],x-wires1[i][0])))             #Bx
        bz += z*0
        #Bz (zero, using the right-hand rule)
    for i in range(len(wires2)):
        j=-current*cable_no/(len(wires2))
        mag = (1/(2*np.pi))*(j/np.sqrt((x-wires2[i][0])**2+(y-wires2[i][1])**2))    #Magnitude of the vector B
        by += mag * (np.cos(np.arctan2(y-wires2[i][1],x-wires2[i][0])))              #By
        bx += mag * (-np.sin(np.arctan2(y-wires2[i][1],x-wires2[i][0])))             #Bx
        bz += z*0
        #Bz (zero, using the right-hand rule)
    for i in range(len(wires3)):
        j = current*cable_no/(len(wires3))
        mag = (1 / (2 * np.pi)) * (
                    j / np.sqrt((x - wires3[i][0]) ** 2 + (y - wires3[i][1]) ** 2))  # Magnitude of the vector B
        by += mag * (np.cos(np.arctan2(y - wires3[i][1], x - wires3[i][0])))  # By
        bx += mag * (-np.sin(np.arctan2(y - wires3[i][1], x - wires3[i][0])))  # Bx
        bz += z * 0
        # Bz (zero, using the right-hand rule)
    for i in range(len(wires4)):
        j = -current*cable_no/(len(wires4))
        mag = (1 / (2 * np.pi)) * (
                    j / np.sqrt((x - wires4[i][0]) ** 2 + (y - wires4[i][1]) ** 2))  # Magnitude of the vector B
        by += mag * (np.cos(np.arctan2(y - wires4[i][1], x - wires4[i][0])))  # By
        bx += mag * (-np.sin(np.arctan2(y - wires4[i][1], x - wires4[i][0])))  # Bx
        bz += z * 0
        # Bz (zero, using the right-hand rule)
    return bx,by,bz


# Hfield for an ideal quadrupole with only 1 wire at each pole.
def ideal_field(x,y, wires1, wires2, wires3, wires4):
    bx=0
    by=0
    bz=0

    j=current*cable_no
    mag = (1/(2*np.pi))*(j/np.sqrt((x-wires1[0])**2+(y-wires1[1])**2))    #Magnitude of the vector B
    by += mag * (np.cos(np.arctan2(y-wires1[1],x-wires1[0])))              #By
    bx += mag * (-np.sin(np.arctan2(y-wires1[1],x-wires1[0])))             #Bx
    bz += z*0
        #Bz (zero, using the right-hand rule)

    j=-current*cable_no
    mag = (1/(2*np.pi))*(j/np.sqrt((x-wires2[0])**2+(y-wires2[1])**2))    #Magnitude of the vector B
    by += mag * (np.cos(np.arctan2(y-wires2[1],x-wires2[0])))              #By
    bx += mag * (-np.sin(np.arctan2(y-wires2[1],x-wires2[0])))             #Bx
    bz += z*0
        #Bz (zero, using the right-hand rule)

    j = current*cable_no
    mag = (1 / (2 * np.pi)) * (j / np.sqrt((x - wires3[0]) ** 2 + (y - wires3[1]) ** 2))  # Magnitude of the vector B
    by += mag * (np.cos(np.arctan2(y - wires3[1], x - wires3[0])))  # By
    bx += mag * (-np.sin(np.arctan2(y - wires3[1], x - wires3[0])))  # Bx
    bz += z * 0
        # Bz (zero, using the right-hand rule)

    j = -current*cable_no
    mag = (1 / (2 * np.pi)) * (j / np.sqrt((x - wires4[0]) ** 2 + (y - wires4[1]) ** 2))  # Magnitude of the vector B
    by += mag * (np.cos(np.arctan2(y - wires4[1], x - wires4[0])))  # By
    bx += mag * (-np.sin(np.arctan2(y - wires4[1], x - wires4[0])))  # Bx
    bz += z * 0
        # Bz (zero, using the right-hand rule)
    return bx,by,bz


#this is used to find the points within each sector of our experimental pole.
def polartocartesian(r, theta, z):
    return [r * np.cos(theta * np.pi / 180), r * np.sin(theta * np.pi / 180), z]



#Defining the polar angles that each "pole" cable bundle will cover
sector1 = np.arange(240, 301, 1)
sector2 = np.arange(330, 391, 1)
sector3 = np.arange(60, 121, 1)
sector4 = np.arange(150, 211, 1)
radius = np.arange(r1, r2 + 0.001, 0.001) #radius change that the poles will be in


points_sec_1 = []
points_sec_2 = []
points_sec_3 = []
points_sec_4 = []

#appending points to the various sectors (4 sectors= 1 for each pole)
z = np.linspace(0,3.10, 30) #spanning the grid space




for k in range(len(z)):
    for i in range(len(radius)):
        for j in range(len(sector1)):
            points_sec_1.append(polartocartesian(radius[i], sector1[j], z[k]))

        for j in range(len(sector2)):
            points_sec_2.append(polartocartesian(radius[i], sector2[j], z[k]))
        for j in range(len(sector3)):
            points_sec_3.append(polartocartesian(radius[i], sector3[j], z[k]))
        for j in range(len(sector4)):
            points_sec_4.append(polartocartesian(radius[i], sector4[j],z[k]))






points_round_1 = np.around(points_sec_1, 2)
points_round_2 = np.around(points_sec_2, 2)
points_round_3 = np.around(points_sec_3, 2)
points_round_4 = np.around(points_sec_4, 2)


#Finters out only unique points
unique_points_1 = np.unique(points_round_1, axis=0)
unique_points_2 = np.unique(points_round_2, axis=0)
unique_points_3 = np.unique(points_round_3, axis=0)
unique_points_4 = np.unique(points_round_4, axis=0)


tpoints1 = np.transpose(unique_points_1)  # this makes it plottable. Not necessary but udefull if you want to see where the poles are.
tpoints2 = np.transpose(unique_points_2)
tpoints3 = np.transpose(unique_points_3)
tpoints4 = np.transpose(unique_points_4)

#x and y spanning the drid space
x = np.arange(-r1, r1+0.001, 0.001)
y = np.arange(-r1, r1+0.001, 0.001)

#making a grid for each bx and by to be found
x,y,z=np.meshgrid(x,y,z)

wires1=unique_points_1
wires2=unique_points_2
wires3=unique_points_3
wires4=unique_points_4

#calling the hfield methof to determine bx,by,bz
bx,by,bz=H_field(x,y,wires1,wires2,wires3,wires4)

#to be clear we are only calculating the H fields but sometimes we refer to it as B field.


xx = np.arange(-r1, r1+0.001, 0.001)
yy = np.arange(-r1, r1+0.001, 0.001)




#writing json file
a_file=open("exam/HRDURS001/data.json", "r")
#a_file=open("data.json", "r")
json_object=json.load(a_file)
a_file.close()


json_object['1']['e']['Bx']=bx[28,29,0]
json_object['1']['e']['By']=by[28,29,0]
json_object['1']['e']['Bz']=0.0


print('Experimental field')
print('bx:', bx[28,29,0])
print('by:', by[28,29,0])

a_file=open("exam/HRDURS001/data.json", "w")
#a_file=open("data.json", "w")
json.dump(json_object, a_file, indent=4)
a_file.close()


#plotting qudrupole in z=0 plane
bx=bx[:,:,0]
by=by[:,:,0]

t0=time.time()
fig, ax = plt.subplots()
sec1=[Wedge((0,0),0.059,240,300, width=0.031),Wedge((0,0),0.059,330,390, width=0.031),Wedge((0,0),0.059,60,120, width=0.031),Wedge((0,0),0.059,150,210, width=0.031)]
p = PatchCollection(sec1)
ax.add_collection(p)
plt.streamplot(xx,yy,bx,by)

plt.xlabel("X")
plt.ylabel("Y")
plt.show()
t1=time.time()
total=t1-t0
print(total)

#Ideal quadrupole field

id_wire1=np.array([0,-0.0435])
id_wire2=np.array([0.0435,0.0])
id_wire3=np.array([0,0.0435])
id_wire4=np.array([-0.0435,0.0])

bx,by,bz=ideal_field(x,y,id_wire1,id_wire2,id_wire3,id_wire4)

print('ideal field')
print('bx:', bx[28,29,0])
print('by:', by[28,29,0])
bx=bx[:,:,0]
by=by[:,:,0]


#plotting ideal field
plt.streamplot(xx,yy,bx,by)
plt.xlabel("X")
plt.ylabel("Y")
plt.show()
