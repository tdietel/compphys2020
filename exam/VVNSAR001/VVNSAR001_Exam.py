# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 14:27:07 2020

@author: VVNSAR001
"""
##Imports
import numpy as np
import matplotlib.pyplot as plt
import json
import time
##For this problem we will be approaching it from a 4th-order Runge-Kutta Method akin to the sort 
##done in the jupyter notebooks

pBeam = 7e12 #eV
c = 3e8 
fg = 223 #T*m^-1
mp = 1.6727e-27 #mass of proton

q = 1.602e-19 #charge of proton
##################################################################
#1a)
##################################################################
def yVar(x,y,z,eV,):
    #We can get the momentum in the z direction from the proton beam energy
    pZ = eV*(q/c)
    #x0,y0,z0 = 0.001,0,0 #intial values
    vx, vy = 0,0 #since proton motion is only in z
    vz = np.sqrt((pZ/mp)**2/(1 + (pZ/mp)**2/c**2))#inverse lorentz to get the velocity from the momentum
    return np.array([x,y,z,vx,vy,vz])


def g(yVar): #to be used in RK4 method - function describes equation 1 in exam paper
    x0,y0,z0,vx0,vy0,vz0 = yVar[0],yVar[1],yVar[2],yVar[3],yVar[4],yVar[5]
    v2 = vx0**2 + vy0**2 + vz0**2
    gamma = np.sqrt(1-(v2)/c**2) #lorentz factor
    #creating matrix from equation 1
    Bx = fg*y0
    By = fg*x0   
    Bz = 0
    ##derivatives
    a = q*gamma*vz0/mp
    ax = -a*Bx
    ay = a*By
    az = 0
    
    return np.array([vx0,vy0,vz0,ax,ay,az])

##################################################################
#1b)
##################################################################
def rk4(func,Yvar,t): #RK4 method as in jupyter notebook
    x,y,z,vx,vy,vz = [],[],[],[],[],[] #arrays for each coordinate to be used in position plots
    x.append(Yvar[0])
    y.append(Yvar[1])
    z.append(Yvar[2])
    vx.append(Yvar[3])
    vy.append(Yvar[4])
    vz.append(Yvar[5])
    
    for i in range(len(t)-1):
    
        dt = t[i+1] - t[i]
        
        k1 = dt * func( Yvar          )
        k2 = dt * func( Yvar + 0.5*k1 )
        k3 = dt * func( Yvar + 0.5*k2 )
        k4 = dt * func( Yvar + 1.0*k3 )

        R =  1./6. * ( k1 + 2*k2 + 2*k3 + k4)
        #summing the R factors that we calculate for each coordinate / velocity into the original array
        Yvar[0]+= R[0]
        Yvar[1]+= R[1]
        Yvar[2]+= R[2]
        Yvar[3]+= R[3]
        Yvar[4]+= R[4]
        Yvar[5]+= R[5]
        #Appending the new things
        x.append(Yvar[0])
        y.append(Yvar[1])
        z.append(Yvar[2])
        vx.append(Yvar[3])
        vy.append(Yvar[4])
        vz.append(Yvar[5])
    return np.array([x,y,z,vx,vy,vz])

y = yVar(0.01,0,0,pBeam)
print(y)
print(g(y))
#We can calculate the max time from the length of the medium and the proton velocity
Vzt = y[5]
tMax = 3.1/Vzt

counter = 1000 #For stepsize
t = np.arange(0,tMax,tMax/counter)
Rx,Ry,Rz,Rvx,Rvy,Rvz = rk4(g,y,t)
##Submitted values:
xf = Rx[-1]
yf = Ry[-1]
zf = Rz[-1]
gamma = np.sqrt(1-(Rvx[-1]**2+Rvy[-1]**2+Rvz[-1]**2)/c**2) #lorentz factor    
##We need to convert the velocity to a momentum in GeV
def vtoPinGeV(v):
    p = mp*(1/5.36)*10**19*v*1e4
    return p
px = vtoPinGeV(Rvx[-1])
py = vtoPinGeV(Rvy[-1])
pz = vtoPinGeV(Rvz[-1])
##Pos
#velocities = []
#print("xf ",zf)
#print("Rx",pz)

Q1b={"x":xf, "y":yf, "z":zf,"px":px,"py":py,"pz":pz}
#print(sol1b)

##################################################################
#1c) 
##################################################################
#So we're given set initial conditions that we need to rerun RK4 with
#i) x positions up to 1cm - varying initial conditions for different trajectories
xi = np.linspace(0.0025,0.01,4)
zi = np.linspace(0,3.1,1000)
xTrajec = []
x=[]
y=[]

for i in range(len(xi)):
    A = yVar(xi[i],0,zi[i],pBeam)
    iRx,iRy,iRz,iRvx,iRvy,iRvz = rk4(g,A,t)
    xTrajec.append(iRx) 
plt.figure(0)
fig,axes = plt.subplots(2,2, dpi=200)
axes[0,0].plot(xTrajec[0],zi)
axes[0,1].plot(xTrajec[1],zi)
axes[1,0].plot(xTrajec[2],zi)
axes[1,1].plot(xTrajec[3],zi)
#plt.setp(axes.get_xticklabels(), rotation=30, horizontalalignment='right')
plt.xlabel('x (m)')
plt.ylabel('z (m)')
plt.show()

plt.figure(1)
plt.plot(xTrajec[0],zi,label='trajectory 1')
plt.plot(xTrajec[1],zi,label='trajectory 2')
plt.plot(xTrajec[2],zi,label='trajectory 3')
plt.plot(xTrajec[3],zi,label='trajectory 4')
plt.legend()
plt.xlabel('x (m)')
plt.ylabel('z (m)')
plt.show()
##################################################################
#ii) y positions up to 1cm - varying initial conditions for different trajectories
yi = np.linspace(0.0025,0.01,4)
zi = np.linspace(0,3.1,1000)
yTrajec = []
for i in range(len(yi)):
    A = yVar(0,yi[i],zi[i],pBeam)
    iRx,iRy,iRz,iRvx,iRvy,iRvz = rk4(g,A,t)
    yTrajec.append(iRy) 
plt.figure(2)
fig,axes = plt.subplots(2,2, dpi=200)
axes[0,0].plot(yTrajec[0],zi)
axes[1,0].plot(yTrajec[1],zi)
axes[0,1].plot(yTrajec[2],zi)
axes[1,1].plot(yTrajec[3],zi)
#plt.setp(axes.get_xticklabels(), rotation=30, horizontalalignment='right')
plt.xlabel('y (m)')
plt.ylabel('z (m)')
plt.show()

plt.figure(3)
plt.plot(yTrajec[0],zi,label='trajectory 1')
plt.plot(yTrajec[1],zi,label='trajectory 2')
plt.plot(yTrajec[2],zi,label='trajectory 3')
plt.plot(yTrajec[3],zi,label='trajectory 4')
plt.legend()
#plt.setp(axes.get_xticklabels(), rotation=30, horizontalalignment='right')
plt.xlabel('y (m)')
plt.ylabel('z (m)')
plt.show()
##################################################################    
#iii) Enter from a point on zaxis and enter at a FINITE angle
vxrange = np.linspace(-2,2,10) 
vyrange = np.linspace(-2,2,10) 
zrange = np.linspace(0,3.1,10)
x = []
y = []
z = []

for i in vxrange:
    for j in vyrange:
        for k in zrange:
            x1, y1, z1, vx, vy, vz = rk4(g,[0.0,0.0,k,i,j,Vzt],t)
            x.append(x1)
            y.append(y1)
            z.append(z1)
            
#fig1,axes1 = plt.subplots(2,2, dpi=200)
plt.figure(4)
plt.plot(z[1],y[1])
plt.xlabel('z (m)')
plt.ylabel('y (m)')
plt.show()

#axes[0,0].plot(z[1],y[1])
#axes[0,1].plot(z[2],y[2])
#axes[1,0].plot(z[3],y[3])
#axes[1,1].plot(z[4],y[4])

plt.figure(5)
plt.plot(z[1],y[1],label='trajectory 1')
plt.plot(z[2],y[2],label='trajectory 2')
plt.plot(z[3],y[3],label='trajectory 3')
plt.plot(z[4],y[4],label='trajectory 4')
plt.xlabel('z (m)')
plt.ylabel('y (m)')
plt.legend()
plt.show()

plt.plot(z[1],x[1],label='trajectory 1')
plt.plot(z[2],x[2],label='trajectory 2')
plt.plot(z[3],x[3],label='trajectory 3')
plt.plot(z[4],x[4],label='trajectory 4')
plt.legend()
plt.xlabel('z (m)')
plt.ylabel('x (m)')
#       plt.savefig('xtrag',dpi=300)
plt.show()
##################################################################
#Q2a) 
##################################################################
l = 3.10
A = 11870 #ampere per cable
n = 48 #num cables
iA = 0.056/2 #m aperture
oA = 0.118/2 #m aperture - diameters so /2 
sector = np.pi/3 #(60 degrees -> pi/3 )
mu0 = 4*np.pi*10e-7
I = A*n
#currents above and below beam +z --- currents next to beam -z 

def BsavAbove(x0,xprime,y0,yprime): #for the currents above the beam where it's +z
    x = x0 - xprime
    y = y0 - yprime
    r = x**2+y**2
    B = (mu0/4*np.pi)*(I/np.sqrt(r))
    #if we have the magnitude we can calculate the components by multiplying it with the unit vector 
    Bx = B*(-1*np.sin(np.arctan(y/x)))
    By = B*(np.cos(np.arctan(y/x)))
    return Bx,By

def BsavBelow(x0,xprime,y0,yprime): #for the currents below the beam where it's -z  
    x = x0 - xprime
    y = y0 - yprime
    r = x**2+y**2
    B = -1*(mu0/4*np.pi)*(I/np.sqrt(r))
    #if we have the magnitude we can calculate the components by multiplying it with the unit vector 
    Bx = B*(-1*np.sin(np.arctan(y/x)))
    By = B*(np.cos(np.arctan(y/x)))
    return Bx,By

##Next we want to setup the geometry for the system that we will use in our plotting and B calculation
#We will create a function to get the x,y coordinates for the specific angles in each trig section
def geo(theta):
    Arange = np.linspace(theta-sector,theta+sector,4)
    x = []
    y = []
    r = np.linspace(iA,oA,4)
    for i in range(len(Arange)):
        for j in range(len(Arange)):
            x.append(r[i]*np.cos(Arange[j]))
            y.append(r[i]*np.sin(Arange[j]))
    return x,y
#We then want to seperate the sections of the problem into different parts that we can individually
#Look at their magnetic fields
    
p1x,p1y = geo(0)
p2x,p2y = geo(np.pi/2)
p3x,p3y = geo(np.pi)
p4x,p4y = geo((3/2)*np.pi)

def Btot(x,y): #summing up the comnpoennts above and below the cables
       return np.array([np.sum(BsavAbove(p2x,x,p2y,y)[0]+BsavAbove(p4x,x,p4y,y)[0] + BsavBelow(p1x,x,p1y,y)[0] + BsavBelow(p3x,x,p3y,y)[0]),np.sum(BsavAbove(p2x,x,p2y,y)[1]+BsavAbove(p4x,x,p4y,y)[1] + BsavBelow(p1x,x,p1y,y)[1] + BsavBelow(p3x,x,p3y,y)[1])])

xi = np.linspace(-0.1,0.1,15)
yi = np.linspace(-0.1,0.1,15) 

def Bfield(): #Calculating the final Bfield using the total B field calculation sum above
    Bx=np.zeros((len(xi),len(xi)))
    By=np.zeros([len(xi),len(xi)])
    for i in range(len(xi)):
        for j in range (len(xi)):
            #B1x,B1y = BsavBelow(p1x,xi[i],p1y,yi[j])
            #B2x,B2y = BsavAbove(p2x,xi[i],p2y,yi[j])
            #B3x,B3y = BsavBelow(p3x,xi[i],p3y,yi[j])
            #B4x,B4y = BsavAbove(p4x,xi[i],p4y,yi[j])
            Bx[i][j]=Btot(xi[i],yi[j])[0]
            By[i][j]=Btot(xi[i],yi[j])[1]
            
    return Bx,By
start = time.time()
Bx,By = Bfield()

Bxm=np.sum(Bx)
Bym=np.sum(By)

plt.streamplot(xi,yi,Bx,By)
plt.xlabel('x (m)')
plt.ylabel('y (m)')
plt.show()
end = time.time()
print("Elapsed Time ",end-start)

print(Bxm,Bym)

Bzm = 0
Q1e={"Bx":Bxm,"By":Bym,"Bz":Bzm}
sol={"b":Q1b,"e":Q1e}

with open('exam/VVNSAR001/answers.json','w') as f:
    json.dump(sol,f)  