# -*- coding: utf-8 -*-
"""
Created on Thu Jul  9 13:49:27 2020

@author: erinj
"""

#####CP Exam #####

import numpy as np
import matplotlib.pyplot as plt
import json
np.random.seed(19)

#y=[x,y,z,vx,vy,vz]
#g=[vx,vy,vz,ax,ay,az]

#Intial values
E=7*10**12  #eV
mp= 938*10**6  #eV*c^2
c=3*10**8   #speed of light

def yvect(x0,y0,z0,E, theta, phi):    #theta[0,pi/2], phi[0,2pi]
    mp= 938*10**6  #eV*c^2
    c=3*10**8   #speed of light
    v=np.sqrt(1-(mp/E)**2)*c
    theta=np.pi-theta   #because we're in the -z direction
    vx=v*np.sin(theta)*np.cos(phi)  
    vy=v*np.sin(theta)*np.sin(phi)
    vz=v*np.cos(theta)
    if theta==np.pi:
        vx=0
        vy=0
    return np.array([x0,y0,z0,vx,vy,vz])

  

def g(y):  #takes input of the form y=[x,y,z,vx,vy,vz]
    q=1.6*10**(-19)
    mp=1.67*10**(-27)  #kg
    K=223     #Tm^-1
    x=y[0]
    y0=y[1]
    vx=y[3]
    vy=y[4]
    vz=y[5]
    
    v2=vx**2+vy**2+vz**2
    Bx=K*y0
    By=K*x
    Bz=0
    
    
    ax=(q/mp)*(-vz*By)
    ay=(q/mp)*(vz*Bx)
    az=0
    
    return np.array([vx,vy,vz,ax,ay,az])



# 4th order Runge Kutta

def RK4(g,y0,dt):  
    y=[]
    y.append(y0)
    z=y[0][2]
    counter=0
    timer=[0]
    while -z<=3.1:# and counter<=200000:
        k1=dt*g(y[-1])
        k2=dt*g(y[-1]+0.5*k1)
        k3=dt*g(y[-1]+0.5*k2)
        k4=dt*g(y[-1]+k3)
        y.append(y[-1]+(1/6)*(k1+2*k2+2*k3+k4))
        z=y[-1][2]
        counter+=1
        timer.append(timer[-1]+dt)
    return np.array(y), timer, counter     

Y=yvect(0.01,0.0,0,E,0,0)
print('y-',Y)
G=g(Y)

    
ans=RK4(g,Y,10**(-12))
#print(ans)
xpos=[]
ypos=[]
zpos=[]

xv=[]

time=[]

for j in range(len(ans[0])):
    xpos.append(ans[0][j][0])
    ypos.append(ans[0][j][1])
    zpos.append(ans[0][j][2])
    xv.append(ans[0][j][3])
    time.append(ans[1][j])

print(ans[0][-1])

def momentum(vx,vy,vz):
    mgeV=1.67*10**(-27)*(1/5.36)*10**19
  #  gamma=1/np.sqrt(1-(vx**2+vy**2+vz**2)/c**2)
    return [mgeV*vx,mgeV*vy,mgeV*vz]  #returns in GeV/c^2

final_pos=[xpos[-1],ypos[-1],zpos[-1]]

final_momentum=momentum(ans[0][-1][3], ans[0][-1][4], ans[0][-1][5])
print('final y=',ans[0][-1])

sol1={"x":final_pos[0], "y":final_pos[1], "z":final_pos[2],"px":final_momentum[0],"py":final_momentum[1],"pz":final_momentum[2]}

#plt.title("Motion of particle with IC x=0.01, y=0.01")
plt.ylabel('x (m)')
plt.xlabel("Time (s)")
plt.plot(time,xpos)
plt.figure()
plt.ylabel('y (m)')
plt.xlabel('time (s)')
plt.plot(time, ypos)
plt.figure()
plt.plot(time,zpos)
plt.ylabel("Z")




###Part C 1: x<1, y=0,z=0, theta=0, phi=0
x=np.linspace(0,0.05,5)
#x=[0.01]
plt.figure()
for i in range(len(x)):
    Y=yvect(x[i],0,0,E,0,0)
    ans=RK4(g,Y,10**(-13))
    xpos=[]
    ypos=[]
    zpos=[]
    time=[]
    
    for j in range(len(ans[0])):
        xpos.append(ans[0][j][0])
        ypos.append(ans[0][j][1])
        zpos.append(ans[0][j][2])
        time.append(ans[1][j])
    #plt.title("Motion of particle starting from various values of x")        
    plt.ylabel('x(m')
    plt.xlabel('z(m)')
    plt.plot(zpos,xpos,label='x0='+str(x[i]))
    plt.legend()
    plt.gca().invert_xaxis()
    
#Part C 2: x=0, y<1, z=0, theta=0, phi=0
plt.figure()
y0=np.linspace(0,0.05,5)
for i in range(len(x)):
    Y=yvect(0,y0[i],0,E,0,0)
    ans=RK4(g,Y,10**(-13))
    xpos=[]
    ypos=[]
    zpos=[]
    time=[]
    
    for j in range(len(ans[0])):
        xpos.append(ans[0][j][0])
        ypos.append(ans[0][j][1])
        zpos.append(ans[0][j][2])
        time.append(ans[1][j])
    #plt.plot("Motion of protons starting at various values of y")        
    plt.ylabel('y (m)')
    plt.xlabel('z (m)')
    plt.plot(zpos,ypos,label='y0='+str(y0[i]))
    plt.legend()   
    plt.gca().invert_xaxis()


#Part C 3: x=0, y=0, z=? with theta=? and phi=?
#plt.figure()
theta=np.random.uniform(0,np.pi/4,5)
phi=np.random.uniform(0,2*np.pi,5)
x0=np.random.uniform(0,0.05,5)
y0=np.random.uniform(0,0.05,5)

plt.figure()
plt.gca().invert_xaxis()
for i in range(len(theta)):
    Y=yvect(x0[i],y0[i],0,E,theta[i], phi[i])
    ans=RK4(g,Y,10**(-13))
    xpos=[]
    ypos=[]
    zpos=[]
    time=[]
    
    for j in range(len(ans[0])):
        xpos.append(ans[0][j][0])
        ypos.append(ans[0][j][1])
        zpos.append(ans[0][j][2])
        time.append(ans[1][j])
    plt.ylabel('y (m)')
    plt.xlabel('z (m)')
    #plt.title('Motion of protons starting from various x,y positions at various angles')
    plt.plot(zpos,ypos,label='phi0='+str(phi[i]))
    #plt.legend()       


##################################
    #PART 2
########################

def area(r):
    return np.pi*r**2

def B(x,y,cz, xpos,ypos):     #x,y are position of point, cz is the current direction ie 1 or -1
    x0=xpos-x
    y0=ypos-y
    r=np.sqrt((x0)**2+(y0)**2) #xpos, ypos are position of magnets
    I=1187 *48
    A=(area(59*10**(-3))-area(28*10**(-3)))/6
    K=I/(0.028*np.pi/3*3.1)
    J=I/A
    mu=4*np.pi*10**(-7)
    if y0==0 and x0>0:
        theta=0
    if y0==0 and x0<0:
        theta=np.pi
    if x0==0:
        if y0>0:
            theta=np.pi/2
        if y0<0:
            theta=3*np.pi/2
    
    if x0!=0 and y0!=0:
        theta=np.arctan(y0/x0)

    theta_hat=cz*np.array([-np.sin(theta),np.cos(theta)])
    
    return mu*J/12*(r*np.arctan(3.1/r)+1.55*np.log(9.61+r**2)-3.507)*theta_hat
    

def B_tot(x,y):
    return B(x,y,-1,0.029,y)+B(x,y,-1,-0.029,y)+B(x,y,1,x,0.029)+B(x,y,1,x,-0.029)

b1=B(0,0,-1,0.029,0)
b2=B(0,0,-1,-0.029,0)
b3=B(0,0,1,0,0.029)
b4=B(0,0,1,0,-0.029)

print(b1)
print(b2)
print(b3)
print(b4)
bt=b1+b2+b3+b4


#create grid
x=np.linspace(-28,28,57)*10**(-3)
y=np.linspace(-28,28,57)*10**(-3)
L=len(x)
Bx=np.zeros((L,L))
By=np.zeros((L,L))
Bxy=np.zeros((L,L))

for i in range(L):
    for j in range(L):
        Bxy[i][j]=np.linalg.norm(B_tot(x[i],y[j]))
        Bx[i][j]=B_tot(x[i],y[j])[0]
        By[i][j]=B_tot(x[i],y[j])[1]
    

xindex=0
yindex=0
for k in range(len(x)):
    if x[k]==0.01:
        xindex=k
    if y[k]==0:
        yindex=k
        
print(Bxy[xindex][yindex])
print(Bx[xindex][yindex])
print(By[xindex][yindex])

sol2={"Bx":Bx[xindex][yindex],"By":By[xindex][yindex], "Bz":0}

def math_B(x,y):
    K=223
    bx=K*y
    by=K*x
    return np.array([bx,by])

Bx_1=np.zeros((L,L))
By_1=np.zeros((L,L))
Bxy_1=np.zeros((L,L))



for i in range(L):
    for j in range(L):
        Bxy_1[i][j]=np.linalg.norm(math_B(x[i],y[j]))
        Bx_1[i][j]=math_B(x[i],y[j])[0]
        By_1[i][j]=math_B(x[i],y[j])[1]

print('start')
print(Bxy[10][10])
print(Bxy[14][28])
print(Bxy[43][30])
print()
print(Bxy_1[10][10])
print(Bxy_1[14][28])
print(Bxy_1[43][30])

b_xline=[]
b1_xline=[]

b_yline=[]
b1_yline=[]


for i in range(len(Bxy)):
    b_xline.append(Bxy[yindex][i])
    b1_xline.append(Bxy_1[yindex][i])
    b_yline.append(Bxy[i][yindex])
    b1_yline.append(Bxy_1[i][yindex])

plt.figure()
plt.plot(y,b_xline,label='Biot-Savart')
plt.plot(y,b1_xline, label='ideal field')
plt.legend()
plt.ylabel('B (T)')
plt.xlabel('y (m)')

plt.figure()
plt.plot(x,b_yline,label='Biot-Savart')
plt.plot(x,b1_yline,label='ideal field')
plt.legend()
plt.ylabel('B (T)')
plt.xlabel('x (m)')

Bxy=np.transpose(Bxy)
plt.figure()
plt.contour(x,y,Bxy,levels=0)
#plt.colorbar() 

Bx=np.transpose(Bx)
By=np.transpose(By)
Bx_1=np.transpose(Bx_1)
By_1=np.transpose(By_1)
plt.streamplot(x,y,Bx,By)
plt.xlabel('x(m)')
plt.ylabel('y(m)')
#plt.streamplot(x,y,Bx_1,By_1)


sol={"1":{"b":sol1,"e":sol2}}
#creating a json file to write my solution into 
with open('exam/JRVERI002/answers.json','w') as f:
    json.dump(sol,f)