import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


K = 223 #T/m
L = 3.1 # m
z0 = L/2
pBeam = 7 * 5.344286e-16 #TeV/c to kgm/s
c= 3e8 #m/s
pCharge = 1.602e-19 #Coulombs#
pMass = 1.67e-27 #kg
radius = 0.056/2


#figure_folder = "./"
figure_folder = "exam/VMRROB003/"

def conver_mom_to_GeV(momentum): #converts momentum in kgm/s to GeV/c
    return momentum/(5.344286e-19)

def in_region(x,y,z): #in magnetic field
    rad = np.sqrt(np.square(x)+np.square(y))
    if z > z0 or z < -z0: return False
    if rad > radius: return False
    return True


def rk4(func, dt, y0): #4th order RK method, loops until proton has left region of magnetic field
    iterations = 1
    y = [y0] #initial vector
    latest_x,latest_y,latest_z = [y[0][0],y[0][1],y[0][2]] #most recent position
    while(in_region(latest_x,latest_y,latest_z) or latest_z <= -z0): #if in magnetic field, or not yet entered
        iterations += 1 #counts number of dt intervals
        last_y = y[-1] #gets the last variable vector
        #applies the rk4 method
        k1 = dt * func(last_y)
        k2 = dt * func(last_y + 0.5*k1)
        k3 = dt * func(last_y + 0.5*k2)
        k4 = dt * func(last_y + 1.0*k3)

        new_y = last_y + 1./6. * ( k1 + 2*k2 + 2*k3 + k4) # computes the approximation to the next value
        latest_x,latest_y,latest_z = [new_y[0],new_y[1],new_y[2]] #gets the new positions
        y=np.vstack([y,new_y]) #appends the new variable vector to the result
    trange = np.linspace(0, iterations*dt, iterations) #determines the time points of the motion
    return y, trange
    
    


class genvelocity: #generalized velocity vector callable class
    def __init__(self, m, K, q): #can initialise different masses, charges, K values
        self.m = m
        self.K = K
        self.q = q
        
    def __call__(self,y): #returns [xdot, ydot, zdot, Fx, Fy, Fz] given a variable vector of position and momentum
        return_array = np.zeros(6)
        momentum = np.asarray([y[3],y[4],y[5]]) #momentum vector of y
        prefactor = c/np.sqrt(np.dot(momentum,momentum)+np.square(self.m)*np.square(c)) # prefactor of velocity value
        
        #relativistic equations of velocity
        return_array[0] = prefactor*momentum[0]
        return_array[1] = prefactor*momentum[1]
        return_array[2] = prefactor*momentum[2]
        
        if in_region(y[0],y[1],y[2]): #if in magnetic field, apply the force of the field
            return_array[3] = prefactor * (-1*self.q*self.K*y[0]*momentum[2]) #prefactor*-qKx pz
            return_array[4] = prefactor * (self.q*self.K*y[1]*momentum[2]) #prefactor*qKy pz
            return_array[5] =prefactor * self.q*self.K*(y[0]*momentum[0]-y[1]*momentum[1]) #prefactor*qK(x px - y py)
            
        else: #no force applied
            return_array[3] = 0 
            return_array[4] = 0
            return_array[5] = 0
        
        return np.asarray(return_array)    

def find_trajectory(y0,g,dt = 1e-11): #given input params, integrate the trajectory
    y_traj,t = rk4(g,dt,y0)
    return y_traj[::,0],y_traj[::,1],y_traj[::,2], y_traj[::,3],y_traj[::,4],y_traj[::,5],t #returns x,y,z,px,py,pz,t
    
def parta(): #finds the variable vector and generalised velocity vector of initial conditions
    y0 = [0.001,0,0,0,0,pBeam] #initial position [x,y,z,px,py,pz]
    print("Initial variable vector:", y0)
    g = genvelocity(pMass, K, pCharge) #initialise generalised velocity class
    print("Initial generalised velocity vector:", g(y0))   

def partb(): #finds the trajectory of the proton with the given initial conditions
    y0 = [0.001,0,-z0,0,0,pBeam] #starting point - entrance x=1mm,z=-1.55m
    g = genvelocity(pMass, K, pCharge)
    x,y,z,px,py,pz, t = find_trajectory(y0,g, 1e-12)
    
    finalpos = [x[-1], y[-1],z[-1]] #position as proton leaves field
    
    print("Initial position:", y0[0], y0[1],y0[2]) #entrance position
    print("Final position:", finalpos) #Final position
    
    final_momentum = conver_mom_to_GeV(np.asarray([px[-1], py[-1], pz[-1]])) #Final momentum vector

    print("Initial momentum:", conver_mom_to_GeV(pBeam)) #Initial given momentum
    print("Final momentum:", np.sqrt(np.dot(final_momentum,final_momentum))) #Final magnitude of momentum
    
    
    #plot and save trajectory in 3d
    fig = plt.figure()
    plt.title("Trajectory of proton displaced 1mm in x direction")
    ax = fig.gca(projection='3d')
    ax.set_xlabel('x (m)')
    ax.set_ylabel('y (m)')
    ax.set_zlabel('z (m)')
    ax.plot(x,y,z)
    plt.savefig(figure_folder + "single3dtrajectory.png")
    
    
    return finalpos, final_momentum

def partcplot(y0,plot_num = 1): #plot_num: 1-x,2-y
    #calculates trajectory of particle with given initial conditions
    g = genvelocity(pMass, K, pCharge)
    x,y,z,px,py,pz, t = find_trajectory(y0,g, 1e-11)
    
    if plot_num == 1:#if x plot
        plt.plot(z,x,'k') #plots zx plane
    if plot_num == 2:
        plt.plot(z,y,'k') #plots zy plane
    
def partc():
    xrange = np.linspace(-0.01,0.01,20) #range of initial x/y values
    #part i
    plt.figure()
    plt.title("Trajectory of protons moving with x displacement")
    plt.xlabel('z (m)')
    plt.ylabel('x (m)')
    for x in xrange: #for each x displacement, plot the trajectory
        y0 = [x,0,-z0,0,0,pBeam]
        partcplot(y0,1)
    plt.tight_layout()
    plt.savefig(figure_folder+"xdisplace.png")
    
    #part ii
    plt.figure()
    plt.title("Trajectory of protons moving with y displacement")
    plt.xlabel('z (m)')
    plt.ylabel('y (m)')
    for y in xrange: #for each y displacement, plot the trajectory
        y0 = [0,y,-z0,0,0,pBeam]
        partcplot(y0,2)
    plt.tight_layout()
    plt.savefig(figure_folder+"ydisplace.png")
    
    #part iii
    thetarange = np.arange(0,np.pi*2-np.pi/4+0.1,np.pi/4) #range of initial theta values (theta in xy plane)
    zrange = np.linspace(-z0-3,-z0-0.1,6) #range of initial z values
    phi = np.pi/1000 #angle from z axis - small to keep protons in field at all times
    g = genvelocity(pMass, K, pCharge)
    fig3d = plt.figure() 
    ax3d = fig3d.gca(projection='3d')
    
    fig2d = plt.figure()
    ax2d = fig2d.gca()

    for zi in zrange: # for each z values
        for thetai in thetarange: #for each theta value
            y0 = [0,0,zi,pBeam*np.sin(phi)*np.cos(thetai),pBeam*np.sin(phi)*np.sin(thetai),pBeam*np.cos(phi)] #convert from spherical to cartesian
            x,y,z,px,py,pz, t = find_trajectory(y0,g, 1e-11) #compute the trajectory
            ax3d.plot(x,y,z,'k') #plot in 3d
            ax2d.plot(x,y,'k') #plot 2d projection on xy plane
            
    ax3d.set_xlabel('x (m)')
    ax3d.set_ylabel('y (m)')
    ax3d.set_zlabel('z (m)')
    ax3d.set_title("Trajectory of every initial condition at fixed angle")
    fig3d.tight_layout()
    fig3d.savefig(figure_folder+"3dangle.png")
    
    ax2d.set_xlabel('x (m)')
    ax2d.set_ylabel('y (m)')
    ax2d.set_title("2d projection of every initial condition at fixed angle")
    fig2d.tight_layout()
    fig2d.savefig(figure_folder+"2dangle.png")

def main(): #main function called by exam_solution
    parta()
    finalpos, finalmom = partb()
    partc()
    return finalpos,finalmom #returns relevant values for json file

#if __name__ == "__main__":
#    main()
  