import q1
import q2
import json
#import numpy as np

#json_folder = "./"
json_folder = "exam/VMRROB003/"

def write_json(finalpos,finalmom,field): #saves results to json file as required
    results = {}
    results['1'] = {}
    results['1']['b'] = {}
    results['1']['e'] = {}
    
    results['1']['b']['x'] = finalpos[0]
    results['1']['b']['y'] = finalpos[1]
    results['1']['b']['z'] = finalpos[2]
    results['1']['b']['px'] = finalmom[0]
    results['1']['b']['py'] = finalmom[1]
    results['1']['b']['pz'] = finalmom[2]
    
    results['1']['e']['Bx'] = field[0]
    results['1']['e']['By'] = field[1]
    results['1']['e']['Bz'] = field[2]
    
    with open(json_folder + "solutions.json", 'w') as outfile: #saves the dictionary to json file
        json.dump(results, outfile)

def main(): #runs each part of exam in order
    finalpos,finalmom = q1.main()
    field = q2.main()
    write_json(finalpos,finalmom, field)

if __name__=="__main__":
    main()