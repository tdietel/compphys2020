import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

#figure_folder = "./"
figure_folder = "exam/VMRROB003/"

#bounds of geometry used in integration
r_bounds = [0.056/2,0.118/2]
z_bounds = [-1.55,1.55]

#given constants 
mu0over4pi = 1e-7
current = 11870 * 48 #48 wires of given current
area = (np.pi/6)*(np.square(r_bounds[1]) - np.square(r_bounds[0])) #area of sector


#integrand from Biot Savart law ito integration variables, positions, direction (+-1 = +-zhat) and vector component
def integrand(rprime,thetaprime,zprime,x,y,z,direction = 1, component = 0): 
    xprime = rprime * np.cos(thetaprime) #converting from cylindrical
    yprime = rprime * np.sin(thetaprime)
    if component == 0: # if x component
        crossprod = direction * (-1*(y-yprime)) #cross product gives for x component
    elif component == 1:
        crossprod = direction*(x-xprime)  #cross product gives for y component
    magcubed = np.sqrt(np.square(x-xprime)+np.square(y-yprime)+np.square(z-zprime))**3
    return rprime*crossprod/magcubed
    

def integrate_bundle(x,y,z, direction, theta_bounds, epsabs = 1e-2): #integrates 1 given bundle
    field = np.zeros(3) #resultant field
    options = {'epsabs': epsabs} #sets the effective precision at which to integrate
    for component in range(2): #since cross product with +-z, Bz=0
        result = integrate.nquad(integrand,[r_bounds, theta_bounds, z_bounds], args = (x,y,z,direction,component),opts = options) #finds each component of the field
        field[component] = result[0]
    return field

def find_field_at_pos(x,y,z, epsabs=1e-2):
    field = np.zeros(3)
        
    #bundle 1 - right side
    direction = -1
    theta_bounds = [-np.pi/6,np.pi/6]
    partial_field = integrate_bundle(x,y,z,direction,theta_bounds, epsabs)
    field += partial_field
    
    #bundle 2 - top side
    direction = 1
    theta_bounds = [np.pi/3,2*np.pi/3]
    partial_field = integrate_bundle(x,y,z,direction,theta_bounds, epsabs)
    field += partial_field
    
    #bundle 3 - left side
    direction = -1
    theta_bounds = [5*np.pi/6,7*np.pi/6]
    partial_field = integrate_bundle(x,y,z,direction,theta_bounds, epsabs)
    field += partial_field
    
    #bundle 4 - bottom side
    direction = 1
    theta_bounds = [4*np.pi/3,5*np.pi/3]
    partial_field = integrate_bundle(x,y,z,direction,theta_bounds, epsabs)
    field += partial_field
    
    field = mu0over4pi*(current/area)* field #taking uniform current density J=I/A
    return field
    
def parte(): #finds field strength at point from part a
    field = find_field_at_pos(1e-3,0,0)
    print(field)
    return field
    

def partf():
    K = 223 #ideal field gradient
    
    plt.figure()
    yfields = []
    expected_yfields = []
    xs = np.linspace(-r_bounds[0]+1e-3,r_bounds[0]-1e-3,8) #maximum x/y values - inner radius
    
    for x in xs: #for each x value
        yfields.append(find_field_at_pos(x,0,0,epsabs=1e-1)[1]) #find calculated field
        expected_yfields.append(K*x) #find ideal field
    plt.plot(xs, yfields, 'k', label = "Calculated")
    plt.plot(xs, expected_yfields, 'b', label = "Ideal")
    plt.legend()
    plt.title("Magnetic field y component at y=z=0")
    plt.xlabel("x (m)")
    plt.ylabel("By (T)")
    plt.savefig(figure_folder + "calculatedvsidealorigin.png")
    
    plt.figure()
    xfields = []
    expected_xfields = []
    for y in xs: #for each y value
        xfields.append(find_field_at_pos(0,y,1,epsabs=1e-2)[0]) #find calculated field
        expected_xfields.append(K*y) #find ideal field
    plt.plot(xs, xfields, 'k', label = "Calculated")
    plt.plot(xs, expected_xfields, 'b', label = "Ideal")
    plt.legend()
    plt.title("Magnetic field x component at x=0, z=1")
    plt.xlabel("y (m)")
    plt.ylabel("Bx (T)")
    plt.savefig(figure_folder + "calculatedvsidealdisplaced.png")
    

def partg(): #generating field map
    K = 223 #ideal field gradient
    r = r_bounds[0] #inside beam 
    lim = r/np.sqrt(2)-1e-3 #defines x/y limit in square slightly smaller than cylinder
    numpoints = 4 # 16 total evaluations
    xs = np.linspace(-lim,lim,numpoints)
    ys = np.linspace(-lim,lim,numpoints)
    Bxs = np.zeros((numpoints,numpoints))
    Bys = np.zeros((numpoints,numpoints))
    expected_Bxs = np.zeros((numpoints,numpoints))
    expected_Bys = np.zeros((numpoints,numpoints))
    for i in range(len(xs)):
        for j in range(len(ys)):
            #for x/y value find field at that point
            field = find_field_at_pos(xs[i],ys[j],0,10)
            Bxs[i][j] = field[0]
            Bys[i][j] = field[1]
            #find ideal field at that point
            expected_Bxs[i][j] = K*ys[j]
            expected_Bys[i][j] = K*xs[i]
            
    #plot map of calculated field in xy plane 
    plt.figure()
    plt.title("Map of magnetic field at z=0")
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")
    plt.streamplot(xs,ys,Bys,Bxs) #streamplot swaps x/y
    plt.xlim((-lim,lim))
    plt.ylim((-lim,lim))
    plt.savefig(figure_folder + "fieldmap.png")
    
    #plot map of ideal field in xy plane 
    plt.figure()
    plt.title("Map of idealised magnetic field at z=0")
    plt.xlabel("x (m)")
    plt.ylabel("y (m)")
    plt.streamplot(xs,ys,expected_Bys,expected_Bxs) #streamplot swaps x/y
    plt.xlim((-lim,lim))
    plt.ylim((-lim,lim))
    plt.savefig(figure_folder + "idealfieldmap.png")
    
            

def main():
    field = parte()
    partf()
    partg()
    return field
    

#if __name__ == "__main__":
#    main()
#    