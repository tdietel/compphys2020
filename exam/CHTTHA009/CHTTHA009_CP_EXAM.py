# chttha009 - thavish chetty - computational physics exams
# section 1 : importing modules
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from mpl_toolkits.mplot3d import Axes3D
import json

# section 2 ; question 1 a
def quadrupole():

    mp = 1.67262192369e-27  # mass of proton in kilograms
    l = 3.10  # in metres
    beam = 7.00e12  # eV
    k = 223.0  # Gradient in T.m^-1
    e = 1.602176634e-19  # coulomb
    c = 299792458.00  # metres per second

    global position
    position = ["x", "y", "z"]
    global momenta
    momenta = ["px", "py", "pz"]

    def ev_to_joules(ev):
        energy = ev*1.602176634e-19
        return energy

    def ev_to_momentum(ev):
        momentum = (e/c)*ev
        return momentum

    def momentum_in_Gev_c(p):
        ev_c = p*(c/e)
        Gev_c = ev_c*1e-9
        return Gev_c


    def mom_to_speed(m, p):
        a = (p/m)**2
        v = np.sqrt((a)/(1 + a/c**2))
        return v


    def inverse_lorentz(vx, vy, vz):
        gamma = (np.sqrt(1 - (vx**2 + vy**2 + vz**2)/(c**2)))
        return gamma

    pz = ev_to_momentum(beam)


    # initials conditions:
    vz0 = mom_to_speed(mp, pz)
    vx0 = 0.0
    vy0 = 0.0
    x0 = 0.001
    y0 = 0.0
    z0 = 0.0
    gamma0 = inverse_lorentz(vx0, vy0, vz0)
    x_range = np.arange(0.0, 0.1 + 0.02, 0.02)
    y_range = np.arange(0.0, 0.1 + 0.02, 0.02)


    T = l/vz0  # time total to travel entire length
    print("Time", T)
    steps = 10000
    deltat = T/steps

    def quad_lensing(t, r):

        x = r[0]
        y = r[1]
        z = r[2]
        vx = r[3]
        vy = r[4]
        vz = r[5]

        gamma = inverse_lorentz(vx, vy, vz)
        a = (e*k*vz*gamma)/mp
        dvxdt = - a*x
        dvydt = a*y
        dydt = vy
        dxdt = vx
        dzdt = vz
        dvzdt = 0.0
        return np.array([dxdt, dydt, dzdt, dvxdt, dvydt, dvzdt])

    t_range = np.arange(0.0, T + deltat, deltat)
    solution = solve_ivp(quad_lensing, (0.0, T), [x0, y0, z0, vx0, vy0, vz0], t_eval=t_range, method="RK45")
    x, y, z, vx, vy, vz = solution.y



    final_v = [vx[steps], vy[steps], vz[steps]]
    global final_x
    final_x = [x[steps], y[steps], z[steps]]
    gamma_f = inverse_lorentz(vx[steps], vy[steps], vz[steps])
    g = (1/gamma_f)*mp
    global final_mom
    final_mom = [momentum_in_Gev_c(g*vx[steps]), momentum_in_Gev_c(g*vy[steps]), momentum_in_Gev_c(g*vz[steps])]

    print(final_x)
    print(final_mom)


    def angled_plot():
        solution = solve_ivp(quad_lensing, (0.0, T), [-0.2, 0.2, z0, -20000, 20000, vz0], t_eval=t_range, method="RK45")
        x, y, z, vx, vy, vz = solution.y
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.plot(x, y, z, label='xyz-trajectory')
        ax.legend()

        plt.show()

    def horizontal_placement():
        fig = plt.figure()
        for i in x_range:
            solution = solve_ivp(quad_lensing, (0.0, T), [i, 0.0, z0, vx0, vy0, vz0], t_eval=t_range, method="RK45")
            x, y, z, vx, vy, vz = solution.y

            ax = fig.gca(projection='3d')
            ax.plot(x, y, t_range, label='xy-trajectory')
            ax.legend()

        plt.show()

    def vertical_placement():
        for j in y_range:
            solution = solve_ivp(quad_lensing, (0.0, T), [0.0, j, z0, vx0, vy0, vz0], t_eval=t_range, method="RK45")
            x, y, z, vx, vy, vz = solution.y

            fig = plt.figure()
            ax = fig.gca(projection='3d')
            ax.plot(t_range, x, y, label='xy-trajectory')
            ax.legend()

            plt.show()

def magnets():
    current = 11870.0
    gridpoints = 24
    neg_current = -1*current
    mu_0 = 1.25663706212e-6
    bundle = 48
    fan = (2*np.pi/6)
    rho_1 = 0.056
    rho_2 = 0.118
    r_split = 8
    t_split = 6
    global position
    position = ["x", "y", "z"]
    global momenta
    momenta = ["px", "py", "pz"]
    global magnetic_field
    magnetic_field = ["Bx", "By", "Bz"]

    def arc(r):
        return (2/6)*np.pi*r

    in_length = arc(rho_1)
    out_length = arc(rho_2)

    def chord(r, o):
        t = r*np.sin(o/2)
        return t

    def sector_generate(N):

        theta_0 = np.full(N//t_split, np.pi/3)
        rho_0 = np.linspace(rho_1, rho_2, N//t_split)
        theta = []
        rho = []
        rho.append(rho_0)
        theta.append(theta_0)
        for k in range(t_split - 1):
            theta_0 = theta_0 + fan/(t_split - 1)
            theta.append(theta_0)
            rho.append(rho_0)

        theta = np.array(theta)
        rho = np.array(rho)
        theta = theta.flatten()
        rho = rho.flatten()
        print(len(theta))
        print(theta)
        pos_amps = np.full(N, current)
        neg_amps = np.full(N,  neg_current)

        rotation_angles = [theta, theta - np.pi/2, theta - np.pi, theta - 3*np.pi/2]

        x_points = []
        y_points = []
        currents = []
        for i in range(len(rotation_angles)):
            x = rho*np.cos(rotation_angles[i])
            y = rho*np.sin(rotation_angles[i])
            if i == 0 or i == 2:
                cur = pos_amps
                currents.append(cur)
            elif i == 1 or i == 3:
                cur = neg_amps
                currents.append(cur)


            x_points.append(x)
            y_points.append(y)


        x_points = np.array(x_points)
        y_points = np.array(y_points)
        currents = np.array(currents)

        x_points = x_points.flatten()
        y_points = y_points.flatten()
        currents = currents.flatten()
        return np.array([x_points, y_points, currents])

    cables = sector_generate(bundle)
    plt.figure()
    plt.plot(cables[0], cables[1], 'bo', markersize=2)
    plt.show()

    x_grid = np.linspace(-0.120, 0.120, gridpoints)
    y_grid = np.linspace(-0.120, 0.120, gridpoints)
    xp, yp = np.meshgrid(x_grid, y_grid)


    def biot_savart(xw, yw, c, xp, yp):
        mag = (mu_0*c)/((2*np.pi)*(np.sqrt((xp - xw)**2 + (yp - yw)**2)))
        by = mag * (np.cos(np.arctan2(yp - yw, xp - xw)))
        bx = mag * (-np.sin(np.arctan2(yp - yw, xp - xw)))
        bx = bx.flatten()
        by = by.flatten()
        return bx, by


    def vectorfield():
        bx, by = biot_savart(cables[0][0], cables[1][0], cables[2][0], xp, yp)

        for j in range(1, bundle*4):
            b_x, b_y = biot_savart(cables[0][j], cables[1][j], cables[2][j], xp, yp)
            bx = bx + b_x
            by = by + b_y

        bx = np.reshape(bx, (gridpoints, gridpoints))
        by = np.reshape(by, (gridpoints, gridpoints))

        plt.figure()
        plt.plot(cables[0][:bundle], cables[1][:bundle], 'ro', markersize=2)
        plt.plot(cables[0][bundle:2*bundle], cables[1][bundle:2*bundle], 'bo', markersize=2)
        plt.plot(cables[0][2*bundle:3*bundle], cables[1][2*bundle:3*bundle], 'ro', markersize=2)
        plt.plot(cables[0][3*bundle:], cables[1][3*bundle:], 'bo', markersize=2)
        plt.quiver(xp, yp, bx, by)
        plt.show()

    def off_centre(xp, yp):
        bx, by = biot_savart(cables[0][0], cables[1][0], cables[2][0], xp, yp)

        for j in range(1, bundle*4):
            b_x, b_y = biot_savart(cables[0][j], cables[1][j], cables[2][j], xp, yp)
            bx = bx + b_x
            by = by + b_y

        bz = []
        bz.append(0.0)
        bz = np.array(bz)

        return bx, by, bz

    vectorfield()

    global Magfield
    Magfield = off_centre(0.001, 0.0)
    print(Magfield)






    #print(b_field)
    #for j in range(1000*4):
     #       b = biot_savart(cables[0][j], cables[1][j], cables[2][j], xp, yp)


























answers = {}





def question_1():
    print("Updating q1 answers")
    q1 = {}
    q1b = {}
    q1e = {}
    for i, label in enumerate(position):
        q1b.update({label: final_x[i]})
    for j, label in enumerate(momenta):
        q1b.update({label: final_mom[j]})
    for k, label in enumerate(magnetic_field):
        q1e.update({label: Magfield[k][0]})
    q1.update({"b": q1b})
    q1.update({"e": q1e})
    answers.update({"1": q1})


def write_file():
    with open('exam/CHTTHA009/answers.json', 'w') as f:
        json.dump(answers, f)


if __name__ == '__main__':
    quadrupole()
    magnets()
    question_1()
    write_file()
