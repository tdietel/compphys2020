# -*- coding: utf-8 -*-

"""

Created on Mon Apr 20 14:21:16 2020

@author: erinj
"""

#CP tut 3
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.integrate
from scipy.optimize import curve_fit
import json

#Normal distribution

def std_norm(x):
    mu=0
    sigma=1
    return 1/(sigma*np.sqrt(2*np.pi))*np.exp(-(x-mu)**2/(2*sigma**2))


#Trapezoid rule

def trap(f,N):
    x=np.linspace(-1,1,N+1)
    a=0
    dx=(x[1]-x[0])
    for i in range(len(x)-1):
        a+=(dx/2)*(f(x[i])+f(x[i+1]))
    return a

#simpson rule
def sim(f,N):
    x=np.linspace(-1,1,N+1)
    a=0
    dx=(x[1]-x[0])
    for i in range(len(x)-1):
        a+=(dx/6)*(f(x[i])+4*f((x[i]+x[i+1])/2)+f(x[i+1]))
    return a



def power(x,n):
    a=[]
    if n==2:
        for i in range(len(x)):
            a.append((133/500)*(x[i])**(-n))
    if n==4:
        for i in range(len(x)):
            a.append((133/625)*(x[i])**(-n))   
    return a

N=[2,4,10,20,40,80,160]



trapsol={}  #solution dict trapezoid 
simsol={}   #solution dict simpson
traps_er=[]   # trap error
simps_er=[]   #Sim error
std=0.682689492137086 #exact value 

#calculate integrals and errors using simpson an trap

for i in range(len(N)):
    t=trap(std_norm,N[i])
    trapsol[str(N[i])]=t
    traps_er.append(np.abs(t-std)/std)
    
    s=sim(std_norm, N[i])
    simsol[str(N[i])]=s
    simps_er.append(np.abs(s-std)/std)


##Richardson extrapolation
    
def rich(method, n, k,t):
    a=(method(std_norm, n)-t**k*method(std_norm, 2*n))/(1-t**k)
    return a



Rtrapsol={}      #Richardson trapezoid
richt_er=[]   #Richardson trap error
Rsimsol={}      #Richardson simpson
richs_er=[]   #Richardson sim error

for i in range(len(N)):
    t=2
    Rtrapsol[str(N[i])]= rich(trap,N[i],2,t)
    richt_er.append(np.abs(rich(trap,N[i],2,t)-std)/std)
    Rsimsol[str(N[i])]=rich(sim,N[i],4,t)
    richs_er.append(np.abs(rich(sim,N[i],4,t)-std)/std)
    


n=[2,3,4]
 

q=[]
qer=[]
sner=[]
rser=[]
gausssol={}

for i in range(len(n)):
    l=sp.integrate.fixed_quad(std_norm, -1,1,n=n[i])
    gausssol[str(n[i])]=l[0]
    q.append(l[0])
    qer.append(np.abs(q[i]-std)/std)
    sner.append(np.abs(std-sim(std_norm,n[i]))/std)
    rser.append(np.abs(rich(sim,n[i],4,t)-std)/std)

print(gausssol)

#Integral  error
plt.figure()
plt.plot(N,traps_er,label='Trapezoid')
plt.plot(N,simps_er,label='Simpson')
#plt.plot(N,power(N,2),'--',label='O(2)')
#plt.plot(N,power(N,4), '--',label='O(4)')
plt.plot(N,richt_er, label='Rich trap')
plt.plot(N,richs_er, label='Rich Sim')
#plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.xlabel('N')
plt.ylabel('Error')
#plt.title("Error of integration methods as a function of number (N) of points")


plt.figure()
plt.plot(n,sner, label="Simpson")
plt.plot(n,qer, label="Gauss quad")
#plt.plot(n,rser,label="Rich Sim")
plt.legend()
plt.xlabel('N')
plt.ylabel("Error")
plt.yscale('log')
  
######################################
#Question 2 -likelihood
######################################


d={"0":3,"1":4,"2":3,"3":8,"4":8,"5":2,"6":5,"7":0,"8":1,"9":2}

counts=[3,4,3,8,8,2,5,0,1,2]
bin_edges=[0,1,2,3,4,5,6,7,8,9,10]
bin_centers=[]
for i in range(len(bin_edges)-1):
    bin_centers.append(0.5*(bin_edges[i]+bin_edges[i+1]))
 

#Plot histogram

h=[]
for i in range(len(counts)):
    for m in range(counts[i]):
        h.append(bin_centers[i])

plt.figure()
plt.hist(h,bins=bin_edges)
plt.xlabel('x')
plt.ylabel('counts')

###Likelihood

def signal(x,sigma,mu,S):
    return S/(sigma*np.sqrt(2*np.pi))*np.exp(-(x-mu)**2/(2*sigma**2))
    

def total(x,sigma,mu,S,B):
    return signal(x,sigma,mu,S) +B

def likelihood(x,y,sigma,mu,S,B):
    a=1
    k=scipy.integrate.quad(signal, mu-sigma,mu+sigma, args=(sigma,mu,S))
    for i in range(len(x)):
        for s in range(y[i]):
            a=a*(0.68/k[0])*total(x[i],sigma,mu,S,B)
    return a

p1=[2,5,20,1]
Lsol=likelihood(bin_centers,counts,p1[0],p1[1],p1[2],p1[3])

print(Lsol)    
    
x=np.linspace(0,10,40)
#plt.plot(x, total(x,p1[0],p1[1],p1[2],p1[3]))


#best fit parameters:
p0=[10,4,30,2]
popt, pcov= curve_fit(total, bin_centers, counts, p0=p0)
print(popt)

print(likelihood(bin_centers,counts,*popt))
print(likelihood(bin_centers,counts, 0.32, 4, 12,4))

for i in range(len(p0)):
    print(i)
    print(pcov[i][i])

###Plots of likelihood for each parameter

#set up range near optimum
sig=[popt[0]-0.2,popt[0]+1]
sigrange=np.linspace(sig[0],sig[1],20)

m=[popt[1]-1,popt[1]+1]
mrange=np.linspace(m[0],m[1],50)

s=[popt[2]-7,popt[2]+7]
srange=np.linspace(s[0],s[1],80)

b=[popt[3]-1,popt[3]+1]
brange=np.linspace(b[0],b[1],60)

Lsig=[]
for i in range(len(sigrange)):
    Lsig.append(likelihood(bin_centers,counts, sigrange[i],popt[1],popt[2],popt[3]))
lmax=0
lmax_index=0
for i in range(len(Lsig)):
    if Lsig[i]==max(Lsig):
       lmax_index=i

print('sigma=',sigrange[lmax_index])

Lmu=[]
L1=[]
L2=[]
L3=[]
for i in range(len(mrange)):
    Lmu.append(likelihood(bin_centers,counts, popt[0],mrange[i],popt[2],popt[3]))
    L1.append(likelihood(bin_centers,counts,0.8,mrange[i],14.2,2.5 ))
    L2.append(likelihood(bin_centers,counts,0.6,mrange[i],14.2,2.5))
    L3.append(likelihood(bin_centers,counts,0.4,mrange[i],14.2,2.5))
lmax=0
lmax_index=0
for i in range(len(Lmu)):
    if Lmu[i]==max(Lmu):
       lmax_index=i

print('mu=',mrange[lmax_index])

Ls=[]
for i in range(len(srange)):
    Ls.append(likelihood(bin_centers, counts, popt[0],popt[1],srange[i],popt[3]))
lmax_index=0
for i in range(len(Ls)):
    if Ls[i]==max(Ls):
       lmax_index=i

print('S=',srange[lmax_index])

Lb=[]
for i in range(len(brange)):
    Lb.append(likelihood(bin_centers,counts, popt[0],popt[1],popt[2],brange[i]))
lmax=0
lmax_index=0
for i in range(len(Lb)):
    if Lb[i]==max(Lb):
       lmax_index=i

print('B=',brange[lmax_index])

fit=total(x,*popt)
plt.plot(x,fit)
       
plt.figure()
plt.plot(sigrange,Lsig)
plt.xlabel('Sigma')
plt.ylabel('Likelihood')

plt.figure()
plt.plot(mrange,Lmu, label="sigma=0.33")
plt.plot(mrange,L1, label="sigma=0.8")
plt.plot(mrange,L2,label="sigma=0.6")
plt.plot(mrange,L3,label="sigma=0.4")
plt.xlabel('Mu')
plt.ylabel('Likelihood')
plt.legend()

plt.figure()
plt.plot(srange,Ls)
plt.xlabel("S")
plt.ylabel('Likelihood')

plt.figure()
plt.plot(brange,Lb)
plt.xlabel("B")
plt.ylabel('Likelihood')



sol={"1":{"a":{"trapezoid":trapsol,"simpson":simsol},"b":{"trapezoid":Rtrapsol,"simpson":Rsimsol},"c":gausssol},"2":{"a":Lsol}}


with open('tut3/JRVERI002/solutions.json','w') as f:
    json.dump(sol,f)
