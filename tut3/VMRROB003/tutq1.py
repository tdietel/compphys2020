import numpy as np
import matplotlib.pyplot as plt
import integration as intg
import scipy.integrate
import json

def write_json(data, filename = "tut3/VMRROB003/solutions.json"):  # writes a dictionary to a json file
    with open(filename, 'w') as outfile:
        json.dump(data, outfile)
    
def load_json(filename = "tut3/VMRROB003/solutions.json"):  # loads a json file into a dictionary
    with open(filename, 'r') as infile:
        data = json.load(infile)
    return data   

def gauss(x, mu, sigma):
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp(-np.square((x-mu)/sigma)/2)

def plot_errors(Ns, gaussNs, traperrors,simperrors,richtraperrors,richsimperrors,gausserrors):
         
    #Q1a plot
    plt.figure()
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel("N")
    plt.ylabel("|Error|")
    plt.plot(Ns, traperrors, 'bo', label = "Trapezoid rule")
    plt.plot(Ns, simperrors, 'go', label = "Simpson rule")
    plt.legend()
    plt.savefig("tut3/VMRROB003/q1a.png")
    
    #Q1b plot
    plt.figure(figsize=(8,8))
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel("N")
    plt.ylabel("|Error|")
    plt.plot(Ns, traperrors, 'b', label = "Trapezoid rule")
    plt.plot(Ns, richtraperrors, 'bo', label = "Trapezoid rule - Richardson")
    plt.plot(Ns, simperrors, 'g', label = "Simpson rule")
    plt.plot(Ns, richsimperrors, 'go', label = "Simpson rule - Richardson")
    plt.legend()
    plt.savefig("tut3/VMRROB003/q1b.png")
    
    #Q1c plot
    plt.figure()
    plt.yscale('log')
    plt.xlabel("N")
    plt.ylabel("|Error|")
    plt.plot(gaussNs, gausserrors, 'ok-', label = "Gaussian quadrature")
    plt.plot(Ns[:2], traperrors[:2], 'bo', label = "Trapezoid rule")
    plt.plot(Ns[:2], simperrors[:2], 'go', label = "Simpson rule")
    plt.legend()
    plt.savefig("tut3/VMRROB003/q1c.png")


def main():
    sigma = 1
    mu = 0
    results = {}
    results["1"] = {}
    results["2"] = {}
    results["1"]["a"] = {}
    results["1"]["b"] = {}
    results["1"]["c"] = {}
    
    
    exact = 0.682689492137
    Ns = [2,4,10,20,40,80,160]
    trapvals = []
    traperrors = []
    simpvals = []
    simperrors=[]
    richtrapvals = []
    richtraperrors = []
    richsimpvals = []
    richsimperrors = []
    gaussvals = []
    gausserrors = []
    
    for N in Ns:
        #Trapezium method
        inttrap = intg.trap_int(gauss, mu, sigma, mu-sigma, mu+sigma, N)
        trapvals.append(inttrap)
        traperrors.append(np.abs((inttrap-exact)/exact))
        
        #Simpson method
        intsimp = intg.simp_int(gauss, mu, sigma, mu-sigma, mu+sigma, N)
        simpvals.append(intsimp)
        simperrors.append(np.abs((intsimp-exact)/exact))
        
        #Trapezium Richardson
        doubleinttrap = intg.trap_int(gauss, mu, sigma, mu-sigma, mu+sigma, N*2)
        richinttrap = intg.richardson(inttrap,doubleinttrap,2,2)
        richtrapvals.append(richinttrap)
        richtraperrors.append(np.abs((richinttrap-exact)/exact))
        
        #Simpson Richardson
        doubleintsimp = intg.simp_int(gauss, mu, sigma, mu-sigma, mu+sigma, N*2)
        richintsimp = intg.richardson(intsimp,doubleintsimp,2,4)
        richsimpvals.append(richintsimp)
        richsimperrors.append(np.abs((richintsimp-exact)/exact))
        
    gaussNs = [2,3,4]
    for N in [2,3,4]:
        intgauss = intg.gauss_int(gauss, N)
        gaussvals.append(intgauss)
        gausserrors.append(np.abs((intgauss-exact)/exact))
        
        
        # recording results for json file
    results["1"]["a"]["trapezoid"] = {}
    for i in range(len(Ns)):
        results["1"]["a"]["trapezoid"][str(Ns[i])] = round(trapvals[i],6)
        
    results["1"]["a"]["simpson"] = {}
    for i in range(len(Ns)):
        results["1"]["a"]["simpson"][str(Ns[i])] = round(simpvals[i],6)
      
    results["1"]["b"]["trapezoid"] = {}
    for i in range(len(Ns)):
        results["1"]["b"]["trapezoid"][str(Ns[i])] = round(richtrapvals[i],6)
        
    results["1"]["b"]["simpson"] = {}
    for i in range(len(Ns)):
        results["1"]["b"]["simpson"][str(Ns[i])] = round(richsimpvals[i],6)
        
    results["1"]["c"] = {}
    for i in range(len(gaussNs)):
        results["1"]["c"][str(gaussNs[i])] = round(gaussvals[i],6)
        
    write_json(results)
    
    plot_errors(Ns, gaussNs, traperrors,simperrors,richtraperrors,richsimperrors,gausserrors)
    