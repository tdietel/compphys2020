import numpy as np

def trap_int(func, mu, sigma, x0, xend, N):  # trapezoid method
    xrange = np.linspace(x0,xend,N+1)
    tot = 0
    for i in range(N):
        tot += ((xrange[i+1]-xrange[i])/2)*(func(xrange[i], mu, sigma)+func(xrange[i+1], mu, sigma))
        
    return tot

def simp_int(func, mu, sigma, x0, xend, N): #simpson method
    xrange = np.linspace(x0,xend,N+1)
    tot = 0
    for i in range(N):
        tot += ((xrange[i+1]-xrange[i])/6)*(func(xrange[i], mu, sigma)+4*func((xrange[i]+xrange[i+1])/2, mu, sigma)+func(xrange[i+1], mu, sigma))
        
    return tot

def richardson(est, diffest, t, k0):  #richardson extrapolation for evaluated integrals
    #est is first integral, diffest is for the second integral for comparison
    return (est-np.power(t,k0)*diffest)/(1-np.power(t,k0))


def gauss_int(func, N):  # gaussian quadrature method
    if N==2:
        xs = [1/np.sqrt(3),-1/np.sqrt(3)]
        ws = [1,1]
    if N==3:
        xs = [0.774597,-0.774597,0]
        ws = [0.5555,0.5555,0.8889]
    if N==4:
        xs = [0.86113631,-0.86113631,0.33998104,-0.33998104]
        ws = [0.34785485,0.34785485,0.65214515,0.65214515]
    tot = 0
    for i in range(N):
        tot += ws[i]*func(xs[i],0,1)
    return tot