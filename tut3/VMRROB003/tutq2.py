import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import tutq1 as t1
import math

def func(x, S, mu, sigma, B):
    return S*((1/(sigma*np.sqrt(2*np.pi)))*np.exp((-1/2)*(np.square((x-mu)/sigma))))+B

def calc_likelihood(xdata, ydata, func, params):
    yguess = func(xdata, *params)
    total = 1
    # uses eqn 1.9 from notes - likelihood given by Poisson distribution of counts per bin
    for i in range(len(xdata)):
        total *= (np.power(yguess[i], ydata[i]) * np.exp(-1*yguess[i]))/math.factorial(ydata[i])
    return total

def parta(xdata, ydata):
    params = [20,5,2,1]
    
    #plot relevant guess
    plt.figure()
    plt.ylabel("Counts")
    plt.xlabel("x")
    plt.bar(xdata,ydata, width = 1, label = "Raw data")
    xrange = np.linspace(0,10,1000)
    yrange = func(xrange, *params)
    plt.plot(xrange, yrange, 'r',label =  "Guessed fit")
    plt.legend()
    plt.savefig("tut3/VMRROB003/q2a.png")
    
    
    #calculate likelihood of guess
    likelihood = calc_likelihood(xdata, ydata, func, params)
    print("Likelihood guess:", likelihood)
    results = t1.load_json()
    results["2"]["a"] = likelihood
    t1.write_json(results)
    return likelihood

def fit(xdata,ydata, p0):
    #curve fit of the parameters
    names = ["S", "mu", "sigma", "B"]
    popt, pcov = curve_fit(func, xdata, ydata, p0, absolute_sigma = True)
    dymin = (ydata - func(xdata, *popt))
    min_chisq = sum(dymin*dymin)
    dof = len(xdata) - len(popt)
    print("chisq/dof", min_chisq/dof)
    print('Fitted parameters with 68% C.I.:')
    
    for i, pmin in enumerate(popt):
        print("%2i %-10s %12f +/- %10f" %(i, names[i], pmin, np.sqrt(pcov[i,i])*np.sqrt(min_chisq/dof)))
    print()
    
    print("Correlation matrix")
      
    for i in range(len(popt)):
        print("%10s"%(names[i]))
        for j in range(i+1):
            print(names[j], "%10f"%(pcov[i,j]/np.sqrt(pcov[i,i]*pcov[j,j])))
        print()

    return popt
    

def partb(xdata, ydata, guesslikelihood):
    p0 = [13,4,0.4,2.5] #initial guess
    
    popt = fit(xdata, ydata,p0)  # fits to the given function
    
    likelihood = calc_likelihood(xdata, ydata, func, popt) # determines the likelihood of the fitted parameters
    print("Likelihood fit:", likelihood)
    print("Relative likelihood", likelihood/guesslikelihood)
    
    #plot fitted curve
    plt.figure()
    plt.ylabel("Counts")
    plt.xlabel("x")
    plt.bar(xdata,ydata, width = 1, label = "Raw data")
    xrange = np.linspace(0,10,1000)
    yrange = func(xrange, *popt)
    plt.plot(xrange, yrange, 'r',label =  "Fitted estimate")
    plt.legend()
    plt.savefig("tut3/VMRROB003/q2b.png")
    
    
    return popt

def plot_contour(param1, param2, xdata, ydata, func, popt, param1index, param2index):  # plots a contour plot of 2 different parameters
    names = ['S','μ', 'σ', 'B']
    likelihood = np.zeros((len(param1), len(param2)))
    for ia,a in enumerate(param1):
        for ib,b in enumerate(param2):
            params = list(popt)
            params[param1index] = a
            params[param2index] = b
            likelihood[ib,ia] = calc_likelihood(xdata,ydata,func, params)
    plt.contourf(param1,param2,likelihood)
    plt.xlabel(names[param1index])
    plt.ylabel(names[param2index])
    plt.colorbar()
    plt.plot(popt[param1index], popt[param2index], 'ro')

def partc(xdata, ydata, popt):
    srange = np.linspace(5,25,100)
    murange = np.linspace(3.5,4.5,100)
    sigrange = np.linspace(1e-9,2,100)
    brange = np.linspace(1,4,100)
    
    plt.figure()
    
    #sb
    plt.subplot(231)
    plot_contour(srange, brange, xdata, ydata, func, popt, 0, 3)
   
    #smu
    plt.subplot(232)
    plot_contour(srange, murange, xdata, ydata, func, popt, 0, 1)
   
    #ssigma
    plt.subplot(233)
    plot_contour(srange, sigrange, xdata, ydata, func, popt, 0, 2)
    
    #musigma
    plt.subplot(234)
    plot_contour(murange, sigrange, xdata, ydata, func, popt, 1, 2)
    
    #mub
    plt.subplot(235)
    plot_contour(murange, brange, xdata, ydata, func, popt, 1, 3)
    
    #sigmab
    plt.subplot(236)
    plot_contour(sigrange, brange, xdata, ydata, func, popt, 2, 3)
    
    
    plt.tight_layout()
    plt.savefig("tut3/VMRROB003/q2c.png")
    

def main():
    xdata = np.arange(0.5,10.5,1)
    ydata = np.asarray([3,4,3,8,8,2,5,0,1,2])
    likelihood = parta(xdata, ydata)
    
    popt = partb(xdata,ydata, likelihood) 
    
    partc(xdata,ydata,popt)
