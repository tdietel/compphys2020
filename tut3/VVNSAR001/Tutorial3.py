# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 07:52:11 2020

@author: hp
"""
import numpy as np
from scipy import integrate as intg
import matplotlib.pyplot as plt
from scipy import stats as sts
import math as mth
import scipy
import json
#intial values
gausA = 0.682689492137086
mean = 0
sigmaUnc = 1  #[0,1] because this is a PDF function so we know that the x ranges only in that interval
samplePoints = [2, 4, 10, 20, 40, 80, 160]
Order = [2,3,4]
trapArr = []
simpArr = []
trapErrArr = []
simpErrArr = []
rTrapA = []
rSimpA = []
solutions = {}

def function(x,mu,sig):
    return (1/(sig*np.sqrt(2*np.pi)))*np.exp(-((x-mu)**2)/2*sig**2)

def trap(mu,sig,N):  
    x = np.linspace(-1,1,N) #creating an x range that has N[i]+1 units
    y = function(x,mean,sigmaUnc)
    delX = x[1] - x[0] #determining the interval difference for np.trapz 
    trapSum = np.trapz(y,x,dx=delX,axis=-1)
    return(trapSum)
    
def simp(mu,sig,N):
    x = np.linspace(-1,1,N) #creating an x range that has N[i]+1 units
    y = function(x,mean,sigmaUnc)
    delX = x[1] - x[0] #determining the interval difference for intg.simps
    simpSum = intg.simps(y,x,dx=delX,axis=-1)
    return(simpSum)

#next the error, we will assume double precision errors, st err = 10e-15 p121
def relError(sumA,area):
    relErr = (sumA-area)/area
    
    return(relErr)

#determining errors
for i in range(len(samplePoints)):
    ###Trap
    trapSum = trap(mean,sigmaUnc,samplePoints[i])
    trapArr.append(trapSum)
    trapErr = np.abs(relError(trapSum,gausA))
    trapErrArr.append(trapErr)
    ###simpson
    simpSum = simp(mean,sigmaUnc,samplePoints[i])
    simpArr.append(simpSum)
    simpErr = np.abs(relError(simpSum,gausA))
    simpErrArr.append(simpErr)

   
##richardson
richErrSimp = []
richErrTrap = []
def rich(t,n,a1,a0):
    return ((t**n*a1)-a0)/((t**n)-1)
print(len(samplePoints))

for i in range(len(samplePoints)):
    if i != len(samplePoints)-1:
        rTrap = rich(2,trapArr[i+1],trapArr[i],2)
        rTrapA.append(rTrap)
        rErrTrap = np.abs(relError(rTrap,gausA))
        richErrTrap.append(rErrTrap)
        ##
        rSimp = rich(2,simpArr[i+1],simpArr[i],4)
        rSimpA.append(rSimp)
        rErrSimp = np.abs(relError(rSimp,gausA))
        richErrSimp.append(rErrSimp)
    else:
        pass

print(richErrSimp,"  Trap ",richErrTrap)


##guasian - We will use the intgerate.fixed_quad function

gf = lambda x: (1/(sigmaUnc*np.sqrt(2*np.pi)))*np.exp(-((x-mean)**2)/(2*sigmaUnc**2))
gfA = []
gfErrA = []
for i in range(2,5):
    gF = intg.fixed_quad(gf,-1,1, n = i)
    gfA.append(gF[0])
    gfErr = np.abs(relError(gF[0],gausA))
    gfErrA.append(gfErr)
print(gfErrA)
#plots
xT = np.linspace(-1,1,10000)
yRange = function(xT,mean,sigmaUnc)
plt.figure(1)
plt.plot(xT,yRange,color='r')
plt.xlabel("Pdf")
plt.ylabel("Probability")
plt.show()
plt.figure(2)
plt.plot(samplePoints,trapErrArr,label="Trapezoidal",color='b')
plt.plot(samplePoints,simpErrArr,label="Simpson",color='g')
plt.legend()
plt.yscale('log')
plt.xlabel("Interval Ranges (N)")
plt.ylabel("log(Relative Error)")
plt.show()
##Rich
xValuesR = np.linspace(0,160,6)
plt.figure(3)
plt.plot(xValuesR,richErrTrap,label="Richardson Trapezoidal",color='b')
plt.plot(xValuesR,richErrSimp,label="Richardson Simpson",color='g')
plt.legend()
plt.yscale('log')
plt.xlabel("Interval Ranges (N)")
plt.ylabel("log(Relative Error)")
plt.show()
##gauss
xValues = [2,3,4]
plt.figure(4)
plt.plot(xValues,gfErrA,label="Gaussian",color='r')
plt.xlabel("Interval Ranges (N)")
plt.ylabel("log(Relative Error)")
plt.yscale('log')
plt.show()

##################################
######## QUESTION 2 ###############
###################################

yHist = [3,4,3,8,8,2,5,0,1,2]
xHist = np.linspace(1,10,10)
#we want to do a norm distribution on the histogram and then check the validity of the fit with the likelihood function
def Ndist(x,S,sig,B,mu):
    fit = (S)*sts.norm.pdf(x,mu,sig) + B
    return fit 
fit = Ndist(xHist,20,2,1,5)
plt.bar(xHist,yHist,width=-1,align='edge',zorder=5)
plt.scatter(xHist,fit,label = 'Normal Distribution Fit',color = 'r',zorder=10)
plt.xlabel('x Intervals')
plt.ylabel('Sample Values')
plt.legend()
plt.show()

#likelihood function
def lfunc(lamd,x):#the Lfunction follows a poisson distribution
    likeHood = ((np.exp(-lamd))/(mth.factorial(x)))*lamd**x
    return likeHood

def Likelihood(S,mu,sigma,B):  #calculating the likelihood - comparing the given values vs calculated values
    yHist = [3,4,3,8,8,2,5,0,1,2]
    xHist = np.linspace(1,10,10)    
    likehood = 1

    
    for i in range(len(yHist)):
        a0 = yHist[i]
        a1 = Ndist(xHist[i],S,sigma,B,mu)
        likehood *= lfunc(a1,a0)      #the likelihood is the product of probabilities from the lfunc
    return likehood

lHoodValue = Likelihood(14.2,4,1.6,4.4)
print(lHoodValue)

#To find the values for which the likelihood is a maximum we can use the covariance matrix
popt,pcov = scipy.optimize.curve_fit(Ndist,xHist,yHist,p0=[3,3.7,2.2,3])
print(popt)
##from the popt we can get the values for which it is a maximum
Sm = 14.2  #vertical
muM =  4 #spreads
sigM = 1.6
Bm = 4.4

fit = Ndist(xHist,Sm,sigM,Bm,muM)
plt.bar(xHist,yHist,width=-1,align='edge',zorder=5)
plt.scatter(xHist,fit,label = 'Normal Distribution Fit',color = 'r',zorder=10)
plt.xlabel('x intervals')
plt.ylabel('Sample Values')
plt.legend()
plt.show()
    
#####
#def contour(p1,p2,x,y,popt,func):
#    parameters = ['S','mu','sig','B']
#    lHood = np.zeros((len(p1), len(p2)))
#    for i in enumerate(p1):
#        for j in enumerate(p2):
#            params = list(popt)
#            likelihood[i,j] = (xdata,ydata,func,params)
#    plt.contourf(p1,p2,likelihood)
#    plt.colorbar()
def writingFile():
    q1 = {}
    a = {}
    b = {}
    trapz_1a = {}
    simps_1a = {}
    trapz_1b = {}
    simps_1b = {}
    quad_1c = {}
    for i, N in enumerate(samplePoints[:-1]):
        trapz_1a.update({str(N): trapErrArr[i]})
        simps_1a.update({str(N): simpErrArr[i]})
        trapz_1b.update({str(N): richErrTrap[i]})
        simps_1b.update({str(N): richErrSimp[i]})
    for j, M in enumerate(Order):
        quad_1c.update({str(M): gfErrA[j]})
    a.update({"Trapezoid": trapz_1a})
    a.update({"Simpson": simps_1a})
    b.update({"Richardson Trapezoid": trapz_1b})
    b.update({"Richardson Simpson": simps_1b})
    q1.update({"a": a})
    q1.update({"b": b})
    q1.update({"c": quad_1c})
    global solutions
    solutions.update({"1": q1})    
    
def write_file():
    with open('tut3/VVNSAR001/tut3Code.json', 'w') as f:
        json.dump(solutions, f)

writingFile()
write_file()


    

    
    