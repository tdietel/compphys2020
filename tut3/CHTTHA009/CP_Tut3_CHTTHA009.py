# thavish chetty - chttha009 - computational physics - tutorial 3
# section 1: importing modules
import numpy as np
import scipy.stats as st
from scipy import integrate
from scipy.optimize import curve_fit, minimize
import matplotlib.pyplot as plt
import json


# section 2: initializing variables
# Question 1: variables
exact_confidence = 0.682689492137086  # exact area under gaussian with +/- sigma as bounds
mean = 50
stdev = 25
low_bound = mean - stdev
up_bound = mean + stdev
step_divider = 2  # halving parameter for step size

# Question 2: variables
S = 20
B = 1
mu_2 = 5
sigma_2 = 2

S_range = np.arange(0.0, 45.0, 1.0)
Mean_range = np.arange(0.0, 15.0, 0.1)
B_range = np.arange(0.0, 5.0, 0.1)
Sigma_range = np.arange(0.1, 5.0, 0.1)
guess = np.array([4, 3, 25, 1])
counts = [0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 6, 8, 9, 9]
histocounts = [3, 4, 3, 8, 8, 2, 5, 0, 1, 2]
bins = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
xx = np.linspace(0, 10, 100)


answers = {}
# global variables I want for my answers
sample_points = [2, 4, 10, 20, 40, 80, 160, 320]
order = [2, 3, 4]
trapz_approx = []
simps_approx = []
r_extra_trapz = []
r_extra_simps = []
quad_approx = []


# section 3: defining classes and functions
def rel_error(numerical, exact):
    return np.absolute((numerical - exact)/exact)


def gaussian(x, mu, sigma):
    return (1 / (sigma * np.sqrt(2 * np.pi))) * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2))


def gaussian2(x, mu, sigma, S, B):
    return S*(1 / (sigma * np.sqrt(2 * np.pi))) * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2)) + B



class pdf(object):
    def __init__(self, mu, sigma, left, right, points):
        self.mu = mu
        self.sigma = sigma
        self.left = left
        self.right = right
        self.points = points
        self.x = np.linspace(self.left, self.right, self.points)
        self.y = gaussian(self.x, self.mu, self.sigma)


def richardson(t, a_h, a_ht, k):
    return ((t**k)*a_ht - a_h)/(t**k - 1)

# section 4: Answering question 1: Integrating, Calculating relative error and Plotting
def integrate_and_plot():
    global sample_points
    global order
    global trapz_approx
    global simps_approx
    trapz_errors = []
    simps_errors = []
    for n in sample_points:
        a = pdf(mean, stdev, low_bound, up_bound, n)
        num_trapz = np.trapz(a.y, a.x)
        num_simps = integrate.simps(a.y, a.x)
        trapz_approx.append(num_trapz)
        simps_approx.append(num_simps)
        trapz_error = rel_error(num_trapz, exact_confidence)
        simps_error = rel_error(num_simps, exact_confidence)
        trapz_errors.append(trapz_error)
        simps_errors.append(simps_error)

    plt.figure()
    plt.plot(sample_points[:-1], trapz_errors[:-1], label="Trapezoidal Rule")
    plt.plot(sample_points[:-1], simps_errors[:-1], label="Simpson's Rule")
    plt.legend()
    plt.xlabel("Number of sample points")
    plt.ylabel("Relative error")
    plt.title("Relative error of numerical integration versus sample points used")
    plt.show()
    global r_extra_trapz
    global r_extra_simps
    for k in range(len(sample_points)):
        if k != len(sample_points) - 1:
            r_trapz = richardson(2, trapz_approx[k], trapz_approx[k+1], 2)
            r_simps = richardson(2, simps_approx[k], simps_approx[k+1], 4)
            r_extra_trapz.append(r_trapz)
            r_extra_simps.append(r_simps)
        else:
            pass
    global quad_approx
    quad_error = []
    for N in order:
        q = integrate.fixed_quad(gaussian, low_bound, up_bound, args=(mean, stdev), n=N)
        q_error = rel_error(q[0], exact_confidence)
        quad_error.append(q_error)
        quad_approx.append(q[0])
    plt.figure()
    plt.plot(order, quad_error, 'o', label="Gaussian Quadrature")
    plt.legend()
    plt.xlabel("Order of Gaussian Quadrature integration")
    plt.ylabel("Relative error")
    plt.title("Relative error of numerical integration versus order")
    plt.show()





def question_1():
    q_1 = {}
    a = {}
    b = {}
    trapz_1a = {}
    simps_1a = {}
    trapz_1b = {}
    simps_1b = {}
    quad_1c = {}
    for i, N in enumerate(sample_points[:-1]):
        trapz_1a.update({str(N): trapz_approx[i]})
        simps_1a.update({str(N): simps_approx[i]})
        trapz_1b.update({str(N): r_extra_trapz[i]})
        simps_1b.update({str(N): r_extra_simps[i]})
    for j, M in enumerate(order):
        quad_1c.update({str(M): quad_approx[j]})
    a.update({"trapezoid": trapz_1a})
    a.update({"simpson": simps_1a})
    b.update({"trapezoid": trapz_1b})
    b.update({"simpson": simps_1b})
    q_1.update({"a": a})
    q_1.update({"b": b})
    q_1.update({"c": quad_1c})
    global answers
    answers.update({"1": q_1})


# question 2: section 1
def fitting_and_likelihood():

    def likelihood(hist, intervals, mean, sigma, background, signal):
        leftcum = st.norm.cdf(intervals[:10], mean, sigma)
        rightum = st.norm.cdf(intervals[1:], mean, sigma)
        probability = rightum - leftcum
        Like_per_bin = st.poisson.pmf(hist, probability*signal + background)
        Like_total = np.prod(Like_per_bin)
        return Like_total
    global L
    L = likelihood(histocounts, bins, mu_2, sigma_2, B, S)
    print(L)


    def log_likelihood(params):
        logll = -np.log(likelihood(histocounts, bins, params[0], params[1], params[2], params[3]))
        return logll


    min_values = minimize(log_likelihood, guess, jac=False)
    print(min_values)

    maxLikelihood = likelihood(histocounts, bins, min_values.x[0], min_values.x[1], min_values.x[3], min_values.x[2])
    print(maxLikelihood)
    plt.figure()
    histogram = np.histogram(counts, bins)
    plt.hist(counts, bins, color="skyblue")
    plt.plot(xx, gaussian2(xx, min_values.x[0], min_values.x[1], min_values.x[3], min_values.x[2]))
    plt.xlabel('x')
    plt.ylabel('counts')
    plt.show()

    def contour_plot(range1, range2, num1, num2, hist, interv, opt_params):
        names = ["Mean", "Sigma", "Background", "Signal Strength"]
        llh = np.zeros((len(range2), len(range1)))
        for i, x in enumerate(range1):
            for j, y in enumerate(range2):
                params = list(opt_params)
                params[num1] = x
                params[num2] = y
                llh[j, i] = likelihood(hist, interv, params[0], params[1], params[2], params[3])

        meshx, meshy = np.meshgrid(range1, range2)
        plt.contourf(meshx, meshy, llh, cmap='viridis')
        plt.plot(opt_params[num1], opt_params[num2], 'bo')
        plt.xlabel(names[num1])
        plt.ylabel(names[num2])
        plt.colorbar()


    plt.figure()
    contour_plot(Mean_range, S_range, 0, 3, histocounts, bins, min_values.x)
    plt.show()

    plt.figure()
    contour_plot(Mean_range, Sigma_range, 0, 1, histocounts, bins, min_values.x)
    plt.show()

    plt.figure()
    contour_plot(Mean_range, B_range, 0, 2, histocounts, bins, min_values.x)
    plt.show()

    plt.figure()
    contour_plot(Sigma_range, B_range, 1, 2, histocounts, bins, min_values.x)
    plt.show()

    plt.figure()
    contour_plot(Sigma_range, S_range, 1, 3, histocounts, bins, min_values.x)
    plt.show()

    plt.figure()
    contour_plot(B_range, S_range, 2, 3, histocounts, bins, min_values.x)
    plt.show()


def question_2():
    q_2 = {}
    q_2.update({"a": L})
    global answers
    answers.update({"2": q_2})


def write_file():
    with open('tut3/CHTTHA009/answers.json', 'w') as f:
        json.dump(answers, f)


if __name__ == '__main__':
    integrate_and_plot()
    fitting_and_likelihood()
    question_1()
    question_2()
    write_file()







