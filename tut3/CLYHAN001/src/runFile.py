# Main run file

import json
import question1
import question2
import producePlots

# writes solutions to solutions.json file
def makeJson():
	Dict = {}

	# Results for Q1
	dict1 = {}
	# part a
	aResults = question1.partA()
	aDict = {"trapezoid": dict(zip(aResults[0],aResults[1][0])), "simpson": dict(zip(aResults[0],aResults[1][1]))}
	# part b
	bResults = question1.partB()
	bDict = {"trapezoid": dict(zip(aResults[0],bResults[1][0])), "simpson": dict(zip(aResults[0],bResults[1][1]))}
	# part c
	cResults = question1.partC()
	cDict = dict(zip(cResults[0],cResults[1]))

	dict1["a"] = aDict
	dict1["b"] = bDict
	dict1["c"] = cDict

	# Results for Q2
	dict2 = {"a": question2.partA()}


	Dict["1"] = dict1
	Dict["2"] = dict2


	# filename = "solutions.json"
	filename = "tut3/CLYHAN001/solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

# produces and saves relevant plots for Q1 and Q2
def plots():
	producePlots.makePlots()


def main():
	# produces output solutions.json file
	makeJson()
	# Produces and saves relevant plots for Q1 and Q2
	plots()
	question2.partBC()

if __name__ == '__main__':
	main()

