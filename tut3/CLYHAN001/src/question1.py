# for preparing required results and plots - will be called by runFile

import numpy as np
from matplotlib import pyplot as plt
import IntegrationMethods as integrate

def partA():
	Nvals = [2, 4, 10, 20, 40, 80, 160] 
	funcs = [integrate.trapezoid, integrate.simpson]
	integrationResults = []
	mu = 0
	sigma = 1

	for func in funcs:
		results = []
		for N in Nvals:
			results.append(func(N, mu, sigma))
		integrationResults.append(results)

	return [Nvals, integrationResults]

def partB():
	Nvals = [2, 4, 10, 20, 40, 80, 160, 320] 
	trapResults = []
	simpResults = []
	mu = 0
	sigma = 1
	
	for N in Nvals:
		trapResults.append(integrate.trapezoid(N,mu,sigma))
		simpResults.append(integrate.simpson(N,mu,sigma))
	
	trapExtrap = integrate.richardson(Nvals, trapResults, 2)
	simpExtrap = integrate.richardson(Nvals, simpResults, 4)



	return [Nvals, [trapExtrap,simpExtrap]]

def partC():
	Nvals = [2,3,4]
	integrationResults = []
	mu = 0
	sigma = 1

	for N in Nvals:
		integrationResults.append(integrate.gaussianQuadrature(N, mu, sigma))

	return [Nvals, integrationResults]




