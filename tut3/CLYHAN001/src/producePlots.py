# Produces plots for questions 1 (tut 3)

import numpy as np
from matplotlib import pyplot as plt
import IntegrationMethods as integrate
import question1


def relativeError(integrationResults):
	exactVal = 0.6826894921370858
	errorResults = np.abs(integrationResults-exactVal)/exactVal

	return errorResults

# helper method for Q1 relative errors plots
def plotErrors(Nvals, errorResults, label, title='blah'):
	plt.plot(Nvals, errorResults, label=label)
	plt.yscale('log')
	plt.xscale('log')
	plt.xlabel('Number of steps N')
	plt.ylabel(r'Relative error $\epsilon$')

# Q1 plot
# produces plot of relative errors for Trapezoid and Simpson methods (with Richardson extrapolations), and Gaussian quadrature method (N=2,3,4 only)
def plotTrapSimpGaussRichardson():
	integrationApprox = question1.partA()
	richardsonVals = question1.partB()
	Nvals = integrationApprox[0]

	plt.clf()
	plotErrors(Nvals, relativeError(np.array(integrationApprox[1][0])),"Trapezoid rule")
	plotErrors(Nvals, relativeError(np.array(integrationApprox[1][1])),"Simpson's rule")
	plt.plot(Nvals, relativeError(np.array(richardsonVals[1][0])),'--',label="Richardson extrapolation - Trapezoid")
	plt.plot(Nvals, relativeError(np.array(richardsonVals[1][1])),'--',label="Richardson extrapolation - Simpson")

	gaussianQuadApprox = question1.partC()
	plt.plot(gaussianQuadApprox[0], relativeError(np.array(gaussianQuadApprox[1])), label="Gaussian quadrature method")

	plt.title("Relative errors for Trapezoid and Simpson's rules, \n and Gaussian quadrature method")
	plt.legend()
	plt.savefig("tut3/CLYHAN001/Q1_RelativeErrors")
	# plt.savefig("Q1_RelativeErrors")



# Produces all necessary plots
def makePlots():
	# Q1
	plotTrapSimpGaussRichardson()


