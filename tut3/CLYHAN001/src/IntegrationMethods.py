import numpy as np
from matplotlib import pyplot as plt

# Normal distribution
def f(x, mu, sigma):
	return (1/(sigma*np.sqrt(2*np.pi)))*np.e**(-((x-mu)**2/(2*sigma**2)))

# Integrates the pdf using manually implemented trapezoidal rule 
def trapezoid(N, mu, sigma):
	lowerBound = mu - sigma
	upperBound = mu + sigma

	stepSize = 2*sigma/N

	integralSum = f(lowerBound, mu, sigma)
	for i in range(1, N):
		integralSum += 2*f(lowerBound+i*stepSize, mu, sigma)
	integralSum += f(upperBound, mu, sigma)
	integralSum = integralSum*0.5*stepSize

	return(integralSum)

# Integrates the pdf using manually implemented simpson rule 
def simpson(N, mu, sigma):
	lowerBound = mu - sigma
	upperBound = mu + sigma
	stepSize = 2*sigma/N

	integralSum = 0
	x = lowerBound
	steps = 0
	while steps<N/2:						## Simpsons rule has N/2 steps - due to rounding errors can't use x<upperBound
		steps+=1
		integralSum += f(x, mu, sigma) + 4*f(x+stepSize, mu, sigma) + f(x+2*stepSize, mu, sigma)
		x += 2*stepSize

	integralSum = integralSum*stepSize/3

	return(integralSum)

# Estimates numerical integration of pdf using Gaussian quadrature method (for N points)
def gaussianQuadrature(N, mu, sigma):
	# nodes and weights from literature for N=2,3,4
	nodes = [np.array([1/np.sqrt(3), -1/np.sqrt(3)]),np.array([0, np.sqrt(3/5), -np.sqrt(3/5)]),np.array([np.sqrt(3/7 - 2*np.sqrt(6/5)/7),-np.sqrt(3/7 - 2*np.sqrt(6/5)/7), np.sqrt(3/7 + 2*np.sqrt(6/5)/7),-np.sqrt(3/7 + 2*np.sqrt(6/5)/7)])]
	weights = [np.array([1,1]),np.array([8/9, 5/9, 5/9]),np.array([(18+np.sqrt(30))/36, (18+np.sqrt(30))/36, (18-np.sqrt(30))/36, (18-np.sqrt(30))/36])]

	# integration limits
	a = mu-sigma
	b = mu+sigma

	# nodes and weights for specified number of points N
	if N>=2 and N<=4:
		xi = nodes[N-2]
		wi = weights[N-2]
	else:
		exit("Don't have nodes and weights for Gaussian quadrature with specified number of steps")

	result = ((b-a)/2)*sum(wi*f(((b-a)/2)*xi + (a+b)/2, mu, sigma))
	
	return result

# input parameter n is the order of the estimate - 2 for trapezoid and 4 for simpson
def richardson(Nvals, integrationResults, n):
	extrapolatedVals = []

	for i in range(len(Nvals)-1):
		t = Nvals[i+1]/Nvals[i]
		extrapolatedVals.append(((t**n)*integrationResults[i+1]-integrationResults[i])/(t**n - 1))

	return extrapolatedVals
