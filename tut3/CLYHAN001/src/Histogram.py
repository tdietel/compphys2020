# Tut 3 Question 2

import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import minimize
import math
import scipy

# Normal distribution 
def f(x, S, mu, sigma, B):
	return S*(1/(sigma*np.sqrt(2*np.pi)))*np.e**(-((x-mu)**2/(2*sigma**2))) + B

# lamda is expected (from normal distribution with given parameters), and k is measured/observed value
def poisson(lamda, k):
	return (lamda**k)*(np.e**(-lamda))/(scipy.special.factorial(k))

# function to calculate the likelihood L for a parameter vector (S, µ, σ, B)
def L(S, mu, sigma, B):
	# histogram data
	x = np.linspace(0.5,9.5,10)						# midpoints of bins
	counts = np.array([3,4,3,8,8,2,5,0,1,2]) 		# bin counts
	likelihood = 1

	# loop over bins
	for i in range(len(counts)):
		observedi = counts[i]
		expectedi = f(x[i], S, mu, sigma, B)
		likelihood *= poisson(expectedi, observedi)		# likelihood is product of conditional probabilities (given by poisson dist)

	return likelihood

def findFit():
	x = np.linspace(0.5,9.5,10)						# midpoints of bins
	counts = np.array([3,4,3,8,8,2,5,0,1,2]) 		# bin counts
	popt,pcov = scipy.optimize.curve_fit(f, x, counts, p0=np.array([30,4,2,1]))#, sigma=np.sqrt(counts)) 
	print("Optimal fit parameters:", popt)
	print("Covariance Matrix:",pcov)

	perr =	np.sqrt(np.diag(pcov))
	print("Error estimates")
	print(perr)

	return popt

# plots histogram with normal distribution best fit
def plotHistFit(paras):
	plt.clf()
	x = np.linspace(0.5,9.5,10)						# midpoints of bins
	counts = np.array([3,4,3,8,8,2,5,0,1,2]) 		# bin counts

	plt.bar(x,counts, width=1, label='Data')
	plt.plot(np.linspace(0,10,1000),f(np.linspace(0,10,1000),*paras),'r-',label="Best fit")
	plt.xlabel("x")
	plt.ylabel("counts")
	plt.legend()
	plt.title("Best fit for binned data")

	plt.savefig("tut3/CLYHAN001/bestFitHistogram")
	# plt.savefig("bestFitHistogram")

def completeFit():
	paras = findFit()
	print("Likelihood for best fit parameters:", L(*paras))
	plotHistFit(paras)
	contourPlots(paras)

def contourPlots(paras):
	paraNames = ["S", r'$\mu$', r'$\sigma$', "B"]

	SS = np.linspace(0,30,70)
	mm = np.linspace(3,5,70)
	sigsig = np.linspace(0,2,70)
	BB = np.linspace(0,5,70)
	paralims = [SS, mm, sigsig, BB]

	plt.clf()
	fig = plt.figure(figsize=(15,7))

	counter = 1
	for i in range(len(paras)):
		for j in range(i+1,len(paras)):
			paraSet = [paras[0],paras[1],paras[2],paras[3]]
			aa = paralims[i]
			bb = paralims[j]
			cc = np.zeros((len(aa),len(bb)))
			for ia,a in enumerate(aa):
			    for ib,b in enumerate(bb):
			    	paraSet[i] = a
			    	paraSet[j] = b
			    	cc[ib,ia] = L(*paraSet)
			plt.subplot(2,3,counter)
			plt.contourf(aa,bb,cc)
			plt.plot(paras[i], paras[j], 'rx')
			plt.colorbar()
			plt.xlabel(paraNames[i])
			plt.ylabel(paraNames[j])
			counter+=1

	plt.subplots_adjust(wspace = 0.4, hspace = 0.3)
	fig.suptitle("Contour plots of likelihood for different parameter combinations")
	plt.savefig("tut3/CLYHAN001/contourPlots")
	# plt.savefig("contourPlots")


######## alternative method ###########
# Negative logarithm of the likelihood function - can be minimized to find maximal L
def minusLogL(paras):
	return -np.log(L(*paras))

# Possible way to find maximal L
def alternativeLMaximisation():
	guess = np.array([20, 3, 2, 1])
	min_values = minimize(minusLogL, guess, jac=False)
	print(min_values)

	# Print likelihood and plot fit obtained from this method (hard-coded results)
	print(L(22.88616744,  3.7368073 ,  1.65149817,  1.33620509))
	plotHistFit([22.88616744,  3.7368073 ,  1.65149817,  1.33620509])
