#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 13:02:48 2020

@author: rayhaan
"""

##############################
#Packages
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from scipy import stats
import scipy
import json
##############################

##############################
#Constants & Lists
mu = 0 #mean 
sig = 1 #sigma
narea = 0.682689492137086 #Exact value of area
N = [2,4,10,20,40,80,160] #Number of segments
tA = [] #Storing my trapazoidal areas
sA = [] #Storing simpson areas
xi = np.linspace(-1,1,10000) #just for plotting smooth gaussian
#deltax = x[1]-x[0]
##############################

##############################
#Functions
def normdist(x,u,s):
    return (1/(s*np.sqrt(2*np.pi)))*np.exp(-((x-u)**2)/(2*s**2))
###############################
    
###############################
#Gaussian
plt.figure(1)
yg = normdist(xi,mu,sig)
plt.plot(xi,yg,color = 'darksalmon')
plt.xlabel('x')
plt.ylabel('y')
plt.savefig('tut3/PRNMOG001/Normalplot.png',dpi=200)
###############################

###############################
#Numerically integrating using Trapz
for i in range(len(N)):
    x = np.linspace(-1,1,N[i]+1) #Creating N+1 points so that we have N intervals
    y = normdist(x,mu,sig) #generating y values
    deltax = x[1] - x[0] #finding length of the intervel
    Ap = np.trapz(y,x,dx=deltax,axis = -1) ##performing the integration using the trapezoidal rule using the numpy package
    tA.append(Ap) #Appending area results for different n to empty list

expectedT = [1/((i)**2) for i in N] #expected behaviour of trap rule
eT = [np.abs((a - narea)/narea) for a in tA] #relative error using trapezoidal rule
# plt.figure(2)
# plt.plot(N,expectedT,label = 'Expected Behaviour')
# plt.plot(N,eT,label = 'Error')
# plt.xlabel('N')
# plt.ylabel('Relative error / Expected behavior')
# plt.legend()
###############################

###############################
#Numerically integrate using Simpsons rule
for i in range(len(N)):
    x = np.linspace(-1,1,N[i]+1) #Creating N+1 points so that we have N intervals
    y = normdist(x,mu,sig) #generating y values
    deltax = x[1] - x[0] #finding length of the intervel
    Sr = integrate.simps(y,x,dx = deltax, axis = -1) #performing the integration using simpsons rule using the scipy package
    sA.append(Sr) #Appending the result for different n to a list
    
expectedS = [ 1/(i**4) for i in N] #expected behaviour of simpson rule
eS = [np.abs((a - narea)/narea) for a in sA] #relative error of simpson rule

#############################
#Plots
plt.figure(3)
plt.plot(N, eS, color = 'b', label = 'Simpson Error' )
plt.plot(N, eT, color = 'r', label = 'Trapezoid Error')
plt.legend()
plt.yscale('log')
plt.xlabel('Number of intervals (N)')
plt.ylabel('log(Relative error)')
plt.savefig('tut3/PRNMOG001/SandTerror.png',dpi = 200)

plt.figure(4)
plt.plot(N,expectedT, color = 'r' , label = 'Expected Error Trapezoid')
plt.plot(N,expectedS, color = 'b', label = 'Expected Error Simpsons')
plt.yscale('log')
plt.legend()
plt.xlabel('Number of intervals (N)')
plt.ylabel('log(Relative error)')
#plt.plot(N,expectedS,color = 'r',label = 'expected behaviour')
plt.legend()
plt.savefig('tut3/PRNMOG001/STexpected.png',dpi=200)
#precision to 15 decimal places due to 64bit Os and 64bit python cant discren numbers smaller than 2.2250738585072014e-308
###############################

###############################
#Richardson extrapolation
RT = []#Storing Richardson extrapolation results for trapezoidal rule
RS = []#Storing Richardson extrapolation results for simpsons' rule
rule = ['Trap','Simp']

for c in rule:
    if c == 'Trap':
        for i in range(len(tA)):
            if i == 0 :
                tAA = tA[i+1] + (1/3)*(tA[i+1] - tA[i])
            elif i == 1:
                tAA = np.nan
            elif 2 <= i < 6:
                tAA = tA[i+1] + (1/3)*(tA[i+1] - tA[i])
            elif i == 6:
                tAA = np.nan
            RT.append(tAA)
    if c == 'Simp':
        for i in range(len(sA)):
            if i == 0 :
                sAA = sA[i+1] + (1/15)*(sA[i+1] - sA[i])
            elif i == 1:
                sAA = np.nan
            elif 2 <= i < 6:
                sAA = sA[i+1] + (1/15)*(sA[i+1] - sA[i])
            elif i == 6:
                sAA = np.nan
            RS.append(sAA)

for i in range(2):
    RS.remove(np.nan)
    RT.remove(np.nan)

#print(RT)
#print(RS)
    
###############################
    
###############################
#Relative uncertainty
    
SuL = [] #emptylist to store results
SuL.append(RS)
SuL.append(RT)
l = [1,2,3,4,5]
lt = ['2-4','10-20','20-40','40-80','80-160']
plt.figure(13)
plt.xticks(l,lt)
plt.scatter(l,RT)
plt.ylim([0.6825,0.6831])
plt.xlabel('N-N used')
plt.ylabel('area')
plt.savefig('tut3/PRNMOG001/Rrich.png',dpi=200)
###############################
plt.figure(14)
plt.xticks(l,lt)
plt.scatter(l,RS)
plt.ylim([0.682,0.683])
plt.xlabel('N-N used')
plt.ylabel('area')
plt.savefig("tut3/PRNMOG001/Srich.png",dpi = 200)
#vSuL = statistics.variance(SuL)
################################
vRT = np.var(RT) #Finding the variance of the extrapolated data 
vRS = np.var(RS)
uRT = np.sqrt(vRT/len(RT)) #calculating the uncertainty on this value
#print(uSuL)
uRS = np.sqrt(vRS/len(RS))   
###############################

###############################
#Gaussian Quadrature Integration
gfg = lambda x: (1/(sig*np.sqrt(2*np.pi)))*np.exp(-((x-mu)**2)/(2*sig**2)) #Defining fucntion to put into integrate.fixed_quad

gA = []
for i in range(2,5):
    plo = integrate.fixed_quad(gfg,-1,1, n = i)
    gA.append(plo[0])
    
eG = [np.abs((a - narea)/narea) for a in gA]

###############################
#Plotting Gaussian Error relative to other methods
plt.figure(5)
plt.plot([2,3,4],eG, color = 'g', label = 'Gaussian Error')
plt.plot(N, eS, color = 'b', label = 'Simpson Error' )
plt.plot(N, eT, color = 'r', label = 'Trapezoid Error')
plt.yscale('log')
plt.legend()
plt.xlim([0,5])
plt.ylabel('log(relative error)')
plt.xlabel('N')
plt.savefig('tut3/PRNMOG001/Gerror.png',dpi=200)


################################
#Question 2
################################
################################
#Data
#sig = 2
y = [3,4,3,8,8,2,5,0,1,2] ##Generating histogram data
x = np.linspace(1,10,10) # x range of data
bins = range(0,11) #Just for display purposes, to show goodness of fit
xs = np.linspace(-100,100,1000)
# plt.figure(6)
# plt.scatter(x,y,color = 'r',label = 'Histogram Data')
# plt.xlabel('x')
# plt.ylabel('counts')
# plt.legend()
#################
def f(x,S,mu,sig,B):
    A = (S)*(1/(sig*np.sqrt(2*np.pi)))*np.exp(-((x-mu)**2)/(2*sig**2)) + B
    return A

yT = f(x,20,5,2,1)
##################################
#Plot for visulation and nice looking plot for writeup
##################################
plt.figure(7)
plt.fill_between(bins,np.concatenate(([0],y)), step="pre",label = 'Data')
#plt.scatter(x,y,label='Fixed Parameters')
plt.scatter(x,yT,label = 'Given parameter plot',color = 'r')
#plt.xlim([-1,11])
plt.xlabel('x')
plt.ylabel('counts')
plt.legend()
plt.savefig('tut3/PRNMOG001/histf.png',dpi = 200)
###################################

###################################
#Likelihood Function 2a
###################################
def Likelihood(S,x1,y1,y2,sig1,B):
    A = [] #Just for storing the P
    for i in range(len(x1)):
        P = (1/(sig1*np.sqrt(2*np.pi)))*(np.exp(-((y1[i]-y2[i])**2)/(2*sig1**2))) #each product term in likelihood
        #print(P)
        A.append(P) #Storing each term
    L = np.prod(A) #Multipliying all terms to produce likelihood
    return L

LLH = Likelihood(20,x,y,yT,2,1) #sig is 2 
print('The likelihood of the given parameters is:',LLH) #Not likely to be close to fit parameters
print('-------------------------------------------')

###################################
#2b (Fit to find optimal values and check that the likelihood is bigger than previous fit)
###################################
def chi2(S,mu,sig,B):
    S = np.sum((y-f(x,S,mu,sig,B))**2) #No error so exlude 
    return S


def fit(S0,mu0,sig0,B0): #Fit function
    if S0 is None or mu0 is None or sig0 is None or B0 is None:
        p0=np.zeros(5)
    else:
        p0=(S0,mu0,sig0,B0)
    popt,pcov = scipy.optimize.curve_fit(f, x, y, p0=p0,absolute_sigma = False) #Curve fit algorithm
    k = len(x) - len(p0) #No of dof
    plt.figure(9)
    plt.scatter(x,y,label = 'data')
    plt.plot(xs,f(xs,*popt),color = 'r',label = "Fit")
    plt.xlim([-1,11])
    plt.xlabel('x')
    plt.ylabel('counts')
    plt.legend()
    plt.savefig('fit.png',dpi=200)
    u = np.sqrt(np.diag(pcov))
    print('The chi squared is:', chi2(*popt))
    print('The reduced chi**2 is:', chi2(*popt)/k)
    print('-------------------------------------------')
    print('The best fi parameters are:')
    print('S = ', popt[0],'\n''mu = ', popt[1],'\n' 'sig = ',popt[2],'\n''B = ',popt[3] )
    print('-------------------------------------------')
    print('The unertainty in the fit parameters are:')
    print('uS = ', u[0],'\n''umu = ', u[1],'\n' 'usig = ',u[2],'\n''uB = ',u[3])
    print('-------------------------------------------')
    
    


fit(5,5,1,3.6) #Applying fit 

ypopt = f(x,14.14,4.5,0.33,2.5)
LLHp = Likelihood(19.21,x,y,ypopt,0.32,2.5)
print('The likelihood of the fit parameters is:',LLHp) #It is less but still not satisfactory
##################################
#2c (Visualize uncertainty in fit paramters) chi**2 plot?
##################################
#s vs u cplot
SS = np.linspace(0,20,100)
uu = np.linspace(0,7,100)
ssig = np.linspace(0.1,1,100)
BB = np.linspace(0,20,100)
cc = np.zeros((len(SS),len(uu)))

#################################
#Contour Plots
for ia,a in enumerate(SS):
    for ib,b in enumerate(uu):
        cc[ib,ia] = chi2(a,b,0.32,2.5)
plt.figure(10)
plt.contourf(uu,SS,cc)
plt.xlabel("mu")
plt.ylabel("S")
plt.savefig('tut3/PRNMOG001/ConS.png',dpi=200)

cc = np.zeros((len(SS),len(uu)))
for ia,a in enumerate(ssig):
    for ib,b in enumerate(uu):
        cc[ib,ia] = chi2(15.0,b,a,2.5)
plt.figure(11)
plt.contourf(uu,SS,cc)
plt.xlabel("mu")
plt.ylabel("sigma")
plt.savefig('tut3/PRNMOG001/ConSig.png',dpi=200)
   
cc = np.zeros((len(SS),len(uu)))     
for ia,a in enumerate(BB):
    for ib,b in enumerate(uu):
        cc[ib,ia] = chi2(15.0,b,0.32,a)
plt.figure(12)
plt.contourf(uu,SS,cc)
plt.xlabel("mu")
plt.ylabel("B")
plt.savefig('tut3/PRNMOG001/ConB.png',dpi=200)

def WJSON(path, fileName, data):
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)

#################################
#Dictionary creation
trapa = {'2':tA[0],'4':tA[1],'10':tA[2],'20':tA[3],'40':tA[4],'80':tA[5],'160':tA[6]}
simpa = {'2':sA[0],'4':sA[1],'10':sA[2],'20':sA[3],'40':sA[4],'80':sA[5],'160':sA[6]}
a = {'trapezoid':trapa,'simpson':simpa}
trapr = {'2':RT[0],'4':None,'10':RT[1],'20':RT[2],'40':RT[3],'80':RT[4],'160':None}
traps = {'2':RS[0],'4':None,'10':RS[1],'20':RS[2],'40':RS[3],'80':RS[3],'160':None}
b = {'trapezoid':trapr,'simpson':traps}
Ga = {'2':gA[0],'3':gA[1],'4':gA[2]}
c = {'c':Ga}
twoa = {'a':LLH}
################################
#Updating main dictionary
one = {'a':a,'b':b,'c':Ga}
dic = {'1':one,'2':twoa}
WJSON('/tut3/PRNMOG001/','results',dic)
    
    