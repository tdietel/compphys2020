#PLLVIN013 CP TUT3
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import scipy.integrate as integrate
import json
import random as rnd
from scipy.optimize import curve_fit, minimize

#Q1a) - Intergrating the PDF using the Trapezoid and Simpson rules: for normal distribution
N_points = [2,4,10,20,40,80,160,320]
N_quad= [2,3,4]
sigma = 1
area_sig = 0.682689492137086 #from res error

#parameters for normal integration
def normal_distribution_function(x,mean,std):
    value = stats.norm.pdf(x,mean,std)
    return value

mean = 100
std = 30
trapz = []
trapzz = []
simpss = []
simps = []
quad = []
mu = []
x_min = mean - std
x_max = mean + std
ptx = np.linspace(x_min, x_max, 1000)
pty = stats.norm.pdf(ptx,mean,std)



#relative error from given 
def rel_error(numerical, exact):
    return np.abs((numerical - exact)/(exact))

#defining the gaussian
def gauss(x, mu, sigma):
    return (1 / (sigma * np.sqrt(2 * np.pi))) * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2))

#richardson function
def richardson(t, a_h, a_ht, i):
    return ((t**i)*a_ht - a_h)/(t**i - 1)

#trapz and simps approx - beez
###############trapz<====3
for i in N_points:
    x = np.linspace(x_min, x_max, i)
    y = gauss(x,mean,std)
    trapz_approx = np.trapz(y,x)
    trapz.append(trapz_approx)
    trapz_err = rel_error(trapz_approx, area_sig)
    trapzz.append(trapz_err)

# print(trapz)

# trapz_expected = [1/((i)**2) for i in N_points]
# trapz_sol = [np.abs((a-area_sig)/area_sig) for a in trapz]
#print(trapz_sol)

# plt.figure(2)
# plt.plot(N_points, trapz_expected, label = 'Expected trapzoidal behaviour')
# plt.plot(N_points, trapz_sol, label = 'Error')
# plt.xlabel('N segments')
# plt.ylabel('Expected error/ Resultant error')
# plt.legend()

###############simps<====3
for i in N_points:
    x = np.linspace(x_min,x_max, i)
    y = normal_distribution_function(x,mean,std)
    simps_approx = integrate.simps(y,x)
    simps.append(simps_approx)
    simps_err = rel_error(simps_approx, area_sig)
    simpss.append(simps_err)


# simps_expected = [1/((i)**4) for i in N_points]
# simps_sol = [np.abs((a-area_sig)/area_sig) for a in simps]
#print(simps_sol)



#1b) richardson extrapolation where 'show = True' for showing Richardson extrapolation
rich_t = []
rich_s = []
for i in range(len(N_points)):
    if i != len(N_points) - 1:
        r_trapz = richardson(2, trapz[i], trapz[i+1], 2)
        r_simps = richardson(2, simps[i], simps[i+1], 4)
        rich_t.append(r_trapz)
        rich_s.append(r_simps)
    else:
        pass


#1c) using gaussian quad for N = 2,3,4 - can also use import from scipy.integrate.quadrature
###############quad<====3
quadd =[]
for i in N_quad:
   q = integrate.fixed_quad(gauss, x_min, x_max, args=(mean, std), n=i)
   quad_err = rel_error(q[0], area_sig)
   quadd.append(quad_err)
   quad.append(q[0])


plt.figure(1)
plt.plot(ptx,pty, color='gray')
plt.fill_between(ptx, pty, color='#e1b1b4', alpha='1.0')
plt.title('Test integration plot', fontsize=10)
plt.xlabel('x', fontsize=8)
plt.ylabel('Probability Density Function', fontsize=8)
plt.show()

#plotting relative errors against each other
plt.figure(2)
plt.plot(N_points[:7], trapzz[:7], color = 'b', label = 'Simpson error')
plt.plot(N_points[:7], simpss[:7], color = 'r', label = 'Trapezoid error')
plt.legend()
plt.yscale('log')
plt.show()




plt.figure(3)
plt.plot(N_quad, quadd, 'ro--', label="Gaussian Quadrature")
plt.legend()
plt.xlabel("Order of integration")
plt.ylabel("Relative error")
plt.title("Relative error versus order of integration method")
plt.show()

answers = {}
q1 = {}
a = {}
b = {}
t1a = {}
s1a = {}
t1b = {}
s1b = {}
q1c = {}
for i, N in enumerate(N_points[:-1]):
	t1a.update({str(N): trapz[i]})
	s1a.update({str(N): simps[i]})
	t1b.update({str(N): rich_t[i]})
	s1b.update({str(N): rich_s[i]})
for j, M in enumerate(N_quad):
	q1c.update({str(M): quad[j]})
a.update({"trapezoid": t1a})
a.update({"simpson": s1a})
b.update({"trapezoid": t1b})
b.update({"simpson": s1b})
q1.update({"a": a})
q1.update({"b": b})
q1.update({"c": q1c})
answers.update({"1": q1})
##########################################################################<==============3##############

#QUESTION 2:a
def gaussian_variant(x, mean, amplitude, standard_deviation, offset):
    return amplitude *(1 / (standard_deviation * np.sqrt(2 * np.pi))) * np.exp( - ((x - mean) / (2*standard_deviation**2))) + offset


#Q2 initialising variables
SS = 20
BG = 1
Mu = 5
Sigma = 2
SS_interval = np.arange(0.0, 40.0, 1.0)
Mu_interval = np.arange(0.0, 14.0, 0.1)
BG_interval = np.arange(0.0, 6.0, 0.1)
Sigma_interval = np.arange(0.1, 6.0, 0.1)
guess = np.array([4, 3, 25, 1])
individual_counts = [0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 6, 8, 9, 9]
counts = [3, 4, 3, 8, 8, 2, 5, 0, 1, 2]
bins = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
hx = np.linspace(0, 10, 100)


def likelihood(items, bins, mu, sigma, background, signal):
    prob = stats.norm.cdf(bins[1:], mu, sigma) - stats.norm.cdf(bins[:-1], mu, sigma)
    LikeBin = stats.poisson.pmf(items, prob*signal + background)
    Like = np.prod(LikeBin)
    return Like

def log_likelihood(p):
    log = -np.log(likelihood(counts, bins, p[0], p[1], p[2], p[3]))
    return log


opt = minimize(log_likelihood, guess, jac=False)
print(opt)

#maxL = likelihood(counts, bins, opt.x[0], opt[1], opt.x[3], opt.x[2])
plt.figure()
histogram = np.histogram(counts, bins)
plt.hist(individual_counts, bins, color="seagreen")
plt.plot(hx, gaussian_variant(hx, opt.x[0], opt.x[3], opt.x[1], opt.x[2]))
plt.xlabel('x')
plt.ylabel('counts')
plt.show()

def contour_plot(values1, values2, first, second, count, bin_width, p):
    names = ["Mean", "Standard Deviation", "Background", "Signal Strength"]
    l = np.zeros((len(values2), len(values1)))
    for i, x in enumerate(values1):
        for j, y in enumerate(values2):
            params = list(p)
            params[first] = x
            params[second] = y
            l[j, i] = likelihood(count, bin_width, params[0], params[1], params[2], params[3])

    gx, gy = np.meshgrid(values1, values2)
    plt.contourf(gx, gy, l, cmap='viridis')
    plt.plot(p[first], p[second], 'bo')
    plt.xlabel(names[first])
    plt.ylabel(names[second])
    plt.colorbar()

#farts
some_meaningless_change = 12


plt.figure()
contour_plot(Mu_interval, SS_interval, 0, 3, counts, bins, opt.x)
plt.show()

plt.figure()
contour_plot(Mu_interval, Sigma_interval, 0, 1, counts, bins, opt.x)
plt.show()

plt.figure()
contour_plot(Mu_interval, BG_interval, 0, 2, counts, bins, opt.x)
plt.show()

plt.figure()
contour_plot(Sigma_interval, BG_interval, 1, 2, counts, bins, opt.x)
plt.show()

plt.figure()
contour_plot(Sigma_interval, SS_interval, 1, 3, counts, bins, opt.x)
plt.show()

plt.figure()
contour_plot(BG_interval, SS_interval, 2, 3, counts, bins, opt.x)
plt.show()

another_meaningless_change = 5

# print("Maxium likelihood")
# print(maxL)
L = likelihood(counts, bins, Mu, Sigma, BG, SS)
print('likelihood')
print(L)

q2 = {}
q2.update({"a": L})
answers.update({"2": q2})



with open('tut3/PLLVIN013/answers.json', 'w') as f:
     json.dump(answers, f)
