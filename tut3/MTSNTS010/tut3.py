#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 22:33:38 2020

@author: ntshembho
"""

import numpy as np
from scipy.stats import norm
from scipy import integrate
import matplotlib.pyplot as plt
import scipy.integrate as spi
from scipy.integrate import simps
from scipy.optimize import curve_fit
from scipy import stats
import json


' ' ' Defining pdf ' ' '

mu = 0.5
sig = 0.1

def pdf(x,mu,sig):
   return 1/(sig*np.sqrt(2*np.pi))*np.exp(-((x-mu)**2)/(2*sig**2))


N = [2,4,10,20,40,80,160]
        
' ' ' Calculating integrals ' ' '

trap_int = []
simps_int = []

for i in range(len(N)):
    x = np.linspace(0.4,0.6,N[i]+1)
    trap_int.append(spi.trapz(pdf(x,mu,sig),x))
    simps_int.append(simps(pdf(x,mu,sig),x))
    
#print('Trap method', trap_int)  
#print('Sims method', simps_int) 
    
N2 = [2,3,4]
quad_int = []

for i in range(2,5):
   xq = np.linspace(0.4,0.6,i)
   quad = integrate.fixed_quad(pdf,0.4,0.6, args= (mu,sig), n=i)
   quad_int.append(quad[0])


' ' ' Calculating error ' ' ' 

error_trap = []
error_simps = []
error_quad = []
exact = 0.682689492
trap_expect = [1/((i)**2) for i in N]
simps_expect = [ 1/(i**4) for i in N] #expected behaviour of simpson rule


for i in range(len(N)):
    error_trap.append(np.absolute((exact-trap_int[i])/exact))
    error_simps.append(np.absolute((exact-simps_int[i])/exact))
for i in range(len(N2)):
    error_quad.append(np.absolute((exact-quad_int[i])/exact))
#    
plt.plot(N, trap_expect, 'r.', label = 'Trap Expected behavior')
plt.plot(N, error_trap, 'k.', label = 'Trap actual behavior')
plt.ylabel('error')
plt.xlabel('`Number of intervals')
plt.yscale('log')
plt.legend()
plt.savefig('trap error')
plt.show()

plt.plot(N, simps_expect, 'r.', label = 'Simps Expected behavior')
plt.plot(N, error_simps, 'k.', label = 'Actual behavior')
plt.ylabel('error')
plt.xlabel('`Number of intervals')
plt.yscale('log')
plt.legend()
plt.savefig('simps error')
plt.show()

plt.plot(N2, error_quad, 'k.', label = 'Gaussian Quadrature method error')
plt.ylabel('error')
plt.xlabel('Order of Gaussian Quadrature')
plt.yscale('log')
plt.savefig('quad error')
plt.legend()
plt.show()


    
' ' ' Richardson Extrap ' ' ' 

def richardson(t, a_h, a_ht, k):
    return ((t**k)*a_ht - a_h)/(t**k - 1)

Rtrap = []
Rsimps = []

for i in range(len(trap_int)):
    if i != len(trap_int)-1:
        Rtrap.append(richardson(2, trap_int[i], trap_int[i+1], 2))
        Rsimps.append(richardson(2, simps_int[i], simps_int[i+1], 4))
        

' ' ' Histogram ' ' ' 

bins = np.linspace(0,10,11)
bins2 = np.linspace(0,10,10)
counts = [0]*3 + [1]*4 + [2]*3 + [3]*8 +[4]*8 + [5]*2 + [6]*5 + [7]*0 + [8]*1 + [9]*2
y = [3,4,3,8,8,2,5,0,1,2]

## plot histogram ##

def pdf2(x,S,mu,sig):
   return S/(sig*np.sqrt(2*np.pi))*np.exp(-((x-mu)**2)/(2*sig**2))

### curve fit ###
sig0 = 2.2
mu0 = 3.7
S0 = 3
#B0=3
	
p0 = [S0, mu0, sig0]           #initial guesses
name = ['S0','sig', 'mu']

tmodel = np.linspace(0,10,10)
popt, pcov = curve_fit(pdf2,bins2,y,p0) #fitting parameters
yfit = pdf2(tmodel,*popt)


#plotting histogram with gaussian
xplot = np.linspace(0,10,1000)
yplot = pdf2(xplot,*popt)
plt.plot(xplot,yplot,'-r',label = 'Fitted Gaussian') #plot gaussian
plt.hist(counts,bins, label = 'Histogram') #plot histogram
plt.xlabel('x')
plt.ylabel('counts')
plt.legend()
plt.savefig('histogram with gaus')
plt.show()

# calculating likelihood
def gaus(x, S,mu,sig,B):
    f = S*stats.norm.pdf(x,mu,sig) + B
    return f

x = np.linspace(1,10,10)

f2 = gaus(x,20,5,2,1)
    
def Likelihood(y1,y2,sig):
    L = []
    for i in range(len(y1)):
        L.append(1/(np.sqrt(2*np.pi*sig**2))*np.exp(-((y2[i]-y1[i])**2)/(2*sig**2)))
    Lprod = np.prod(L)
    return Lprod

Likelihood = Likelihood(f2,y,2)
#Maxlike = Likelihood(f2,yfit,popt[2])

Maxlike0 = []
Like0 = []
for i in range(len(y)):
    Maxlike0.append(1/(np.sqrt(2*np.pi*popt[2]**2))*np.exp(-((f2[i]-yfit[i])**2)/(2*popt[2]**2)))#Likelihood(f2,yfit,3.7)
    
    
Maxlike1 = np.prod(Maxlike0)

print('real likelihood', Likelihood)
print('Maxlikelihood', Maxlike1)

#print('real maxlikelihood', Maxlike)

### Uncertainty 

def chi2(S,mu,sig):
    S = np.sum((y-pdf2(x,S,mu,sig))**2) #No error so exlude 
    return S

SS = np.linspace(10,50,100)
uu = np.linspace(0,7,100)
ssig = np.linspace(0,10,100)
cc = np.zeros((len(SS),len(uu)))
dd = np.zeros((len(uu),len(ssig)))


#Contour Plots
for ia,a in enumerate(SS):
    for ib,b in enumerate(uu):
        cc[ib,ia] = chi2(a,b,popt[2])
#plt.figure(10)
plt.contourf(uu,SS,cc)
plt.xlabel("mu")
plt.ylabel("S")
plt.colorbar()
plt.savefig('countour') 
plt.show()


' ' ' Writing Solutions to json ' ' ' 


trapazoida = {'2':trap_int[0] , '4':trap_int[1] , '10':trap_int[2] , '20':trap_int[3], '40':trap_int[4] , '80':trap_int[5] , '160':trap_int[6]}
simpsona = {'2':simps_int[0] , '4':simps_int[1] , '10':simps_int[2] , '20':simps_int[3], '40':simps_int[4] , '80':simps_int[5] , '160':simps_int[6]}

trapazoidb = {'2': Rtrap[0] , '4':Rtrap[1] , '10':Rtrap[2] , '20':Rtrap[3], '40':Rtrap[4] , '80':Rtrap[5] , '160': Rtrap[5] }
simpsonb = {'2': Rsimps[0] , '4':Rsimps[1] , '10':Rsimps[2] , '20':Rsimps[3], '40': Rsimps[4], '80': Rsimps[5] , '160': Rsimps[5]}

quad = {'2': quad_int[0], '3':quad_int[1], '4':quad_int[2]}

a = {'trapezoid': trapazoida, 'simpson':simpsona }
b = {'trapezoid': trapazoidb, 'simpson':simpsonb }
#c = {'2':, '3': '4': quad}


one = {'a': a ,'b': b,'c': quad}
two = {'a':Likelihood}

data = {'1':one, '2': two}

#with open('solutions.json', 'w') as f:
#    json.dump(data, f)
    
with open('tut3/MTSNTS010/solutions.json','w') as g:
    json.dump(data,g)
