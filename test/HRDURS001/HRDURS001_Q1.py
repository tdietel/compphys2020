from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import math
import time
import json
font = {'family': 'serif',
        'color':  'navy',
        'weight': 'normal',
        'size': 12,
        }

#The Poisson equation describes the electric potential of a charge distribution 

#potential depends simulatanously on x and y 

#

#discretisation = 1 x 1 mm 

colorinterpolation = 50
colourMap = plt.cm.jet

lenx=100  #splitting into 100 by 100 grid
leny=100
X, Y =np.meshgrid(np.arange(0,lenx), np.arange(0,leny)) #initializing mesh
v=np.zeros_like(X) # potential like the mesh grid

#implementing boundary conditions
v[(leny-1):,:]=0.#top
v[:1, :]=0.      #bottom
v[:, (lenx-1):]=0. #right
v[:,:1]=0.#left
v[19:80,39:41]=0.0 #electrode plate

#implementing charge distribution (point)
charge=np.zeros((lenx,leny))
q= 1.0
charge[59:61,49:51]=q


#start timer to see run time
start=time.time()

h=(0.01/100)**2 #h value for relaxation method
epsilon_0=1.0 #epsilon set to 1
itera=0 #initial iteration

a=1/(26000000) #This I am not sure of but just to make things reflect the sample solutions it seems that the answers are off by this factor. 

relax_r=1.0 #initial relaxation 

omega=1.5 #this relaxation parameter just works far better than other I tried. 
#implementing initial v values
for iteration in range(0,lenx):
    
    for i in range(1, leny-1, 1):
        for j in range(1, lenx-1, 1):
                v[j,i] = (0.25 * (v[j,i+1] + v[j,i-1] + v[j+1,i] + v[j-1,i]) +charge[i][j]/(h*4*epsilon_0))*a

#applying iteration after iteration until tolerance of 0.0001 is accepted
while (relax_r> 0.0001):
    print(relax_r)                
    dvm=0.0   
    for j in range(1,leny-1):
        for i in range(1,lenx-1):
            R=(v[j,i-1]+v[j-1,i]+v[j,i+1]+v[j+1,i]-4.0*v[j,i]+charge[i][j]/(h*4*epsilon_0))
            dv= 0.25 * omega *R
            v[j,i]+=dv
            dvm=np.max([np.abs(dv), dvm])  #implementing stopping criteria for the simuation
    relax_r=dvm/np.max(np.abs(v))  #change in v over v 
    itera+=1

end=time.time()

print(itera)
print("simulation time:", end-start)
 #Efeild is just the gradient of V 
Ex=np.zeros((lenx,leny))
Ey=np.zeros((lenx,leny))

Ey, Ex =np.gradient(-v)


plt.title("potential map")
plt.contourf(X, Y, v*a, 50, cmap=colourMap)
plt.colorbar()
plt.quiver(X,Y,Ex, Ey)
print("P1:" , v[50,60]*a)
print("mag", a*np.sqrt(Ex[50][60]**2+Ey[50][60]**2))
print("angle", np.arctan(Ey[50][60]/Ex[50][60])*180/np.pi)
print("P2:", v[90][50]*a)
print("mag", a*np.sqrt(Ex[90][50]**2+Ey[90][50]**2))
print("angle", np.arctan(Ey[90][50]/Ex[90][50])*180/np.pi)
print("P3:",v[90,90]*a)
print("mag", a*np.sqrt(Ex[90][90]**2+Ey[90][90]**2))
print("angle", np.arctan(Ey[90][90]/Ex[90][90])*180/np.pi)
plt.show()          

#writing json file
a_file=open("test/HRDURS001/data.json", "r")
json_object=json.load(a_file)
a_file.close()


json_object['1']['a']['P1']=v[50,60]*a
json_object['1']['a']['P2']=v[90][50]*a
json_object['1']['a']['P3']=v[90,90]*a
json_object['1']['b']['P1']['phi']=np.arctan(Ey[50][60]/Ex[50][60])*180/np.pi
json_object['1']['b']['P1']['mag']=a*np.sqrt(Ex[50][60]**2+Ey[50][60]**2)
json_object['1']['b']['P2']['phi']=np.arctan(Ey[90][50]/Ex[90][50])*180/np.pi
json_object['1']['b']['P2']['mag']=a*np.sqrt(Ex[90][50]**2+Ey[90][50]**2)
json_object['1']['b']['P3']['phi']=np.arctan(Ey[90][90]/Ex[90][90])*180/np.pi
json_object['1']['b']['P3']['mag']=a*np.sqrt(Ex[90][90]**2+Ey[90][90]**2)
a_file=open("test/HRDURS001/data.json", "w")
json.dump(json_object, a_file, indent=4)
a_file.close()

