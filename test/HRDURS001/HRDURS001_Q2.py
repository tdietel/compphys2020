
#URSULA HARDIE 
#CP TEST 1
#QUESTION 1
from __future__ import division
import json
import numpy as np
import matplotlib.pyplot as plt
import math
import scipy
from scipy.optimize import curve_fit
import timeit
from timeit import default_timer as timer
import sys
from scipy.linalg import solve
from scipy.stats import chisquare

np.set_printoptions(threshold=sys.maxsize)

font = {'family': 'serif',
        'color':  'navy',
        'weight': 'normal',
        'size': 12,
        }
#INITIAL VALUES

R1=63
R2=240
R3=190
RA=3.0
Rs=1.5
Vs=7.50

#Solving the current array using the mesh method 
currents=[]
for i in range(1,501):
	A=[[R1+RA+i, -RA, -i],[-RA, RA+R2+R3, -R3],[-i, -R3, i+R3+Rs]]
	b=[0, 0, Vs]
	x=np.linalg.solve(A,b) #solves for arbitrary currents 
	amm=x[0]-x[1]
	currents.append(x[0]-x[1])
	if amm <0.001 and amm >0 :
		print(i)


RX=np.arange(1,501,1)
#if no current flows through the bridge then I


def f(current,a,b,c,d):
	return d+(a-d)/(1+(current/c)**b) #function to fit to
										#seems better to use an exponential 
										#however this produces a result with more precision
										#Function used comes from previous projects 
#test parameters
a0=-0.03
b0=2
c0=30
d0=0.03
p0=[a0,b0,c0,d0]
popt,pcov = scipy.optimize.curve_fit( f, RX, currents , p0=p0) #cuve fitting 
fit=f(RX, *popt)

#So my initial current direction was incorrect but this doesnt change anything because current was arbitrary
c_fit=fit*(-1)

c_currents=np.zeros(len(currents))

for i in range(len(currents)):
	c_currents[i]=currents[i]*(-1)
#Plotting the currents vs changing R
plt.plot(RX,c_fit,lw=2,label="curvefit")
plt.plot(RX, c_currents,'r',lw=5, alpha=0.3, label="matrix data")
plt.legend()
plt.xlabel("Resistance of Rx (ohms)")
plt.ylabel("Current (Amperes)")
plt.show()



RX_p=np.arange(1,501,0.0001) #division of possible Rx values into small increments
betterfit=f(RX_p,*popt)
for i in range(len(RX_p)):
	if betterfit[i]> -0.00000001 and betterfit[i] < 0.00000001: #optimizing parameters
		bal= RX_p[i]  #this is the value of  Rx for the wheatsone bridge



#writing json file
a_file=open("test/HRDURS001/data.json", "r")
json_object=json.load(a_file)
a_file.close()


json_object['2']['a']['I500ohms']=np.min(currents)
json_object['2']['b']=bal
a_file=open("test/HRDURS001/data.json", "w")
json.dump(json_object, a_file, indent=4)
a_file.close()
