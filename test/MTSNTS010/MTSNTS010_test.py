# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 17:03:43 2020

@author: Ntshembho
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import scipy.constants as const
import scipy.integrate as spi
import json

' ' ' Question 1 ' ' '

## Setting up parameters
L = 100 #mm
N = 100
h = L/N #1mm 
eps = const.epsilon_0  ##F/m

## Charge density matrix

rho0 = 1 #charge density
rho = np.zeros((N,N))
rho[int(round(N*3/5)),int(round(N*1/2))]= rho0

## parameters for plotting

X = np.linspace(0,L,N)
Y = np.linspace(0,L,N)
X,Y = np.meshgrid(X,Y)

## creating 
def phi(u):
    X = np.linspace(0,L,N)
    Y = np.linspace(0,L,N)

    iterations=0
    diff = 1000
    old =0
    etrod=[[False for _ in range(0,100)] for _ in range(0,100)]
    for j in range (0,100):
        if(j>20) and (j<80):
            etrod[40][j] = True
    ##Relaxation method for solving PDE, taken from Notes        
    while (diff>0.0001):
        new =0
        for i in range(1,len(X)-1):
            for j in range(1,len(X)-1):
                if not etrod[i][j]:
                    update = 1/4*(u[i-1,j]+u[i+1,j]+u[i,j-1]+u[i,j+1]) +rho[i,j]
    #                print('update',update)
                    new +=update
                    u[i,j]= update
        iterations +=1
        diff = abs(old-new)
        old = new
    one_a = []
    points = [[50,50],[90,50],[90,90]] 
    k=0
    for point in points:
        k+=1
        i = point[0]
        j = point[1]
        one_a.append(u[i][j])
        print("p"+str(k)+" "+str(u[i][j]))
    plt.imshow(u.transpose()) 
    plt.xlabel('width [mm]')
    plt.ylabel('length [mm]')
    plt.savefig('phi image')
    plt.show()
    plt.contour(X,Y,u.transpose(),60)
    plt.colorbar()
    plt.ylabel("phi y")
    plt.xlabel("phi x")
    plt.savefig('phi contour')
    plt.show()
  
    X = np.linspace(0,L,N)
    Y = np.copy(X)
    X,Y = np.meshgrid(X,Y)

    return u,one_a

u = np.zeros((N,N))
M,one_a = phi(u)

def Elect(M):
    X = np.linspace(0,L,N)
    Y = np.copy(X)
    X,Y = np.meshgrid(X,Y)
    E = np.gradient(M)
    Ex = -E[0]
    Ey = -E[1]
#    Ex = np.zeros((N,N))
#    Ey = np.zeros((N,N))
#    for i in range(N-1):
#        for j in range(N-1):
#            Ex[i][j] = -(M[i+1][j]-M[i-1][j])/(2*h)
#            Ey[i][j] = -(M[i][j+1]-M[i][j-1])/(2*h)
    Etot = np.sqrt(Ex**2+Ey**2)
    plt.contour(X,Y,Etot.transpose(),45)
    plt.colorbar()
    plt.ylabel("Ey")
    plt.xlabel("Ex")
    plt.savefig('electric field')
    plt.show()
#    plt.imshow(Emag.transpose()) 
    
    # getting electric field P1,P2,P3
    points = [[50,50],[90,50],[90,90]]
    Emag = []
    Eangle = []
    
    for k in range(len(points)):       
        i = points[k][0]
        j = points[k][1]
        Emag.append(np.sqrt(Ex[i,j]**2+ Ey[i,j]**2))
        Eangle.append(180/np.pi*np.arctan( Ey[i,j]/Ex[i,j]))

    print("Emag",Emag)
    print("Angle", Eangle)

    return Etot,Emag,Eangle, Ex

Etot, Emag, Eangle, Ex  = Elect(M)

def Electrode(Ex):
    y = np.linspace(20,80,60)
    rho_elect = []
    for j in range(20,80):
        rho_elect.append(Ex[40][j])
    
    Qtot = spi.trapz(rho_elect,y)
    print(Qtot)
    
    
    plt.plot(y,rho_elect)
    plt.ylabel('rho electrode')
    plt.xlabel('Length [mm]')
    plt.savefig('rho rod')
    plt.show()
    return Qtot
#
Electrode(Ex)



' ' ' Question 2 ' ' ' 

# Defining values
R1 = 63 #ohms
R2 = 240 #ohms
R3 = 190 #ohms
RA = 300 #ohms
Rs = 1.50 #ohms
Vs = 7.50 #ohms
I0 = Vs/Rs
V_eff = Vs-I0*Rs

## defining arrays 
Rx = np.linspace(0,500,501)
Ia = np.linspace(0,500,501)

R = np.zeros((3,3)) #MAtrix with resistences
V = np.zeros(3) #matrix with voltages
V[0] = Vs

def current(Rx):
    R = np.zeros((3,3)) 
    for i in range(len(Rx)):
        R[0,0] = Rx[i]+R3+Rs
        R[0,1] = -Rx[i]
        R[0,2] = -R3
        R[1,0] = -Rx[i]
        R[1,1] = Rx[i]+R1+RA
        R[1,2] = -RA
        R[2,0] = -R3
        R[2,1] = -RA
        R[2,2] = R3+RA+R2
    
        Rinv = np.linalg.inv(R)
        I = Rinv.dot(V)
#        print(I)
#        print (Itemp)
        #Sanity check, voltage drops are equal
        if (Rx[i]==500):
            print("Vrs= ",Vs - I[0]*Rs)
            print("Vrs= ",I[1]*R1+I[2]*R2)

        Ia[i] = I[1]-I[2]
    
    plt.plot(Rx,Ia,'r-')
    plt.xlabel('Rx[ohms]')
    plt.ylabel('Ia[amps]')
    plt.savefig('Ia vs Rx')
    plt.show()
    
    ###Rx for 0 current ###
    Rx0 = R1*R3/R2
    print(Rx0)
    return Ia[500], Rx0
    
Ia500, Rx0 = current(Rx)



' ' ' Dictionary ' ' ' 
#
a = {"P1":one_a[0],"P2":one_a[1], "P3":one_a[2]}
P1 = {"mag":Emag[0], "phi":Eangle[0]}
P2 = {"mag":Emag[1], "phi":Eangle[1]}
P3={"mag":Emag[2], "phi":Eangle[2]}
b = {"P1": P1,"P2": P2, "P3":P3}
one = {"a":a,"b":b}
atwo = {"I500ohms": Ia500}
two = {"a": atwo, "b":Rx0}
data = {"1": one, "2":two}
#
#with open('solutions.json', 'w') as f:
#    json.dump(data, f)
#
#
#
with open('test/MTSNTS010/solutions.json','w') as g:
    json.dump(data,g)

