import json
import q1, q2

#json_folder = "./"
json_folder = "test/VMRROB003/"


def run(): # manages the running of the solution as well as writing to the json file
    results = {}
    
    points = ['P1','P2','P3']
    results['1'] = {}
    results['1']['a']= {}
    results['1']['b']= {}
    
    #q1
    phis, Es = q1.main(load = False)
    for i in range(len(points)):
        results['1']['a'][points[i]] = phis[i]
        results['1']['b'][points[i]] = {}
        results['1']['b'][points[i]]['mag'] = Es[i][0]
        results['1']['b'][points[i]]['phi'] = Es[i][1]
      
    
    #q2
    I500, root = q2.main()
    results['2'] = {}
    results['2']['a'] = {}
    results['2']['a']['I500ohms'] = I500
    results['2']['b'] = root
    
    with open(json_folder + "solutions.json", 'w') as outfile: #saves the dictionary to json file
        json.dump(results, outfile)
    
    
    
if __name__ == "__main__":
    run()