import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton as nt

#initialise parameters
R1=63
R2=240
R3=190
RA=3
RS=1.5
VS=7.5
#folder to save - need different for pipeline
#figure_folder = "./"
figure_folder = "test/VMRROB003/"

def setup_matrix(Rx=49.875): #initialises the linear equation system for a given Rx
    #matrix determined using Kirchoff's Laws
    matrix = np.asarray([[0,1,1,0,0,-1],
                         [1,-1,0,1,0,0],
                         [0,0,1,1,-1,0],
                         [0,-R2,R3,-RA,0,0],
                         [R1,0,0,-RA,-Rx,0],
                         [-R1,-R2,0,0,0,-RS]])
    output_vector = np.asarray([0,0,0,0,0,-VS])
    return matrix, output_vector

def I_through_ammeter(Rx): #defines a function that returns the current through the ammeter as a function of Rx
    matrix, vector = setup_matrix(Rx)
    I = np.linalg.solve(matrix, vector) #solves the linear system to obtain all the currents
    return I[3] #defined the 4th current to be through the ammeter
 
def parta():
    #plots the current through the ammeter as a function of Rx
    Rxs = np.linspace(0,500,50)
    I_amms = []
    for Rx in Rxs:
        I_amms.append(I_through_ammeter(Rx))
        
    #setup plot
    plt.figure()
    plt.title("Current through ammeter as function of Rx")
    plt.xlabel("Rx (Ohms)")
    plt.ylabel("Current through ammeter (A)")
    plt.xlim((0,500))
    plt.plot(Rxs, I_amms)
    plt.savefig(figure_folder + "currentRx.png")

def main():
    
    parta()
    
    I500 = I_through_ammeter(500) # finds the current at Rx = 500 ohms
    
    # To find Rx where the current through the ammeter is 0, we want to find the root of I_through_ammeter
    # We use the secant method from scipy optimize starting at Rx=45 (which we can guess graphically from 2a)
    root = nt(I_through_ammeter,45, tol = 1e-12) 
    
    return I500, root
