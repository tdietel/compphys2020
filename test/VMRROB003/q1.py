import numpy as np
import matplotlib.pyplot as plt
import pickle
import scipy.integrate as intg

#folder to save - need different for pipeline
#figure_folder = "./"
figure_folder = "test/VMRROB003/"

e0 = 1/4 # rescaled units - think a factor 1/4 missing from sample solutions
q= 1 # 1C
numpoints = 101 # assumes right boundary lies on 10cm

#h = 0.001 # in metres
#tol = 1e-10 #initial update of order 1e-7, need difference much less than that

h = 1 #in mm
tol = 1e-5 #initial update of order 1e-1, need difference much less than that


def on_plate(i,j): #used to define where the plate of 0V lies
    if j == 40 and (i >= 20 and i<=80): #using array coordinates - ij = yx from top left
        return True
    return False

def cart_to_polar(x,y): #converts a vector in cartesion to polar - angle given in degrees
    mag = np.sqrt(np.square(x)+np.square(y))
    phi = np.arctan2(y,x) * 180/np.pi
    return mag,phi

def relaxation(u): #relaxation method as found in notes
    diff = 1000
    p= 1.9 #damping coefficient, chosen to match best case from examples
    while diff > tol:
        diff = 0.
        
        for i in range(1,numpoints-1): #exludes all grounded plates from updates
            for j in range(1,numpoints-1):
                if not on_plate(i,j):
                    un = (1-p)* u[i][j] + p/4. * (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])  #eqn 5.13 in notes - i-1/j-1 will already be updated
                    if j == 60 and i == 50: #if at the point charge add its value
                        un +=  p*(h**2/(4*e0)) * q
                        
                    diff += abs(un - u[i][j]) #sums the difference in values
                    u[i][j] = un #updates the value
    return u

def parta(load = False):
    
    if not load: #must compute all values
        u0 = np.zeros((numpoints,numpoints)) #initialise potential grid
        u = relaxation(u0) #find potential
        
        with open("potential.pkl","wb") as f: #stores the potential array to a file - used for faster testing
            pickle.dump(u, f)
    else:
        with open("potential.pkl","rb") as f: #loads the potential array from a file - used for faster testing
            u = pickle.load(f)
            u = np.asarray(u)
    
    #plots the potential over the grid
    plt.figure()
    plt.title("Potential map over grid")
    plt.xlabel("x (cm)")
    plt.ylabel("y (cm)")
    X,Y = np.meshgrid(np.linspace(0,10,numpoints),np.linspace(0,10,numpoints))
    plt.contourf(X,Y,u, 15)
    plt.colorbar()
    plt.savefig(figure_folder + "phimap.png")
    
    #finds and prints the potential at each location - uses array coordinates
    phiP1 = u[50,50]
    phiP2 = u[50,90]
    phiP3 = u[10,90]
    print("Phi___________")
    print("P1:", phiP1) 
    print("P2:", phiP2) 
    print("P3:", phiP3) 
    return u, [phiP1,phiP2,phiP3]

def partb(u):
    # determines the electric field as E=-grad(V)
    ey, ex = -1 * np.asarray( np.gradient(u, h))  #returns in opposite order
    
    #plots the Efield with 2 different visualisations
    fig,ax = plt.subplots()
    plt.title("Electric field over grid")
    X,Y = np.meshgrid(np.linspace(0,10,numpoints),np.linspace(0,10,numpoints)) #sets up the Cartesian coordinates

    ax.streamplot(X,Y,ex,ey) #better for seeing the 'flow' of the field
    plt.xlabel("x (cm)")
    plt.ylabel("y (cm)")
    plt.savefig(figure_folder + "Efield.png")
    
    fig,ax = plt.subplots()
    plt.title("Electric field over grid")
    ax.quiver(X,Y,ex,ey) #better for seeing magnitudes of the field
    plt.xlabel("x (cm)")
    plt.ylabel("y (cm)")
    plt.savefig(figure_folder + "Efield_quiver.png")
    
    ey = -ey #ey in cartesian is opposite in array coordinates
    
    #prints magnitude and angle (in deg) of field at each location
    EP1 = cart_to_polar(ex[50][50],ey[50][50])
    EP2 = cart_to_polar(ex[50][90],ey[50][90])
    EP3 = cart_to_polar(ex[10][90],ey[10][90])
    print("E field___________")
    print("P1:", EP1) 
    print("P2:", EP2) 
    print("P3:", EP3) 
    
    return ex,ey, [EP1,EP2,EP3]
    

def partc(ex,ey):
    chargedist = np.zeros(numpoints) #for a line in y-direction
    exdy,exdx = np.asarray( np.gradient(ex, h)) #find gradient of ex scalar field
    eydy,eydx = np.asarray( np.gradient(ey, h)) #find gradient of ey scalar field
    eydy = -eydy #switch to cartesian in y - exdy not used so not included
    
    for i in range(len(chargedist)): #use Gauss' Law along x=40, i.e. the plate
        chargedist[i] = e0 * (exdx[i][40]+eydy[i][40]) #computes the divergence
        
    plt.figure()
    plt.title("Charge distribution along electrode")
    plt.xlabel("y (mm)")
    plt.ylabel("Charge induced (C)")
    plt.plot(chargedist)
    plt.xlim((20,80))
    plt.savefig(figure_folder + "induceddist.png")
    
    induced_charge = intg.simps(chargedist[20:80])
    print("Charge Induced" ,induced_charge)

#main method - load allows you to load previous potential maps from a file, saves time when testing
#default load=False calculates the potential map completely
def main(load=False): 
    geometry() 
    u, phis = parta(load)
    ex,ey, Es = partb(u)
    partc(ex,ey)
    return phis,Es


def geometry(): #used to check the geometry of the measurement points, grounded plates and point charge by setting arbitrary values
    
    u0 = np.zeros((numpoints,numpoints))
    for i in range(0,numpoints):
        for j in range(0,numpoints):
            if j == 60 and i == 50: # if point charge
                u0[i][j] = 1
            if on_plate(i,j): #grounded
                u0[i][j] -= 1
            if i ==0 or j == 0 or i == numpoints-1 or j == numpoints-1: #grounded
                
                u0[i][j] -= 1
    
    u0 += 1
    #3 measurement points
    u0[50][50] = 3
    u0[50][90] = 3
    u0[10][90] = 3
    
    plt.figure()
    plt.title("Geometry of the setup")
    plt.xlabel("x (cm)")
    plt.ylabel("y (cm)")
    plt.imshow(u0, extent = [0,10,0,10], origin = 'upper')
    plt.savefig(figure_folder + "geometry.png")

#if __name__ == "__main__":
##    main(load=False)
#    geometry()