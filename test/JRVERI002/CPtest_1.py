# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 12:28:38 2020

@author: erinj
"""
import numpy as np
import matplotlib.pyplot as plt
import json

#Question 1


#creating the x and y coordinate points
L=101  #number of points: 101 so that we can have a point every millimeter
x=np.linspace(0,0.10,L)  
y=np.linspace(0,0.10,L)


#creating lattices for coords(xy), potential(u) and charge density(rho)
#these latices are constructe such that we can access x and y in the normal
#coordinate form a[x][y]
xy=np.zeros([L,L])
u=np.zeros([L,L])
rho=np.zeros([L,L])


#Finding the index for the position of the charge and P1,P2,P3
ci=0  #charge (6,5)
cj=0
p1=0   #position of point p1 (5,5), p2 (9,5), p3 (9,9)
p2x=0  #note p3=[p2x][p2x]
for i in range(len(x)):
    if x[i]==0.06:
        ci+=i
    if y[i]==0.05:
        cj+=i
        p1+=i
    if x[i]==0.09:
        p2x+=1

rho[ci][cj]=1

#Sanity check: plot charge density
#note contourf plots the transpose!
rho1=np.transpose(rho)

plt.contourf(x,y,rho1)
plt.colorbar()
plt.xlabel('x')
plt.ylabel('y')
plt.title('Net Charge distribution')

#position of electrode, find incides of positions (4,2)-(4,8)
xi=0
yi=0
yf=0
for i in range(len(x)):
    if x[i]==0.04:
        xi+=i
    if y[i]==0.02:
        yi+=i
    if y[i]==0.08:
        yf+=i
        
#damped relaxation method (taken from notes)
r=1        #define random r>0.1 
counter=0  #count number of iterations it takes
while r>0.1:
    r=0
    h=x[1]-x[0]
    epsilon=8.85*10**(-12)
    w=1.91  #weighting number, trial an error to find the best
    for i in range(1,len(xy)-1):    #ensures boundaries=0 
        for j in range(1,len(xy[i])-1):
            if j in range(yi,yf+1) and i==xi: #makes sure that potential=0 at the electrode
                u[i][j]=0
            else:                      #updates all other coords
                un=(1-w)*u[i][j]+w*(1/4)*(u[i-1][j]+u[i][j-1]+u[i+1][j]+u[i][j+1]+h**2*(rho[i][j]/epsilon))
                r+=np.abs(un-u[i][j])
                u[i][j]=un
            
                
    counter+=1



print(counter)  



#part a 
ap1=u[p1][p1]
ap2=u[p2x][p1]
ap3=u[p2x][p2x]


 
print('potential at point p1=',u[p1][p1])
print('p2=', u[p2x][p1])
print('P3=', u[p2x][p2x])


u1=np.transpose(u)  #plot transpose!
            
plt.figure()
plt.contourf(x,y,u1,levels=20)
plt.colorbar()
plt.xlabel('x')
plt.ylabel('y')


#finding electric field using potential difference E=du/dx
def efield(u):
    ex=np.zeros([len(u), len(u)])
    ey=np.zeros([len(u), len(u)])
    d=x[2]-x[0]
    for i in range(1,len(u)-1):
        for j in range(1,len(u)-1):
            ex[i][j]=-(u[i+1][j]-u[i-1][j])/d
            ey[i][j]=-(u[i][j+1]-u[i][j-1])/d
    return ex,ey

E=efield(u)

#Plot streamlines
Ex1=np.transpose(E[0])
Ey1=np.transpose(E[1])


plt.streamplot(x,y,Ex1,Ey1)

#b-calculate magnitude of E and angle P1,P2,P3
Etot=np.zeros([len(u),len(u)])
angles=np.zeros([len(u),len(u)])

Ex=E[0]
Ey=E[1]

for i in range(len(Ex)):
    for j in range(len(Ex[i])):
        Etot[i][j]=np.sqrt(Ex[i][j]**2+Ey[i][j]**2)
        angles[i][j]=np.arctan(Ey[i][j]/Ex[i][j])
        
Ep1=Etot[p1][p1]
Ap1=angles[p1][p1]

Ep2=Etot[p2x][p1]
Ap2=angles[p2x][p1]

Ep3=Etot[p2x][p2x]
Ap3=angles[p2x][p2x]
        
print('Efield at P1='+str(Etot[p1][p1])+',angle='+str(angles[p1][p1]))
print('Efield at P2='+str(Etot[p2x][p1])+',angle='+str(angles[p2x][p1]))
print('Efield at P3='+str(Etot[p2x][p2x])+',angle='+str(angles[p2x][p2x]))
  
Q1={"a":{"P1":ap1,"P2":ap2,"P3":ap3},"b":{"P1":{"mag":Ep1,"phi":Ap1},"P2":{"mag":Ep2,"phi":Ap2},"P3":{"mag":Ep3,"phi":Ap3}}}


# Calculate charge distribution on electrode: rho=epsilon0*E
eps=8.85*10**(-12)
c_dist=[]
y1=[]
for k in range(yi,yf+1):
    c_dist.append(eps*Etot[xi][k])
    y1.append(y[k])



plt.figure()
plt.plot(y1,c_dist)
plt.xlabel('y')
plt.ylabel('Charge (C)')
plt.title('distribution of charge in the electrode')


###########################################################
# Question2

#create matrix an solve for the 6 currents

def matrix(rX):

    M=np.zeros((6,6))
    
    r1=63
    r2=240
    r3=190
    rA=3
    rs=1.5
    vs=7.5
    
    #input values into matix
    M[0][0]=rs*((r1/(r1+r2))-(r3/(rX+r3)))
    M[0][1]=rA
    
    
    M[1][1]=1
    M[1][2]=1
    M[1][3]=-1
    
    M[2][1]=-rA
    M[2][2]=r1
    M[2][5]=-rX
    
    M[3][1]=-1
    M[3][4]=-1
    M[3][5]=1
    
    M[4][1]=rA
    M[4][3]=r2
    M[4][4]=-r3
    
    M[5][0]=1
    M[5][2]=-1
    M[5][5]=-1
    
    
    
    b=np.zeros(6)
    b[0]=vs*((r1/(r1+r2))-(r3/(r3+rX)))
    
    x=np.linalg.solve(M,b)
    return x

#a plot current through A for Rx between 0ohms an 500ohms
rX=np.linspace(0,500,100)

currents=[]

for i in range(len(rX)):
    currents.append(matrix(rX[i]))

IA=[]

for i in range(len(currents)):
    IA.append(currents[i][1])
    
    
for i in range(len(IA)):
    if IA[i]==max(IA):
        print(rX[i])

plt.figure()    
plt.plot(rX,IA)
plt.xlabel('Rx')
plt.ylabel('Current')


#b calculate the value of Rx that results in balance
r1=63
r2=240
r3=190
    

balance=(r1*r3)/r2

print(balance)
print(matrix(balance))

I500=IA[-1]

Q2={"a":{"I500ohms":I500},"b":balance}

sol={"1":Q1,"2":Q2}

#creating a json file to write my solution into 
with open('test/JRVERI002/answers.json','w') as f:
    json.dump(sol,f)
