# computational physics  - test 1 - thavish chetty - chttha009
# section 1 importing modules
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import root  # root finder
from scipy.integrate import simps
import json


# section 2: question 1

def electronic_box():
    # converts length to grid index
    def convert(position, step):
        point = position // step
        return point

    e0 = 8.8541878128e-12  # permittivity constant
    step = 1e-3  # grid step size 1mm
    q = 1.00  # charge of point charge
    h = 0.1  # height of box in m
    nx = h/step + 1  # number of x-grid points
    ny = nx  # number of y-grid points

    # variable names for json file
    global p_positions
    p_positions = ["P1", "P2", "P3"]
    global e_field_labels
    e_field_labels = ["mag", "phi"]

    # x, y grid indices
    nx = int(nx)
    ny = int(ny)
    # x, y positions of p1, p2, p3 and the point charge in metres
    p1x = 0.05
    p1y = 0.05
    p2x = 0.09
    p2y = 0.05
    p3x = 0.09
    p3y = 0.09
    qx = 0.06
    qy = 0.05
    wirex = 0.04  # wire x position
    wireb = 0.02  # wire bottom position
    wiret = 0.02 + 0.06  # wire top position

    # converting metres to index position
    p1xp = int(convert(p1x, step))
    p1yp = int(convert(p1y, step))
    p2xp = int(convert(p2x, step))
    p2yp = int(convert(p2y, step))
    p3xp = int(convert(p3x, step))
    p3yp = int(convert(p3y, step))
    qxp = int(convert(qx, step))
    qyp = int(convert(qy, step))
    # charge density matrix with zeroes every except at point charge position
    rho = np.zeros((nx, ny))
    rho[qyp, qxp] = q/e0

    # wire index positions
    wirexp = int(convert(wirex, step))
    wirebp = int(convert(wireb, step))
    wiretp = int(convert(wiret, step))
    wire_points = wiretp - wirebp
    print(wire_points)

    #Grid Points of the potential matrix in 2d dimensions
    potential = np.zeros((ny, nx))
    pot = np.zeros((ny, nx))

    # iterations needed to update the potential matrix to get a reasonable solution
    max_iterations = 25000000  # max iterations
    tol = 1e-24  # tolerance reached for
    # RELAXATION METHOD
    def relaxation(iterations, precision):
        for t in range(iterations):
            pot_old = pot.copy()
            # potential at point is equal to all 4 nearesr surrounding points
            pot[1:-1, 1:-1] = ((pot_old[1:-1, 2:] + pot_old[1:-1, :-2] + pot_old[2:, 1:-1] + pot_old[:-2, 1:-1])*step**2 + (rho[1:-1, 1:-1])*step**4)/(4*step**2)

            # Applying Boundary Conditions
            # Zero potential at the wall and wire
            pot[:, 0] = 0
            pot[:, nx - 1] = 0
            pot[0, :] = 0
            pot[ny - 1, :] = 0
            pot[int(wirebp): int(wiretp), int(wirexp)] = 0

            # if precision is reaached stop method and count iterations
            if np.max(abs(pot - pot_old)) < precision:
                print("Number of iterations", t+1)
                break
            else:
                print("Did not converge")

    # GAUSS SEIDEL ITERATIVE METHOD
    def gauss_seidel(iterations, precision):
        for t in range(iterations):
            pot_old = pot.copy()   # duplicating grid to create old potential
            # uodating potential
            pot[1:-1, 1:-1] = ((pot[1:-1, 2:] + pot_old[1:-1, :-2] + pot[2:, 1:-1] + pot_old[:-2,1:-1]) * step ** 2 + (rho[1:-1, 1:-1]) * step ** 4) / (4 * step ** 2)
            # Applying Boundary Conditions
            pot[:, 0] = 0
            pot[:, nx - 1] = 0
            pot[0, :] = 0
            pot[ny - 1, :] = 0
            pot[int(wirebp): int(wiretp), int(wirexp)] = 0

            if np.max(abs(pot - pot_old)) < precision:
                print("Number of iterations", t + 1)
                break
            else:
                print("Did not converge")

    # gauss_seidel(max_iterations, tol)
    relaxation(max_iterations, tol)

    # plot potential field
    def plot_potential(pot):
        plt.imshow(pot, cmap='viridis')
        plt.colorbar()
        plt.xlabel(r'$\phi_{x}(V)$')
        plt.ylabel(r'$\phi_{y}(V)$')
        plt.show()
    # plotting e_field over grid
    def electric_field_plot(V):
        v, u = np.gradient(V)  #gradient of electric field
        x = np.linspace(0, 100, 101)
        y = x
        fig, axarr = plt.subplots()
        im1 = axarr.contourf(x, y, V, 20, cmap='viridis')
        axarr.streamplot(x, y, -u, -v, color="red")
        fig.colorbar(im1, orientation='vertical', ax=axarr, label=(r"$\phi(V)$"))
        axarr.set_xlabel(r"$x(mm)$")
        axarr.set_ylabel(r"$y(mm)$")
        plt.show()

    plot_potential(pot)
    electric_field_plot(pot)
    global Pot_p
    Pot_p = [pot[p1yp][p1xp], pot[p2yp][p2xp], pot[p3yp][p3xp]]

    Ey, Ex = np.gradient(pot)
    Ex = np.negative(Ex)
    Ey = np.negative(Ey)

    # calculating e_field_vector_components
    E1x = Ex[p1yp][p1xp]
    E2x = Ex[p2yp][p2xp]
    E3x = Ex[p3yp][p3xp]
    E1y = Ey[p1yp][p1xp]
    E2y = Ey[p2yp][p2xp]
    E3y = Ey[p3yp][p3xp]

    # calculating magnitudes
    mag1 = np.sqrt(E1x**2 + E1y**2)
    mag2 = np.sqrt(E2x**2 + E2y**2)
    mag3 = np.sqrt(E3x**2 + E3y**2)

    # calculating angles
    phi1 = np.degrees(np.arctan2(E1y, E1x))
    phi2 = np.degrees(np.arctan2(E2y, E2x))
    phi3 = np.degrees(np.arctan2(E3y, E3x))

    # magnitudes and angles of e-field
    global E_polar
    E_polar = [[mag1, phi1], [mag2, phi2], [mag3, phi3]]

    # calculating charge density
    def charge_density(x_field):
        E_left = x_field[wirebp:wiretp, wirexp - 1]
        E_right = x_field[wirebp: wiretp, wirexp + 1]
        lc = []
        for i in range(len(E_left)):
            lambd = e0*(E_right[i] - E_left[i])
            lc.append(lambd)
        return lc

    line_charge = charge_density(Ex)

    # line charge density as a function of length
    def plot_line_charge():
        global y
        y = np.linspace(wireb, wiret, wire_points)
        print(y)
        plt.figure()
        plt.xlabel("Length of wire(m)")
        plt.ylabel("Charge Density (C/m)")
        plt.plot(y, line_charge, color="violet")
        plt.show()

    # function to integrate charge
    def charge(line):
        qw = simps(line, y)
        return qw


    # plotting line charge  and calculating total charge on wire
    plot_line_charge()
    wire_charge = charge(line_charge)
    print("Charge on wire is", wire_charge)

# section 3:  question 2 calculations
def wheatstone_bridge():
    print("Wheatstone Bridge")
    # Resistances and Voltages
    R1 = 63.0
    R2 = 240.0
    R3 = 190.0
    Ra = 3.00
    Rs = 1.50
    Vs = 7.50
    Guess = 50.0 # Guess resistance of Rx for Root finder
    Rx_range = np.linspace(0, 501, 501)  # list of variable resistance
    # function creating matrix (3 by 3) using mesh analysis and Kirchoff's Loop Law
    def matrix_solve(Rx):
        voltages = np.zeros(3)  # voltage vector
        voltages[0] = Vs  # initialising source voltage
        matrix = []
        for i in range(3):  # first loop
            row = np.zeros(3)
            if i == 0:
                row[0] = Rs + R3 + Rx
                row[1] = - R3
                row[2] = - Rx
                matrix.append(row)
            if i == 1:  # second loop
                row[0] = - R3
                row[1] = Ra + R2 + R3
                row[2] = -Ra
                matrix.append(row)
            if i == 2:  # third loop
                row[0] = -Rx
                row[1] = -Ra
                row[2] = Rx + Ra + R1
                matrix.append(row)  # appending rows to matrix
        currents = np.linalg.solve(matrix, voltages)  # using linalg to solve system of linear equations
        ammeter_current = currents[1] - currents[2]   # picking out the current through ammeter
        return ammeter_current

    # list of ammeter currents
    ammeter_currents = []
    for r in range(len(Rx_range)):
        ai = matrix_solve(Rx_range[r])
        ammeter_currents.append(ai)

    # making variables global to be written in dictionary
    global Rx
    Rx = root(matrix_solve, Guess)  # using a rootfinder to determine resistance which makes current zero
    global I500ohms
    I500ohms = (matrix_solve(500))


    # plotting current versus resistance
    plt.figure()
    plt.xlabel('Resistance' + r'$(\Omega)$')
    plt.title("Plot showing Current through Ammeter versus varying resistance, Rx")
    plt.ylabel("Current(A)")
    plt.axhline(y=0.0, color='r', linestyle='-')
    plt.plot(Rx_range, ammeter_currents)
    plt.show()

# creating dictionary full of answers and updating question 1
answers = {}
def question1():
    print("Updating question 1 answers")
    q1 = {}
    q1a = {}
    q1b = {}
    q1bp1 = {}
    q1bp2 = {}
    q1bp3 = {}
    b = [q1bp1, q1bp2, q1bp3]
    for i, label in enumerate(p_positions):
        q1a.update({label: Pot_p[i]})
    q1.update({'a': q1a})

    for j, name in enumerate(p_positions):
        for k, label in enumerate(e_field_labels):
            b[j].update({label: E_polar[j][k]})
        q1b.update({name: b[j]})

    q1.update({'b': q1b})
    answers.update({"1": q1})

# updating answers for question 2
def question2():
    print("Update question 2")
    q2 = {}
    q2a = {}
    q2a.update({"I500ohms": I500ohms})
    q2.update({'a': q2a})
    q2.update({'b': Rx.x[0]})
    answers.update({"2": q2})


# writing answers to a json file
def write_file():
    with open('answers.json', 'w') as f:
        json.dump(answers, f)


# running all functions for the class test
if __name__ == '__main__':
    electronic_box()
    question1()
    wheatstone_bridge()
    question2()
    write_file()