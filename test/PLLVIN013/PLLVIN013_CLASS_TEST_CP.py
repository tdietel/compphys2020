import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import bisect
from scipy.integrate import cumtrapz
import json

L = 10  #Length of each side 
N = 101 #choosing an odd number to ensure we have a middle val
dL = L/N    #step

#our x and y axis with 
x = np.linspace(0,L,N)
y = np.copy(x)
X,Y = np.meshgrid(x,y)

#charge density rho
rho_0 = 1.0
rho = np.zeros((N,N))
rho[int(round(N/2.0)), int(round(N/2.0+10))] = rho_0

#voltage with initial value 0 
V = np.zeros((N,N))

#solving this using the jacobi
iterations = 0
eps = 1e-8
error = 1e4
while iterations < 1e3 and error > eps:
    V_temp = np.copy(V)
    error = 0 
    for j in range (1, N-1):
        for i in range(1, N-1):
            V[i,j] = 0.25*(V[i+1, j] + V[i-1,j] + V[i, j-1] + V[i,j+1] +rho[i,j]*dL**2)
            error += abs(V[i,j]-V_temp[i,j])
    iterations +=1
    V[20:80,40] = 0 #rod with zero potential

#To plot 1b) 
def Electric_Field(V, n):
    v, u = np.gradient(V)   #we defined our electric field as the gradient of the potential using the built-in numpu function

    x = np.linspace(0, 10, n) # x-axis
    fig, axarr = plt.subplots()

    im1 = axarr.contourf(x, x, V, 20,cmap = 'plasma')
    axarr.streamplot(x, x, -u, -v, color="k")   #to show our electric field lines
    fig.colorbar(im1, orientation='vertical', ax=axarr,label=(r"$\phi(V)$"))
    axarr.set_xlabel("$x(cm)$")
    axarr.set_ylabel("$y(cm)$")
    axarr.set_title("Potential map with overplotted electric field lines of point charge and rod")
    
Electric_Field(V, 101) #plot for E-field for 1b

plt.figure()

#Plotting for 1a map of the potential
plt.imshow(V,cmap = 'plasma')
plt.colorbar(label = (r'$\phi(V)$'))
plt.xlabel('x (mm)')
plt.ylabel('y (mm)')
plt.title('Potential map of point charge and rod - PDE solution')

#The potentials at P1,P2,P3 for 1a
P1 = V[49,49]
P2 = V[49,89]
P3 = V[9,89]
print("Potential at P1 = ",P1)
print("Potential at P2 = ",P2)
print("Potential at P3 = ",P3)

E_fieldy, E_fieldx = np.gradient(V)
E_fieldy = -E_fieldy
E_fieldx = -E_fieldx

#The E-field at positions P1, P2, P3
E_P1x = E_fieldx[49,49]
E_P2x = E_fieldx[49,89]
E_P3x = E_fieldx[9,89]
print('Electric field at P1 = ',E_P1x)
print('Electric field at P2 = ',E_P2x)
print('Electric field at P3 = ',E_P3x)

#calculating the angle 
E_P1y = E_fieldy[49,49]
E_P2y = E_fieldy[49,89]
E_P3y = E_fieldy[9,89]

#angle 
phi1 = np.arctan(E_P1y/E_P1x)*(180/np.pi)
phi2 = np.arctan(E_P2y/E_P2x)*(-180/np.pi)
phi3 = np.arctan(E_P3y/E_P3x)*(-180/np.pi)

print('Angle phi at P1 = ', phi1)
print('Angle phi at P2 = ', phi2)
print('Angle phi at P3 = ', phi3)

# MAGNITUDE
mag1 = np.sqrt(E_P1x**2 + E_P1y**2)
mag2 = np.sqrt(E_P2x**2 + E_P2y**2)
mag3 = np.sqrt(E_P3x**2 + E_P3y**2)

#question 1c)
epsilon_0 = 8.8541878128e-12
current = []
Efield_above = E_fieldx[19:80,40]
Efield_below = E_fieldx[19:80,38]

for i in range(len(Efield_above)):
    I = (Efield_above[i] - Efield_below[i])*epsilon_0
    current.append(I)

y_axis = np.linspace(0,0.6,61)
plt.figure()
plt.xlabel('y (mm)')
plt.ylabel(r'Charge distribution $\rho(y)$' )
plt.title('Charge distribution along the y-axis of the electrode')
plt.grid()
plt.plot(y_axis, current, color = 'k')

charge = cumtrapz(current) 
print('The total charge is =', charge, 'C')

#QUESTION 2 - initializing me variables
Rs, R1, R2, R3, Ra, Vs = 1.50, 63, 240, 190, 3.0, 7.5 

#defining the matrix  - system linear equations obtained using Kirchoffs loop laws
def matrixR_x(R):
    global Rs,R1,R2,Ra,Vs
    matrix = [[Rs,R3,0, 0, R, 0],[-1, 1, 1, 0, 0, 0],[0, -R3,R2,Ra,0,0],[0, 0, -1, 1, 0, 1],[0, 0, 0,-Ra ,-R ,R1],[0, -1, 0, -1,1,   0]]
    voltage = [Vs,0,0,0,0,0]
    current = np.linalg.solve(matrix,voltage)
    return current[3] 

#plot of Resistance versus current for 2a)
R =  np.linspace(0,500,100)
I = [matrixR_x(i) for i in R]
plt.figure()
plt.plot(R, I, color = 'k')
plt.axhline(c = 'r')
plt.grid()
plt.title(r'Values for different values of $R_x$ between $0 \Omega$ and $500 \Omega$')
plt.xlabel(r'Resistance ($\Omega$)')
plt.ylabel('Current (A)')

#Solution for 2a)
I500 = matrixR_x(500)
print("Resistance for current 500 = ", I500)

#Value of R_x for 2b)
root = bisect(matrixR_x, 45,55)
print("Resistance when the current is 0 through the ammeter =", root)

plt.show()

answers ={}

def question1():
    print("Question 1 answers")
    q1 = {}
    q1a = {}
    q1b = {}
    Vp1 = {}
    Vp2 = {}
    Vp3 = {}
    MA1 = {}
    MA2 = {}
    MA3 = {}

    MA1.update({"mag": mag1})
    MA2.update({"mag": mag2})
    MA3.update({"mag": mag3})
    MA1.update({"phi": phi1})
    MA2.update({"phi": phi2})
    MA3.update({"phi": phi3})
    q1b.update({"P1": MA1})
    q1b.update({"P2": MA2})
    q1b.update({"P3": MA3})
    Vp1.update({"P1": P1})
    Vp2.update({"P2": P2})
    Vp3.update({"P3": P3})
    q1a.update(Vp1)
    q1a.update(Vp2)
    q1a.update(Vp3)
    q1.update({"a": q1a})
    q1.update({"b": q1b})

    answers.update({"1": q1})

def question2():
    print("Question 2 answers")
    q2 = {}
    q2a = {}
    q2a.update({"I500ohms": I500})
    q2.update({'a': q2a})
    q2.update({'b': root})
    answers.update({"2": q2})


def write_file():
     with open('test/PLLVIN013/answers.json', 'w') as f:
        json.dump(answers, f)


question1()
question2()
write_file()