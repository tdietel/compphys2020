import json
import question1
import question2
import time

# savepath = ''
savepath = 'test/CLYHAN001/'

# writes solutions to solutions.json file
def makeJson():
	Dict = {}

	# Results for Q1
	dict1 = {}
	q1_answers = question1.answer_question()
	mag, phi = q1_answers[2], q1_answers[3]
	aDict = dict(zip(q1_answers[0], q1_answers[1]))
	bDict = {'P1':{'mag':mag[0],'phi':phi[0]}, 'P2':{'mag':mag[1],'phi':phi[1]}, 'P3':{'mag':mag[2],'phi':phi[2]}}

	dict1["a"] = aDict
	dict1["b"] = bDict

	# Results for Q2
	dict2 = {}
	q2_answers = question2.answer_question()
	dict2["a"] = {"I500ohms": q2_answers[0]}
	dict2["b"] = q2_answers[1]

	Dict["1"] = dict1
	Dict["2"] = dict2

	filename = savepath+"solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

	filename = savepath+"solutions.json"
	with open(filename, 'w') as outfile:
		json.dump(Dict, outfile)

def main():
	t0 = time.perf_counter()
	makeJson()
	print('Total runtime (in seconds):', time.perf_counter()-t0)

if __name__=='__main__':
	main()