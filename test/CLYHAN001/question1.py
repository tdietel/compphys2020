# all positions and lengths measured in mm
import numpy as np
from matplotlib import pyplot as plt
import math
import time
from matplotlib.patches import Circle

# savepath = ''
savepath = 'test/CLYHAN001/'

# grid positions of important points
P1x, P1y = 50, 50
P2x, P2y = 90, 50
P3x, P3y = 90, 90
qx, qy = 60, 50		# point charge
rodx, rody_lower, rody_upper = 40, 20, 80	# endpoints of rod

L = 103		# number of gridpoints with 1mm spacing - side length of box is 102mm, since we are taking walls to have 1mm thickness, and interior to have 10cm length and height. Fix zero potential at very outer grid points
h = 1		# grid spacing is 1mm
e0 = 1.0	# choice of units to match sample solutions

# damped relaxation method (implented using equations in the lecture notes)
def relax(phi,rho,L,h=1):
	niter = 0
	diff = 1000
	p = 1.90
	tolerance = 10**(-10)
	
	while diff > tolerance and niter<=2500:
		diff = 0.

		# keeps boundaries at zero potential (doesn't update edge walls, i.e. indices 0 and L-1)
		for i in range(1,L-1):
			for j in range (1,L-1):

				# don't update points along electrode (held at zero potential)
				if i==rodx and rody_lower<=j<=rody_upper:			
					continue

				# approximate new value			
				newval = (1-p)*phi[i,j] + p * (1./4. * (phi[i-1,j]+phi[i,j-1]+phi[i+1,j]+phi[i,j+1]) + (h**2)*rho[i,j]/(4*e0))					

				diff += abs( newval - phi[i,j] )
				phi[i,j] = newval

		niter += 1

	print('Relaxation method iterations:', niter)
	return phi

# find the potential in the box using the relaxation method - returns phi, and potential at the required reference points
def find_potential():
	phi = np.zeros([L,L])   # potential

	# point charge gives only point of non-zero charge density
	rho = np.zeros_like(phi)
	rho[qx, qy] = 1.0

	t0 = time.perf_counter()
	phi=relax(phi,rho,L,h)
	print("Relaxation method runtime (in seconds):", time.perf_counter()-t0)

	# potential at P1, P2, P3
	P1 = phi[P1x, P1y]
	P2 = phi[P2x, P2y]
	P3 = phi[P3x, P3y]

	return [phi,[P1,P2,P3]]

# compute electric field as minus the gradient of the potential
def find_E_field(phi):
	# swap x and y since contourf 'swaps' order
	V = np.zeros([L,L])
	for i in range(L):
		for j in range(L):
			V[i,j] = phi[j,i]
	phi = V

	gradx, grady = np.gradient(phi)
	Ex, Ey = -gradx, -grady

	# electric field at P1, P2, P3 - note that all x and y labels are inverted due to conventions required for contour plots later on
	P1_Emag = np.sqrt(Ex[P1y, P1x]**2 + Ey[P1y, P1x]**2)
	P2_Emag = np.sqrt(Ex[P2y, P2x]**2 + Ey[P2y, P2x]**2)
	P3_Emag = np.sqrt(Ex[P3y, P3x]**2 + Ey[P3y, P3x]**2)

	# get direction angle (in degrees)
	P1_Edir = math.atan2(Ex[P1y, P1x], Ey[P1y, P1x])*180/np.pi
	P2_Edir = math.atan2(Ex[P2y, P2x], Ey[P2y, P2x])*180/np.pi
	P3_Edir = math.atan2(Ex[P3y, P3x], Ey[P3y, P3x])*180/np.pi

	return [Ex, Ey, [P1_Emag, P2_Emag, P3_Emag], [P1_Edir, P2_Edir, P3_Edir]]

# Produce plots of potential and electric field
def visualize_fields(phi, Ex, Ey):
	x = np.arange(0,L,h)
	y = np.arange(0,L,h)

	# swap x and y since contourf 'swaps' order
	V = np.zeros([L,L])
	for i in range(len(x)):
		for j in range(len(y)):
			V[i,j] = phi[j,i]
	phi = V

	# produce plots
	fig = plt.figure(figsize=(7.5,6))
	ax = fig.add_subplot(111)
	contourPlot = ax.contourf(x, y, phi, 20)
	ax.streamplot(x, y, Ey, Ex, color='w')
	# reference point markers
	plt.plot(P1x, P1y, 'r+')
	plt.plot(P2x, P2y, 'r+')
	plt.plot(P3x, P3y, 'r+')
	plt.xlabel('x (mm)')
	plt.ylabel('y (mm)')
	plt.title("Visualization of potential and electric field")
	plt.colorbar(contourPlot,ax=ax,orientation='vertical', label=r'$\phi$ (V)')
	plt.savefig(savepath+'field_visualization1')
	plt.clf()


	fig = plt.figure(figsize=(6,6))
	ax = fig.add_subplot(111)
	color = 2 * np.log(np.hypot(Ey, Ex))
	ax.streamplot(x, y, Ey, Ex, color=color, linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
	ax.add_artist(Circle((qx, qy), 2, color='#aa0000'))
	plt.xlabel('x (mm)')
	plt.ylabel('y (mm)')
	plt.title("Visualization of electric field")
	plt.savefig(savepath+'field_visualization2')
	plt.clf()

	# plot to indicate magnitude of electric field - arrows are too close together, so plot is cluttered. Was not able to reduce density of arrows to obtain a clearer plot
	# not included in report
	fig = plt.figure(figsize=(7.5,6))
	ax = fig.add_subplot(111)
	contourPlot = ax.contourf(x, y, phi, 20)
	plt.quiver(x, y, Ey, Ex)
	# reference point markers
	plt.plot(P1x, P1y, 'r+')
	plt.plot(P2x, P2y, 'r+')
	plt.plot(P3x, P3y, 'r+')
	plt.xlabel('x (mm)')
	plt.ylabel('y (mm)')
	plt.title("Visualization of potential and electric field")
	plt.colorbar(contourPlot,ax=ax,orientation='vertical', label=r'$\phi$ (V)')
	plt.savefig(savepath+'field_visualization3')
	plt.clf()
	
# called by runFile to produce required plot and answers for solutions.json
def answer_question():
	answers = [['P1', 'P2', 'P3']]

	# get potential field
	potential_results = find_potential()
	phi = potential_results[0]

	# append potential at P1, P2 and P3 to answers (as subarray)
	answers.append(potential_results[1])

	# get electric field
	eField = find_E_field(phi)
	Ex, Ey = eField[0], eField[1]

	# append electric field magnitude and direction at P1, P2 and P3 to answers (as subarrays)
	answers.append(eField[2])
	answers.append(eField[3])

	visualize_fields(phi, Ex, Ey)

	# find induced charge density along electrode
	induced_charge = induced_charge_on_electrode(Ey)
	answers.append(induced_charge)

	return answers

# find charge induced on electrode -> (line) charge density = e0*Ex|x=4, the sum along length to get total charge
# remember to pass Ey from previous calcs, and swap x and y coordinates because of inversion for contour plot
def induced_charge_on_electrode(Ex):
	# coordinates for electrode
	yvals = np.arange(rody_lower, rody_upper, h)
	x = rodx

	electrode_charge_density = []

	for y in yvals:
		electrode_charge_density.append(e0*Ex[y, x])

	fig = plt.figure(figsize=(7,6))
	plt.plot(yvals, electrode_charge_density)
	plt.title(r'Induced charge distribution $\rho(y)$ along electrode')
	plt.xlabel('y (mm)')
	plt.ylabel(r'Charge density $\rho$ (C/mm)')
	plt.savefig(savepath+'electrode_charge')
	plt.clf()

	# total induced charge - charge density times length
	total_charge = sum(electrode_charge_density)*(rody_upper-rody_lower)
	print('Total charge induced on electrode', total_charge)

	return total_charge

# used during report writing to output table 
def main():	
	answers = answer_question()

	print(answers)

	# table for report
	print('P1 & ' + str(round(answers[1][0],6)) + ' & ' + str(round(answers[2][0],6)) + ' & ' + str(round(answers[3][0],6)) + ' \\\\')
	print('\\hline')
	print('P2 & ' + str(round(answers[1][1],6)) + ' & ' + str(round(answers[2][1],6)) + ' & ' + str(round(answers[3][1],6)) + ' \\\\')
	print('\\hline')
	print('P3 & ' + str(round(answers[1][2],6)) + ' & ' + str(round(answers[2][2],6)) + ' & ' + str(round(answers[3][2],6)) + ' \\\\')
	print('\\hline')





