import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import bisect, newton
import time

# savepath = ''
savepath = 'test/CLYHAN001/'

# Creates matrix of current coefficients, for the given circuit arrangement
def build_matrix(RX):
	# given circuit quantities
	R1 = 63.0
	R2 = 240.0
	R3 = 190.0
	RA = 3.00
	R5 = 1.50

	# matrix of current coefficients
	matrix = []
	
	matrix.append([R5,0,R3,0,0,RX])
	matrix.append([0,-R2,R3,-RA,0,0])
	matrix.append([0,0,0,-RA,R1,-RX])
	matrix.append([1,-1,-1,0,0,0])
	matrix.append([0,1,0,-1,-1,0])
	matrix.append([0,0,1,1,0,-1])

	return np.asarray(matrix)

def build_vec():
	VS = 7.50

	return [VS,0,0,0,0,0]

# Solves the matrix equation for the currents, for the variable resistance value RX
def solve_currents(RX):
	solution = np.linalg.solve(build_matrix(RX), build_vec())
	return solution

# Outputs the current through the ammeter
def ammeter_current(RX):
	return solve_currents(RX)[3]

# Solves the matrix equation for multiple RX values
def solve_ammeter_current(RX_vals=np.arange(0,500,0.1)):
	IA_vals = []

	for RX in RX_vals:
		IA_vals.append(ammeter_current(RX))

	return IA_vals

# Plots the current through the ammeter, over the range of RX values indicated
def plot_ammeter_current(RX_vals, IA_vals):
	fig = plt.figure()
	plt.plot(RX_vals,IA_vals)
	plt.title(r'Current $I_A$ through ammeter for different $R_X$')
	plt.xlabel(r'Resistance $R_X$ ($\Omega$)')
	plt.ylabel(r'Measured current $I_A$ (A)')
	plt.savefig(savepath+'IA_versus_RX')
	# plt.show()
	plt.clf()

# Find the value of RX for which no current flows through the ammeter, after computing current through ammeter for RX between 0 and 500 Ohms
# Implementation of the bisection method for root finding
def find_root(RX_vals, currents):
	t0 = time.perf_counter()

	tolerance = 10**(-20)
	# find closest value to root in already computed currents
	min_index = np.argmin(np.abs(currents))
	min_current = currents[min_index]
	RX_min = RX_vals[min_index]

	# find initial bracket limits
	if min_current*currents[min_index+1]<0:
		RX_lower = RX_vals[min_index]
		RX_upper = RX_vals[min_index+1]
		current_lower = currents[min_index]
		current_upper = currents[min_index+1]
	else:
		RX_lower = RX_vals[min_index-1]
		RX_upper = RX_vals[min_index]
		current_lower = currents[min_index-1]
		current_upper = currents[min_index]
	
	# implement bisection method
	while abs(min_current)>tolerance:
		RX_mid = (RX_upper + RX_lower)/2.0
		mid_current = solve_currents(RX_mid)[3]

		# check for sign change
		if mid_current*current_lower<0:
			current_upper = mid_current
			RX_upper = RX_mid
		else:
			current_lower = mid_current
			RX_lower = RX_mid

		# update min_current
		if abs(mid_current)<abs(min_current):
			min_current = mid_current
			RX_min = RX_mid

	root = RX_min
	t = time.perf_counter()-t0

	return [root, t]

# Find root using bisection method from scipy.optimize
def find_root_bisect(RX_vals, IA_vals):
	t0 = time.perf_counter()
	# estimate good initial brackets from points generated for plot of current vs RX
	min_index = np.argmin(np.abs(IA_vals))
	min_current = IA_vals[min_index]
	RX_min = RX_vals[min_index]

	if min_current<0:
		lower_bracket = RX_vals[min_index-1]
		upper_bracket = RX_min
	else:
		lower_bracket = RX_min
		upper_bracket = RX_vals[min_index+1]

	# find root
	root = bisect(ammeter_current, lower_bracket, upper_bracket)
	t = time.perf_counter()-t0

	return [root, t]

# Find root using secant method from scipy.optimize
def find_root_newton(RX_vals, IA_vals):
	t0 = time.perf_counter()

	# estimate good starting point from points generated for plot of current vs RX
	min_index = np.argmin(np.abs(IA_vals))
	min_current = IA_vals[min_index]
	initial_val = RX_vals[min_index]
	
	# find root
	root = newton(ammeter_current, initial_val)
	t = time.perf_counter()-t0

	return [root, t]

# Tests which method of root finding is best. Also used to verify answer obtained from direct implementation of the bisection method
def test_root_finding():
	# estimate initial values for root finding
	RX_vals = np.arange(0,500,0.1)
	IA_vals = solve_ammeter_current(RX_vals)

	bisect_times = []
	secant_times = []
	own_times = []

	# average times over 10 runs
	for i in range(10):
		# methods from scipy.optimize
		bisect_method = find_root_bisect(RX_vals, IA_vals)
		secant_method = find_root_newton(RX_vals, IA_vals)
		# own implementation using bisecion method
		own_method = find_root(RX_vals, IA_vals)

		bisect_times.append(bisect_method[1])
		secant_times.append(secant_method[1])
		own_times.append(own_method[1])

	# output results for table in report
	print('Root-finding method & Result for $R_X (\Omega)$ & Implementation time (s) \\\\ [0.5ex]')
	print('\\hline\\hline')
	print('Bisection method (scipy.optimize) & ' + str(bisect_method[0]) + ' & ' + str(round(sum(bisect_times)/10.,6)) + str(' \\\\'))
	print('\\hline')
	print('Secant method (scipy.optimize) & ' + str(secant_method[0]) + ' & ' + str(round(sum(secant_times)/10.,6)) + str(' \\\\'))
	print('\\hline')
	print('Bisection method (own implementation) & ' + str(own_method[0]) + ' & ' + str(round(sum(own_times)/10.,6)) + str(' \\\\'))
	print('\\hline')

# called by runFile to produce required plot and answers for solutions.json
def answer_question():
	answers = []

	# part a - current when RX = 500 ohms
	answers.append(solve_currents(500.0)[3])

	# part a - plot of IA versus RX
	RX_vals = np.arange(0,500,0.1)
	IA_vals = solve_ammeter_current(RX_vals)
	plot_ammeter_current(RX_vals, IA_vals)

	# part b - find value of RX for which no current flows through the ammeter
	answers.append(find_root(RX_vals, IA_vals)[0])

	return answers

# used during report writing for table 
def main():
	test_root_finding()
