#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 21:05:29 2020

@author: rayo
"""
import numpy as np 
import matplotlib.pyplot as plt
from scipy.optimize import newton
from scipy.integrate import simps
import json

#------------------------------
#1a
#------------------------------


xmax = 0.1                   # end of box x m
ymax = 0.1                   # end of box y m

x0 = 0                       # starting point x initial
y0 = 0                       # startting point y initial

xp = 101                     # number of x points
yp = 101                     # number of y points

dx = (x0-xmax)/(xp-1)        # x spacing
dy = (y0-ymax)/(yp-1)        # y spacing

q = 1.0                      # 1C columb
eps_0 = 8.8541878128e-12     # epsilon 0 F/m (farads per metre)
c = q/eps_0                  # just q/eposilon_0

V = np.zeros((xp,yp))        # create arry with 100 zero point potentials, potential in box
Q = np.zeros((xp,yp))        # array of charges
V_last = np.zeros((xp,yp))   # duplicates V
x = np.linspace(x0,xmax,xp)  # for plotting 
y = np.linspace(x0,xmax,xp)  # for plotting


Q[49,59] = c #defining charge

for i in range(10**6):
                
    V_last = V.copy()
    
    V[1:-1,1:-1] = (((V_last[1:-1, 2:] + V_last[1:-1, :-2])*dy**2 + (V_last[2:, 1:-1] + V_last[:-2, 1:-1])*dx**2 + Q[1:-1, 1:-1] * dx**2 * dy**2)/(2 * (dx**2 + dy**2)))
                
    #boundary conditions            
    V[xp-1,:] = 0  #bottom wall
    V[0,:] = 0 # top wall          ####Boundary conitions
    V[:, 0] = 0 #left wall
    V[:,yp-1] = 0#right wall
    V[19:80,39] = 0 #slither potential
    
    # dont want this to run forever
    if np.max(abs(V-V_last)) < 1e-7: # use numpy to check
        print(f'solution converged in {i+1} steps') # checking how long this nonsense takes to converge
        break
else:
    print('You messed up somewhere')
    
    
P = [V[49][49],V[49][89],V[89][89]]    
print('The potential is =',P,'V')

plt.imshow(V,cmap = 'jet')
plt.colorbar(label=(r"$\phi(V)$"))
#plt.clim(0,100000)
plt.xlabel(r'$x(mm)$')
plt.ylabel(r'$y(mm)$')
plt.savefig('test/PRNMOG001/poisson.png',dpi = 300)
plt.show()

# def plot2D(x, y, p):
#     fig = plt.figure()
#     ax = fig.gca(projection='3d')
#     X, Y = np.meshgrid(x, y)
#     ax.plot_surface(X, Y, p[:], rstride=1, cstride=1, cmap='jet', linewidth=0, antialiased=False)
#     ax.view_init(45, 60)
#     ax.set_xlabel(r'$x(m)$')
#     ax.set_ylabel(r'$y(m)$')
#     ax.zaxis.set_rotate_label(False)
#     ax.set_zlabel(r'$\phi(V)$',rotation = 90)
#     plt.savefig('test/PRNMOG001/2dplotV.png')
    
    
# plot2D(x,y,V)
    
    
#---------------------------------------
#1b
#---------------------------------------
    
    
def plotE(psi, n):
    v, u = np.gradient(psi) #grad of E field
    
    x = np.linspace(0, 10, n) # x-axis
    fig, axarr = plt.subplots()

    im1 = axarr.contourf(x, x, psi, 20,cmap = 'jet') #basically makes x,y grid
    axarr.streamplot(x, x, -u, -v, color="white") #plots the E field vectors
    fig.colorbar(im1, orientation='vertical', ax=axarr,label=(r"$\phi(V)$")) #adds colorbar
    axarr.set_xlabel("$x(cm)$")
    axarr.set_ylabel("$y(cm)$")
    fig.savefig('test/PRNMOG001/Efield.png',dpi = 100)


plotE(V,xp)

pi = np.pi
Ey,Ex = np.gradient(V) #-x and -y components of E field
Ex = -Ex
Ey = -Ey
mag = np.sqrt(Ex**2 + Ey**2) #mag of E field
angle = np.arctan(Ey/Ex)*180/pi #angle of E field 
print('\n')
Angles = [180-angle[49][49],360+angle[49][89],angle[89][89]]
mag = [mag[49][49],mag[49][89],mag[89][89]]
print('The angles are in degrees ',Angles)
print('\n')


#----------------------------------------
#1c)
#----------------------------------------


lam = []
E_perp_above = Ex[19:80,40] #electric field to the right of the line
E_perp_below = Ex[19:80,38] #electric field to the left of the line


for i in range(len(E_perp_above)):
    a = (E_perp_above[i] - E_perp_below[i])*eps_0 #applying boundary condition
    lam.append(a)
    
    
    
y = np.linspace(0,0.6,61) #the corresponding y points where the line charge density was calculated

plt.figure(10)
plt.plot(y,lam,color = 'orangered')
plt.grid()
plt.xlabel(r'$y(m)$')
plt.ylabel(r'$\lambda(C/m)$')
plt.savefig('test/PRNMOG001/chargedensity.png',dpi = 300)

dy = y[1] - y[0] #spacing of y
charge = simps(lam,y,dy,axis = -1) #using simpsons rule to calculate integral of line charge density 
print('The total charge is =', charge, 'C')
print('\n')


#----------------------------------------
#2a
#----------------------------------------


R1 = 63.0    #resistance 1
R2 = 240.0   #resistor 2
R3 = 190.0   #resistor 3
Ra = 3.0     #resistor a
Rs = 1.5     #source resistance
Vs = 7.5     #source voltage


def solveI(Rx):
    global Rs
    global R1
    global R2
    global Ra
    global Vs
    M = [[-Rs,-R3,0, 0, -Rx, 0], #kirchoffs laws matrix
          [1, -1, -1, 0, 0, 0],
          [0, R3,-R2,-Ra,0,0],
          [0, 0, 1, -1, 0, -1],
          [0, 0, 0,Ra ,Rx ,-R1],
          [0, 1, 0, 1,-1,   0]]
    V = [-Vs,0,0,0,0,0]
    I = np.linalg.solve(M,V) #solve matrix equation
    return I[3] #just return 

Rx = np.linspace(0,500,100001) #resistance from 0 to 500
I = [solveI(i) for i in Rx]  #calculating current

plt.figure(11)
plt.plot(Rx,I,color ='b')
plt.xlabel(r'$\Omega_{x}$')
plt.ylabel(r'$I(A)$')
plt.grid()
plt.axhline(c = 'k')
plt.savefig('test/PRNMOG001/Ohms.png',dpi = 100)

I500 = solveI(500) #current going through at 500 ohms 
print('\n')
print(r'The current at 500 ohms =',I500,'A')


#--------------------------------------
#2b
#--------------------------------------


A = newton(solveI,40) #resistance for minimum current
print('\n')
print('The resistance for 0 current is =',A, r'ohms')
print('\n')

#--------------------------------------
#JSON stuff
#--------------------------------------

def WJSON(path, fileName, data): #just writing json file
    filePathNameWExt = './' + path + '/' + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        json.dump(data, fp)
        
a1 = {"P1":P[0],"P2":P[1],"P3":P[2]}

bP1 = {"mag":mag[0],"phi":Angles[0]}
bP2 = {"mag":mag[1],"phi":Angles[1]}
bP3 = {"mag":mag[2],"phi":Angles[2]}

q2 = {"a":{"I500ohms":I500},"b":A}

dic = {"1":{"a":a1,"b":{"P1":bP1,"P2":bP2,"P3":bP3}},"2":q2}


WJSON('test/PRNMOG001/','results',dic)
