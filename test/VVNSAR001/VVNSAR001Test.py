# -*- coding: utf-8 -*-
"""############################################################################
Created on Fri Jun 12 12:09:16 2020

@author: VVNSAR001
"""
import json
import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.integrate import simps
############################################################################
#Q1 a/b) - We will use the relaxation method to solve the Poisson's equation
############################################################################
L = 101 #LxL grid
n=10
h = L/n
def potentialMatrix(L):
    U = np.zeros((L,L)) 
    
    U[0,0:99] = 0
    U[0:99,99] = 0
    U[99,0:100] = 0
    U[0:99,0] = 0
    U[20:80,40] = 0
   
    return U
#boundary = [[0,np.arange(0,99.1)],[np.arange(0,99.1),99],[99,np.arange(0,100.1)],[np.arange(0,99.1),0],[np.arange(20,80.1),40]]
#bndry1 = [0,np.arange(0,99.1)]
#bndry2 = [np.arange(0,99.1),99]
#bndry3 = [99,np.arange(0,100.1)]
#bndry4 = [np.arange(0,99.1),0]
#bndry5 = [np.arange(20,80.1),40]
U= potentialMatrix(L)
#print(boundary)
#print(U)
omega = 1.5 #relaxation constant for elliptic (poisson) pde problems
def relax(U,R,h): #u=potential, R=charge density, D=constant in front,v=,h=grid with constant spacing h 
    niter = 0
    diff = 1000
    while diff > 0.1 and niter<1000: #1000 #our condition for when the solution isn't changing to some level of precision anymore
        diff = 0
        for i in range(1,L-1):
            for j in range(1,L-1):
#                if (i,j) is not bndry1 or bndry2 or bndry3 or bndry4 or bndry5:
                    
                #approximating value
                value =1/4*(U[i-1,j] + U[i+1,j] + U[i,j-1] + U[i,j+1] + 4*R[i,j]*h**2)
                diff += np.abs(value-U[i,j])
                U[i,j]=value
        niter +=1 
    U[0,0:99] = 0
    U[0:99,99] = 0
    U[99,0:100] = 0
    U[0:99,0] = 0
    U[19:80,39] = 0
    return U
############################################################################
#Plotting question 1 a/b
############################################################################
def plotQ1(U,L):
    plt.imshow(U)
    plt.colorbar()
    plt.ylabel("Y(mm)")
    plt.xlabel("X(mm)")    
    ###
    x,y = np.gradient(U) #x = -Ex and y = -Ey
    xAxis = np.linspace(0,10,L)
    figure, axisArr = plt.subplots(1,1)
    vectorMag = np.sqrt(x**2 + y**2)
    im = axisArr.contourf(xAxis,xAxis,vectorMag/(np.max(vectorMag)),20)
    axisArr.streamplot(xAxis,xAxis,x,-y,color='k')
    figure.colorbar(im,orientation='horizontal',ax=axisArr,label=r'Velocity Magnitude')
    angles = np.arctan(y/x)*np.pi*180
    axisArr.set_xlim([0,10])
    axisArr.set_ylim([0,10])
    Ex = -x
    Ey = -y
    
    return angles,vectorMag, Ex, Ey
    
R = np.zeros_like(U) #charge density
R[50,60]=1 #intializing location of charge
#print(R)
#potential at P1 = 5,5; P2 = 9,5; P3 = 9,9
h=1
U = relax(U,R,h)
P1 = U[49,49]
P2 = U[89,49]
P3 = U[89,89]

print("Potentials: P1: ",P1,"  P2: ",P2,"  P3: ",P3)
angles, magnitudes, Ex, Ey = plotQ1(U,L)
print("Angles: P1: ",angles[49,49]-720," P2: ",angles[89,49]+360," P3: ",angles[89,89]-360)
print("Magnitudes: P1: ",magnitudes[49,49]," P2: ",magnitudes[89,49]," P3: ",magnitudes[89,89])

plt.show()
#print(U)
    
############################################################################
#1c)
############################################################################
#Electrode is on [19:80,39] so Efield to left and right is.. :
epsilon0 = 1
ExLeft = Ex[19:80,38]
ExRight = Ex[19:80,40]
electrodeDiff = []
for i in range(len(ExLeft)):
    electrodeDiff.append(ExRight[i]-ExLeft[i])

y = np.linspace(0,60,61)
plt.plot(y,electrodeDiff)
plt.xlabel('y(mm)')
plt.ylabel('Line Density (C/mm)')
plt.show()

totalCharge = simps(electrodeDiff,y,(y[1]-y[0]),axis=-1)
print("The total charge on the electrode is: ",totalCharge )

############################################################################
#q2a) 
############################################################################
R1 = 63
R2 = 240
R3 = 190
Ra = 3
Rs = 1.5
Vs = 7.5
##Manually assigning matrix: 
def resistanceMatrix(Rx):
    M = np.zeros((3,3))
    M[0][0] = -Rs
    M[0][1] = -Rs - Rx -R3
    M[0][2] = -R3
    M[1][0] = -Rs - R1 - R2
    M[1][1] = -Rs
    M[1][2] = R2
    M[2][0] = R1
    M[2][1] = -Rx
    M[2][2] = Ra
    return M

V = np.zeros(3)
V[0] = -7.5
V[1] = -7.5
V[2] = 0
I1=[]
I2=[]
I3=[]
Rx = np.arange(0,501,1)
#print(Rx)
#print(M)
R0=0

for i in range(len(Rx)):
    M = resistanceMatrix(Rx[i])
    I=np.linalg.solve(M,V)
#    print("Rx=",Rx[i]," Currents: ",I)
    I1.append(I[0])
    I2.append(I[1])
    I3.append(I[2])

############################################################################
##2b) 
############################################################################    
magBool = False      
##we know that current3 = 0 between 49.8-49.9 so we will iterate values between to find the closest to 0
RI0value = np.arange(49.8,49.9,0.00000005)
start = time.time()
I[2] = 1
while magBool == False:
    start = time.time()
    prevCurr = I[2]
    for i in range(len(RI0value)): 
        
        M = resistanceMatrix(RI0value[i]) #should be Rx
        I=np.linalg.solve(M,V)
        
        #print("Rx=",RI0value[i]," Currents: ",I)
        if I[2]*prevCurr < 0 :
            Value1 = RI0value[i]
            Value2 = RI0value[i+1]
            magBool = True
                      
print("I: ",Value1," I+1: ",Value2)
end = time.time()
print("Elapsed Time ",end-start)
#Rx0 =(49.874900000002484,49.87500000000249) -> (-2.65037172e-08,6.58401628e-16) 
############################################################################
#Q2 Plotting
############################################################################
plt.figure(0)
plt.plot(Rx,I1,color='red',label="I1")
plt.plot(Rx,I2,color='blue',label="I2")
plt.plot(Rx,I3,color='green',label="I3")
plt.xlabel("Rx Values (Ohm)")
plt.ylabel("Current (amperes)")
plt.legend()
plt.show()
############################################################################
#Json section
############################################################################
angle1 = angles[49,49]-720
angle2 = angles[89,49]+360
angle3 = angles[89,89]-360
vMag1 = magnitudes[49,49]
vMag2 = magnitudes[89,49]
vMag3 = magnitudes[89,89]

Q1={"a":{"P1":P1,"P2":P2,"P3":P3},"b":{"P1":{"mag":vMag1,"phi":angle1},"P2":{"mag":vMag2,"phi":angle2},"P3":{"mag":vMag3,"phi":angle3}}}
I3a = I2[500] #Q2
Rxb = 49.8749
Q2={"a":{"I500ohms":I3a},"b":Rxb}
sol={"1":Q1,"2":Q2}

with open('test/VVNSAR001/answers.json','w') as f:
    json.dump(sol,f)    
############################################################################
#Done :)
############################################################################    
    