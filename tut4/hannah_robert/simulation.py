import numpy as np
import matplotlib.pyplot as plt
import math as m
import attenuation_handler as att
import c_scatter_handler as cscat

co_energy = np.asarray([1.173228,1.332492]) # start energies of photons in MeV
nai_attenuations, al_attenuations = att.get_attenuations() # gets the attenuation coefficients for a range of energies for both elements

#global individual_energies  # the energies recorded by the detector - if not summed
individual_energies = []
recorded_energies = []	# energies recorded by the detector (summed depositions from multiple interactions for single photon)

#numpy random generates values with the possibility of getting 0, allowing for the removal that possibility
#also allowing for scaling
def generate_random(minVal = 0, maxVal = 1, include_min = False): 
	random_num = minVal
	while random_num == minVal:
		random_num = (maxVal-minVal) * np.random.random() + minVal
		if include_min == True:
			break
	return random_num

#using units of mm in Cartesion coords, defining the source to be at the origin
#with the detector along the x axis
# returns location type, with 0 = vacuum, 1 = Al shielding, 2 = inside NaI detector
def find_relevant_volume(x,y,z):
	radial_distance = np.sqrt(np.square(y)+np.square(z))  # distance from x-axis
	
	if x<25 or x>(25+40+0.3): #x value in vacuum (or PMT but ignoring that)
		return 0 
	if (radial_distance > 20+2): #r value in vacuum
		return 0 
	if x < 25+0.3: #in front window
		return 1 
	if radial_distance > 20: #in outer shielding
		return 1 
	return 2 #in detector

# generates direction in which photon is emitted, and determines whether photon hits detector window or not
def generate_photon():
	r = 1                # distance in mm to detector window

	theta = np.arccos(1 - 2*generate_random(include_min=True))
	phi = 2 * np.pi * generate_random(include_min=True)

	# need to find where this photon intersects the x = 25mm plane

	# gives normalised direction vector in cartesian coordinates  - unit vector for photon trajectory
	lvec = spherical_to_cartesian(r,theta,phi)

	# if x<0, then photon is emitted in direction opposite to detector
	if lvec[0]<0:
		return [0]                    # photon escapes

	# if x>0, need to check if photon hits detector
	# photon line
	l0 = np.array([0,0,0])            # initial point

	# plane at detector face
	p0 = np.array([25,0,0])            # point on plane
	nhat = np.array([1,0,0])        # unit normal to plane

	# check if photon travels parallel to plane
	if np.dot(lvec, nhat) == 0:
		return [False]                    # photon escapes

	# point where photon intersects plane
	d = np.dot(p0-l0, nhat)/np.dot(lvec, nhat)
	intersectionPoint = d*lvec

	# check if photon hits window
	if find_relevant_volume(*intersectionPoint)==1:
		return [True, intersectionPoint, lvec] 
	else:
		return [False]

#given a position an energy, determine where in the geometry it is, and return the relevant attenuation coefficients for the material and energy
def get_material_coefficients(material_type,E):
	if material_type == 0: return 0 # shouldn't initiate the calculation if its in vacuum - this shouldn't happen
	if material_type == 1: #in aluminium
		att_coefficients = att.get_closest_coefficients(al_attenuations, E)
	if material_type == 2: #in detector
		att_coefficients = att.get_closest_coefficients(nai_attenuations, E)
	
	total_attenuation = np.sum(att_coefficients)  # total attenuation coefficient is sum of all others 
	return [att_coefficients, total_attenuation]

#given a starting point and energy, finds out how far the photon is likely to go before interacting 
def determine_distance_travelled(att_coefficients, total_attenuation): 
	random_num = generate_random()
	distance = -(1/total_attenuation) * np.log(random_num) # generates distances from exponential distribution
	return distance


# Given a starting point x0, distance d and direction rhat, determine the endpoint of the path (cartesian coordinates)
# input parameter types: x0 and rhat: numpy arrays
def determine_path_endpoint(x0, d, rhat):
	# make sure nhat is normalised direction
	rhat = rhat/np.linalg.norm(rhat)
	return x0 + d*rhat

# Randomly determines the type of interaction as given by the attenuation coefficients
def determine_interaction_type(att_coefficients, total_attenuation):
	random_num = generate_random()
	interaction_probabilities = att_coefficients/total_attenuation #normalise the probabilities
	if random_num < interaction_probabilities[0]: #if random number falls into probability range of CS
		return 0 #compton scatter
	return 1 #photoelectric effect

# for a given interaction, records any individual energy deposition in the detector, and determines whether to continue with the given photon
def handle_interaction(energy, material_type, interaction_type):
	if interaction_type == 1: #pe effect
		if material_type == 2: # if in detector
			individual_energies.append(energy) #record full energy of photon in detector
		return [False] # return no further action
	else: #compton scatter
		new_energy,scatter_angle = cscat.sample_scatter_energies(energy) #randomly finds the new photon energy according to Klein-Nishina formula
		scatter_phi = 2 * np.pi * generate_random(include_min=True)                        # generate phi isotropically
		if material_type == 2: 
			individual_energies.append(energy-new_energy) #if in detector records difference in photon energies
		return [True, new_energy, scatter_angle, scatter_phi] # returns that the photon continues, with its new energy/angle
		 
	
# convert from spherical to cartesian coordinates
def spherical_to_cartesian(r, theta, phi):
	x = r * np.sin(theta) * np.cos(phi)
	y = r * np.sin(theta) * np.sin(phi)
	z = r * np.cos(theta)

	return np.array([x,y,z])

# convert from cartesian to spherical coordinates
def cartesian_to_spherical(x, y, z):
	r = m.sqrt(np.square(x) + np.square(y) + np.square(z))
	theta = m.atan2(m.sqrt(np.square(x) + np.square(y)), z)
	phi = m.atan2(y,x)

	return np.array([r, theta, phi])

# determine new direction after compton scatter
# input parameters: phi and theta (relative to original direction nhat_old) from compton scattering simulation
# outputs unit vector for new position
def direction_after_compton(nhat_old, theta, phi):
	nhat_spherical = cartesian_to_spherical(*nhat_old)   

	nhat_new = np.array([nhat_spherical[0], nhat_spherical[1] + theta, nhat_spherical[2] + phi])
	nhat_new_cartesian = spherical_to_cartesian(*nhat_new)

	return nhat_new_cartesian

# handle energy recording by detector (considering summing multiple interactions from both photon)
def record_energy_deposition():
	global individual_energies

	if len(individual_energies) == 0:
		return
	totalDeposition = sum(individual_energies)
	# record total deposition from all interactions
	recorded_energies.append(totalDeposition)
	# clear array for individual depositions for next photon

	individual_energies = []


## main method for full simulation
def main():
	np.random.seed(1)
	# numDecays = int(input("Enter the number of decays:\n"))
	numDecays=1000000
	
	# loop over decay steps
	for j in range(numDecays):
		# each decay produces 2 gammas - track separately
		
		for i in range(len(co_energy)):
			E = co_energy[i]                       # energy of photon - will be updated as interactions occur

			# determine emission direction
			emission = generate_photon()

			# check if hits detector
			if not (emission[0]):
				continue                                    # misses detector
			# hits detector, so proceed
			else:

				photon_pos = emission[1]                # point at which photon hits detector window
				
				nhat = emission[2]                        # unit vector direction of photon
				material_type = 1                       # always hits front window
				
				while material_type != 0:
					att_coefficients, total_attenuation = get_material_coefficients(material_type,E)
					d = determine_distance_travelled(att_coefficients, total_attenuation) # find distance before interaction at given location/energy

					photon_pos = determine_path_endpoint(photon_pos, d, nhat)  #find new location where interaction occurs
					material_type = find_relevant_volume(*photon_pos) # find the material at the interaction point

					# if in vacuum, stop iterating
					if material_type == 0:		
						break

					# if still in detector/shielding, interaction occurs
					att_coefficients, total_attenuation = get_material_coefficients(material_type, E)
					interaction_type = determine_interaction_type(att_coefficients, total_attenuation)
					# handle interaction
					interaction = handle_interaction(E, material_type, interaction_type)

					# check if photon continues
					if not (interaction[0]):
						break					# photon has deposited all energy into detector
					# photon still has energy - now need to determine new direction and energy
					else:
						E = interaction[1]                        # new energy of photon
						# scatter angles (compton scatter)
						scatter_angle = interaction[2]             # theta
						scatter_phi = interaction[3]            # phi

						nhat = direction_after_compton(nhat, scatter_angle, scatter_phi)    # new direction for photon

		# sum and record energy depositions from both gammas in a single emission - both go through and interact only about 1% of the time
		record_energy_deposition()
 
	plot_energies = recorded_energies
	
	# without modification
	# modified_energies = plot_energies  
	
	#modify the energies to have a 3% resolution
	modified_energies = np.zeros(len(plot_energies))
	for i in range(len(plot_energies)):
		modified_energies[i] = np.random.normal(plot_energies[i], plot_energies[i]*0.03)
	
	#plot the ADC spectrum
	plt.figure()
	values = np.histogram(modified_energies, 100)[0]
	xvals = np.linspace(min(modified_energies), max(modified_energies), 100)
	plt.bar(xvals, values, width=(max(modified_energies)-min(modified_energies))/100)
	plt.xlabel("Energy (MeV)")
	plt.ylabel("Counts")
	plt.axvline(x=co_energy[0], color='r', linestyle='--', label="Photopeak E = 1.17 MeV")
	plt.axvline(x=co_energy[1], color='b', linestyle='--', label="Photopeak E = 1.33 MeV")
	plt.axvline(x=sum(co_energy), color='y', linestyle='--', label="Combined photopeak")
	plt.legend()
	plt.savefig("Figures/modifiedADCspectrum")
	plt.show()

if __name__ == '__main__':
	main()

					





