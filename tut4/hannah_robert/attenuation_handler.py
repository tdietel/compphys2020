import numpy as np

files = ['nai_attenuations.txt','aluminium_attenuations.txt']  # energy, compton scatter, photoelectric effect in MeV, mass attenuation coefficients cm^2/g
densities = [2.7,3.67] #al, nai density in g/cm^3

def read_file(filename, density): 
    # reads in relevant mass attenuation coefficients and changes to normal attenuation coefficients
    f = open(filename, 'r')
    data = f.read()
    data = data.split()
    data = np.array(data, 'float') 
    data = np.resize(data, [int(len(data)/3), 3])
    data.T[1] = (data.T[1]*density)/10  # convert from cm^2/g to 1/mm
    data.T[2] = (data.T[2]*density)/10
    f.close()
    return data    

def get_attenuations(): #find the attenuation coefficients for aluminium and NaI
    attenuations  = []
    for i in range(2):
        attenuations.append(read_file(files[i], densities[i]))
    return attenuations

def get_closest_coefficients(element_attenuations, energy):  
    # finds the attenuation coefficients for the energies closest to the given energy
    # we have continuous energy spectrum, but attenuations only given for discrete steps
    energies = element_attenuations.T[0]
    absolute_diffs = np.abs(energies-energy) # finds differences between energy steps and given energy
    smallest_index = absolute_diffs.argmin() # finds index of smallest difference
    closest_coefficients = element_attenuations[smallest_index] # finds the attenuation coefficients for closest energy
    return closest_coefficients[1:] #compton scatter, pe effect
