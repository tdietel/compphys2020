# Class to create some plots for report and other testing
import random
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import simulation as si
import c_scatter_handler as cscat

# Generate N randomly distributed points on a sphere of radius r, given some seed
def randSphere(r, N, seed):
    # gamma 1
    phi1 = np.zeros(N)
    theta1 = np.zeros(N)
    # gamma 2
    phi2 = np.zeros(N)
    theta2 = np.zeros(N)

    random.seed(seed)

    for i in range(N):
        # gamma 1 emission angles
        theta1[i] = np.arccos(1 - 2*random.random())
        phi1[i] = 2 * np.pi * random.random()

        # gamma 2 emission angles
        theta2[i] = np.arccos(1 - 2*random.random())
        phi2[i] = 2 * np.pi * random.random()


    x1 = r * np.sin(theta1) * np.cos(phi1)
    y1 = r * np.sin(theta1) * np.sin(phi1)
    z1 = r * np.cos(theta1)

    x2 = r * np.sin(theta2) * np.cos(phi2)
    y2 = r * np.sin(theta2) * np.sin(phi2)
    z2 = r * np.cos(theta2)

    # 2D plot
    plt.scatter(phi1, theta1, s=0.5, label='Gamma 1')
    plt.scatter(phi2, theta2, s=0.5, label='Gamma 2')
    plt.legend()

    plt.xlabel(r'$\phi$')
    plt.ylabel(r'$\theta$')
    plt.title('Distribution of photon emission angles')
    plt.savefig("Figures/emission_angle_dist.png")
    plt.show()

    # 3D plot
    fig = plt.figure(figsize=(7,6))
    ax = Axes3D(fig)
    ax.scatter(x1,y1,z1, label='Gamma 1', s=0.8)
    ax.scatter(x2,y2,z2, label='Gamma 2', s=0.8)
    plt.legend()
    plt.title("Distribution of emitted photons over unit sphere")
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.savefig("Figures/emission_angle_sphere_dist.png")
    plt.show()

# determine fraction of photons which hit detector - should be approx the same for both photons
def countHits(numDecays):
    countHits1 = 0                    # number of gamma 1s which hit detector
    countHits2 = 0                    # number of gamma 2s which hit detector

    for i in range(numDecays):
        if si.generate_photon()[0]:
            countHits1 += 1
        if si.generate_photon()[0]:
            countHits2 += 1

    print(countHits1/numDecays, countHits2/numDecays)


def check_both(numDecays): #generates two photons, checks how often both go through detector
    countHits = 0
    for i in range(numDecays):
        result1 = si.generate_photon()[0]
        result2 = si.generate_photon()[0]
        if result1 and result2: # if both photons enter detector
            countHits += 1
    print(countHits)

def plot_scatter_distribution(seed=1):
    #plot the angular distribution of Compton scatters at different energies
    plt.figure()
    np.random.seed(seed)
    plt.title("Angular distribution")
    
    energies = [0.1,1,100]
    
    for j in range(len(energies)): #generate distribution for each energy
        energy = energies[j]
        
        angles = []
        for i in range(100000):
            new_energy, scatter_angle = cscat.sample_scatter_energies(energy)
            angles.append(scatter_angle)
            
        plt.subplot(int("31"+str(j+1)))
        plt.hist(angles, bins = 100, label = str(energy)+" MeV")
        plt.xlabel("Angle (radians)")
        plt.ylabel("Counts")
        plt.legend()
    
    plt.tight_layout()
    plt.savefig("Figures/scatterDistribution.png")
    plt.show()

# generate detector geometry plot
def plot_detector_geometry():
    x = np.linspace(15, 60, 70)
    y = np.linspace(-25, 25, 70)
    z = np.linspace(-25, 25, 70)

    detectorPointsX = []
    detectorPointsY = []
    detectorPointsZ = []

    shieldX = []
    shieldY = []
    shieldZ = []

    # 3D plot
    for xpoint in x:
        for ypoint in y:
            for zpoint in z:
                material = si.find_relevant_volume(xpoint, ypoint, zpoint)
                if material==2:
                    detectorPointsX.append(xpoint)
                    detectorPointsY.append(ypoint)
                    detectorPointsZ.append(zpoint)
                if material==1:
                    shieldX.append(xpoint)
                    shieldY.append(ypoint)
                    shieldZ.append(zpoint)

    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(detectorPointsX,detectorPointsY,detectorPointsZ, label='Detector material', s=1.5)
    ax.scatter(shieldX,shieldY,shieldZ, label='Shielding', s=1.5)
    ax.scatter([0], [0], [0], s=8, color='r', label='Source')
    plt.legend()
    plt.title("Detector geometry")
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.savefig("Figures/3D_geometry")
    plt.show()

    # 2D cross section perpendicular to detector axis
    y = np.linspace(-28, 28, 250)
    z = np.linspace(-28, 28, 250)
    detectorPointsY = []
    detectorPointsZ = []
    shieldY = []
    shieldZ = []

    xpoint = 28
    for ypoint in y:
        for zpoint in z:
            material = si.find_relevant_volume(xpoint, ypoint, zpoint)
            if material==2:
                detectorPointsY.append(ypoint)
                detectorPointsZ.append(zpoint)
            if material==1:
                shieldY.append(ypoint)
                shieldZ.append(zpoint)

    fig = plt.figure()
    plt.scatter(detectorPointsY,detectorPointsZ, label='Detector material', s=1.5)
    plt.scatter(shieldY,shieldZ, label='Shielding', s=1.5)
    plt.legend()
    plt.title("Cross section of detector at x=30mm")
    plt.xlabel("y")
    plt.ylabel("z")
    plt.savefig("Figures/detector_cross_section")
    plt.show()


def main():
    randSphere(1, 5000, 1)
    countHits(100000)
    check_both(1000000)
    plot_scatter_distribution()
    plot_detector_geometry()

if __name__ == '__main__':
    main()



