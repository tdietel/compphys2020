import numpy as np

mec2 = 0.511  # mass of electron times c squared

# the rejection function for epsilon used to achieve the correct distribution
def rejection_func(epsilon, E0):
     t = mec2*(1-epsilon)/(E0*epsilon)
     sin2 = t*(2-t)
     g = 1-(epsilon*sin2)/(1+np.square(epsilon))
     return g, sin2
    
#samples the Compton scattering angle from the Klein-Nishina formula
#follows the methedology used by Geant4
def sample_scatter_energies(originalE):
    epsilon0 = mec2/(mec2+2*originalE) #minimum epsilon value - epsilon = E1/E0
    alpha1 = np.log(1/epsilon0)
    alpha2 = (1-np.square(epsilon0))/2
    accepted = False
   
    
    from simulation import generate_random
    while not accepted:
        r = generate_random(include_min = True) # 3 random numbers used for the generation of the distribution
        r1 = generate_random(include_min = True)
        r2 = generate_random(include_min = True)
        
        if r < alpha1/(alpha1+alpha2): # if this condition is met, sample from first function
            epsilon = np.exp(-r1*alpha1)
        else:
            epsilon = np.square(epsilon0)+(1-np.square(epsilon0))*r1 # else sample from this
        if epsilon < epsilon0 or epsilon > 1: #if greater than original energy or less than minimum energy
            continue
        g, sin2 = rejection_func(epsilon, originalE) #test for rejection
        
        if g >= r2:
            accepted = True
            new_energy = epsilon*originalE
            scatter_angle = np.arccos(1-(mec2*(1-epsilon)/(originalE*epsilon)))
    return new_energy, scatter_angle
    
