#CODE BY Erin JArvie, Ursula Hardie, Sarel van Deventer, Ntshembho Mtsetwene 
#IMPORTING PACKAGES
TravelLines=[]
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate
from scipy.optimize import curve_fit
import math


# this means that our random numbers stay the same for every run so we #
# can see exactly how things change when we cahnge things #
np.random.seed(0)

#counts the number of atoms after each time step
# and the decays that happen in each time step


#DEFINITIONS
#1.DECAY FUNCTION WHICH COUNTS DECAYS PER SECOND
def decay(N0,p,t_steps):
    N=np.zeros(t_steps,dtype=int) #Nuclei that do not decay
    d=np.zeros(t_steps, dtype=int) #Nuclei that do decay
    N[0]=N0  #initial number of nuclei
    
    for i in range(len(N)-1):
        #generate random numbers for each nucleus
        r=np.random.uniform(low=0,high=1,size=N[i])
        
        #count the number of surviving nuclei
        N[i+1]=np.count_nonzero(r>p)
        
        #count the number of decayed nuclei in each time step
        d[i+1]=np.count_nonzero(r<p)
        
    return d,N

#2.DEFINING THE KLEIN NISHNA FORMULA        
def KN(E,theta):
    re=(0.0073)*(0.38616)
    c=3*10**8              #ms-1
    me=511.00/(c**2)          #kev/c^2
    Z=(11+53)
    alpha=E/(me*c**2)
    dsig=Z*re**2*(1/(1+alpha*(1-np.cos(theta))))**2*((1+np.cos(theta)**2)/2)*(1+(alpha**2*(1-np.cos(theta))**2)/((1+np.cos(theta)**2)*(1+alpha*(1-np.cos(theta)))))
    return dsig

#3. DEFINING A NORMAL DISTRIBUTION 
def normal(x,mu,sigma):
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp(-0.5*((x-mu)/sigma)**2)

#4. DEFINING ENEGIES IN A COMPTON SCATTER
def Compton(E,theta):
    c=3*10**8      #ms-1
    me=511.00/(c**2)    #kev/c^2
    return E-E/(1+(E/(me*c**2))*(1-np.cos(theta)))   

#5. ENERGIES THAT ARENT ABSORBED IN COMPTON SCATTER
def scat_reab(E,theta):
    c=3*10**8      #ms-1
    me=511.00/(c**2)    #kev/c^2
    return E/(1+(E/(me*c**2))*(1-np.cos(theta)))   

#6.ROUNDS UP A NUMBER TO THE CLOSEST 10
def roundup(x):
    m=x/10.0
    return math.ceil(m)* 10

#7.DEFINES THE SIGMA FOR ENERGY RESOLUTION
def eres_sigma(E, R):
    return E*R/2.35

#8.Solid Angle
def s_ang(d,a):
    return 2*np.pi*(1-(d/(np.sqrt(d**2+a**2))))

def percentage_detected(S,e):
    return S*e

#VARIABLES AND PROCESSES




#DEFINING DECAYS 
t0=0 #tstart in seconds
tf= 300 #t end in seconds (5min)
N0= 10**8*(s_ang(0.025, 0.04)/(4*np.pi))#number of nuclei to begin with, geometry is also taken into account
p=4.17*10**(-6)  #halflife of 46 min  

#Note: assume that for each decay two gammas #
# are released, 1173.2kev an 1332.5kev       #

d=decay(N0,p,tf)   #This is two arrays: the first being the decays in each second, the 2nd being the unstable nuclei that remain


    
x=np.linspace(t0,tf,len(d[0]))
#plt.plot(x,d[0])
#plt.xlabel('t(s)')
#plt.ylabel('Decays')

###### cross sections ######
# Cross section 1=1.17MeV gamma 2=1.33MeV gamma

#Mass Attenuations from XCOM
PE1=2.518*10**(-3) #Photo-electric
PE2=1.990*10**(-3)
CS1=5.002*10**(-2) #Compton scatter
CS2=4.689*10**(-2)

#Probability of interaction is mass attenuation*rho 
#*Note: this is prob/cm
rho=3.67

p_PE1=PE1*rho
p_CS1=CS1*rho
p_PE2=PE2*rho
p_CS2=CS2*rho



# Counting the number of Compton and Photoelec events happen in
# each cm of the 4cm long detector


NC1=np.zeros(4)  #counts how many compton events happen for 1173keV
p1=np.zeros(4)   #counts how many PE events for 1173KeV
NC2=np.zeros(4) #counts how many compton events for 1333keV
p2=np.zeros(4)  #counts how many PE events for 1333keV

d2=d[0]  
d1=d[0] 


energies=[]  #array for energies deposited for histogram at the end

#note each energy gamma gets its own loop since they are separate entities

#These two loops are where I was getting strange results#
#If you switch which one comes first you get vastly different#
#numbers out. It be a bit strange but I don't know if it's
#a huge problem

#So the two loops below tell us if the gamma ray is detected as a scatter or full absoption 

x=np.linspace(0,4,4)

for s in range(len(x)):          #loops over cm's
    for t in range(len(d1)):   #loops over number of photons/sec
        k=np.random.uniform(low=0,high=1,size=d1[t])
        for l in  range(len(k)):
            if k[l]<p_CS1:
                NC1[s]+=1     #adds an event to our counter
                if d1[t]>0:
                    d1[t]-=1  #removes the interacted photon
            if k[l]<p_PE1:
                p1[s]+=1
                energies.append(1173.2)
                if d1[t]>0:
                    d1[t]-=1            

                    
for m in range(len(x)):          #loops over cm's
    for i in range(len(d2)):   #loops over number of photons/sec
        r=np.random.uniform(low=0,high=1,size=d2[i])
        for n in  range(len(r)):
            if r[n]<p_CS2:
                NC2[m]+=1
                if d2[i]>0:
                    d2[i]-=1
            if r[n]<p_PE2:
                p2[m]+=1
                energies.append(1332.5)
                if d2[i]>0:
                    d2[i]-=1


      
print('Compton events 1.17MeV=',NC1)
print('Photoelc events 1.17MeV=',p1)
print('Compton events 1.3MeV=', NC2)
print('Photoelec events 1.3MeV',p2)

TNC1=int(sum(NC1))
Tp1=int(sum(p1))
TNC2=int(sum(NC2))
Tp2=int(sum(NC2))


#Klein Nishina formula- cross section for scattering angle
#Note: input energy in keV


t=np.linspace(-np.pi, np.pi, 60)
k1=[]
k2=[]
for i in range(len(t)):
    k1.append(KN(1173.00,t[i]))
    k2.append(KN(1332.5,t[i]))
  
#fitting a normal distribution to our KN distribution
#plt.figure()  
#plt.plot(t,k1, label="1173 KeV")   
#plt.plot(t,k2,label="1332 KeV") 
#plt.xlabel('Theta')
#plt.ylabel('dsig')
#plt.legend()
    
a=k1[0]   
b=k2[0] 
for i in range(len(k1)):
    k1[i]=k1[i]-a         #shift it down so the min is at 0
    k2[i]=k2[i]-b
    
    
integral1=scipy.integrate.simps(y=k1,x=t)  #integrate so that the 
integral2=scipy.integrate.simps(y=k2,x=t)  #distribution can be
normalize1=1/integral1                     #normalized
normalize2=1/integral2

for i in range(len(k1)):
    k1[i]=k1[i]*normalize1
    k2[i]=k2[i]*normalize2
    


mu=0.1
sigma=0.4
p0=[mu,sigma]
popt1, pcov1 = curve_fit(normal, t, k1, p0=p0)
popt2, pcov2 = curve_fit(normal,t,k2,p0=p0)

fit1=normal(t,*popt1)
fit2=normal(t,*popt2)

#plt.figure()
#plt.plot(t,fit1,'--', label='fit1')
#plt.plot(t,k1, label="1173 KeV")
#plt.plot(t,fit2,'--', label='fit2')
#plt.plot(t,k2,label="1332 KeV")
#plt.ylabel('dsig')
#plt.xlabel('Theta')
#plt.legend()

####Chi-Squared Goodness of fit###
# X^2=sum{(data-fit)^2/fit}
dymin1 = (k1-normal(t,*popt1))
min_chisq1 = sum((dymin1*dymin1)/normal(t,*popt1))
dof1 = len(t) - len(popt1)

dymin2 = (k2-normal(t,*popt2))
min_chisq2 = sum((dymin2*dymin2)/normal(t,*popt2))
dof2 = len(t) - len(popt2)

print()
print ("Chi square per degree of freedom 1: ",min_chisq1/dof1)

print ("Chi square per degree of freedom 2: ",min_chisq2/dof2)
print()



params=['mu', 'sigma']

#for i,pmin in enumerate(popt1):
    #print(params[i])
    #print((i,pmin,np.sqrt(pcov1[i,i])*np.sqrt(min_chisq1/dof1)))
    #print("%2i %12f +/- %10f"%(i,pmin,np.sqrt(pcov2[i,i])*np.sqrt(min_chisq2/dof2)))


  
angles1=np.random.normal(popt1[0], popt1[1], size=TNC1)
angles2=np.random.normal(popt2[0], popt2[1], size=TNC2)


  
#Compton scatter-Energy of scattered photon
#E-compton=deposited energy
rescattered_energies=[]
#Append energy deposited in compton scatter 
for i in range(len(angles1)):
    if np.abs(angles1[i])>5*10**(-2):  #attempt to get rid of some of the zeros
        energies.append(Compton(1173.2,angles1[i]))
        rescattered_energies.append(scat_reab(1173.2,angles1[i]))

for i in range(len(angles2)):
    if np.abs(angles2[i])>5*10**(-2):
        energies.append(Compton(1332.5,angles2[i]))
        rescattered_energies.append(scat_reab(1173.2,angles1[i]))



r_energies=np.zeros(len(rescattered_energies))
for i in range(len(rescattered_energies)):
    r_energies[i]=roundup(rescattered_energies[i])


with open('xcomdata.txt', 'r') as f:
    
        n=100
        energy=np.zeros(n)
        attenu=np.zeros(n)
        i=0

        
        for line in f:
            line= line.strip()
            columns=line.split()
            energy[i]= float(columns[0])*1000
            attenu[i]=float(columns[1])
            
                
            i=i+1

#This code accounts for the reabsorption of compton scattering gamma rays. 
for i in range(len(rescattered_energies)):
    k=np.random.uniform(low=0,high=1,size=len(rescattered_energies))
    for l in range(len(energy)):
        if r_energies[i]==energy[l]:
            
            if k[i] < rho*attenu[l]:
                energies.append(r_energies[i])
                #print(r_energies[i])



#Energy resolution
# What does the following code do? 
#1. defines the standard deviation for a detected energy based on the resolution of the detector
#2. The detected vaue if then processed to be a value within a normal distribution with mu=real energy and sigma=energy resolution
R=0.03  #Energy resolution as given in our brief. 

detected_energies=[]
for i in range(len(energies)):
        detected_energies.append(np.random.normal(loc=energies[i], scale=eres_sigma(energies[i], R)))

scale=percentage_detected(N0, 0.12)
#PLOTTING COMPTON EDGES
CE1_line=np.linspace(0,1000,1000)
CE2_line=np.linspace(0,1000,1000)
CE1=np.full( len(CE1_line), 963.00)
CE2=np.full(len(CE1_line), 1118.60)


#print('%.2f'%pre)
#print(Compton(1173,np.pi))
weight=np.full(len(detected_energies), 0.12)
#plt.hist(energies, bins=1000)

plt.hist(detected_energies, bins=1048, weights=weight)
#plt.plot(CE1, CE1_line)
#plt.plot(CE2, CE1_line)
plt.xlabel('Energy (keV)')
plt.ylabel(r'Counts')

plt.xlim(0,1400)
plt.show()

#------------------------------------------------------------------------------------------------------------
#Tracing start        
#------------------------------------------------------------------------------------------------------------
detectorRadius = 20.00          # mm, detector radius
detectorHeight = 40.00          # mm, detector height
detectorOffset = 25.00         # mm, distance of detector from source, +z direction
CEnergies = [1173.00,1332.5] #creating an array of our energy values
CCS=[CS1,CS2] #array of comton scatter mass attenuations 
CPE=[PE1,PE2] #array of photo electric mass attenuations

def attenuation(sig, rho):      # Calculates distance x that is traveled by a particle through a specific medium (characterized by rho)
    num = np.random.rand()
    x = np.log(num)/(-rho*sigma)*10.00     ## x in mm
    return x
TravelLines = []

def distanceCalculation(r,r1,x):     #Calculates the travel distance that the particle's allowed to travel 
    rho1 = r1[0]
    phi1 = r1[1]
    z1 = r1[2]   
    rho2 = x*np.sin(r[1])
    phi2 = r[2]
    z2 = x*np.cos(r[1])
    rho3 = rho1 + rho2
    phi3 = phi1 + phi2
    z3 = z1 + z2
    x1 = z1
    x2 = z3
    y1 = rho1*np.cos(phi1)
    y2 = rho3*np.cos(phi3)
    line = [(x1, x2), (y1, y2)]
    originline = [(-25.00,0), (0.00, y1)]
    TravelLines.append(originline)
    TravelLines.append(line)
    if (np.abs(rho3) < detectorRadius) & (z3 < detectorHeight) & (z3 > 0):
        return [True, (rho3,phi3,z3)]
    else:
        return [False, (rho3,phi3,z3)]    
EList = []
R = 0.03
def tracingInDetector(r,r1):
    sigCompton = np.interp(r1[0],CEnergies,CCS)
    sigPhoto = np.interp(r1[0],CEnergies,CPE)
    x = attenuation(sigCompton+sigPhoto,rho)
    distance = distanceCalculation(r,r1,x)
    if distance[0]==True: #when an interaction occurs
        ival = (sigCompton/sigPhoto)+1
        rand = np.random.rand()*ival
        if rand<1 :#PE occurs
            EList.append(eres_sigma(r1[0],0.03))
            return 
        else: #scattering occurs
            randomAngle = np.random.uniform(0,np.pi) #random phi - but just testing for now - need to get random theta angles based on compton scattering
            comptr = Compton(r[0],randomAngle)
            randomPhi = np.random.uniform(0,np.pi)
            rnew = (comptr, randomAngle, randomPhi)
            EList.append(eres_sigma(r[0] - comptr,0.03))
            r0new = distance[1]
            tracingInDetector(rnew, r0new)
    elif len(EList) > 0:
            return
    else: #when no interaction occurs
            return False
totLines=[]
def plotDetector(): #plots the detector setup
    plt.plot((0.0,40.00), (-20.00,-20.00), 'k')
    plt.plot((0.00,40.00), (20.00,20.00), 'k')
    plt.plot((0.00,0.00), (-20.00,20.00), 'k')
    plt.plot((40.00,40.00), (-20.00,20.00), 'k') #plotting the squares + source of the detector
    plt.xlabel('Distance from detector (mm)')
    plt.ylabel('Height from detector (mm)')
    for i in totLines:
        iLines = 0
        r = np.random.rand()
        g = np.random.rand()
        b = np.random.rand()
        for j in i:
            if (iLines > 0) & (j[0][0] == -25.00):
                continue
            plt.plot(j[0], j[1], c=(r,g,b))
            iLines +=1
    plt.scatter(-25.00,0.00, c='g')
    plt.show()
        
#runs the whole tracer setup using plotdetector(), tracingInDetector() and distanceCalculation()
totLines = []
fullList = []
ammount = 150
for i in range(ammount):
    EList = []
    randomNum = np.random.rand()
    r1 = (1173.0, (np.random.rand())*0.6747, np.random.rand()*2*np.pi)
    r2 = (1332.5, (np.random.rand())*0.6747, np.random.rand()*2*np.pi)
    rho1 = np.abs(25.00 * np.tan(r1[1]))
    rho2 = np.abs(25.00 * np.tan(r2[1]))
    ri1 = (rho1, r1[2], 0.1)
    ri2 = (rho2, r2[2], 0.1)
    if randomNum > 0.5: #smaller energy value
        tracingInDetector(r1, ri1)
    else:         #refers to the larger energy value
        tracingInDetector(r2, ri2)
    deposit = np.sum(EList)
    if deposit > 0:
        fullList.append(deposit)
    totLines.append(TravelLines)
    TravelLines = []
plotDetector()
#------------------------------------------------------------------------------------------------------------
#Tracing End
#------------------------------------------------------------------------------------------------------------


